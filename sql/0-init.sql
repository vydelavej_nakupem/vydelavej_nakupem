-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Počítač: wm124.wedos.net:3306
-- Vytvořeno: Pon 17. dub 2017, 23:06
-- Verze serveru: 10.0.21-MariaDB
-- Verze PHP: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `d137303_vydel`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `advertiser`
--

CREATE TABLE IF NOT EXISTS `advertiser` (
  `id` int(10) unsigned NOT NULL,
  `advertiser_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL COMMENT 'nadřazený obchod',
  `account_status` enum('Active','Deactive') NOT NULL,
  `language` varchar(16) NOT NULL,
  `advertiser_name` varchar(255) NOT NULL,
  `advertiser_alias` varchar(255) NOT NULL COMMENT 'kdyby bylo jmeno treba zmenit (kratsi)',
  `program_url` varchar(255) NOT NULL,
  `relationship_status` enum('joined','notjoined') NOT NULL,
  `mobile_supported` tinyint(3) unsigned NOT NULL,
  `mobile_tracking_certified` tinyint(3) unsigned NOT NULL,
  `commission_link` varchar(1024) NOT NULL,
  `logo` varchar(126) NOT NULL,
  `desc_short` varchar(2048) NOT NULL,
  `desc_full` text NOT NULL,
  `desc_extra` text,
  `cashback_customer` float unsigned NOT NULL DEFAULT '0' COMMENT 'hodnota kolik procent dostane zákazník',
  `cashback_value` float unsigned NOT NULL COMMENT 'hodnota kolik procent dostaneme my',
  `cashback_const` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'hodnota je maximalni moznost, ale ne konstatni',
  `cashback_percent` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `url` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `category_id` int(11) unsigned DEFAULT NULL,
  `is_favourite` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `show_in_menu` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'zda se zobrazi ikonka v menu',
  `source` enum('cj','vydelavejnakupem') NOT NULL COMMENT 'odkud advertiser je',
  `browser_panel` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `first_published` datetime DEFAULT NULL,
  `site_domain` varchar(16) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4851255 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `advertiser`
--

INSERT INTO `advertiser` (`id`, `advertiser_id`, `parent_id`, `account_status`, `language`, `advertiser_name`, `advertiser_alias`, `program_url`, `relationship_status`, `mobile_supported`, `mobile_tracking_certified`, `commission_link`, `logo`, `desc_short`, `desc_full`, `desc_extra`, `cashback_customer`, `cashback_value`, `cashback_const`, `cashback_percent`, `url`, `active`, `category_id`, `is_favourite`, `show_in_menu`, `source`, `browser_panel`, `first_published`, `site_domain`) VALUES
(2270432, 2270432, NULL, 'Active', 'en', 'AliExpress by Alibaba.com', 'Aliexpress', 'http://www.aliexpress.com/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-10578899-1463521489000', '1483887759.png', '', '', '', 5, 7, 1, 1, 'aliexpress', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', NULL),
(2538890, 2538890, NULL, 'Active', 'en', 'AVG Technologies', '', 'http://www.avg.com/affiliate/us-en/homepage.html', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-10660579-1476794458000', '10659002.jpg', '<p>AVG patř&iacute; ke světov&eacute; &scaron;pičce v boji s online hrozbami s v&iacute;ce než 200 miliony aktivn&iacute;ch uživatelů.</p>\n', '<p>AVG patř&iacute; ke světov&eacute; &scaron;pičce v boji s online hrozbami s v&iacute;ce než 200 miliony aktivn&iacute;ch uživatelů.</p>\n', '', 30, 60, 0, 1, 'avg', 1, NULL, 0, 0, 'cj', 1, '2017-04-12 23:39:06', NULL),
(2983194, 2983194, NULL, 'Active', 'en', 'ASTRATEX - CZ', '', 'http://www.astratex.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11316235-1424791602000', '10951742.jpg', '<p>ASTRATEX.cz je největ&scaron;&iacute; prodejce spodn&iacute;ho pr&aacute;dla a plavek.</p>\n', '<p>ASTRATEX.cz je největ&scaron;&iacute; prodejce spodn&iacute;ho pr&aacute;dla a plavek.</p>\n\n<p>E-shop nab&iacute;z&iacute; d&aacute;msk&eacute; i p&aacute;nsk&eacute; spodn&iacute; pr&aacute;dlo, plavky, luxusn&iacute; nočn&iacute; pr&aacute;dl, sportovn&iacute; oblečen&iacute; nebo termopr&aacute;dlo.</p>\n', NULL, 7, 7, 0, 1, 'astratex', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(3007908, 3007908, NULL, 'Active', 'en', 'Bonatex - CZ', '', 'http://www.bonatex.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11316236-1487072547000', '10959481.jpg', '<p>Bonatex.cz je česk&yacute; internetov&yacute; obchod s povlečen&iacute;m, ložn&iacute;m pr&aacute;dlem a bytov&yacute;mi doplňky.&nbsp;</p>\n', '<p>Bonatex.cz je česk&yacute; internetov&yacute; obchod s povlečen&iacute;m, ložn&iacute;m pr&aacute;dlem a bytov&yacute;mi doplňky.&nbsp;Nab&iacute;z&iacute;&nbsp;kvalitn&iacute; povlečen&iacute;,&nbsp;prostěradla ve frot&eacute; nebo jersey &uacute;pravě z bavlny, bambusu, sat&eacute;nu nebo flanelu,&nbsp;ručn&iacute;ky a osu&scaron;ky s exkluzivn&iacute;mi vzory, kter&eacute; jinde neseženete. Česk&aacute; značka Bonatex je garantem kvality prod&aacute;van&eacute;ho zbož&iacute;. V&scaron;ec</p>\n', NULL, 5, 10, 0, 1, 'bonatex', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(3132685, 3132685, NULL, 'Active', 'en', 'Studio Moderna CZ', '', 'http://www.topshop.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-10896358-1454936946000', '', '', '', NULL, 0, 9, 1, 1, 'studio-moderna-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(3278515, 3278515, NULL, 'Active', 'en', 'CSOB Pojistovny - CZ', 'ČSOB Pojišťovna', 'http://www.csobpoj.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-10885432-1467119478000', '11152799.jpg', '<p>ČSOB Poji&scaron;ťovna nab&iacute;z&iacute; &scaron;irok&eacute; spektrum poji&scaron;těn&iacute;</p>\n', '<p>ČSOB Poji&scaron;ťovna nab&iacute;z&iacute; &scaron;irok&eacute; spektrum poji&scaron;těn&iacute;, mezi kter&aacute; patř&iacute; životn&iacute;&nbsp;i neživotn&iacute;&nbsp;poji&scaron;těn&iacute; občanů, mal&yacute;ch&nbsp;a středn&iacute;ch&nbsp;podnikatelům i velk&yacute;m korporac&iacute;.&nbsp;</p>\n', '', 8, 15, 1, 1, 'csob-pojistovny', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(3304249, 3304249, NULL, 'Active', 'en', 'ASTRATEX.SK', '', 'http://www.astratex.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11509927-1452507090000', '1480211838.png', '<p>ASTRATEX.cz je největ&scaron;&iacute; prodejce spodn&iacute;ho pr&aacute;dla a plavek.</p>\n', '<p>ASTRATEX je v&yacute;znamn&yacute; predajca spodnej bielizne, plaviek a ko&scaron;ieľok, na internete p&ocirc;sobi už od roku 2001. Ročne ASTRATEX obsl&uacute;ži desiatky tis&iacute;c z&aacute;kazn&iacute;kov. O ich spokojnosti vypoved&aacute; vysok&eacute; percento opakovan&yacute;ch n&aacute;kupov. V&scaron;etok tovar je na sklade, takže sa vybavuje 100% objedn&aacute;vok a nedoch&aacute;dza k ru&scaron;eniu objedn&aacute;vok zo strany obchodu.</p>\n', '', 6, 12, 0, 1, 'astratex-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-10 23:52:14', 'sk'),
(3342166, 3342166, NULL, 'Active', 'en', 'Kasa - CZ', '', 'http://www.kasa.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-10903661-1462362695000', '1480276246.jpg', '<p>Kasa.cz&nbsp;je jednou&nbsp;z největ&scaron;&iacute;ch internetov&yacute;ch n&aacute;kupn&iacute;</p>\n', '<p>Kasa.cz&nbsp;je jednou&nbsp;z největ&scaron;&iacute;ch internetov&yacute;ch n&aacute;kupn&iacute; česk&eacute;ho internetu.</p>\n\n<p>Vyb&iacute;rejte ze &scaron;irok&eacute; nab&iacute;dky od dom&aacute;c&iacute;ch spotřebičů, elektorniky, sportovn&iacute;ho vybaven&iacute;, chovatelsk&yacute;ch potřeb, kosmetiky, n&aacute;bytku, zahradn&iacute; tecniky, dětsk&eacute;ho vybaven&iacute; a&nbsp;spousty dal&scaron;&iacute;ch doplňků pro voln&yacute; čas a z&aacute;bavu.</p>\n', '', 4, 8, 0, 1, 'kasa-cz', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(3409221, 3409221, NULL, 'Active', 'en', 'Allianz Pojistovna, A.s. - CZ', 'Allianz', 'https://online.allianz.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-10922270-1483526755000', '11227595.jpg', '<p>Allianz poji&scaron;ťovna, a. s. je&nbsp;souč&aacute;st&iacute; největ&scaron;&iacute;ho světov&eacute;ho poji&scaron;ťovac&iacute;ho koncernu Allianz Group.</p>\n', '<p>Allianz poji&scaron;ťovna, a. s. je&nbsp;souč&aacute;st&iacute; největ&scaron;&iacute;ho světov&eacute;ho poji&scaron;ťovac&iacute;ho koncernu Allianz Group, kter&yacute; chr&aacute;n&iacute; život a&nbsp;majetky 80 milionů lid&iacute; ve&nbsp;v&iacute;ce než&nbsp;70&nbsp;zem&iacute;ch.<br />\n<br />\nV&nbsp;Česk&eacute; republice působ&iacute;me od r. 1993. Od&nbsp;t&eacute; doby pro&scaron;lo na&scaron;ima rukama přes 9&nbsp;milionů smluv, a&nbsp;drž&iacute;me si&nbsp;tak m&iacute;sto 3.&nbsp;největ&scaron;&iacute; poji&scaron;ťovny u&nbsp;n&aacute;s.<br />\n<br />\nPoji&scaron;ťujeme v&iacute;ce než 700&nbsp;tis&iacute;c česk&yacute;ch řidičů, majitelů nemovitost&iacute; a&nbsp;turistů. Na&scaron;e filozofie je&nbsp;poskytovat nej&scaron;ir&scaron;&iacute; kryt&iacute; spolu s&nbsp;nadstandardn&iacute; p&eacute;č&iacute; o&nbsp;klienty.<br />\n<br />\nAllianz penzijn&iacute; společnost&nbsp;nab&iacute;z&iacute; důchodov&eacute; spořen&iacute; a doplňkov&eacute; penzijn&iacute; spořen&iacute;.</p>\n', '', 6, 12, 1, 1, 'allianz-direct', 1, NULL, 0, 1, 'cj', 1, '2017-03-02 19:28:19', 'cz'),
(3659001, 3659001, NULL, 'Active', 'en', 'INTERNATIONAL PROGRAM VUELING', '', 'http://www.vueling.com', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11047810-1484936279000', '1480276174.jpg', '', '', NULL, 0, 0, 1, 1, 'international-program-vueling', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(3662652, 3662652, NULL, 'Deactive', 'en', 'Nakupvakci - CZ', '', 'http://www.nakupvakci.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11267176-1452790516000', '11460878.png', '', '', NULL, 3, 11, 1, 1, 'nakupvakci', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(3662813, 3662813, NULL, 'Active', 'en', 'Hotely.cz', '', 'http://www.hotely.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11053060-1451564359000', '1480263184.png', '<p>Hotely.cz patř&iacute; mezi předn&iacute; zprostředkovatele ubytovac&iacute;ch služeb.</p>\n', '<p>Hotely.cz patř&iacute; mezi předn&iacute; zprostředkovatele ubytovac&iacute;ch služeb a kromě ubytov&aacute;n&iacute; po cel&eacute; ČR nab&iacute;z&iacute; tak&eacute; celou řadu l&aacute;zeňsk&yacute;ch, relaxačn&iacute;ch, romantick&yacute;ch i v&iacute;kendov&yacute;ch pobytů.</p>\n', '', 2, 5, 1, 1, 'hotely-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(3707193, 3707193, NULL, 'Active', 'en', 'SLEVOTEKA.CZ', '', 'https://www.slevoteka.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11120891-1467207156000', '11634950.jpg', '', '', NULL, 4, 11, 1, 1, 'slevoteka', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(3716953, 3716953, NULL, 'Active', 'en', 'HyperSlevy', '', 'http://www.hyperslevy.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11066192-1459407374000', '1480263203.gif', '<p>HyperSlevy.cz jsou port&aacute;lem zab&yacute;vaj&iacute;c&iacute; se prodejem hromadn&yacute;ch slev.</p>\n', '<p>HyperSlevy.cz jsou port&aacute;lem zab&yacute;vaj&iacute;c&iacute; se prodejem hromadn&yacute;ch slev. Denně nab&iacute;z&iacute;me několik des&iacute;tek &uacute;žasn&yacute;ch nab&iacute;dek se slevou 50-90% z oblasti zbož&iacute;, cestov&aacute;n&iacute;, kosmetiky a gastronomie.</p>\n', NULL, 2.5, 5, 1, 1, 'hyperslevy', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 00:01:14', 'cz'),
(3769367, 3769367, NULL, 'Active', 'en', 'Zlavohit.sk', '', 'http://www.zlavohit.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11106094-1404203875000', '1479376234.jpg', '', '', NULL, 2, 4, 1, 1, 'zlavohit-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(3772579, 3772579, NULL, 'Active', 'en', 'CenyNaDne.sk', '', 'http://www.cenynadne.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11108478-1348486523000', '1480258278.jpg', '<p>Cenynadne.sk prin&aacute;&scaron;a každ&yacute; deň atrakt&iacute;vne pobyty, tovar a služby za v&yacute;razne zn&iacute;žen&eacute; ceny.</p>\n', '<p>Cenynadne.sk prin&aacute;&scaron;a každ&yacute; deň atrakt&iacute;vne pobyty, tovar a služby za v&yacute;razne zn&iacute;žen&eacute; ceny.</p>\n', NULL, 2.5, 5, 1, 1, 'cenynadne-sk', 1, NULL, 0, 0, 'cj', 1, '2017-03-05 23:35:38', 'sk'),
(3773527, 3773527, NULL, 'Active', 'en', 'Boomer - SK', '', 'http://www.boomer.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11128891-1468571119000', '1480255382.png', '', '', NULL, 2, 5, 1, 1, 'boomer-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(3774518, 3774518, NULL, 'Active', 'en', 'INTERNATIONAL PROGRAM IBERIA EXPRESS', '', 'http://www.iberiaexpress.com/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11847285-1448385035000', '', '', '', NULL, 0, 0, 1, 1, 'international-program-iberia-express', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(3781746, 3781746, NULL, 'Active', 'en', 'Slevadne - CZ', '', 'http://www.slevadne.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11130816-1470319801000', '1480277655.jpg', '', '', NULL, 2, 5, 1, 1, 'slevadne-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(3792995, 3792995, NULL, 'Active', 'en', 'Primazlavy.sk', '', 'http://www.primazlavy.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11124992-1454419663000', '1480276992.jpg', '', '', NULL, 1, 3, 1, 1, 'primazlavy-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(3795100, 3795100, NULL, 'Active', 'en', 'Auto Europe Car Rentals', '', 'http://www.autoeurope.com', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12234084-1472830142000', '', '', '', NULL, 0, 3, 1, 1, 'auto-europe-car-rentals', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(3803104, 3803104, NULL, 'Active', 'en', 'Adrop.sk', '', 'http://www.adrop.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11438866-1440489624000', '1480211349.png', '<p>Port&aacute;l Adrop.sk pon&uacute;ka&nbsp;darčeky pre mužov i ženy, pre dvoch i skupiny.</p>\n', '<p>Port&aacute;l Adrop.sk pon&uacute;ka&nbsp;darčeky pre mužov i ženy, pre dvoch i skupiny. Vyberať m&ocirc;žete z kateg&oacute;ri&iacute; adrenal&iacute;nov&eacute;&nbsp;darčeky, &scaron;port, auto-moto, voda, vzduch, relax&aacute;cia a kr&aacute;sa, romantika, kurzy a určite&nbsp;neprehliadnite &scaron;peci&aacute;lnu ponuku.</p>\n', '', 6, 12, 1, 1, 'adrop-sk', 1, NULL, 0, 0, 'cj', 1, '2017-03-05 23:39:57', 'sk'),
(3804135, 3804135, NULL, 'Active', 'en', 'Certovskezlavy.sk', '', 'http://www.certovskezlavy.sk/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11132496-1477306784000', '1480255530.jpg', '', '', NULL, 2, 4, 1, 1, 'certovskezlavy-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(3846628, 3846628, NULL, 'Active', 'en', 'Zlavodom.SK', '', 'http://www.zlavodom.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11188565-1389084668000', '', '', '', NULL, 0, 4, 1, 1, 'zlavodom-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(3855115, 3855115, NULL, 'Active', 'en', 'Megazlava.SK', '', 'http://www.megazlava.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11212877-1428498998000', '1480276554.png', '', '', NULL, 2, 5, 1, 1, 'megazlava-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(3857051, 3857051, NULL, 'Active', 'en', '123zlavy.sk', '', 'http://www.123zlavy.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11189622-1457958639000', '1480208930.jpg', '<p>123zlavy.sk je zameran&yacute; na to aby v&aacute;m prin&aacute;&scaron;al každ&yacute; deň bezkonkurenčn&uacute; ponuku s v&yacute;raznou zľavou.</p>\n', '<p>Port&aacute;l 123zlavy.sk je zameran&yacute; na to aby v&aacute;m prin&aacute;&scaron;al každ&yacute; deň bezkonkurenčn&uacute; ponuku s v&yacute;raznou zľavou, aby ste si mohli vo va&scaron;om meste vysk&uacute;&scaron;ať to, po čom ste vždy t&uacute;žili, za fantastick&eacute; ceny. Využ&iacute;vame silu hromadn&eacute;ho nakupovania. Vyjedn&aacute;me pre v&aacute;s tie najlep&scaron;ie akcie v meste so zľavami až do 95 % v re&scaron;taur&aacute;ci&aacute;ch, baroch, beauty sal&oacute;noch, na wellness, z&aacute;bavu, v&iacute;kendov&eacute; pobyty, &scaron;portov&eacute; akcie a in&eacute; zauj&iacute;mav&eacute; služby ako aj tovary.<br />\n&nbsp;</p>\n', '', 2, 4, 1, 1, '123zlavy-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-10 23:50:06', 'sk'),
(3924040, 3924040, NULL, 'Active', 'en', 'Bombapobyt.sk', '', 'http://www.bombapobyt.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11271835-1382946281000', '1480255311.jpg', '', '', NULL, 2, 5, 1, 1, 'bombapobyt-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(3930502, 3930502, NULL, 'Active', 'en', 'Zbozomat.cz', '', 'http://www.zbozomat.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11277838-1455029440000', '11966842.jpg', '', '', NULL, 3, 8, 1, 1, 'zbozomat-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(3936405, 3936405, NULL, 'Active', 'en', 'Slever.cz', '', 'http://www.slever.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11305080-1486728420000', '12008569.jpg', '<p>Slever.cz je port&aacute;l, kter&yacute; v&aacute;m každ&yacute; den přin&aacute;&scaron;&iacute; novou v&yacute;hodnou slevu!</p>\n', '<p>Slever.cz je port&aacute;l, kter&yacute; v&aacute;m každ&yacute; den přin&aacute;&scaron;&iacute; novou v&yacute;hodnou slevu!</p>\n', NULL, 5, 8, 0, 1, 'slever-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 00:04:44', 'cz'),
(3938143, 3938143, NULL, 'Deactive', 'en', 'Newgo.cz', '', 'http://www.newgo.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11285723-1459327712000', '', '', '', NULL, 0, 8, 1, 1, 'newgo-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(3946019, 3946019, NULL, 'Active', 'en', 'Zlavolam.sk', '', 'http://www.zlavolam.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11319489-1447426800000', '1480280512.png', '<p>Na port&aacute;li Zlavolam.sk n&aacute;jdete každ&yacute; deň množstvo 50% - 90% zliav na r&ocirc;zne produkty a služby!</p>\n', '<p>Na port&aacute;li Zlavolam.sk n&aacute;jdete každ&yacute; deň množstvo 50% - 90% zliav na r&ocirc;zne produkty a služby!</p>\n', NULL, 2, 4, 1, 1, 'zlavolam-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 23:55:28', 'sk'),
(3947734, 3947734, NULL, 'Deactive', 'en', 'Ilovetravel.cz', '', 'http://www.ilovetravel.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11420274-1476979903000', '1479376981.png', '', '', NULL, 0, 5, 1, 1, 'ilovetravel-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(3949914, 3949914, NULL, 'Active', 'en', 'Bata - CZ', 'Baťa.cz', 'http://www.bata.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11332299-1461842320000', '12064025.jpg', '<p>Sortiment největ&scaron;&iacute;ho prodejce bot&nbsp;v ČR je online.</p>\n', '<p>Největ&scaron;&iacute; prodejce bot v ČR Baťa nab&iacute;z&iacute; svůj sortiment online. Nakupujte d&aacute;msk&eacute; p&aacute;nsk&eacute; i dětsk&eacute; boty online od světozn&aacute;m&eacute;ho prodejce.</p>\n', '', 5, 8, 1, 1, 'bata-cz', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(3958272, 3958272, NULL, 'Active', 'en', 'Primacestovani.cz', '', 'http://www.primacestovani.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11311875-1451385573000', '1480276975.png', '', '', NULL, 4, 8, 1, 1, 'primacestovani-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4017417, 4017417, NULL, 'Active', 'en', 'Venda.cz', '', 'http://www.venda.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11377375-1482918986000', '1489010625.png', '<p>Venda.cz je modern&iacute; projekt, specializovan&yacute; na poskytov&aacute;n&iacute; v&yacute;hodn&yacute;ch nab&iacute;dek a zaj&iacute;mav&yacute;ch slev sv&yacute;m z&aacute;kazn&iacute;kům.</p>\n', '<p>Venda.cz je modern&iacute; projekt, specializovan&yacute; na poskytov&aacute;n&iacute; v&yacute;hodn&yacute;ch nab&iacute;dek a zaj&iacute;mav&yacute;ch slev sv&yacute;m z&aacute;kazn&iacute;kům.</p>\n', NULL, 6, 11, 1, 1, 'venda-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:03:45', 'cz'),
(4038908, 4038908, NULL, 'Active', 'en', 'Conrad.cz', '', 'http://www.conrad.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11398614-1487242084000', '1488231574.jpg', '<p>Conrad Electronic je největ&scaron;&iacute; prodejce elektroniky v Evropě!</p>\n', '<p>Conrad Electronic je největ&scaron;&iacute; prodejce elektroniky v Evropě! Jen v Česk&eacute; republice nab&iacute;z&iacute; v&iacute;ce než 140&nbsp;000 v&yacute;robků, kter&eacute; pokr&yacute;vaj&iacute; t&eacute;měř v&scaron;e, co m&aacute; něco společn&eacute;ho s elektronikou a technikou.</p>\n', NULL, 4, 8, 0, 1, 'conrad-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 22:39:34', 'cz'),
(4051258, 4051258, NULL, 'Active', 'en', 'Bata.sk', 'Baťa.sk', 'http://www.bata.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11428773-1453128610000', '1480212691.jpg', '<p>Nakupujte sortiment světozn&aacute;m&eacute;ho prodejce bot Baťa online.</p>\n', '<p>Největ&scaron;&iacute; prodejce bot v ČR Baťa nab&iacute;z&iacute; svůj sortiment online. Nakupujte d&aacute;msk&eacute; p&aacute;nsk&eacute; i dětsk&eacute; boty online od světozn&aacute;m&eacute;ho prodejce.</p>\n', NULL, 5, 8, 1, 1, 'bata-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4052024, 4052024, NULL, 'Active', 'en', 'Damejidlo.cz', '', 'http://www.damejidlo.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11417680-1478082650000', '12236275.jpg', '<p>D&aacute;meJ&iacute;dlo.cz nab&iacute;z&iacute; &scaron;irokou nab&iacute;dku restaurac&iacute; a rozvozu j&iacute;dla po cel&eacute; republice.</p>\n', '<p>Na DameJidlo.cz naleznete v&iacute;ce než 1000 restaurac&iacute; ze 123 měst ČR a celou &scaron;k&aacute;lu různ&yacute;ch druhů kuchyn&iacute; - od Asie po Zeleninu. V&scaron;e přehledně zatř&iacute;děn&eacute; a zobrazen&eacute;. J&iacute;st můžete už během p&aacute;r kliknut&iacute;. Stač&iacute; vybrat si podle hodnocen&iacute; a kategorie restauraci, do ko&scaron;&iacute;ku vložit požadovan&eacute; j&iacute;dlo, vybrat způsob &uacute;hrady a j&iacute;dlo je na cestě.</p>\n\n<p>&nbsp;</p>\n', '', 3, 5, 1, 1, 'damejidlo-cz', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4065356, 4065356, NULL, 'Active', 'en', 'SportObchod.cz', '', 'http://www.sportobchod.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11477762-1479478526000', '12357509.jpg', '<p>SportObchod.cz - port&aacute;l s velikou &scaron;k&aacute;lou sportovn&iacute;ch potřeb.</p>\n', '<p>Na port&aacute;lu SportObchod.cz najdete &scaron;irok&eacute; spektrum sportovn&iacute;ch potřeb, od brusl&iacute;, hokejov&eacute; a fotbalov&eacute; v&yacute;stroje, tenisov&yacute;ch a badmintonov&yacute;ch raket po nosiče kol, sportovn&iacute; v&yacute;živu, sportovn&iacute; oblečen&iacute;, powerball a mnoho dal&scaron;&iacute;ho!</p>\n', '', 2, 5, 1, 1, 'sportobchod-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4066260, 4066260, NULL, 'Active', 'en', 'PRIODY.com', '', 'https://www.priody.com/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12777498-1485983189000', '', '', '', NULL, 0, 0, 1, 1, 'priody-com', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4067754, 4067754, NULL, 'Active', 'en', 'Adrop.cz', '', 'http://www.adrop.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11438906-1449053315000', '1480211339.png', '<p>Adrop.cz - z&aacute;žitky jako d&aacute;rek!</p>\n', '<p>Port&aacute;l Adrop.cz nab&iacute;z&iacute; d&aacute;rky pro muže i ženy, pro dvojice i skupiny. Vyb&iacute;rat můžete z kategori&iacute; adrenalinov&eacute; d&aacute;rky, sport, auto-moto, voda, vzduch, relaxace a kr&aacute;sa, romantika, kurzy a určitě nepřehl&eacute;dněte speci&aacute;ln&iacute; nab&iacute;dku.</p>\n', NULL, 6, 12, 1, 1, 'adrop-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4067781, 4067781, NULL, 'Deactive', 'en', 'Darcekovy-raj.sk', '', 'http://www.darcekovy-raj.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11434953-1438254568000', '1480275980.png', '', '', NULL, 7, 3, 0, 1, 'darcekovy-raj-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4098645, 4098645, NULL, 'Active', 'en', 'Parfemy-Elnino.cz', '', 'https://www.parfemy-elnino.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11466391-1456419923000', '1484077971.gif', '<p>Na port&aacute;lu internetov&eacute; parfumerie Parfemy-Elnino.cz najdete v&iacute;ce než 438 000 kvalitn&iacute;ch parf&eacute;mů skladem.</p>\n', '<p>Na port&aacute;lu internetov&eacute; parfumerie Parfemy-Elnino.cz najdete v&iacute;ce než 438 000 kvalitn&iacute;ch parf&eacute;mů skladem ihned k expedici! 3 v&yacute;dejn&iacute; m&iacute;sta pro osobn&iacute; odběry s možnost&iacute; prodeje v Praze a Nov&eacute; Pace + 15 v&yacute;dejn&iacute;ch m&iacute;st ULOŽENKA po cel&eacute; ČR!</p>\n', NULL, 2, 5, 1, 1, '', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4120215, 4120215, NULL, 'Deactive', 'en', 'Geekshop.cz', '', 'http://www.geekshop.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11579331-1481556649000', '12520165.jpg', '<p>Jedin&yacute; ofici&aacute;ln&iacute; internetov&yacute; prodejce licencovan&yacute;ch komiksov&yacute;ch triček.</p>\n', '<p>Jedin&yacute; ofici&aacute;ln&iacute; internetov&yacute; prodejce licencovan&yacute;ch triček s motivy&nbsp;Superman, Batman,&nbsp;DC Originals a seri&aacute;lu Big Bang Theory. Naleznete u zde pouze origin&aacute;ln&iacute; komiksov&eacute; a seri&aacute;lov&eacute; potisky. Jejich trička jsou pr&eacute;miov&eacute; kvality, v&scaron;e se tiskne v ČR. Na česk&eacute;m trhu působ&iacute; od roku 2007.</p>\n', NULL, 7.5, 15, 1, 1, 'geekshop-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 00:00:00', 'cz'),
(4132083, 4132083, NULL, 'Active', 'en', 'Snowboards.cz', '', 'http://www.snowboards.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11958376-1459508740000', '1480277748.png', '<p>E-shop Snowboards.cz&nbsp;nab&iacute;z&iacute; sportovn&iacute; oblečen&iacute; a vybaven&iacute;.</p>\n', '<p>Prodej sportovn&iacute;ch potřeb a vybaven&iacute; nejen pro sportovce. Cyklistika,&nbsp;snowboarding, kiteboarding, lyžov&aacute;n&iacute;.</p>\n', NULL, 4, 8, 1, 1, 'snowboards-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4132084, 4132084, NULL, 'Active', 'en', 'Sporty.cz', '', 'http://www.sporty.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11957234-1459508752000', '1480277822.png', '<p>Sporty.cz nab&iacute;z&iacute; &scaron;irok&yacute; sortiment zbož&iacute; pro běh, cyklistiku, outdoor, snowboarding, skialpinismum a ostatn&iacute; sporty.</p>\n', '<p>Za port&aacute;lem Sporty.cz stoj&iacute; t&yacute;m aktivn&iacute;ch sportovců, d&iacute;ky tomu V&aacute;m nab&iacute;z&iacute; osobně vyzkou&scaron;en&eacute; zbož&iacute;, jehož kvalitu si na vlastn&iacute; kůži ověřili. Port&aacute;l funguje od r. 2009 a nab&iacute;z&iacute; &scaron;irok&yacute; sortiment zbož&iacute; pro běh, cyklistiku, outdoor, snowboarding, skialpinismum a ostatn&iacute; sporty.</p>\n', NULL, 4, 8, 1, 1, 'sporty-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:17:17', 'cz'),
(4134473, 4134473, NULL, 'Active', 'en', 'Zlavovysvet.sk', '', 'http://zlavovysvet.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11505979-1457951969000', '1479374088.jpg', '', '', NULL, 5, 11, 1, 1, 'zlavovysvet-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4136467, 4136467, NULL, 'Active', 'en', 'DesignShoes.cz', '', 'http://www.designshoes.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11581222-1383821246000', '1480258455.png', '<p>Port&aacute;l DesignShoes.cz nab&iacute;z&iacute; pouze origin&aacute;ln&iacute; a značkov&eacute; boty s důrazem na design a skvělou cenu.</p>\n', '<p>Port&aacute;l DesignShoes.cz nab&iacute;z&iacute; pouze origin&aacute;ln&iacute; a značkov&eacute; boty s důrazem na design a skvělou cenu.</p>\n', NULL, 7, 7, 0, 1, 'designshoes-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4145051, 4145051, NULL, 'Active', 'en', 'Pradlo-Elegant.cz', '', 'http://www.pradlo-elegant.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11565276-1382607201000', '', '', '', NULL, 0, 3, 1, 1, 'pradlo-elegant-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4155694, 4155694, NULL, 'Active', 'en', 'Harmoline.cz', '', 'http://www.harmoline.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11643273-1387190663000', '1488233720.png', '<p>Port&aacute;l Harmoline.cz nab&iacute;z&iacute; řadu produktů na hubnut&iacute;.</p>\n', '<p>Port&aacute;l Harmoline.cz nab&iacute;z&iacute; řadu produktů na hubnut&iacute; společnosti Norphampton Biotech obsahuj&iacute;c&iacute;ch &uacute;činn&eacute; př&iacute;rodn&iacute; l&aacute;tky nejvy&scaron;&scaron;&iacute; kvality. Jedinečnou v&yacute;hodou produktů Harmoline je individu&aacute;ln&iacute; ře&scaron;en&iacute; na m&iacute;ru. Jin&eacute; &uacute;činn&eacute; l&aacute;tky potřebuje sportovec, kter&yacute; touž&iacute; po vyr&yacute;sovan&yacute;ch svalech, a jin&eacute; zaberou u ženy s dlouhodobou nadv&aacute;hou zapř&iacute;činěnou nevyv&aacute;žen&yacute;m j&iacute;deln&iacute;čkem s přem&iacute;rou uhlohydr&aacute;tů.</p>\n', NULL, 100, 200, 1, 0, 'harmoline-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 23:15:20', 'cz'),
(4201179, 4201179, NULL, 'Active', 'en', 'Smarty.cz', '', 'http://www.smarty.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11687781-1455274620000', '12734087.png', '<p>Smarty.cz nab&iacute;z&iacute; &scaron;irokou nab&iacute;dku&nbsp;mobiln&iacute;ch telefonů, tabletů a dal&scaron;&iacute;ho př&iacute;slu&scaron;enstv&iacute;.</p>\n', '<p>Smarty.cz je česk&yacute; e-shop nab&iacute;zej&iacute;c&iacute; &scaron;irokou nab&iacute;dku&nbsp;mobiln&iacute;ch telefonů, tabletů a dal&scaron;&iacute;ho př&iacute;slu&scaron;enstv&iacute;.</p>\n', NULL, 1, 2, 1, 1, 'smarty-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4213975, 4213975, NULL, 'Active', 'en', 'VelkéSamolepky.cz', '', 'http://www.velkesamolepky.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11675254-1390834363000', '1483878797.gif', '<p>&Scaron;irok&yacute; v&yacute;běr dekorativn&iacute;ch kr&aacute;sn&yacute;ch samolepek na zeď.</p>\n', '<p>Vyberte si&nbsp;samolepky na zeď, kter&eacute; dopln&iacute; př&iacute;jemnou atmosf&eacute;ru Va&scaron;eho domova a dodaj&iacute; mu to spr&aacute;vn&eacute; kouzlo.</p>\n', NULL, 5, 10, 1, 1, 'velkesamolepky-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4230368, 4230368, NULL, 'Active', 'en', 'ActionZážitky.sk', '', 'http://www.actionzazitky.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11732707-1455540865000', '1480210183.jpg', '<p>ActionZ&aacute;žitky.sk je port&aacute;l pre milovn&iacute;kov adrenal&iacute;nov&yacute;ch z&aacute;žitkov, ktor&yacute; pon&uacute;ka bezkonkurenčn&eacute; z&aacute;žitky na cel&yacute; život!</p>\n', '<p>ActionZ&aacute;žitky.sk je port&aacute;l pre milovn&iacute;kov adrenal&iacute;nov&yacute;ch z&aacute;žitkov, ktor&yacute; pon&uacute;ka bezkonkurenčn&eacute; z&aacute;žitky na cel&yacute; život!</p>\n', NULL, 2, 4, 1, 1, 'actionzazitky-sk', 1, NULL, 0, 0, 'cj', 1, '2017-03-05 23:38:27', 'sk'),
(4238401, 4238401, NULL, 'Active', 'en', 'Spotřebiče Whirlpool.cz', '', 'http://www.spotrebice-whirlpool.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11774362-1432196084000', '1480277853.jpg', '<p>Spotřebiče Whirlpool.cz prod&aacute;v&aacute; &scaron;pičkov&eacute; spotřebiče.</p>\n', '<p>Spotřebiče Whirlpool.cz prod&aacute;v&aacute; &scaron;pičkov&eacute; spotřebiče značek Whirlpool, Indesit, Kitchen Aid, Bauknecht, Hot Point a dal&scaron;&iacute;. Najdete zde myčky, pračky, chladnčky a jin&eacute; spotřebiče.</p>\n', NULL, 3, 6, 0, 1, 'spotrebice-whirlpool-cz', 1, NULL, 0, 0, 'cj', 0, '2017-04-09 22:39:02', 'cz'),
(4267436, 4267436, NULL, 'Active', 'en', 'Pilulka.cz', '', 'http://www.pilulka.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11781997-1447861587000', '12923185.jpg', '<p>Pilulka.cz je online l&eacute;k&aacute;rna s des&iacute;tkami odběrn&yacute;ch m&iacute;st po cel&eacute; ČR.&nbsp;</p>\n', '<p>Pilulka.cz je online l&eacute;k&aacute;rna s des&iacute;tkami odběrn&yacute;ch m&iacute;st po cel&eacute; ČR. Nakoupit můžete z pohodl&iacute; domova, zaplatit rychle a bezpečně online a př&iacute;padně objednat expres kur&yacute;ra, kter&yacute; V&aacute;m zbož&iacute; do 4 hodin doveze až domů!</p>\n', '', 3, 5, 1, 1, 'pilulka-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4269013, 4269013, NULL, 'Active', 'en', 'Erectan.cz', '', 'http://www.erectan.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11758549-1396615871000', '1480260726.jpg', '<p>Na port&aacute;lu v&yacute;robce Erectan.cz najdete kompletn&iacute; řadu př&iacute;pravků pro dokonal&eacute; prokrven&iacute; pohlavn&iacute;ch org&aacute;nů a podporu erekce.</p>\n', '<p>Na port&aacute;lu v&yacute;robce Erectan.cz najdete kompletn&iacute; řadu př&iacute;pravků pro dokonal&eacute; prokrven&iacute; pohlavn&iacute;ch org&aacute;nů a podporu erekce. Nab&iacute;z&iacute; doplňky stravy pro muže pro jednor&aacute;zov&eacute; i dlouhodob&eacute; už&iacute;v&aacute;n&iacute;.</p>\n', NULL, 10, 20, 1, 1, 'erectan-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 22:47:49', 'cz'),
(4285555, 4285555, NULL, 'Active', 'en', 'Slevoking.cz', '', 'http://slevoking.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11782045-1398762648000', '1480277676.png', '<p>Slevoking.cz je prvn&iacute;m&nbsp;největ&scaron;&iacute;m slevov&yacute;m port&aacute;lem v Česku a na Slovensku se zaměřen&iacute;m&nbsp;na v&yacute;hodn&eacute; nab&iacute;dky v oblasti cestov&aacute;n&iacute;.</p>\n', '<p>Slevoking.cz je prvn&iacute;m&nbsp;největ&scaron;&iacute;m slevov&yacute;m port&aacute;lem v Česku a na Slovensku se zaměřen&iacute;m&nbsp;na v&yacute;hodn&eacute; nab&iacute;dky v oblasti cestov&aacute;n&iacute;. Vět&scaron;ina nab&iacute;zen&yacute;ch hotelů je nav&iacute;c Slevokingem osobně ověřen&aacute;.</p>\n', NULL, 2, 4, 1, 1, 'slevoking-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 00:06:34', 'cz'),
(4285556, 4285556, NULL, 'Active', 'en', 'Zlavoking.sk', '', 'http://zlavoking.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11782106-1483371887000', '1480280561.png', '<p>Zlavoking.sk je port&aacute;l zameran&yacute; na v&yacute;hodn&eacute; ponuky v oblasti cestovania, n&aacute;jdete tu dve prehľadn&eacute; kateg&oacute;rie - pobyty a z&aacute;jazdy.</p>\n', '<p>Zlavoking.sk je port&aacute;l zameran&yacute; na v&yacute;hodn&eacute; ponuky v oblasti cestovania, n&aacute;jdete tu dve prehľadn&eacute; kateg&oacute;rie - pobyty a z&aacute;jazdy. V&scaron;etky prezentovan&eacute; hotely s&uacute; osobne<br />\nvysk&uacute;&scaron;an&eacute;! &nbsp;</p>\n', NULL, 3, 6, 1, 1, 'zlavoking-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 23:57:38', 'sk'),
(4307021, 4307021, NULL, 'Active', 'en', 'Kytary.cz', '', 'http://kytary.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11836480-1464600526000', '1480276500.jpg', '<p>Kytary.cz je nejlep&scaron;&iacute; prodejce hudebn&iacute;ch n&aacute;strojů v ČR.</p>\n', '<p>Kytary.cz je nejlep&scaron;&iacute; prodejce hudebn&iacute;ch n&aacute;strojů v ČR. Nakupujte ze &scaron;irok&eacute;ho sortimentu hudebn&iacute;ch n&aacute;strojů online.</p>\n', NULL, 1, 3, 1, 1, 'kytary-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4311505, 4311505, NULL, 'Active', 'en', 'HotelVoucher.sk', '', 'http://www.hotelvoucher.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11836442-1403515827000', '1480263080.jpg', '', '', NULL, 2, 5, 1, 1, 'hotelvoucher-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4316775, 4316775, NULL, 'Active', 'en', 'Tarifomat.cz', '', 'https://tarifomat.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11921371-1440076086000', '1479377634.gif', '<p>Tarifomat je největ&scaron;&iacute;m zprostředkovatelem mobiln&iacute;ch tarifů a internetu v ČR. Z&aacute;roveň je velmi vyzn&aacute;mn&yacute;m hr&aacute;čem na poli energi&iacute;.</p>\n', '<p>Tarifomat je největ&scaron;&iacute;m zprostředkovatelem mobiln&iacute;ch tarifů a internetu v ČR. Z&aacute;roveň je velmi vyzn&aacute;mn&yacute;m hr&aacute;čem na poli energi&iacute;.</p>\n\n<p>Na port&aacute;lu Tarifomat.cz V&aacute;m přehledn&aacute; kalkulačka zdarma srovn&aacute; nab&iacute;dky v&scaron;ech poskytovatelů, vyberete si tu nejv&yacute;hodněj&scaron;&iacute; a Tarifomat V&aacute;m ji zprostředkuje a zař&iacute;d&iacute; ve&scaron;kerou administrativu.&nbsp;</p>\n', NULL, 200, 300, 0, 0, 'tarifomat-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:24:41', 'cz'),
(4322332, 4322332, NULL, 'Active', 'en', 'Nazuby.cz', '', 'http://www.nazuby.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11883390-1483526835000', '1479377608.png', '<p>NaZuby.cz je eshop s velkou nab&iacute;dkou produktů z oblasti &uacute;stn&iacute; hygieny.</p>\n\n<p>&nbsp;</p>\n', '<p>NaZuby.cz je eshop s velkou nab&iacute;dkou produktů z oblasti &uacute;stn&iacute; hygieny. Nab&iacute;z&iacute; &scaron;irok&yacute; sortiment produktů.</p>\n', NULL, 2.5, 5, 0, 1, 'nazuby-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 22:39:08', 'cz'),
(4327375, 4327375, NULL, 'Active', 'en', 'Answear.cz', '', 'http://answear.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11845756-1468933186000', '13050789.jpg', '<p>ANSWEAR.cz nab&iacute;z&iacute; d&aacute;msk&eacute;, p&aacute;nsk&eacute; a dětsk&eacute; značkov&eacute; oblečen&iacute; a boty.</p>\n', '<p>ANSWEAR.cz to jsou n&aacute;kupy bez omezen&iacute;. V nab&iacute;dce najdete oblečen&iacute;, obuv a doplňky v&iacute;ce než 200 nejm&oacute;dněj&scaron;&iacute;ch světov&yacute;ch značek. Mezi nimi jsou zn&aacute;m&eacute; a obl&iacute;ben&eacute; popul&aacute;rn&iacute; značky (Mango, Vero Moda), sportovn&iacute; značky (Adidas, Nike, New Balance), kultovn&iacute; džinov&eacute; značky (Levi&rsquo;s, Lee, Wrangler), jak i značky vy&scaron;&scaron;&iacute; tř&iacute;dy (Diesel, Guess Jeans, Tommy Hilfiger, Valentino, DKNY).</p>\n', '', 4, 8, 1, 1, 'answear-cz', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4329824, 4329824, NULL, 'Active', 'en', 'Tutor.cz', '', 'http://www.tutor.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12719226', '', '', '', NULL, 0, 0, 1, 1, 'tutor-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4344860, 4344860, NULL, 'Active', 'en', 'Stoklasa.cz', '', 'http://www.stoklasa.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11912252-1475660034000', '13171008.jpg', '<p>Stoklasa.cz - textiln&iacute; galanterie, v&yacute;tvarn&eacute; a kreativn&iacute; potřeby, dekorace, bižuterie, m&oacute;dn&iacute; doplňky a kabelky.</p>\n', '<p>Velkoobchod a maloobchod Stoklasa textiln&iacute; galanterie s.r.o. je stabiln&iacute; a expanduj&iacute;c&iacute; společnost s dlouholetou tradic&iacute;. Z&aacute;klad na&scaron;&iacute; nab&iacute;dky tvoř&iacute;&nbsp;textiln&iacute; galanterie, v&yacute;tvarn&eacute; a kreativn&iacute; potřeby, dekorace, bižuterie, m&oacute;dn&iacute; doplňky a kabelky.</p>\n', NULL, 5, 10, 0, 1, 'stoklasa-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-30 20:21:20', 'cz'),
(4352963, 4352963, NULL, 'Active', 'en', 'Footshop.cz', '', 'https://www.footshop.cz/cs/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11907360-1459247826000', '1488233411.jpg', '<p>Na port&aacute;lu Footshop.cz najdete d&aacute;mskou, p&aacute;nskou i dětskou obuv.</p>\n', '<p>Na port&aacute;lu Footshop.cz najdete d&aacute;mskou, p&aacute;nskou i dětskou obuv, d&aacute;le značkov&eacute; skate,&nbsp;hip-hop a sneakers boty, k&scaron;iltovky, batohy, peněženky a mnoho dal&scaron;&iacute;ho. Ve&scaron;ker&eacute; zbož&iacute; je&nbsp;skladem a připraveno k okamžit&eacute;mu odesl&aacute;n&iacute;! &nbsp;</p>\n', NULL, 7.5, 15, 1, 1, 'footshop-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 23:10:11', NULL),
(4356323, 4356323, NULL, 'Active', 'en', 'Tescoma.cz', '', 'http://eshop.tescoma.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11919999-1466172884000', '1318654.png', '<p>Tescoma je česk&aacute; společnost s 20-ti letou tradic&iacute;, kter&aacute; nab&iacute;z&iacute; kompletn&iacute; sortiment kuchyňsk&yacute;ch potřeb</p>\n', '<p>Tescoma je 100% česk&aacute; společnost s 20-ti letou tradic&iacute;, kter&aacute; nab&iacute;z&iacute; kompletn&iacute; sortiment kuchyňsk&yacute;ch potřeb. Jedn&aacute; se o origin&aacute;ln&iacute;, kvalitn&iacute; a dokonale funkčn&iacute; v&yacute;robky, kter&eacute; Tescoma nejen prod&aacute;v&aacute;, ale ve vlastn&iacute;m Design centru tak&eacute; sama vyv&iacute;j&iacute; a testuje. Řada v&yacute;robků nese označen&iacute; světov&yacute; patent a někter&eacute; z&iacute;skaly tak&eacute; prestižn&iacute; oceněn&iacute; za design.</p>\n', '', 4, 8, 0, 1, 'tescoma-cz', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4371373, 4371373, NULL, 'Active', 'en', 'Beate-uhse.cz', '', 'http://www.beate-uhse.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11919925-1481207996000', '1480253992.jpg', '<p>Beate-uhse.cz - elegantn&iacute; m&oacute;da a spodn&iacute; pr&aacute;dlo pro ženy i muže, stylov&eacute; doplňky do dom&aacute;cnosti, hračky a DVD pro dospěl&eacute; a dal&scaron;&iacute;.</p>\n', '<p>Na port&aacute;lu Beate-uhse.cz najdete elegantn&iacute; m&oacute;du a spodn&iacute; pr&aacute;dlo pro ženy i muže, stylov&eacute; doplňky do dom&aacute;cnosti, hračky a DVD pro dospěl&eacute; a dal&scaron;&iacute;.</p>\n', '', 7, 15, 1, 1, 'beate-uhse-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-12 23:56:08', 'cz'),
(4379461, 4379461, NULL, 'Active', 'en', 'CK ALEXANDRIA.CZ', 'ALEXANDRIA.CZ', 'http://www.alexandria.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-11958558-1477580270000', '1480258363.jpg', '<p>CK Alexandria patř&iacute; mezi největ&scaron;&iacute; česk&eacute; cestovn&iacute; kancel&aacute;ře na trhu.</p>\n', '<p>Cestovn&iacute; kancel&aacute;ř Alexandria patř&iacute; k největ&scaron;&iacute;m na česk&eacute;m trhu. Zajistěte si letn&iacute; dovolenou v nejobl&iacute;beněj&scaron;&iacute;ch destinac&iacute;ch nebo zimn&iacute; dovolenou na lyž&iacute;ch.&nbsp;Nab&iacute;dka kancel&aacute;ře&nbsp;je celoročn&iacute;, v nab&iacute;dce najdete tak&eacute;&nbsp;z&aacute;jezdy do exotick&yacute;ch zem&iacute;.</p>\n', NULL, 1, 3, 1, 1, 'ck-alexandria-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 20:42:20', 'cz'),
(4384608, 4384608, 4384608, 'Active', 'en', 'Vivantis.cz', '', 'http://www.vivantis.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11974894-1438261743000', '1484508590.png', '<p>E-shop s &scaron;irokou nab&iacute;dkou oblečen&iacute;, m&oacute;dn&iacute;ch doplňků a kosmetiky.</p>\n', '<p>E-shop s &scaron;irokou nab&iacute;dkou oblečen&iacute;, m&oacute;dn&iacute;ch doplňků a kosmetiky.</p>\n', NULL, 7, 5, 1, 1, 'vivantis-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4385202, 4385202, NULL, 'Active', 'en', 'CestovatelskyObchod.cz', '', 'http://www.cestovatelskyobchod.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11946197-1410942922000', '13239019.gif', '<p>Port&aacute;l CestovatelskyObchod.cz nab&iacute;z&iacute; potřeby (ne)jen pro cestovatele a v&yacute;letn&iacute;ky.</p>\n', '<p>Port&aacute;l CestovatelskyObchod.cz nab&iacute;z&iacute; potřeby (ne)jen pro cestovatele a v&yacute;letn&iacute;ky. Najdete tu &scaron;irok&yacute;&nbsp;sortiment outdoorov&eacute;ho vybaven&iacute;, od batohů, bot a oblečen&iacute;, po doplňky jako jsou z&aacute;mky,&nbsp;l&eacute;k&aacute;rničky, trekov&eacute; hole, adapt&eacute;ry a mnoho dal&scaron;&iacute;ho!</p>\n', NULL, 5, 10, 1, 1, 'cestovatelskyobchod-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 22:45:51', 'cz'),
(4386089, 4386089, NULL, 'Active', 'en', 'mBank.cz', '', 'http://www.mbank.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11935891-1430407798000', '13218306.jpg', '<p>mBank vstoupila na česk&yacute; trh v roce 2007 a svou bezpoplatkovou politikou a transparentn&iacute;m př&iacute;stupem ke sv&yacute;m z&aacute;kazn&iacute;kům způsobila v bankovn&iacute;m sektoru skutečnou revoluci.</p>\n', '<p>mBank vstoupila na česk&yacute; trh v roce 2007 a svou bezpoplatkovou politikou a transparentn&iacute;m př&iacute;stupem ke sv&yacute;m z&aacute;kazn&iacute;kům způsobila v bankovn&iacute;m sektoru skutečnou revoluci. V současn&eacute; době se mBank řad&iacute; mezi nejv&yacute;znamněj&scaron;&iacute; česk&eacute; banky.&nbsp;</p>\n', '', 250, 500, 1, 0, 'mbank-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-27 23:20:19', 'cz'),
(4400192, 4400192, NULL, 'Active', 'en', 'Different.cz', '', 'http://www.Different.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11988464-1449741178000', '1483882411.gif', '<p>Na port&aacute;lu Different.cz najdete d&aacute;msk&eacute;, p&aacute;nsk&eacute; i dětsk&eacute; oblečen&iacute; a doplňky popul&aacute;rn&iacute;ch světov&yacute;ch značek.</p>\n', '<p>Na port&aacute;lu Different.cz najdete d&aacute;msk&eacute;, p&aacute;nsk&eacute; i dětsk&eacute; oblečen&iacute; a doplňky popul&aacute;rn&iacute;ch světov&yacute;ch značek, kter&eacute; pr&aacute;vě let&iacute;, jako Desigual, Crocs, Disaster, Melissa, EMU a dal&scaron;&iacute;!</p>\n', NULL, 5, 11, 1, 1, 'different-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4409645, 4409645, NULL, 'Active', 'en', 'KvetinyExpres.cz', '', 'http://kvetinyexpres.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12081313-1452252879000', '1480276274.png', '<p>Port&aacute;l KvětinyExpres.cz nab&iacute;z&iacute; rozvozu a doručov&aacute;n&iacute; květin a tak&eacute; d&aacute;rkov&eacute; ko&scaron;e, v&iacute;na a luxusn&iacute; čokol&aacute;dy.</p>\n', '<p>Port&aacute;l KvětinyExpres.cz nab&iacute;z&iacute; kromě rozvozu a doručov&aacute;n&iacute; květin v Praze, Ostravě, Brně a Bratislavě tak&eacute; d&aacute;rkov&eacute; ko&scaron;e, v&iacute;na a luxusn&iacute; čokol&aacute;dy.</p>\n', NULL, 5, 10, 1, 1, 'kvetinyexpres-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:32:46', 'cz'),
(4411734, 4411734, NULL, 'Active', 'en', 'Otehotnenie.sk', '', 'http://www.otehotnenie.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12030126-1484665375000', '1480276856.png', '', '', NULL, 0, 10, 1, 1, 'otehotnenie-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4429232, 4429232, NULL, 'Active', 'en', 'Modino.cz', '', 'http://modino.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12086143-1421059731000', '13518277.jpg', '<p>Port&aacute;l Modino.cz nab&iacute;z&iacute; nejnověj&scaron;&iacute; m&oacute;dn&iacute; trendy z Francie a &Scaron;panělska!</p>\n', '<p>Port&aacute;l Modino.cz nab&iacute;z&iacute; nejnověj&scaron;&iacute; m&oacute;dn&iacute; trendy z Francie a &Scaron;panělska! Najdete zde d&aacute;mskou m&oacute;du, spodn&iacute; pr&aacute;dlo, obuv i doplňky. Nab&iacute;z&iacute;me &scaron;irok&yacute; sortiment zbož&iacute;, velk&yacute; v&yacute;běr barev a velikost&iacute;.</p>\n', NULL, 3.5, 7, 1, 1, 'modino-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-05 23:21:55', 'cz'),
(4429236, 4429236, NULL, 'Active', 'en', 'Modino.sk', '', 'http://modino.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12087433-1421151360000', '1480276609.png', '', '', NULL, 3, 7, 1, 1, 'modino-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4429877, 4429877, NULL, 'Active', 'en', 'Prodeti.cz', '', 'http://www.prodeti.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11999731-1482402615000', '13367959.png', '<p>Prodeti.cz je online n&aacute;kupn&iacute; klub pro maminky a děti.</p>\n', '<p>Prodeti.cz je online n&aacute;kupn&iacute; klub pro maminky a děti, kde najdete dětsk&eacute; oblečen&iacute; a obuv, hračky, hry, plenky, dětskou v&yacute;živu a &scaron;irok&yacute; sortiment kojeneck&yacute;ch potřeb.</p>\n', '', 4, 8, 1, 1, 'prodeti-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-21 22:41:50', 'cz'),
(4429957, 4429957, NULL, 'Active', 'en', 'Answear.sk', '', 'http://answear.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12408852-1477566335000', '1480211424.jpg', '', '', NULL, 6, 8, 1, 1, 'answear-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4432968, 4432968, NULL, 'Active', 'en', 'Butlers.cz', '', 'http://www.butlers.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12070029-1421314643000', '1480255830.jpg', '<p>S polečnost Butlers přin&aacute;&scaron;&iacute; skvěl&eacute; n&aacute;pady, d&aacute;rky pro kamar&aacute;dy, rady pro kuchařsk&eacute; nad&scaron;ence, inspiraci pro individualisty, n&aacute;dhern&eacute; věci vyroben&eacute; pro v&aacute;&scaron; domov.</p>\n', '<p>S polečnost Butlers přin&aacute;&scaron;&iacute; skvěl&eacute; n&aacute;pady, d&aacute;rky pro kamar&aacute;dy, rady pro kuchařsk&eacute; nad&scaron;ence, inspiraci pro individualisty, n&aacute;dhern&eacute; věci vyroben&eacute; pro v&aacute;&scaron; domov. A to v&scaron;e v dobr&eacute; kvalitě za nejlep&scaron;&iacute; cenu.</p>\n', NULL, 5, 10, 1, 1, 'butlers-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4438849, 4438849, NULL, 'Active', 'en', 'Pilulka.sk', '', 'http://www.pilulka.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12050739-1468398508000', '1480276890.png', '<p>Pilulka.cz je online l&eacute;k&aacute;rna s des&iacute;tkami odběrn&yacute;ch m&iacute;st po cel&eacute; SR.</p>\n', '<p>Pilulka.cz je online l&eacute;k&aacute;rna s des&iacute;tkami odběrn&yacute;ch m&iacute;st po cel&eacute; SR. Nakoupit můžete z pohodl&iacute; domova, zaplatit rychle a bezpečně online a př&iacute;padně objednat expres kur&yacute;ra, kter&yacute; V&aacute;m zbož&iacute; do 4 hodin doveze až domů!</p>\n', NULL, 3, 5, 1, 1, 'pilulka-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4442114, 4442114, NULL, 'Deactive', 'en', 'Bookio.cz', '', 'http://bookio.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12098071-1422020920000', '1480255356.png', '', '', NULL, 15, 33, 1, 1, 'bookio-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4442115, 4442115, NULL, 'Active', 'en', 'Bookio.sk', '', 'http://bookio.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12098086-1422020850000', '1480255364.png', '<p>Na port&aacute;li Bookio.sk si m&ocirc;žete z pohodlia domova cez internet jednoducho zarezervovať st&ocirc;l vo svojej obľ&uacute;benej re&scaron;taur&aacute;cii.</p>\n', '<p>Na port&aacute;li Bookio.sk si m&ocirc;žete z pohodlia domova cez internet jednoducho zarezervovať st&ocirc;l vo svojej obľ&uacute;benej re&scaron;taur&aacute;cii.</p>\n', NULL, 13, 27, 1, 0, 'bookio-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-23 23:18:10', 'sk'),
(4444762, 4444762, NULL, 'Active', 'en', 'HALBO.cz', '', 'http://www.halbo.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12141756-1456744465000', '1488233226.png', '<p>Na port&aacute;lu Halbo.cz najdete d&aacute;mskou, p&aacute;nskou a dětskou obuv, m&oacute;dn&iacute; doplňky top značek Rieker, Tamaris, Mustang, Jana, Marco Tozzi a Olympikus.</p>\n', '<p>Na port&aacute;lu Halbo.cz najdete d&aacute;mskou, p&aacute;nskou a dětskou obuv, m&oacute;dn&iacute; doplňky top značek Rieker, Tamaris, Mustang, Jana, Marco Tozzi a Olympikus.</p>\n', NULL, 5, 10, 0, 1, 'halbo-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 23:07:06', 'cz'),
(4444861, 4444861, NULL, 'Active', 'en', 'Snowboards.sk', '', 'http://www.snowboards.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12466147-1451992340000', '1480277759.png', '', '', NULL, 4, 8, 1, 1, 'snowboards-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4444863, 4444863, NULL, 'Deactive', 'en', 'Sporty.sk', '', 'http://www.sporty.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12466197-1451997011000', '1479374561.png', '', '', NULL, 0, 8, 1, 1, 'sporty-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4447520, 4447520, NULL, 'Active', 'en', 'Stoklasa.sk', '', 'http://www.stoklasa-sk.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12069987-1455024634000', '1480277881.jpg', '', '', NULL, 5, 5, 0, 1, 'stoklasa-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4455265, 4455265, NULL, 'Active', 'en', 'IBERIA EU', '', 'http://www.iberia.com', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12119525-1467875119000', '', '', '', NULL, 0, 0, 1, 1, 'iberia-eu', 0, NULL, 0, 0, 'cj', 1, NULL, NULL);
INSERT INTO `advertiser` (`id`, `advertiser_id`, `parent_id`, `account_status`, `language`, `advertiser_name`, `advertiser_alias`, `program_url`, `relationship_status`, `mobile_supported`, `mobile_tracking_certified`, `commission_link`, `logo`, `desc_short`, `desc_full`, `desc_extra`, `cashback_customer`, `cashback_value`, `cashback_const`, `cashback_percent`, `url`, `active`, `category_id`, `is_favourite`, `show_in_menu`, `source`, `browser_panel`, `first_published`, `site_domain`) VALUES
(4460474, 4460474, NULL, 'Active', 'en', 'VMD-drogerie.cz', '', 'http://www.vmd-drogerie.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12102201-1422959262000', '1490550699.png', '<p>VMD drogerie nab&iacute;z&iacute;&nbsp;parf&eacute;my, kosmetika,&nbsp;drogerie, drogistick&eacute; zbož&iacute;, hygienick&eacute; prostředky, autokosmetika, mal&iacute;řsk&eacute; a natěračsk&eacute; hmoty</p>\n', '<p>Internetov&yacute; obchod VMD drogerie nab&iacute;z&iacute; &scaron;irokou nab&iacute;dku drogistick&eacute;ho zbož&iacute; a mnoho dal&scaron;&iacute;ho. V e-shopu pohodlně z domova nakoup&iacute;te parf&eacute;my, kosmetiku, dětsk&eacute; zbož&iacute;, hygienick&eacute; prostředky, čistic&iacute; a &uacute;klidov&eacute; prostředky, průmyslovou&nbsp;chemii, netradičn&iacute; d&aacute;rky, dekorace do bytu na p&aacute;rty, v&scaron;e pro dům a zahradu, potravin&aacute;řsk&eacute; v&yacute;robky, chovatelsk&eacute; prostředky, autokosmetiku, mal&iacute;řsk&eacute; a natěračsk&eacute; hmoty, m&oacute;du&nbsp;a doplňky, &scaron;perky, slunečn&iacute; a dioptrick&eacute; br&yacute;le, zdravotn&eacute; potřeby, ekologick&eacute; v&yacute;robky.</p>\n', NULL, 1, 3.5, 1, 1, 'vmd-drogerie-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 21:39:15', 'cz'),
(4475577, 4475577, NULL, 'Active', 'en', 'TorriaCars.cz', '', 'http://www.torriacars.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12106881-1422870064000', '1480278035.png', '', '', NULL, 3, 6, 1, 1, 'torriacars-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4475584, 4475584, NULL, 'Active', 'en', 'TorriaCars.sk', '', 'http://www.torriacars.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12106917-1422872476000', '1480278059.png', '', '', NULL, 3, 6, 1, 1, 'torriacars-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4482699, 4482699, NULL, 'Active', 'en', 'Triola.cz', '', 'http://www.triola.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12150500-1462954713000', '1480278160.jpg', '<p>Triola.cz nab&iacute;z&iacute; d&aacute;msk&eacute; spodn&iacute; pr&aacute;dlo, plavky, nočn&iacute; pr&aacute;dlo.</p>\n', '<p>Renomovan&aacute; česk&aacute; značka Triola nab&iacute;z&iacute; komplexn&iacute; sortiment v oblasti d&aacute;msk&eacute;ho spodn&iacute;ho pr&aacute;dla a mnohem v&iacute;ce. Vyb&iacute;rejte z d&aacute;msk&eacute;ho spodn&iacute;ho pr&aacute;dla značek TRIOLA a Like YOU nebo d&aacute;msk&yacute;ch&nbsp;a p&aacute;nsk&eacute;&yacute;ch plavek&nbsp;TRIOLA a GUARD by Triola.</p>\n', NULL, 7, 8, 0, 1, 'triola-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 21:51:35', 'cz'),
(4489910, 4489910, NULL, 'Active', 'en', 'Chefshop.cz', '', 'http://www.chefshop.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12221237-1474360404000', '1480255477.jpg', '<p>E-shop, ve kter&eacute;m pohodlně nakoup&iacute;te&nbsp;v&scaron;e potřebn&eacute; pro př&iacute;pravu j&iacute;del, ingredience i n&aacute;dob&iacute;.</p>\n', '<p>Jistě jste již sly&scaron;eli o na&scaron;&iacute; &Scaron;kole vařen&iacute; Chefparade. V t&eacute; již několik let uč&iacute;me milovn&iacute;ky gastronomie&nbsp;vařit klasick&aacute; evropsk&aacute;, ale i exotick&aacute; j&iacute;dla. Dal&scaron;&iacute;m logick&yacute;m krokem pak bylo&nbsp;založit e-shop, Chefshop.cz, ve kter&eacute;m pohodlně nakoup&iacute;te&nbsp;v&scaron;echny ingredience potřebn&eacute; pro př&iacute;pravu těchto j&iacute;del. Od t&eacute; doby již ale uplynula nějak&aacute; doba a my jsme se&nbsp;rozrostli a s n&aacute;mi i n&aacute;&scaron; sortiment. V současnosti u n&aacute;s kromě potravin naleznete&nbsp;hrnce, p&aacute;nve a dal&scaron;&iacute; věci na vařen&iacute;, velkou &scaron;k&aacute;lu produktů na pečen&iacute;, kuchyňsk&eacute; roboty a dal&scaron;&iacute; vychyt&aacute;vky a v neposledn&iacute; řadě kr&aacute;sn&eacute; tal&iacute;ře, misky hrnky a dal&scaron;&iacute; věci na v&aacute;&scaron; stůl a to včetně unik&aacute;tn&iacute; japonsk&eacute; keramiky Made in Japan, kterou v Evropě nikde jinde nekoup&iacute;te!</p>\n', NULL, 5, 10, 0, 1, 'chefshop-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 00:00:00', 'cz'),
(4502358, 4502358, NULL, 'Active', 'de', 'OTTO - CZ', '', 'http://www.otto-shop.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12215036-1434455217000', 'ottoshop.png', '<p>M&oacute;da, značky, trendy. Objevte pestr&yacute; svět OTTO!</p>\n', '<p>Nekonečn&yacute; v&yacute;běr z v&iacute;ce než 40 000 produktů. Nakupujte m&oacute;du na OTTO-shop.cz a užijte si neopakovateln&yacute; z&aacute;žitek z nakupov&aacute;n&iacute;.</p>\n', NULL, 3, 6, 1, 1, 'otto-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4503047, 4503047, NULL, 'Active', 'en', 'Deewatch', '', 'https://www.deewatch.com/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12147815-1462350059000', '1491946946.png', '<p>Deewatch - žensk&eacute; hodinky, n&aacute;ramky a doplňky.</p>\n', '<p>Deewatch prod&aacute;v&aacute; žensk&eacute; hodinky, n&aacute;ramky a doplňky. Hodinky kombinuj&iacute; skandin&aacute;vskou kvalitu a jednoduchost s exluzivn&iacute;m a romantick&yacute;m designem z jižn&iacute; Evropy.</p>\n', '', 4, 8, 1, 1, 'deewatch', 1, NULL, 0, 0, 'cj', 1, '2017-04-11 23:42:26', NULL),
(4506402, 4506402, NULL, 'Active', 'en', 'Sushitime.cz', '', 'http://www.sushitime.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12184963-1480087846000', '1489011788.jpg', '<p>Sushitime.cz nab&iacute;z&iacute; doručen&iacute; čerstvě připraven&eacute;ho j&iacute;dla až k Va&scaron;im dveř&iacute;m!</p>\n', '<p>Vychutnat si dobr&eacute; j&iacute;dlo doma, v kancel&aacute;ři anebo na p&aacute;rty bez n&aacute;mahy a starost&iacute; s př&iacute;pravou je jedn&iacute;m z největ&scaron;&iacute;ch požitků dne&scaron;ka. Proto port&aacute;l Sushitime.cz nab&iacute;z&iacute; doručen&iacute; čerstvě připraven&eacute;ho j&iacute;dla až k Va&scaron;im dveř&iacute;m!</p>\n', NULL, 4, 8, 1, 1, 'sushitime-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:23:08', 'cz'),
(4507590, 4507590, NULL, 'Active', 'en', 'Posters.cz', '', 'http://www.posters.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12173181-1475660002000', '13693481.png', '', '', NULL, 5, 10, 0, 1, 'posters-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4513714, 4513714, NULL, 'Active', 'en', 'Shirtinator.cz', '', 'http://www.shirtinator.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12195929-1452690327000', '1480277526.png', '<p>Nechte si vyrobit origin&aacute;ln&iacute; tričko s potiskem podle vlastn&iacute; fantazie na port&aacute;lu Shirtinator.cz!</p>\n', '<p>Nechte si vyrobit origin&aacute;ln&iacute; tričko s potiskem podle vlastn&iacute; fantazie na port&aacute;lu Shirtinator.cz! &nbsp;<br />\nSpecializujeme se na individu&aacute;ln&iacute; potisk d&aacute;msk&yacute;ch, p&aacute;nsk&yacute;ch i dětsk&yacute;ch triček, mikin, dal&scaron;&iacute;ho doplňkov&eacute;ho textilu a d&aacute;rkov&yacute;ch předmětů.</p>\n', NULL, 5, 10, 1, 1, 'shirtinator-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 23:10:44', 'cz'),
(4513716, 4513716, NULL, 'Active', 'en', 'Shirtinator.sk', '', 'http://www.shirtinator.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12197638-1452690449000', '1480277533.png', '', '', NULL, 5, 10, 1, 1, 'shirtinator-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4514885, 4514885, NULL, 'Deactive', 'en', 'Shooos.cz', '', 'http://www.shooos.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12384721-1446027805000', '1479377923.jpg', '', '', NULL, 0, 9, 1, 1, 'shooos-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4514887, 4514887, NULL, 'Active', 'en', 'Shooos.sk', '', 'http://www.shooos.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12384854-1485771432000', '1480277572.jpg', '<p>Shooos.sk&nbsp; - dvaja nad&scaron;enci, ktor&iacute; sa rozhodli pred&aacute;vať pekn&eacute; top&aacute;nky.</p>\n', '<p>Shooos.sk&nbsp; - dvaja nad&scaron;enci, ktor&iacute; sa rozhodli pred&aacute;vať pekn&eacute; top&aacute;nky. Mysl&iacute;me si, že top&aacute;nky s&uacute; v&yacute;znamnou časťou oblečenia a viete nimi d&ocirc;kladne odprezentovať svoj &scaron;t&yacute;l a vkus. Maj&uacute; byť preto n&aacute;padn&eacute; a dobre viditeľn&eacute;. Na&scaron;im cieľom je pred&aacute;vať top&aacute;nky, ktor&eacute; sa n&aacute;m p&aacute;čia a pon&uacute;kať značky a modely, ktor&eacute; nie s&uacute; bežne dostupn&eacute;.</p>\n', NULL, 5, 9, 1, 1, 'shooos-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 23:12:05', 'sk'),
(4515416, 4515416, NULL, 'Deactive', 'en', 'Avon.cz', '', 'https://www.avon.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12166925-1478518570000', '1480211434.jpg', '<p>AVON je již v&iacute;ce než 125 let synonymem kr&aacute;sy.</p>\n', '', NULL, 0, 0, 1, 1, 'avon-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4520838, 4520838, NULL, 'Active', 'en', 'Čedok.cz', '', 'http://www.cedok.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12234805-1475499472000', '1483559902.gif', '<p>Čedok je česk&aacute; cestovn&iacute; kancel&aacute;ř s dlouholetou tradic&iacute;.&nbsp;</p>\n', '<p>Čedok nab&iacute;z&iacute; &scaron;irokou nab&iacute;dku pobytov&yacute;ch a pozn&aacute;vac&iacute;ch z&aacute;jezdů po cel&eacute;m světě. Objednejte si&nbsp;online tuzemsk&eacute; z&aacute;jezdy, zahraničn&iacute; z&aacute;jezdy, služebn&iacute; cesty nebo dovolenou &scaron;itou na m&iacute;ru,</p>\n', '', 1, 0, 1, 1, 'cedok-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4532235, 4532235, NULL, 'Active', 'en', 'ZlavoMAX.sk', '', 'http://www.zlavomax.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12219951-1485346613000', '1480280549.jpg', '', '', NULL, 3, 6, 1, 1, 'zlavomax-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4556917, 4556917, NULL, 'Active', 'en', 'Tescoma.sk', '', 'http://eshop.tescoma.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12292256-1450186085000', '1480277984.png', '<p>Tescoma je&nbsp;česk&aacute; společnost s 20-ti letou tradic&iacute;, kter&aacute; nab&iacute;z&iacute; kompletn&iacute; sortiment kuchyňsk&yacute;ch potřeb.</p>\n', '<p>Tescoma je 100% česk&aacute; společnost s 20-ti letou tradic&iacute;, kter&aacute; nab&iacute;z&iacute; kompletn&iacute; sortiment kuchyňsk&yacute;ch potřeb. Jedn&aacute; se o origin&aacute;ln&iacute;, kvalitn&iacute; a dokonale funkčn&iacute; v&yacute;robky, kter&eacute; Tescoma nejen prod&aacute;v&aacute;, ale ve vlastn&iacute;m Design centru tak&eacute; sama vyv&iacute;j&iacute; a testuje. Řada v&yacute;robků nese označen&iacute; světov&yacute; patent a někter&eacute; z&iacute;skaly tak&eacute; prestižn&iacute; oceněn&iacute; za design.</p>\n', NULL, 5, 8, 1, 1, 'tescoma-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4571294, 4571294, NULL, 'Active', 'en', 'RodinneBaleni.cz', '', 'http://www.rodinnebaleni.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12275665-1436450503000', '1480277165.jpg', '<p>Na port&aacute;lu RodinneBaleni.cz najdete &scaron;irok&yacute; v&yacute;běr doplňků do bytu od A až do Z.</p>\n', '<p>Na port&aacute;lu RodinneBaleni.cz najdete &scaron;irok&yacute; v&yacute;běr doplňků do bytu od A až do Z. Internetov&yacute; obchod s bohat&yacute;mi zku&scaron;enostmi, kter&yacute; nab&iacute;z&iacute; komfortn&iacute; služby a to nejlep&scaron;&iacute; zbož&iacute; od česk&yacute;ch i zahraničn&iacute;ch v&yacute;robců a design&eacute;rů. F&eacute;rově a rychle.</p>\n', NULL, 5, 10, 0, 1, 'rodinnebaleni-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 22:36:33', 'cz'),
(4577514, 4577514, NULL, 'Active', 'en', 'EXIM Tours.cz', '', 'https://www.eximtours.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12294952-1482919023000', '1484768825.gif', '<p>Cestovn&iacute; kancel&aacute;ř EXIM tours&nbsp;patř&iacute; mezi největ&scaron;&iacute; cestovn&iacute; kancel&aacute;ře na česk&eacute;m trhu.</p>\n', '<p>Cestovn&iacute; kancel&aacute;ř EXIM tours působ&iacute; na trhu již od r. 1993 a v současnosti patř&iacute; mezi největ&scaron;&iacute; cestovn&iacute; kancel&aacute;ře na česk&eacute;m trhu. Nab&iacute;z&iacute; pobytov&eacute; a pozn&aacute;vac&iacute; z&aacute;jezdy, last minute, exotiku, eurov&iacute;kendy, lyžov&aacute;n&iacute; i letenky.</p>\n', '', 2, 3.5, 0, 1, 'eximtours-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4589073, 4589073, NULL, 'Active', 'en', 'Footshop.eu', '', 'http://www.footshop.eu/en/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12384664-1459334551000', '', '', '', NULL, 0, 15, 1, 1, 'footshop-eu', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4589075, 4589075, NULL, 'Active', 'en', 'Bet-at-home.com', '', 'https://www.bet-at-home.com/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12327200-1478510436000', '1479378229.gif', '', '', NULL, 0, 0, 1, 1, 'bet-at-home-com', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4590827, 4590827, NULL, 'Active', 'en', 'Terezia.eu', '', 'http://www.terezia.eu', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12444914-1449748832000', '1480277975.jpg', '', '', NULL, 7, 15, 1, 1, 'terezia-eu', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4597444, 4597444, NULL, 'Active', 'en', 'Chytapust.cz', '', 'https://www.chytapust.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12347574-1486634389000', '1480258317.png', '<p>Obchod Chyť a pusť nab&iacute;z&iacute; &scaron;irok&yacute; sortiment ryb&aacute;řsk&yacute;ch potřeb.</p>\n', '<p>Obchod Chyť a pusť nab&iacute;z&iacute; &scaron;irok&yacute; sortiment ryb&aacute;řsk&yacute;ch potřeb. Vyzkou&scaron;ejte a zařaďte se mezi tis&iacute;ce spokojen&yacute;ch ryb&aacute;řů, kteř&iacute; si obl&iacute;bili kvalitn&iacute; v&yacute;robky, bleskov&eacute; dod&aacute;n&iacute;, f&eacute;rov&eacute; ceny a odbornou pomoc.</p>\n', NULL, 4, 8, 1, 1, 'chytapust-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 00:22:08', 'cz'),
(4601714, 4601714, NULL, 'Active', 'en', 'Face-up.cz', '', 'http://www.face-up.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12373510-1444982981000', '1492025713.png', '<p>Face-up.cz - revolučn&iacute; kosmetick&yacute; př&iacute;stroj pro čistou pleť bez akn&eacute; na dom&aacute;c&iacute; použit&iacute;.</p>\n', '<p>Na port&aacute;lu Face-up.cz najdete revolučn&iacute; kosmetick&yacute; př&iacute;stroj pro čistou pleť bez akn&eacute; na dom&aacute;c&iacute; použit&iacute;. Jedn&aacute; se o novou metodu s 90% &uacute;činnost&iacute; v klinick&yacute;ch studi&iacute;ch s př&iacute;jemnou a jednoduchou aplikac&iacute;!</p>\n', '', 8, 15, 1, 1, 'face-up-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-12 21:35:13', 'cz'),
(4602889, 4602889, NULL, 'Active', 'en', 'ZOOT.sk', '', 'http://www.zoot.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12334488-1448030610000', '1480280485.jpg', '', '', NULL, 4, 9, 1, 1, 'zoot-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4608876, 4608876, NULL, 'Active', 'en', 'Euronics.cz', '', 'http://www.euronics.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12467478-1476283282000', '1480262823.jpg', '<p>Euronics.cz&nbsp;představuje specialistu v&nbsp;oblasti prodeje spotřebn&iacute; elektroniky v&scaron;eho druhu.</p>\n', '<p>Elektro, kter&eacute; chcete m&iacute;t. Nakupujte elektroniku od&nbsp;EURONICS. Atraktivn&iacute; ceny, hust&aacute; s&iacute;ť elektroprodejen, osobn&iacute; odběr na 80 prodejn&aacute;ch zdarma.</p>\n', '', 3, 1, 0, 1, 'euronics-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4609486, 4609486, NULL, 'Active', 'en', 'Ageo.cz', '', 'http://www.ageo.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12384688-1446021080000', '1480211366.png', '<p>Online prodeje drogerie a dom&aacute;c&iacute;ch potřeb.</p>\n', '<p>Online prodeje drogerie a dom&aacute;c&iacute;ch potřeb. Chceme b&yacute;t t&iacute;m, na koho se obr&aacute;t&iacute;te, když V&aacute;m dojde cokoliv od toaletn&iacute;ho pap&iacute;ru, přes čist&iacute;c&iacute; prostředky až po luxusn&iacute; parf&eacute;m.</p>\n', NULL, 2.25, 4.5, 1, 1, 'ageo-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4612000, 4612000, NULL, 'Active', 'en', 'Bigbrands.cz', '', 'https://www.bigbrands.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12449698-1477920571000', '1480255257.png', '<p>N&aacute;kupn&iacute; klub, ve kter&eacute;m u&scaron;etř&iacute;te až 85% z ceny&nbsp;n&aacute;kupu.</p>\n', '<p>N&aacute;kupn&iacute; klub pouze pro pozvan&eacute;, ve kter&eacute;m u&scaron;etř&iacute;te až 85% ceny zn&aacute;m&yacute;ch značek, design&eacute;rsk&eacute; m&oacute;dy, oblečen&iacute;, obuvi, doplňků, sportovn&iacute;ho oblečen&iacute;.</p>\n', NULL, 4, 8, 1, 1, 'bigbrands-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4612007, 4612007, NULL, 'Active', 'en', 'Bigbrands.sk', '', 'https://www.bigbrands.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12449752-1475659601000', '1480255270.png', '<p>N&aacute;kupn&iacute; klub, ve kter&eacute;m u&scaron;etř&iacute;te až 85% z ceny n&aacute;kupu.</p>\n', '<p>N&aacute;kupn&iacute; klub pouze pro pozvan&eacute;, ve kter&eacute;m u&scaron;etř&iacute;te až 85% ceny zn&aacute;m&yacute;ch značek, design&eacute;rsk&eacute; m&oacute;dy, oblečen&iacute;, obuvi, doplňků, sportovn&iacute;ho oblečen&iacute;.</p>\n', NULL, 4, 8, 1, 1, 'bigbrands-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4612012, 4612012, NULL, 'Active', 'en', 'Rozbaleno.cz', '', 'http://www.rozbaleno.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12487996-1478614573000', '1480277454.png', '<p>Na Rozbaleno.cz si můžete vybrat z velmi &scaron;irok&eacute;ho sortimentu zbož&iacute;. Specializuje se předev&scaron;&iacute;m na prodej rozbalen&eacute;ho zbož&iacute; a zbož&iacute; z nadbytečn&yacute;ch z&aacute;sob ofici&aacute;ln&iacute;ch distributorů.</p>\n', '<p>Na Rozbaleno.cz si můžete vybrat z velmi &scaron;irok&eacute;ho sortimentu zbož&iacute;, jako jsou dom&aacute;c&iacute; spotřebiče, elektronika, mobiln&iacute; telefony, PC, audio technika, ale i oblečen&iacute; světov&yacute;ch značek, produkty pro děti, vybaven&iacute; pro turistiku, camping, a tak&eacute; v&scaron;e potřebn&eacute; pro dom&aacute;cnost a automobily. Specializujeme se předev&scaron;&iacute;m na prodej rozbalen&eacute;ho zbož&iacute; a zbož&iacute; z nadbytečn&yacute;ch z&aacute;sob ofici&aacute;ln&iacute;ch distributorů.</p>\n', NULL, 3, 5, 1, 1, 'rozbaleno-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 00:12:38', 'cz'),
(4615508, 4615508, NULL, 'Deactive', 'en', 'Tipsport.sk', '', 'https://www.tipsport.sk/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12480149-1453719425000', '1480278080.png', '', '', NULL, 15, 30, 1, 1, 'tipsport-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4616713, 4616713, NULL, 'Active', 'en', 'RentalCars.com', '', 'http://www.Rentalcars.com', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12566161-1464277553000', '1480277144.gif', '<p>Rentalcars.com je předn&iacute; světov&aacute; služba pro online půjčov&aacute;n&iacute; aut.</p>\n', '<p>Rentalcars.com je předn&iacute; světov&aacute; služba pro online půjčov&aacute;n&iacute; aut.</p>\n', '', 2.5, 5, 1, 1, 'rentalcars-com', 1, NULL, 0, 1, 'cj', 1, '2017-02-21 22:34:17', NULL),
(4620184, 4620184, NULL, 'Active', 'en', 'Fotofinish.cz', '', 'http://www.fotofinish.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12373724-1479452993000', '1489435667.png', '<p>Fotoknihy, plak&aacute;ty, fotografi, kalend&aacute;ře a mnoho dal&scaron;&iacute;ho pohodlně online a v nejvy&scaron;&scaron;&iacute; kvalitě.</p>\n', '<p>FotoFinish to je kvalitn&iacute; tisk fotografi&iacute;, pohlednic, plak&aacute;tů, kalend&aacute;řů, fotoknih a dal&scaron;&iacute;ch v&yacute;robků. On-line, rychle, levně a v prvotř&iacute;dn&iacute; kvalitě!</p>\n', NULL, 5, 10, 1, 1, 'fotofinish-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 21:08:42', 'cz'),
(4625942, 4625942, NULL, 'Active', 'de', 'Sheego CZ', '', 'http://www.sheego.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12414235-1448016724000', '1480277511.png', '<p>SHEEGO - d&aacute;msk&aacute; m&oacute;da od velikosti 40.</p>\n', '<p>SHEEGO nen&iacute; jen online obchod. S inspiracemi můžete sledovat aktu&aacute;ln&iacute; m&oacute;dn&iacute; trendy a naj&iacute; styl, kter&yacute; v&aacute;m nejl&eacute;pe vyhoduvje od velikosti 40 až do velikosti 58.</p>\n', NULL, 3, 6, 1, 1, 'sheego-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 23:02:03', 'cz'),
(4626556, 4626556, NULL, 'Deactive', 'en', 'WOODIES.CZ', '', 'http://www.woodies.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12398480-1482939681000', '', '', '', NULL, 0, 15, 1, 1, 'woodies-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4631233, 4631233, NULL, 'Deactive', 'en', 'ShopKilpi.cz', '', 'http://www.shopkilpi.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12399267-1480946474000', '1483877985.jpg', '<p>V e-shopu Kilpi poř&iacute;d&iacute;te značkov&eacute; outdoorov&eacute; a sportovn&iacute; oblečen&iacute; př&iacute;mo od v&yacute;robce!</p>\n', '<p>V e-shopu Kilpi poř&iacute;d&iacute;te značkov&eacute; outdoorov&eacute; a sportovn&iacute; oblečen&iacute; př&iacute;mo od v&yacute;robce! Vyr&aacute;b&iacute;me z nejmoderněj&scaron;&iacute;ch materi&aacute;lů a důraz klademe tak&eacute; na origin&aacute;ln&iacute; vzhled a styl. Na v&yacute;voji a testov&aacute;n&iacute; na&scaron;ich v&yacute;robků se pod&iacute;l&iacute; řada extr&eacute;mn&iacute;ch i amat&eacute;rsk&yacute;ch sportovců, v každ&eacute;m kousku oblečen&iacute; tak můžete objevovat praktick&eacute; vychyt&aacute;vky, kter&eacute; jistě ocen&iacute;te.</p>\n', NULL, 5, 10, 1, 1, 'shopkilpi-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4631322, 4631322, NULL, 'Active', 'en', 'Ekskluzywna.cz', '', 'http://ekskluzywna.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12421652-1448363846000', '1491947460.jpg', '<p>Ekskluzywna.cz - d&aacute;msk&eacute; a p&aacute;nsk&eacute; pr&aacute;dlo a doplňky, dětsk&eacute; oblečen&iacute;, plavky a velk&yacute; v&yacute;běr erotick&eacute;ho spodn&iacute;ho pr&aacute;dla.</p>\n', '<p>Port&aacute;l Ekskluzywna.cz nab&iacute;z&iacute; d&aacute;msk&eacute; a p&aacute;nsk&eacute; pr&aacute;dlo a doplňky, dětsk&eacute; oblečen&iacute;, plavky a velk&yacute; v&yacute;běr erotick&eacute;ho spodn&iacute;ho pr&aacute;dla.</p>\n', '', 5, 8, 1, 1, 'ekskluzywna-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-11 23:51:00', 'cz'),
(4634072, 4634072, NULL, 'Active', 'en', 'BIBLOO.cz', '', 'https://www.bibloo.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12399526-1460967180000', '1480255196.jpg', '<p>Na port&aacute;lu BIBLOO.cz nab&iacute;z&iacute;me d&aacute;msk&eacute;, p&aacute;nsk&eacute; a dětsk&eacute; oblečen&iacute;, boty a doplňky zn&aacute;m&yacute;ch i nov&yacute;ch značek.</p>\n', '<p>Na port&aacute;lu BIBLOO.cz nab&iacute;z&iacute;me d&aacute;msk&eacute;, p&aacute;nsk&eacute; a dětsk&eacute; oblečen&iacute;, boty a doplňky zn&aacute;m&yacute;ch i nov&yacute;ch značek. Ve&scaron;ker&eacute; zbož&iacute; m&aacute;me skladem a doručujeme už do druh&eacute;ho dne! Tě&scaron;&iacute; n&aacute;s, že se k n&aacute;m často vrac&iacute;te, m&aacute;me 99 % spokojen&yacute;ch z&aacute;kazn&iacute;ků podle hodnocen&iacute; Heureka.</p>\n', NULL, 5, 10, 1, 1, 'bibloo-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4634073, 4634073, NULL, 'Active', 'en', 'BIBLOO.sk', '', 'https://www.bibloo.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12401653-1468841232000', '1480255218.jpg', '<p>Na port&aacute;li BIBLOO.sk pon&uacute;kame d&aacute;mske, p&aacute;nske a detsk&eacute; oblečenie, top&aacute;nky a doplnky zn&aacute;mych i nov&yacute;ch značiek.</p>\n', '<p>Na port&aacute;li BIBLOO.sk pon&uacute;kame d&aacute;mske, p&aacute;nske a detsk&eacute; oblečenie, top&aacute;nky a doplnky zn&aacute;mych i nov&yacute;ch značiek. V&scaron;etok tovar m&aacute;me skladom a doručujeme už do druh&eacute;ho dňa! Te&scaron;&iacute; n&aacute;s, že sa k n&aacute;m často vraciate, m&aacute;me 98% spokojn&yacute;ch z&aacute;kazn&iacute;kov (hodnotenie Heureka.sk).</p>\n', NULL, 4, 8, 1, 1, 'bibloo-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'sk'),
(4636513, 4636513, NULL, 'Deactive', 'en', 'StyloveHodiny.cz', '', 'http://stylovehodiny.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12473167-1452682576000', '1480277916.jpg', '', '', NULL, 10, 20, 1, 1, 'stylovehodiny-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4639051, 4639051, NULL, 'Active', 'en', 'Bambino.sk', '', 'http://www.bambino.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12425697-1466082493000', '1480254192.png', '<p>Bambino.sk je rodinn&yacute; eshop, ktor&yacute; sa snaž&iacute; &iacute;sť neust&aacute;le bliž&scaron;ie k mamičk&aacute;m.</p>\n', '<p>Bambino.sk je rodinn&yacute; eshop, ktor&yacute; sa svoj&iacute;m pr&iacute;stupom a z&aacute;kazn&iacute;ckym servisom snaž&iacute; &iacute;sť neust&aacute;le bliž&scaron;ie k mamičk&aacute;m v Slovenskej republike. N&aacute;jdete u n&aacute;s v&scaron;etko od hračiek, oblečenia, top&aacute;nok až po v&yacute;bavu do izbičky, detsk&uacute; v&yacute;živu, či plienky.</p>\n', '', 5, 8, 1, 1, 'bambino-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-11 00:05:26', 'sk'),
(4643822, 4643822, NULL, 'Active', 'en', 'Freshlabels.cz', '', 'http://www.freshlabels.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12450317-1478510360000', '1480263144.gif', '<p>Na port&aacute;lu Freshlabels.cz najdete origin&aacute;ln&iacute; p&aacute;nskou a d&aacute;mskou m&oacute;du, boty,&nbsp;doplňky a designov&eacute; d&aacute;rky.&nbsp;</p>\n', '<p>Na port&aacute;lu Freshlabels.cz najdete origin&aacute;ln&iacute; p&aacute;nskou a d&aacute;mskou m&oacute;du, boty,&nbsp;doplňky a designov&eacute; d&aacute;rky od popul&aacute;rn&iacute;ch i neotřel&yacute;ch&nbsp;značek z cel&eacute;ho světa, jako jsou např. Komono,&nbsp;Nike, Herschel Supply, Adidas Originals,&nbsp;Carhartt, Vans, Happy Socks, Wemoto,&nbsp;Keepcup, Vagabond, New Balance, Ichi, Wemoto nebo WeSC.</p>\n', NULL, 7, 1, 0, 1, 'freshlabels-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4645120, 4645120, NULL, 'Active', 'en', 'LidskaSila.cz', '', 'https://lidskasila.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12469879-1452248013000', '1480276351.png', '', '', NULL, 5, 0, 1, 1, 'lidskasila-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4647453, 4647453, NULL, 'Active', 'en', 'KARE-shop.cz', '', 'http://www.kare-shop.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12431116-1467629488000', '1480276222.jpg', '', '', NULL, 6, 12, 1, 1, 'kare-shop-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4648080, 4648080, NULL, 'Active', 'en', 'Homein.cz', '', 'http://www.homein.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12436504-1466064657000', '1480263111.jpg', '<p>Poznejte půvab a eleganci bytov&yacute;ch doplňků na Homein.cz!</p>\n\n<p>&nbsp;</p>\n', '<p>M&aacute;me r&aacute;di design. Hled&aacute;te bytov&eacute; doplňky, kter&eacute; se stanou ozdobou Va&scaron;eho interi&eacute;ru? Doplňky do bytu nemus&iacute; b&yacute;t pouze praktick&eacute;. Poznejte půvab a eleganci bytov&yacute;ch doplňků na Homein.cz!</p>\n\n<p>&nbsp;</p>\n', NULL, 5, 10, 1, 1, 'homein-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-24 00:32:15', 'cz'),
(4648084, 4648084, NULL, 'Active', 'en', 'Bagin.cz', '', 'http://www.bagin.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12436549-1449148885000', '1480211797.jpg', '<p>Milujete nakupovan&iacute;? Na port&aacute;lu Bagin.cz najdete vysoce kvalitn&iacute; stylov&eacute; n&aacute;kupn&iacute; a cestovn&iacute; ta&scaron;ky, ekota&scaron;ky a kabelky Reisenthel a Envirosax!</p>\n', '<p>Milujete nakupovan&iacute;? Na port&aacute;lu Bagin.cz najdete vysoce kvalitn&iacute; stylov&eacute; n&aacute;kupn&iacute; a cestovn&iacute; ta&scaron;ky, ekota&scaron;ky a kabelky Reisenthel a Envirosax!</p>\n', NULL, 5, 10, 1, 1, 'bagin-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4650717, 4650717, NULL, 'Active', 'en', 'Florea.cz', '', 'http://www.florea.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12486179-1453881237000', '1480262926.jpg', '<p>Na port&aacute;lu Florea.cz zakoup&iacute;te velmi kvalitn&iacute; maxim&aacute;lně čerstv&eacute; květiny př&iacute;mo z holandsk&eacute; květinov&eacute; burzy či od pěstitelů.</p>\n', '<p>Na port&aacute;lu Florea.cz zakoup&iacute;te velmi kvalitn&iacute; maxim&aacute;lně čerstv&eacute; květiny př&iacute;mo z holandsk&eacute; květinov&eacute; burzy či od pěstitelů. Pohodlně objednejte online a my v&aacute;m květiny dovezeme na&scaron;imi chlazen&yacute;mi vozy kamkoliv po cel&eacute; ČR. Vynechali jsme n&aacute;kupč&iacute;, velkoobchod i květin&aacute;řstv&iacute;, proto můžeme nab&iacute;zet ty nejlep&scaron;&iacute; ceny za kvalitn&iacute; květiny.</p>\n', NULL, 7.5, 15, 1, 1, 'florea-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 23:31:07', 'cz'),
(4687529, 4687529, NULL, 'Active', 'en', 'OKAY.cz', '', 'http://www.okay.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12626852-1470994281000', '1480276726.png', '<p>V e-shopu&nbsp;OKAY&nbsp;nakoup&iacute;te elektroniku, b&iacute;l&eacute; zbož&iacute;, autopř&iacute;slu&scaron;enstv&iacute; a n&aacute;bytek. Osobn&iacute; odběr v největ&scaron;&iacute; s&iacute;ti poboček.</p>\n', '<p>V e-shopu&nbsp;OKAY&nbsp;nakoup&iacute;te elektroniku, b&iacute;l&eacute; zbož&iacute;, autopř&iacute;slu&scaron;enstv&iacute; a n&aacute;bytek. Osobn&iacute; odběr v největ&scaron;&iacute; s&iacute;ti poboček.&nbsp;</p>\n\n<p>&nbsp;</p>\n', '', 3, 6, 0, 1, 'okay-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4687530, 4687530, NULL, 'Active', 'en', 'OKAY.sk', '', 'http://www.okay.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12628301-1470994304000', '1480276753.png', '<p>V e-shopu&nbsp;OKAY&nbsp;nakoup&iacute;te elektroniku, b&iacute;l&eacute; zbož&iacute;, autopř&iacute;slu&scaron;enstv&iacute; a n&aacute;bytek. Osobn&iacute; odběr v největ&scaron;&iacute; s&iacute;ti poboček.</p>\n', '<p>V e-shopu&nbsp;OKAY&nbsp;nakoup&iacute;te elektroniku, b&iacute;l&eacute; zbož&iacute;, autopř&iacute;slu&scaron;enstv&iacute; a n&aacute;bytek. Osobn&iacute; odběr v největ&scaron;&iacute; s&iacute;ti poboček.</p>\n', '', 3, 6, 0, 1, 'okay-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4687536, 4687536, NULL, 'Active', 'en', 'JENA-nabytek.cz', '', 'http://www.jena-nabytek.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12630248-1485360805000', '1480276194.png', '<p>JENA-nabytek.cz je předn&iacute;m česk&yacute;m prodejcem kompletn&iacute;ho sortimentu n&aacute;bytku a bytov&yacute;ch doplňků.</p>\n', '<p>Port&aacute;l JENA-nabytek.cz je již od roku 1999 předn&iacute;m česk&yacute;m prodejcem kompletn&iacute;ho sortimentu n&aacute;bytku a bytov&yacute;ch doplňků. M&aacute;me nejlep&scaron;&iacute; ceny a zbož&iacute; dod&aacute;me rychleji než ostatn&iacute;, drtiv&aacute; vět&scaron;ina je připravena k okamžit&eacute;mu odesl&aacute;n&iacute; v na&scaron;ich skladech.</p>\n', NULL, 3, 6, 1, 1, 'jena-nabytek-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:34:54', 'cz'),
(4689086, 4689086, NULL, 'Active', 'en', 'Eyerim.cz', '', 'https://www.eyerim.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12504744-1487067437000', '1480262796.jpg', '<p>Na port&aacute;lu EYERIM.cz najdete tis&iacute;ce autentick&yacute;ch modelů slunečn&iacute;ch, dioptrick&yacute;ch a lyžařsk&yacute;ch br&yacute;l&iacute;.</p>\n', '<p>Na port&aacute;lu EYERIM.cz najdete tis&iacute;ce autentick&yacute;ch modelů slunečn&iacute;ch, dioptrick&yacute;ch a lyžařsk&yacute;ch br&yacute;l&iacute;.</p>\n', NULL, 4, 8, 1, 1, 'eyerim-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 23:35:41', 'cz'),
(4691071, 4691071, NULL, 'Active', 'en', 'Differenta.sk', '', 'https://www.differenta.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12521975-1457094233000', '1480258513.png', '<p>Na port&aacute;lu Different.cz najdete d&aacute;msk&eacute;, p&aacute;nsk&eacute; i dětsk&eacute; oblečen&iacute; a doplňky popul&aacute;rn&iacute;ch světov&yacute;ch značek.</p>\n', '<p>Na port&aacute;lu Different.cz najdete d&aacute;msk&eacute;, p&aacute;nsk&eacute; i dětsk&eacute; oblečen&iacute; a doplňky popul&aacute;rn&iacute;ch světov&yacute;ch značek, kter&eacute; pr&aacute;vě let&iacute;, jako Desigual, Crocs, Disaster, Melissa, EMU a dal&scaron;&iacute;!</p>\n', NULL, 5, 11, 1, 1, 'differenta-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4694431, 4694431, NULL, 'Active', 'en', 'GrilyKrby.cz', '', 'http://www.grilykrby.cz', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12522012-1457700101000', '1480263014.png', '<p>Na port&aacute;lu GrilyKrby.cz najdete kompletn&iacute; nab&iacute;dku zahradn&iacute;ch, plynov&yacute;ch a elektrick&yacute;ch grilů značky Weber, včetně ve&scaron;ker&eacute;ho př&iacute;slu&scaron;enstv&iacute;.</p>\n', '<p>Na port&aacute;lu GrilyKrby.cz najdete kompletn&iacute; nab&iacute;dku zahradn&iacute;ch, plynov&yacute;ch a elektrick&yacute;ch grilů značky Weber, včetně ve&scaron;ker&eacute;ho př&iacute;slu&scaron;enstv&iacute;, od grilovac&iacute;ho n&aacute;řad&iacute; po dřevěn&eacute; uhl&iacute;, brikety a čist&iacute;c&iacute; prostředky určen&eacute; pro grily.</p>\n\n<p>&nbsp;</p>\n', NULL, 4, 8, 1, 1, 'grilykrby-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-23 23:29:41', 'cz'),
(4694500, 4694500, NULL, 'Active', 'en', 'WATERSAVERS.eu', '', 'http://www.watersavers.eu/e-shop/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12515238-1458549344000', '1480278187.jpg', '', '', NULL, 10, 20, 1, 1, 'watersavers-eu', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4697950, 4697950, NULL, 'Active', 'en', 'AXA ASSISTANCE - CZ', '', 'https://www.axa-assistance.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12585181-1473927833000', '1480254151.jpg', '<p>AXA ASSISTANCE je souč&aacute;st&iacute; jedn&eacute; z největ&scaron;&iacute;ch poji&scaron;ťovac&iacute;ch skupin na světě.</p>\n', '<p>AXA ASSISTANCE je souč&aacute;st&iacute; jedn&eacute; z největ&scaron;&iacute;ch poji&scaron;ťovac&iacute;ch skupin na světě. Na&scaron;im klientům nab&iacute;z&iacute;me poji&scaron;těn&iacute; l&eacute;čebn&yacute;ch v&yacute;loh, asistenčn&iacute; služby, &uacute;razov&eacute; poji&scaron;těn&iacute;, poji&scaron;těn&iacute; odpovědnosti za &scaron;kodu, poji&scaron;těn&iacute; zavazadel a poji&scaron;těn&iacute; cestov&aacute;n&iacute; letadlem.</p>\n', '', 10, 15, 1, 1, 'axa-assistance-cz', 1, NULL, 0, 1, 'cj', 1, '2017-04-11 23:32:50', 'cz'),
(4710182, 4710182, NULL, 'Active', 'en', 'Muziker.com', '', 'http://www.muziker.com', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12545670-1459413171000', '1480276666.png', '', '', NULL, 1, 3, 1, 1, 'muziker-com', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4711521, 4711521, NULL, 'Active', 'en', 'ČESKÁ POJIŠŤOVNA.cz', '', 'http://www.ceskapojistovna.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12555862-1487239891000', '1480280636.jpg', '<p>Největ&scaron;&iacute; poji&scaron;ťovna na česk&eacute;m trhu s v&iacute;ce než 185letou tradic&iacute;.</p>\n', '<p>Největ&scaron;&iacute; poji&scaron;ťovna na česk&eacute;m trhu s v&iacute;ce než 185letou tradic&iacute; poskytov&aacute;n&iacute; životn&iacute;ho i neživotn&iacute;ho poji&scaron;těn&iacute;.&nbsp; V současn&eacute; době spravuje t&eacute;měř osm milionů pojistn&yacute;ch smluv.</p>\n', '', 5, 10, 1, 1, 'ceska-pojisťovna-cz', 1, NULL, 0, 1, 'cj', 1, '2017-03-05 23:25:26', 'cz'),
(4711524, 4711524, NULL, 'Active', 'en', 'Generali - CZ', '', 'http://www.generali.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12534931-1487153361000', '1480263035.jpg', '<p>Poji&scaron;ťovna Generali patř&iacute; mezi největ&scaron;&iacute; česk&eacute; poji&scaron;ťovny.</p>\n', '<p>Poji&scaron;ťovna Generali patř&iacute; mezi největ&scaron;&iacute; česk&eacute; poji&scaron;ťovny. Individu&aacute;ln&iacute; př&iacute;stup, kvalitn&iacute; pojistn&eacute; produkty životn&iacute;ho i neživotn&iacute;ho poji&scaron;těn&iacute; připraven&eacute; na z&aacute;kladě potřeb klientů a &scaron;pičkov&yacute; servis jsou pro poji&scaron;ťovnu Generali samozřejmost&iacute;.</p>\n\n<p>&nbsp;</p>\n', '', 6, 10, 1, 1, 'generali-cz', 1, NULL, 0, 1, 'cj', 1, '2017-04-12 21:36:41', 'cz'),
(4713734, 4713734, NULL, 'Deactive', 'en', 'iErect.cz', '', 'http://ierect.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12565565-1461843754000', '1480276081.jpg', '', '', NULL, 12, 25, 1, 1, 'ierect-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4727030, 4727030, NULL, 'Active', 'cz', 'BRASTY.CZ ', '', 'https://www.brasty.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12594958-1464183397000', '1480255845.png', '<p>Brasty.cz nab&iacute;z&iacute; velk&yacute; v&yacute;běr origin&aacute;ln&iacute;ch a vysoce kvalitn&iacute;ch d&aacute;msk&yacute;ch a pansk&yacute;ch hodinek, parf&eacute;mů a kosmetiky.</p>\n', '<p>Brasty.cz nab&iacute;z&iacute; velk&yacute; v&yacute;běr origin&aacute;ln&iacute;ch a vysoce kvalitn&iacute;ch d&aacute;msk&yacute;ch a pansk&yacute;ch hodinek, parf&eacute;mů a kosmetiky TOP značek za v&yacute;hodn&eacute; ceny s&nbsp;v&yacute;born&yacute;m servisem. Z&nbsp;na&scaron;&iacute; nab&iacute;dky, ve kter&eacute; je přes 6000 produktů si určitě vyberete! T&eacute;měř každ&yacute; si u n&aacute;s přijde na sv&eacute;, zcela jedno, jestli se jedn&aacute; o m&oacute;dn&iacute;, klasick&eacute; nebo sportovn&iacute; zbož&iacute; nebo zda si chce s&aacute;m sobě udělat radost nebo hled&aacute; odpov&iacute;daj&iacute;c&iacute; d&aacute;rek k&nbsp;určit&eacute; př&iacute;ležitosti.</p>\n', NULL, 5.5, 11, 0, 1, 'brasty-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 22:35:21', 'cz'),
(4731601, 4731601, NULL, 'Active', 'en', 'CK České kormidlo - CZ', 'České kormidlo', 'http://www.ceskekormidlo.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12598961-1476979967000', '1480258374.png', '<p>Vyberte si dovolenou snů na CK Česk&eacute; kormidlo.</p>\n', '<p>CK Česk&eacute; kormidlo se specializuje na pobytov&eacute; z&aacute;jezdy. Je autorizovan&yacute;m prodejcem z&aacute;jezdů stovky renomovan&yacute;ch a poji&scaron;těn&yacute;ch cestovn&iacute;ch kancel&aacute;ř&iacute; působ&iacute;c&iacute;ch na česk&eacute;m a slovensk&eacute;m trhu.</p>\n', NULL, 2, 4, 1, 1, 'ck-ceske-kormidlo-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 20:34:13', 'cz'),
(4731700, 4731700, NULL, 'Active', 'en', 'VelkyKosik.cz', '', 'http://www.velkykosik.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12622838-1466767976000', '1480278121.gif', '<p>Velk&yacute;Ko&scaron;&iacute;k.cz nab&iacute;z&iacute; velk&yacute; v&yacute;běr zbož&iacute; v kategorii auto-moto, elektro, hudba, pro děti, bydlen&iacute; a dom&aacute;cnost, erotika, knihy, tělo a zdrav&iacute;, d&aacute;rky, film, m&oacute;da a doplňky a voln&yacute; čas a hobby.</p>\n', '<p>Port&aacute;l Velk&yacute;Ko&scaron;&iacute;k.cz m&aacute; v&iacute;ce než 25 let zku&scaron;enost&iacute; a již přes 400 v&yacute;dejn&iacute;ch m&iacute;st po cel&eacute; ČR! Nab&iacute;z&iacute; velk&yacute; v&yacute;běr zbož&iacute; v kategorii auto-moto, elektro, hudba, pro děti, bydlen&iacute; a dom&aacute;cnost, erotika, knihy, tělo a zdrav&iacute;, d&aacute;rky, film, m&oacute;da a doplňky a voln&yacute; čas a hobby.</p>\n', NULL, 6, 10, 1, 1, 'velkykosik-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:06:46', 'cz'),
(4733564, 4733564, NULL, 'Active', 'en', 'I-Moda.cz', '', 'https://www.i-moda.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12622986-1466775942000', '1480263240.png', '<p>I-Moda.cz - d&aacute;msk&aacute; a p&aacute;nsk&aacute; m&oacute;da v &scaron;irok&eacute; &scaron;k&aacute;le velikost&iacute;, barev i střihů, d&aacute;le &scaron;perky, hodinky a dal&scaron;&iacute; m&oacute;dn&iacute; doplňky.</p>\n', '<p>Hled&aacute;te pěkn&eacute; a modern&iacute; oblečen&iacute; pro běžn&eacute; no&scaron;en&iacute; či pro speci&aacute;ln&iacute; př&iacute;ležitost? Na port&aacute;lu I-Moda.cz si jistě vyberete. Nab&iacute;z&iacute; d&aacute;mskou a p&aacute;nskou m&oacute;du v &scaron;irok&eacute; &scaron;k&aacute;le velikost&iacute;, barev i střihů, d&aacute;le &scaron;perky, hodinky a dal&scaron;&iacute; m&oacute;dn&iacute; doplňky.</p>\n', '', 6, 11, 1, 1, 'i-moda-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-13 00:02:27', 'cz'),
(4735530, 4735530, NULL, 'Deactive', 'en', 'Maine Lobster Now', '', 'https://www.mainelobsternow.com/', 'notjoined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12706424-1475871687000', '', '', '', NULL, 0, 10, 1, 1, 'maine-lobster-now', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4736194, 4736194, NULL, 'Active', 'en', 'Rozbalene.sk', '', 'http://www.rozbalene.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12596443-1473062816000', '1480277175.png', '<p>Na Rozbalene.sk si m&ocirc;žete vybrať z veľmi &scaron;irok&eacute;ho sortimentu tovaru. &Scaron;pecializujeme sa predov&scaron;etk&yacute;m na predaj rozbalen&eacute;ho tovaru a tovar z nadbytočn&yacute;ch z&aacute;sob ofici&aacute;lnych distrib&uacute;torov.</p>\n', '<p>Na Rozbalene.sk si m&ocirc;žete vybrať z veľmi &scaron;irok&eacute;ho sortimentu tovaru, ako s&uacute; dom&aacute;ce spotrebiče, elektronika, mobiln&eacute; telef&oacute;ny, PC, audio technika, ale aj oblečenie svetov&yacute;ch značiek, produkty pre deti, vybavenie pre turistiku, camping, a tiež v&scaron;etko potrebn&eacute; pre dom&aacute;cnosť a automobily. &Scaron;pecializujeme sa predov&scaron;etk&yacute;m na predaj rozbalen&eacute;ho tovaru a tovar z nadbytočn&yacute;ch z&aacute;sob ofici&aacute;lnych distrib&uacute;torov.</p>\n', NULL, 3, 5, 1, 1, 'rozbalene-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 00:10:21', 'sk'),
(4738067, 4738067, NULL, 'Active', 'en', 'Lentiamo', '', 'https://www.lentiamo.co.uk/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12656043-1475483205000', '', '', '', NULL, 0, 11, 1, 1, 'lentiamo', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4738082, 4738082, NULL, 'Active', 'en', 'SoftCotton.cz', '', 'http://www.softcotton.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12583183-1486732419000', '1480277792.jpg', '<p>SOFT COTTON vyr&aacute;b&iacute; frot&eacute; ručn&iacute;ky a župany z nejkvalitněj&scaron;&iacute; Egejsk&eacute; bavlny.</p>\n', '<p>SOFT COTTON je registrovan&aacute; ochrann&aacute; zn&aacute;mka Francouzsk&eacute; společnosti, kter&aacute; ve sv&eacute; tov&aacute;rně od roku 1986 vyr&aacute;b&iacute; frot&eacute; ručn&iacute;ky a župany z nejkvalitněj&scaron;&iacute; Egejsk&eacute; bavlny, kter&aacute; se vyznačuje vět&scaron;&iacute; savost&iacute;, měkkost&iacute;, přirozen&yacute;m leskem a del&scaron;&iacute; trvanlivost&iacute; d&iacute;ky dlouh&eacute;mu vl&aacute;knu.</p>\n', NULL, 5, 8, 1, 1, 'softcotton-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:19:04', 'cz'),
(4741913, 4741913, NULL, 'Deactive', 'en', 'Passiana', '', 'http://www.passiana.com', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12622619-1470165998000', '', '', '', NULL, 0, 15, 1, 1, 'passiana', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4742199, 4742199, NULL, 'Active', 'en', 'Fonetip.cz', '', 'http://www.fonetip.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12606488-1465309391000', '1480262936.png', '<p>Fonetip.cz nab&iacute;z&iacute; satelitn&iacute; techniku, autodoplňky, mobiln&iacute; telefony a tablety, PC techniku, digit&aacute;ln&iacute; fotoapar&aacute;ty, elektrotechniku, potřeby pro dům a zahradu, dětsk&eacute; zbož&iacute; a chovatelsk&eacute; potřeby.</p>\n', '<p>Na port&aacute;lu Fonetip.cz nab&iacute;z&iacute;me satelitn&iacute; techniku, autodoplňky, mobiln&iacute; telefony a tablety, PC techniku, digit&aacute;ln&iacute; fotoapar&aacute;ty, elektrotechniku, potřeby pro dům a zahradu, dětsk&eacute; zbož&iacute; a chovatelsk&eacute; potřeby. M&aacute;me přes 1 130&nbsp;000&nbsp;ks zbož&iacute; na vlastn&iacute;m skladě. Obsloužili jsme již v&iacute;ce než 100&nbsp;000 spokojen&yacute;ch z&aacute;kazn&iacute;ků.</p>\n', '', 2, 4, 1, 1, 'fonetip-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-12 21:30:38', 'cz'),
(4742200, 4742200, NULL, 'Active', 'en', 'MrakyHracek.cz', '', 'http://www.mrakyhracek.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12606503-1479728737000', '1480276653.png', '<p>V internetov&eacute;m hračk&aacute;řstv&iacute; MrakyHraček.cz najdete kvalitn&iacute; certifikovan&eacute; hračky za v&yacute;hodn&eacute; ceny</p>\n', '<p>V internetov&eacute;m hračk&aacute;řstv&iacute; MrakyHraček.cz najdete kvalitn&iacute; certifikovan&eacute; hračky za v&yacute;hodn&eacute; ceny, nejen zn&aacute;m&yacute;ch značek Bruder, Lego, Smooby, Woody aj., ale i hračky, kter&eacute; děti znaj&iacute; z televize. Ve&scaron;ker&eacute; v&yacute;robky jsou rozděleny do kategori&iacute;, kter&eacute; vystihuj&iacute; určit&eacute; z&aacute;liby a kon&iacute;čky dět&iacute;. Pohodlně si tak vyberete tu pravou dřevěnou, plastovou nebo i plechovou hračku pro chlapce nebo d&iacute;vku.</p>\n', NULL, 4, 4, 0, 1, 'mrakyhracek-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4745707, 4745707, NULL, 'Active', 'en', 'DMXgear.cz', '', 'http://dmxgear.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12633746-1467895404000', '1480258721.png', '<p>Jsme DMXGEAR.CZ, největ&scaron;&iacute; n&aacute;kupn&iacute; galerie s luxusn&iacute;m p&aacute;nsk&yacute;m spodn&iacute;m pr&aacute;dlem.</p>\n', '<p>Jsme DMXGEAR.CZ, největ&scaron;&iacute; n&aacute;kupn&iacute; galerie s luxusn&iacute;m p&aacute;nsk&yacute;m spodn&iacute;m pr&aacute;dlem. Najdete u n&aacute;s p&aacute;nsk&eacute; slipy, p&aacute;nsk&aacute; tanga, boxerky, tren&yacute;rky, jockstrapy/jocksy, string tanga. Nab&iacute;z&iacute;me i &scaron;irokou nab&iacute;dku triček, kraťasů a elastick&yacute;ch kalhot, kr&aacute;tk&yacute;ch elasť&aacute;ků, vysok&yacute;ch ponožek a dal&scaron;&iacute;ho sportovn&iacute;ho oblečen&iacute;. Ve&scaron;ker&eacute; zbož&iacute; je origin&aacute;ln&iacute;, zas&iacute;lan&eacute; př&iacute;mo od v&yacute;robců. C&iacute;lem DMXGEAR.CZ je nab&iacute;zet kvalitn&iacute; spodn&iacute; pr&aacute;dlo a oblečen&iacute; pro v&scaron;echny, kteř&iacute; si potrp&iacute; na kvalitě, materi&aacute;lech a neotřel&eacute;m stylu. Hlavn&iacute;m sloganem na&scaron;eho e-shopu je OBLEČ SE DO NĚČEHO POŘ&Aacute;DN&Eacute;HO!</p>\n', NULL, 6, 12, 1, 1, 'dmxgear-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 23:03:56', 'cz'),
(4747013, 4747013, NULL, 'Active', 'en', 'ActiveCzech.com', '', 'http://www.activeczech.com/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12602822-1464858388000', '1480210413.jpg', '<p>Port&aacute;l ActiveCzech.com nab&iacute;z&iacute; vzru&scaron;uj&iacute;c&iacute; z&aacute;žitky v Česk&eacute; republice.</p>\n', '<p>Port&aacute;l ActiveCzech.com nab&iacute;z&iacute; vzru&scaron;uj&iacute;c&iacute; z&aacute;žitky v Česk&eacute; republice, jako jsou vzdu&scaron;n&eacute; a vodn&iacute; sporty, střelba, z&aacute;vodn&iacute; okruhy a simul&aacute;tory, Segway, okružn&iacute; plavby a vyhl&iacute;dkov&eacute; lety, ale i gastronomick&eacute; z&aacute;žitky, spa a mas&aacute;že.</p>\n', NULL, 7.5, 15, 1, 1, 'activeczech-com', 1, NULL, 0, 0, 'cj', 1, '2017-02-23 23:25:28', NULL),
(4754392, 4754392, NULL, 'Active', 'en', 'Craftholic.cz', '', 'http://craftholic.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12622770-1466757709000', '1480258395.png', '<p>Craftholic jsou neodolateln&eacute; ply&scaron;ov&eacute; hračky i origin&aacute;ln&iacute; designov&yacute; doplněk do interi&eacute;ru.</p>\n', '<p>Craftholic jsou neodolateln&eacute; ply&scaron;ov&eacute; hračky i origin&aacute;ln&iacute; designov&yacute; doplněk do interi&eacute;ru. Čtyři hrdinov&eacute; s různ&yacute;mi charaktery - vesel&yacute; RAB, zaduman&yacute; SLOTH, &scaron;ibalsk&yacute; KORAT, mlčenliv&aacute; LORIS - jsou na česk&eacute;m trhu dostupn&iacute; ve velikosti S, M, L a Jr. (Junior). Pravidelně vych&aacute;z&iacute; nov&eacute; kolekce, kdy každ&aacute; m&aacute; svůj styl a př&iacute;běh, jedno v&scaron;ak maj&iacute; společn&eacute; - nikdy nejsou nudn&eacute;!</p>\n', NULL, 5, 10, 1, 1, 'craftholic-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-23 23:42:23', 'cz'),
(4754401, 4754401, NULL, 'Active', 'en', 'KIMGROUP.cz', '', 'https://www.kimgroup.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12634790-1487340743000', '1479371356.jpg', '<p>Port&aacute;l KIMgroup.cz nab&iacute;z&iacute; mobiln&iacute; telefony, tablety, tisk&aacute;rny, televize, outdoorov&eacute; kamery, chytr&eacute; hodinky, wheelboardy, sv&iacute;tilny a dal&scaron;&iacute; technick&eacute; vychyt&aacute;vky, včetně velk&eacute;ho v&yacute;běru př&iacute;slu&scaron;enstv&iacute;.</p>\n', '<p>Port&aacute;l KIMgroup.cz nab&iacute;z&iacute; mobiln&iacute; telefony, tablety, tisk&aacute;rny, televize, outdoorov&eacute; kamery, chytr&eacute; hodinky, wheelboardy, sv&iacute;tilny a dal&scaron;&iacute; technick&eacute; vychyt&aacute;vky, včetně velk&eacute;ho v&yacute;běru př&iacute;slu&scaron;enstv&iacute;. Od roku 2015 je společnost KIM Group s.r.o. tak&eacute; v&yacute;hradn&iacute;m distributorem v&yacute;robce mobiln&iacute;ch telefonů - značky i-mobile.</p>\n', NULL, 5, 10, 1, 1, 'kimgroup-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-02 19:30:31', 'cz');
INSERT INTO `advertiser` (`id`, `advertiser_id`, `parent_id`, `account_status`, `language`, `advertiser_name`, `advertiser_alias`, `program_url`, `relationship_status`, `mobile_supported`, `mobile_tracking_certified`, `commission_link`, `logo`, `desc_short`, `desc_full`, `desc_extra`, `cashback_customer`, `cashback_value`, `cashback_const`, `cashback_percent`, `url`, `active`, `category_id`, `is_favourite`, `show_in_menu`, `source`, `browser_panel`, `first_published`, `site_domain`) VALUES
(4754411, 4754411, NULL, 'Active', 'en', 'FOLLY.cz', '', 'http://www.folly.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12646180-1481723645000', '1480262952.png', '<p>Folly.cz - doplňky stravy, bio kosmetika</p>\n', '<p>Folly.cz si klade za c&iacute;l b&yacute;t mezi ostatn&iacute;mi česk&yacute;mi eshopy v&yacute;jimečn&yacute; a předev&scaron;&iacute;m, tě&scaron;it sv&eacute; z&aacute;kazn&iacute;ky. Najdete zde doplňky stravy pro zdrav&yacute; život a &scaron;t&iacute;hlou linii, a velk&yacute; v&yacute;běr př&iacute;rodn&iacute; a bio kosmetiky pro ženy, muže i děti.</p>\n\n<p>&nbsp;</p>\n', '', 6, 10, 1, 1, 'folly-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-12 21:33:24', 'cz'),
(4754417, 4754417, NULL, 'Active', 'en', 'inSPORTline.cz', '', 'https://www.insportline.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12613630-1483459648000', '1480276118.jpg', '<p>Port&aacute;l inSPORTline.cz je největ&scaron;&iacute;m v&yacute;robcem a prodejcem fitness v ČR.&nbsp;</p>\n', '<p>Port&aacute;l inSPORTline.cz je největ&scaron;&iacute;m v&yacute;robcem a prodejcem fitness v ČR. Zakoup&iacute;te zde trenaž&eacute;ry, posilovac&iacute; stroje a pomůcky, vybaven&iacute; na letn&iacute; i zimn&iacute; sporty, cyklistiku, outdoor a kempov&aacute;n&iacute;, vod&aacute;ck&eacute; potřeby, venkovn&iacute; hračky a trampol&iacute;ny, moto potřeby a velk&yacute; v&yacute;běr sportovn&iacute;ho oblečen&iacute; pro muže, ženy i děti</p>\n', NULL, 2, 5, 1, 1, 'insportline-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4754441, 4754441, NULL, 'Active', 'en', 'BELABEL.cz', '', 'http://www.belabel.cz/', 'notjoined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12618791-1466513154000', '1480254069.png', '<p>Na port&aacute;lu Belabel.cz si vytvoř&iacute;te vlastn&iacute; tričko nebo d&aacute;rek.</p>\n', '<p>Na port&aacute;lu Belabel.cz si vytvoř&iacute;te vlastn&iacute; tričko nebo d&aacute;rek, jako placku, magnet, hrnek, pol&scaron;t&aacute;ř, ply&scaron;&aacute;ka, k&scaron;iltovku nebo vak na z&aacute;da. Na v&yacute;běr m&aacute;te množstv&iacute; barev a v&scaron;echny v&yacute;robky si můžete ozdobit libovoln&yacute;m obr&aacute;zkem, vlastn&iacute; fotkou nebo textem. &nbsp;</p>\n\n<p>&nbsp;</p>\n', NULL, 7, 15, 1, 1, 'belabel-cz', 0, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4754451, 4754451, NULL, 'Active', 'en', 'BEZPOTISKU.cz', '', 'http://www.bezpotisku.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12620274-1479826106000', '1480255169.png', '<p>Port&aacute;l Bezpotisku.cz přin&aacute;&scaron;&iacute; oblečen&iacute; a m&oacute;du bez potisku za rozumn&eacute; ceny.</p>\n', '<p>Port&aacute;l Bezpotisku.cz přin&aacute;&scaron;&iacute; oblečen&iacute; a m&oacute;du bez potisku za rozumn&eacute; ceny. Najdete zde kvalitn&iacute; oblečen&iacute; pro muže, ženy i děti, u kter&eacute;ho nemus&iacute;te platit za drahou značku. Nab&iacute;z&iacute;me tak&eacute; osobn&iacute; odběr zdarma, s možnost&iacute; v&scaron;e vyzkou&scaron;et. Na v&yacute;běr m&aacute;te &scaron;irokou &scaron;k&aacute;lu barev a velikost&iacute; triček, t&iacute;lek, ko&scaron;il, mikin i doplňků.</p>\n', '', 3, 5, 1, 1, 'bezpotisku-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4754455, 4754455, NULL, 'Active', 'en', 'BEZPOTLACE.sk', '', 'http://www.bezpotlace.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12621445-1479311980000', '1480255157.png', '<p>Port&aacute;l Bezpotlace.sk prin&aacute;&scaron;a oblečenie a m&oacute;du bez potlače za rozumn&eacute; ceny.</p>\n', '<p>Port&aacute;l Bezpotlace.sk prin&aacute;&scaron;a oblečenie a m&oacute;du bez potlače za rozumn&eacute; ceny. N&aacute;jdete tu kvalitn&eacute; oblečenie pre mužov, ženy aj deti, u ktor&eacute;ho nemus&iacute;te platiť za drah&uacute; značku. Pon&uacute;kame tiež osobn&eacute; odber zadarmo, s možnosťou v&scaron;etko vysk&uacute;&scaron;ania. Na v&yacute;ber m&aacute;te &scaron;irok&uacute; &scaron;k&aacute;lu farieb a veľkost&iacute; tričiek, tielok, ko&scaron;ieľ, mik&iacute;n aj doplnkov.</p>\n', NULL, 5, 10, 1, 1, 'bezpotlace-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'sk'),
(4754462, 4754462, NULL, 'Active', 'en', 'Cedr.cz', '', 'https://www.cedr.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12604227-1464957271000', '1489012083.png', '<p>Cedr.cz nab&iacute;z&iacute; kr&aacute;sn&eacute; bytov&eacute; doplňky a dekorace, drobn&yacute; n&aacute;bytek, kuchyňsk&eacute; potřeby a d&aacute;rkov&eacute; předměty.</p>\n', '<p>E-shop Cedr.cz nab&iacute;z&iacute; kr&aacute;sn&eacute; bytov&eacute; doplňky a dekorace, drobn&yacute; n&aacute;bytek, kuchyňsk&eacute; potřeby a d&aacute;rkov&eacute; předměty designov&yacute;ch značek jako je ANDREA HOUSE, BASTILON COLLECTIONS, EMERALD, Jaliang, KERSTEN, L&eacute;ku&eacute;, MARIEKE, M&eacute;tro Interieur, MOOD, MY drap, NICI, SANTORO, Trudi + Sevi, TURNOWSKY a ZAK! designs.</p>\n', NULL, 8, 13, 1, 1, 'cedr-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:28:03', 'cz'),
(4754464, 4754464, NULL, 'Active', 'en', 'Dovolenka.sme.sk', '', 'http://dovolenka.sme.sk/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12648659-1469614086000', '1480275900.png', '', '', NULL, 1, 1, 1, 1, 'dovolenka-sme-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4754470, 4754470, NULL, 'Active', 'en', 'Sizeer.cz', '', 'http://sizeer.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12637679-1485437739000', '1480277619.png', '<p>Sizeer - obuv a oblečen&iacute; z kategorie streetwear.</p>\n', '<p>Sizeer je evropskou s&iacute;t&iacute; v&iacute;ce než 90 multibrandov&yacute;ch prodejen s obuv&iacute; a oblečen&iacute;m z kategorie streetwear. V e-shopu http://sizeer.cz najdete t&eacute;měř v&scaron;e, co zrovna nos&iacute; evropsk&eacute; ulice - Nike Sportswear, Lacoste, adidas Originals, Converse, New Balance, Vans, New Era, Reebok, Puma, Timberland, Birkenstock, Beats &ndash; ikony, limitovan&eacute; kolekce i novinky. 100 % originality!</p>\n', NULL, 5, 10, 1, 1, 'sizeer-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 23:14:50', 'cz'),
(4754475, 4754475, NULL, 'Active', 'en', 'Sizeer.sk', '', 'http://sizeer.sk/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12637957-1486125796000', '1480277631.png', '<p>Sizeer - obuv a oblečen&iacute; z kateg&oacute;rie streetwear.</p>\n', '<p>Sizeer je eur&oacute;pskou sieťou viac ako 90 multibrandov&yacute;ch predajn&iacute; s obuvou a oblečen&iacute;m z kateg&oacute;rie streetwear. V e-shope http://sizeer.sk n&aacute;jdete takmer v&scaron;etko, čo pr&aacute;ve nos&iacute; eur&oacute;pskej ulice - Nike Sportswear, Lacoste, adidas Originals, Converse, New Balance, Vans, New Era, Reebok, Puma, Timberland, Birkenstock, Beats - ikony, limitovanej kolekcie aj novinky. 100% originality!</p>\n', NULL, 5, 10, 1, 1, 'sizeer-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 23:15:46', 'sk'),
(4757094, 4757094, NULL, 'Deactive', 'en', 'Zásilkonoš.cz', '', 'http://www.zasilkonos.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12610212-1478271122000', '1479379133.jpg', '', '', NULL, 15, 30, 1, 1, 'zasilkonos-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4757122, 4757122, NULL, 'Active', 'en', 'Lottoland.com', '', 'https://www.lottoland.com/cs', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12654708-1480673691000', '', '', '', NULL, 0, 0, 1, 1, 'lottoland-com', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4758292, 4758292, NULL, 'Active', 'en', 'Bonami.cz', '', 'https://www.bonami.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12607658-1484313023000', '1483560380.PNG', '<p>Bonami.cz - každ&yacute; den nov&aacute; inspirace pro va&scaron;e bydlen&iacute;.</p>\n', '<p>Nakupujte origin&aacute;ln&iacute; n&aacute;bytek a jedinečn&eacute; bytov&eacute; doplňky. Port&aacute;l Bonami představuje každ&yacute; den origin&aacute;ln&iacute; kolekce design&eacute;rů a značek.</p>\n', NULL, 3, 5, 0, 1, 'bonami-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4758296, 4758296, NULL, 'Active', 'en', 'SUPERZOO.cz', '', 'http://www.superzoo.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12607708-1487323042000', '1484826844.jpg', '', '', NULL, 4, 9, 1, 1, 'superzoo-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4765932, 4765932, NULL, 'Active', 'en', 'Bonprix SK', '', 'http://www.bonprixshop.eu/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12647550-1479460000000', '1480256825.jpg', '', '', NULL, 4, 9, 1, 1, 'bonprix-sk', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4766423, 4766423, NULL, 'Active', 'en', 'Kiwi.com', '', 'http://kiwi.com/us/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12624156-1486481789000', '1480276409.png', '<p>Kiwi.com (b&yacute;val&yacute; Skypicker) umožňuje cestovatelům naj&iacute;t a zabookovat nejlevněj&scaron;&iacute; možn&eacute; lety.</p>\n', '<p>Kiwi.com (b&yacute;val&yacute; Skypicker) umožňuje cestovatelům naj&iacute;t a zabookovat nejlevněj&scaron;&iacute; možn&eacute; lety.</p>\n', '', 1.5, 3, 1, 1, 'kiwi-com', 1, NULL, 0, 1, 'cj', 1, '2017-02-27 23:38:43', NULL),
(4769945, 4769945, NULL, 'Active', 'en', 'ProdejParfemu.cz', '', 'http://www.prodejparfemu.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12645014-1483008611000', '14677808.png', '<p>Parf&eacute;my, kosmetika, vlasov&aacute; kosmetika a mnoho dal&scaron;&iacute;ho naleznete na ProdejParfemu.cz</p>\n', '<p>ProdejParfemu.cz je e-shop prod&aacute;vaj&iacute;c&iacute; kvalitn&iacute; parf&eacute;my a kosmetiku světozn&aacute;m&yacute;ch značek.</p>\n', NULL, 3, 4, 1, 1, 'prodejparfemu-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4769949, 4769949, NULL, 'Deactive', 'en', 'Fashion4VIP.net', '', 'https://www.fashion4vip.net', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12650848-1469628447000', '1479373768.png', '', '', NULL, 5, 10, 1, 1, 'fashion4vip-net', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4770057, 4770057, NULL, 'Active', 'en', 'TRAVELIST.cz', '', 'https://travelist.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12642478-1468838047000', '1479373859.png', '<p>TRAVELIST - luxusn&iacute; dovolen&eacute; a pobyty za přijateln&eacute; ceny.</p>\n', '<p>Travelist vyjedn&aacute;v&aacute; exkluz&iacute;vn&iacute; ceny za luxusn&iacute;, osobně vybran&eacute; hotely a pobyty doma i v zahranič&iacute; se slevou až do 70% z ceny nab&iacute;zen&eacute; jinde online. Jak to dok&aacute;zali? I ty nejluxusněj&scaron;&iacute; hotely na světě netě&scaron;&iacute; voln&eacute; pokoje... Star&aacute;j&iacute; se o sv&eacute; členy a nach&aacute;z&iacute; pro ně ceny jinde nedosažiteln&eacute;.</p>\n', '', 80, 175, 0, 0, 'travelist-cz', 1, NULL, 0, 1, 'cj', 1, '2017-04-13 00:21:06', 'cz'),
(4770601, 4770601, NULL, 'Active', 'en', 'FLiXBUS.CZ', '', 'https://www.flixbus.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12639031-1484342410000', '1489434852.png', '<p>Levn&eacute; cestov&aacute;n&iacute; autobusem po cel&eacute; Evropě.</p>\n', '<p>Cestujte levně autobusem se společnost&iacute;&nbsp;FlixBus&nbsp;- 100 000 denn&iacute;ch spojů, až 1.000 destinac&iacute;, v&iacute;ce než 20 evropsk&yacute;ch zem&iacute;!&nbsp;D&iacute;ky jednoduch&eacute;mu způsobu rezervace a dennodenně rostouc&iacute; nab&iacute;dce poskytuje&nbsp;milionům cestuj&iacute;c&iacute;ch možnost objevovat svět za m&aacute;lo peněz. Zelen&eacute; autobusy přitom uspokojuj&iacute; nejvy&scaron;&scaron;&iacute; požadavky v&nbsp;oblasti bezpečnosti a ekologie a nab&iacute;zej&iacute; tak udržitelnou a pohodlnou alternativu individu&aacute;ln&iacute; dopravy.&nbsp;</p>\n', NULL, 3, 6, 1, 1, 'flixbus-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 20:54:12', 'cz'),
(4786083, 4786083, NULL, 'Active', 'en', 'Eukanuba-shop.cz', '', 'https://www.eukanuba-shop.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12654681-1481100050000', '1487717156.png', '<p>Eukanuba-shop.cz je&nbsp;ofici&aacute;ln&iacute; e-shop s krmivem Eukanuba&nbsp;v Česk&eacute; republice.</p>\n', '<p>Eukanuba-shop.cz je&nbsp;ofici&aacute;ln&iacute; e-shop s krmivem Eukanuba&nbsp;v Česk&eacute; republice. V nab&iacute;dce najdete granule pro psy a&nbsp;kočky, pro dospěl&aacute; zv&iacute;řata i &scaron;těnata, pro zdrav&iacute; a fyzick&yacute; v&yacute;kon.</p>\n', NULL, 6, 12, 1, 1, 'eukanuba-shop-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 23:45:56', 'cz'),
(4786110, 4786110, NULL, 'Active', 'en', '4camping.cz', '', 'https://www.4camping.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12684494-1473856130000', '1480209092.png', '<p>Na port&aacute;lu 4camping.cz poř&iacute;d&iacute;te vybaven&iacute; na kempov&aacute;n&iacute; a do př&iacute;rody.</p>\n', '<p>Na port&aacute;lu 4camping.cz poř&iacute;d&iacute;te vybaven&iacute; do př&iacute;rody, jako jsou stany, spac&aacute;ky, karimatky, batohy, obuv, oblečen&iacute; a d&aacute;le vybaven&iacute; na vodn&iacute; a zimn&iacute; sporty, kempov&aacute;n&iacute; a turistiku.</p>\n', NULL, 4, 5, 0, 1, '4camping-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4786873, 4786873, NULL, 'Active', 'en', 'Plant Therapy', '', 'https://www.planttherapy.com/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12664432-1471977676000', '1480276940.jpg', '', '', NULL, 0, 7.5, 1, 1, 'plant-therapy', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4805149, 4805149, NULL, 'Active', 'en', 'Chefshop.sk', '', 'https://www.chefshop.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12697025-1487075649000', '1480255488.jpg', '<p>E-shop, ve kter&eacute;m pohodlně nakoup&iacute;te&nbsp;v&scaron;e potřebn&eacute; pro př&iacute;pravu j&iacute;del, ingredience i n&aacute;dob&iacute;.</p>\n', '<p>Určite ste už počuli o&nbsp;na&scaron;ej&nbsp;&Scaron;kole vařen&iacute; Chefparade&nbsp;v Prahe. V&nbsp;tej už niekoľko&nbsp; rokov uč&iacute;me milovn&iacute;kov gastron&oacute;mie variť klasick&eacute; eur&oacute;pske, ale aj exotick&eacute; jedl&aacute;. Ďal&scaron;&iacute;m logick&yacute;m krokom potom bolo založiť e-shop Chefshop.cz spolu s&nbsp;Chefshop.sk, v&nbsp;ktor&yacute;ch pohodlne nak&uacute;pite v&scaron;etky ingrediencie potrebn&eacute; pre pr&iacute;pravu t&yacute;chto jed&aacute;l. Od tej doby už ale uplynula nejak&aacute; doba a&nbsp;my sme sa rozr&aacute;stli a&nbsp;s&nbsp;nami i&nbsp;n&aacute;&scaron; sortiment. V&nbsp;s&uacute;časnosti u&nbsp;n&aacute;s okrem potrav&iacute;n n&aacute;jdete hrnce, panvice a&nbsp;ďal&scaron;ie veci na varenie, veľk&uacute; &scaron;k&aacute;lu produktov na pečenie, kuchynsk&eacute; roboty a&nbsp;ďal&scaron;ie vychyt&aacute;vky a&nbsp;v&nbsp;neposlednej rade kr&aacute;sne taniere, misky, hrnčeky a&nbsp;ďal&scaron;ie veci na v&aacute;&scaron; st&ocirc;l a&nbsp;to vr&aacute;tane unik&aacute;tnej japonskej keramiky Made in Japan, ktor&uacute; v&nbsp;Eur&oacute;pe nikde inde nenak&uacute;pite!</p>\n', NULL, 5, 10, 0, 1, 'chefshop-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 00:17:01', 'sk'),
(4805198, 4805198, NULL, 'Active', 'en', 'KnihyDobrovsky.cz', '', 'https://www.knihydobrovsky.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12733241-1483373829000', '1480276327.jpg', '<p>&Scaron;irok&yacute; v&yacute;běr knih na e-shopu KnihyDobrovsky.cz a osobn&iacute; odběr zdarma na prodejn&aacute;ch po cel&eacute; ČR.</p>\n', '<p>Obrovsk&yacute; v&yacute;běr + v&scaron;echny obl&iacute;ben&eacute; kn&iacute;žky skladem! Osobn&iacute; odběr ZDARMA - 24 prodejen po cel&eacute; ČR | 1 000 000 spokojen&yacute;ch z&aacute;kazn&iacute;ků.</p>\n', '', 4, 6, 1, 1, 'knihydobrovsky-cz', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4806885, 4806885, NULL, 'Active', 'en', 'FotoŠkoda.cz', '', 'https://www.fotoskoda.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12747635-1480079946000', '1489436332.png', '<p>Specializovan&yacute; e-shop na foto-videotechniku a fotografick&eacute; služby.</p>\n', '<p>Foto&Scaron;koda.cz nab&iacute;z&iacute; komplexn&iacute; služby v oblasti fotografov&aacute;n&iacute;. Vyberte si ze &scaron;irok&eacute; nab&iacute;dkydigit&aacute;ln&iacute;ch&nbsp;fotoapar&aacute;tů, videokamer, objektivů, stativů,&nbsp;fotokurzů a ve&scaron;ker&eacute;ho př&iacute;slu&scaron;enstv&iacute;, kter&eacute; v&aacute;s jen napadne.</p>\n\n<p>.</p>\n', NULL, 5, 10, 0, 1, 'fotoskoda-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 21:18:52', 'cz'),
(4806888, 4806888, NULL, 'Active', 'en', 'iFertility.cz', '', 'https://www.ifertility.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12736116-1478186391000', '', '', '', NULL, 0, 10, 1, 1, 'ifertility-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4807354, 4807354, NULL, 'Active', 'en', 'Blacklane', '', 'https://www.blacklane.com', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12711049-1476091924000', '', '', '', NULL, 0, 0, 1, 1, 'blacklane', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4813636, 4813636, NULL, 'Active', 'en', 'Dotykačka.cz', '', 'https://www.dotykacka.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12696981-1476456044000', 'dotykacka.png', '<p>Dotykacka.cz je spolehliv&yacute; a cenově dostupn&yacute; pokladn&iacute; software s intuitivn&iacute;m ovl&aacute;d&aacute;n&iacute;m.</p>\n', '<p>Dotykacka.cz je spolehliv&yacute; a cenově dostupn&yacute; pokladn&iacute; software s intuitivn&iacute;m ovl&aacute;d&aacute;n&iacute;m. Dotykačka je určen&aacute; pro podnik&aacute;n&iacute; v gastronomii, provozov&aacute;n&iacute; hotelů nebo poskytov&aacute;n&iacute; služeb. Společně s na&scaron;imi odoln&yacute;mi dotykov&yacute;mi obrazovkami, tisk&aacute;rnami a dal&scaron;&iacute;m př&iacute;slu&scaron;enstv&iacute;m jde o nejkomplexněj&scaron;&iacute; pokladn&iacute;, skladov&yacute; a evidenčn&iacute; syst&eacute;m na česk&eacute;m trhu.</p>\n', NULL, 5, 10, 1, 1, 'dotykacka-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 00:29:07', 'cz'),
(4828144, 4828144, NULL, 'Active', 'en', 'Nakup-nabytek.cz', '', 'http://www.nakup-nabytek.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12734700-1486371790000', '1492033316.jpg', '<p>Nakup-nabytek.cz - eshop se &scaron;irokou nab&iacute;dkou bytov&eacute;ho n&aacute;bytku</p>\n', '<p>Nakup-nabytek.cz je eshop se &scaron;irokou nab&iacute;dkou bytov&eacute;ho n&aacute;bytku (20 000 druhů). Zaměřuje se na n&aacute;bytek, kter&yacute; je modern&iacute;, levněj&scaron;&iacute;, a přesto kvalitn&iacute;. Ale tak&eacute; zde najdete kousky luxusněj&scaron;&iacute;. Produkty ryze česk&eacute; v&yacute;roby, ale i např. z&nbsp;Německa, Polska, Slovenska, D&aacute;nska či Loty&scaron;ska.</p>\n', '', 6, 10, 1, 1, 'nakup-nabytek-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-12 23:41:56', 'cz'),
(4829964, 4829964, NULL, 'Active', 'en', 'Countrylife.cz', '', 'http://www.countrylife.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12723859-1481291225000', '1484165479.jpg', '<p>Biopotraviny,&nbsp;ekologick&eacute; čistic&iacute; prostředky a př&iacute;rodn&iacute; kosmetika.&nbsp;</p>\n', '<p>Countrylife.cz nab&iacute;z&iacute;&nbsp;v&iacute;ce než 2 500 v&yacute;robků, z toho bezm&aacute;la 1 500 položek v biokvalitě. Sortiment zahrnuje trvanliv&eacute; a chlazen&eacute; potraviny, ovoce a zeleninu, pečivo, ekologick&eacute; čistic&iacute; prostředky a př&iacute;rodn&iacute; kosmetiku.&nbsp;</p>\n', NULL, 5, 10, 1, 1, 'countrylife-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4836093, 4836093, NULL, 'Active', 'en', 'Babickarstvi.cz', '', 'http://www.babickarstvi.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12735885-1479324349000', '1487531125.png', '<p>Babičk&aacute;řstv&iacute; v&aacute;m přin&aacute;&scaron;&iacute; inspiraci, jak udělat va&scaron;&iacute; babičce nebo dědečkovi radost.</p>\n', '<p>Babičk&aacute;řstv&iacute; v&aacute;m přin&aacute;&scaron;&iacute; inspiraci, jak udělat va&scaron;&iacute; babičce nebo dědečkovi radost.<br />\n<br />\nD&aacute;rky na Babičk&aacute;řstv&iacute; vych&aacute;z&iacute; z pozn&aacute;n&iacute; potřeb seniorů, kter&eacute; jsme čerpali z vlastn&iacute; mnohalet&eacute; zku&scaron;enosti se seniory, z v&yacute;zkumů a studi&iacute; a z diskuz&iacute; s odborn&iacute;ky.<br />\nNa&scaron;e d&aacute;rky nejenom potě&scaron;&iacute;, ale pomohou zlep&scaron;it kvalitu života babiček a dědečků. Někter&eacute; d&aacute;rky pečlivě vyb&iacute;r&aacute;me od specializovan&yacute;ch dodavatelů, někter&eacute; d&aacute;rky sami vytv&aacute;ř&iacute;me.</p>\n', NULL, 10, 20, 0, 1, 'babickarstvi-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4844263, 4844263, NULL, 'Active', 'en', 'WOODMINT.cz', '', 'https://www.woodmint.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12754853-1480517214000', '', '', '', NULL, 0, 15, 1, 1, 'woodmint-cz', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4844265, 4844265, NULL, 'Active', 'en', 'WOODMINT.sk', '', 'https://www.woodmint.sk/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12754851-1480517198000', '', '', '', NULL, 0, 15, 1, 1, 'woodmint-sk', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4846352, 4846352, NULL, 'Active', 'en', 'Delmas.cz', '', 'http://www.delmas.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12766897-1479996901000', '1488232944.jpg', '<p>Na port&aacute;lu www.delmas.cz&nbsp; najdete d&aacute;msk&eacute; kabelky, p&aacute;nsk&eacute; ta&scaron;ky a dal&scaron;&iacute; doplňky světov&yacute;ch značek.</p>\n', '<p>Na port&aacute;lu www.delmas.cz&nbsp; najdete d&aacute;msk&eacute; kabelky, p&aacute;nsk&eacute; ta&scaron;ky a dal&scaron;&iacute; doplňky světov&yacute;ch značek, jako je Desigual, Guess, Calvin Klein, Braun B&uuml;ffel, ale tak&eacute; od česk&yacute;ch v&yacute;robců Sněžka N&aacute;chod, Hajn a dal&scaron;&iacute;.</p>\n\n<p>V&yacute;hradn&iacute; on-line prodejci značkov&yacute;ch kufrů RIMOWA v ČR. Velmi &uacute;spě&scaron;n&iacute; jsme i v prodeji zavazadel Samsonite, American Tourister a March.</p>\n', NULL, 4, 8, 1, 1, 'delmas-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 23:02:24', 'cz'),
(4846918, 4846918, NULL, 'Active', 'en', 'TOTAL-STORE.CZ', '', 'https://www.total-store.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12774157-1481636278000', '1484164715.gif', '<p>Na eshopu Total-Store.cz naleznete sportovn&iacute; oblečen&iacute; světov&yacute;ch značek.</p>\n', '<p>Na eshopu Total-Store.cz naleznete sportovn&iacute; d&aacute;msk&eacute;, p&aacute;nsk&eacute; i dětsk&eacute; oblečen&iacute;, boty a doplňky popul&aacute;rn&iacute;ch světov&yacute;ch značek, jako jsou O&rsquo;neill, Under Armour, Adidas, Nike, Puma, Reebok, converse, Lotto, Select, New Balance a dal&scaron;&iacute;.</p>\n', NULL, 5, 10.5, 1, 1, 'total-store-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4846924, 4846924, NULL, 'Active', 'en', 'TOTAL-STORE.SK', '', 'https://www.total-store.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12776068-1480688104000', '1484164875.gif', '<p>Na eshopu Total-Store.cz naleznete sportovn&iacute; oblečen&iacute; světov&yacute;ch značek.</p>\n', '<p>Na eshopu Total-Store.cz naleznete sportovn&iacute; d&aacute;msk&eacute;, p&aacute;nsk&eacute; i dětsk&eacute; oblečen&iacute;, boty a doplňky popul&aacute;rn&iacute;ch světov&yacute;ch značek, jako jsou O&rsquo;neill, Under Armour, Adidas, Nike, Puma, Reebok, converse, Lotto, Select, New Balance a dal&scaron;&iacute;</p>\n', NULL, 5, 10.5, 1, 1, 'total-store-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4851127, 4851127, NULL, 'Active', 'en', 'GymBeam.cz', '', 'https://gymbeam.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12776023-1481114472000', '1482247889.png', '<p>GymBeam.cz to je v&scaron;e pro fitness na jednom m&iacute;stě.&nbsp;</p>\n', '<p>GymBeam.cz to je v&scaron;e pro fitness na jednom m&iacute;stě. V &scaron;irok&eacute; nab&iacute;dce potravinov&yacute;ch doplňků najdete proteiny, aminokyseliny, vitam&iacute;ny a des&iacute;tky dal&scaron;&iacute;ch produktů. V e-shopu seženete i dal&scaron;&iacute; potřeby pro fitness jako jsou &scaron;ejkry, rukavice, band&aacute;že aj.</p>\n', NULL, 10, 2.5, 0, 1, 'gymbeam-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4851131, 4851131, NULL, 'Active', 'en', 'GymBeam.sk', '', 'https://gymbeam.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12776185-1481114867000', '1482247905.png', '<p>GymBeam.cz to je v&scaron;e pro fitness na jednom m&iacute;stě.</p>\n', '<p>GymBeam.cz to je v&scaron;e pro fitness na jednom m&iacute;stě. V &scaron;irok&eacute; nab&iacute;dce potravinov&yacute;ch doplňků najdete proteiny, aminokyseliny, vitam&iacute;ny a des&iacute;tky dal&scaron;&iacute;ch produktů. V e-shopu seženete i dal&scaron;&iacute; potřeby pro fitness jako jsou &scaron;ejkry, rukavice, band&aacute;že aj.</p>\n', NULL, 10, 2.5, 0, 1, 'gymbeam-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'sk'),
(4851152, 4851152, NULL, 'Active', 'en', 'Vemzu.cz', '', 'https://www.vemzu.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12781018-1481621190000', '1483880999.png', '<p>VEMZU to je&nbsp;jedinečn&aacute; nab&iacute;dka designov&eacute;ho zbož&iacute;.</p>\n', '<p>VEMZU to je&nbsp;jedinečn&aacute; nab&iacute;dka designov&eacute;ho zbož&iacute;. E-shop pro každ&eacute;ho, kdo nechce aby jeho &scaron;atn&iacute;k a byt vypadal stejně jako tis&iacute;ce dal&scaron;&iacute;ch. U&nbsp;vět&scaron;iny produktů garantujeme nejen&nbsp;prvotř&iacute;dn&iacute; kvalitu a origin&aacute;ln&iacute; design, ale i nejlep&scaron;&iacute; cenu na&nbsp;trhu - v opačn&eacute;m př&iacute;padě v&aacute;m rozd&iacute;l doplat&iacute;me.</p>\n', NULL, 6, 12, 1, 1, 'vemzu-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4851153, 2498951, NULL, 'Active', 'de', 'Tchibo - CZ', '', 'http://www.tchibo.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-10591090-1464959789000', '1490550617.png', '<p>E-shop Tchibo.cz je popul&aacute;rn&iacute; prodejce k&aacute;vy a &scaron;irok&eacute;ho sortimentu zbož&iacute;.</p>\n', '<p>Každou středu objevujte v e-shopu&nbsp;Tchibo.cz nov&yacute; svět v&yacute;robků z kategorie m&oacute;da, dom&aacute;cnost, technika nebo sport.</p>\n', '<p>Z&aacute; n&aacute;kupy v kategori&iacute;ch Cestov&aacute;n&iacute; a TchiboCard nedostanete odměnu.</p>\n', 5, 10, 1, 1, '', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4851154, 3600617, NULL, 'Active', 'en', 'ZOOT.cz', '', 'http://www.zoot.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11028633-1452774453000', '1484168997.gif', '<p>ZOOT.cz je n&aacute;kupn&iacute; port&aacute;l přin&aacute;&scaron;ej&iacute;c&iacute; nejnověj&scaron;&iacute; trendy.</p>\n', '<p>ZOOT.cz je n&aacute;kupn&iacute; port&aacute;l přin&aacute;&scaron;ej&iacute;c&iacute; nejnověj&scaron;&iacute; trendy za nejlep&scaron;&iacute; ceny ze světa m&oacute;dy,&nbsp;designu a vychyt&aacute;vek.&nbsp;</p>\n', '', 4, 8, 1, 1, 'zoot', 1, NULL, 1, 1, 'cj', 1, '2017-02-09 00:00:00', 'cz'),
(4851155, 2458053, NULL, 'Active', 'en', 'Cosme-De.com', '', 'http://www.cosme-de.com', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12843962-1487174408000', '', '', '', NULL, 0, 10, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851156, 3804912, NULL, 'Active', 'en', 'IZIGET', '', 'http://www.lacewigsbuy.com', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11313914-1478628938000', '', '', '', NULL, 0, 16, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851157, NULL, 4384608, 'Active', '', '', 'Krasa.cz', '', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12086036', '1484506914.png', '<p>Internetov&yacute; obchod s produkty pro kr&aacute;su.</p>\n', '<p>Internetov&yacute; obchod s produkty pro kr&aacute;su&nbsp;<a href="https://www.krasa.cz/">krasa.cz</a>&nbsp;je největ&scaron;&iacute;m internetov&yacute;m obchodem s kosmetick&yacute;mi př&iacute;pravky a př&iacute;stroji v Česk&eacute; republice.</p>\n', NULL, 5, 3, 1, 1, 'krasa-cz', 1, NULL, 0, 0, 'vydelavejnakupem', 1, '2017-02-09 00:00:00', NULL),
(4851158, NULL, 4384608, 'Active', '', '', 'Hodinky.cz', '', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11977764', '1484507242.jpg', '<p>Největ&scaron;&iacute;m internetov&yacute;m obchod&nbsp;s&nbsp;hodinkami v&nbsp;Česk&eacute; republice.</p>\n', '<p>Internetov&eacute; hodin&aacute;řstv&iacute; Hodinky.cz&nbsp;je největ&scaron;&iacute;m internetov&yacute;m obchodem s&nbsp;hodinkami v&nbsp;Česk&eacute; republice.</p>\n', NULL, 5, 3, 1, 1, 'hodinky-cz', 1, NULL, 0, 0, 'vydelavejnakupem', 1, '2017-02-09 00:00:00', NULL),
(4851159, NULL, 4384608, 'Active', '', '', 'Prozdravi.cz', '', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11977732', '1484507630.jpg', '<p>E-shop s doplňky stravy, př&iacute;řodn&iacute; kosmetikou, zdravou v&yacute;živou.</p>\n', '<p>Internetov&yacute; obchod s produkty pro zdrav&iacute; Prozdravi.cz je největ&scaron;&iacute;m e-shopem s&nbsp;doplňky stravy, př&iacute;rodn&iacute; kosmetikou, zdravotn&iacute;mi př&iacute;stroji a zdravou v&yacute;živou v Česk&eacute; republice.</p>\n', NULL, 5, 3, 1, 1, 'prozdravi-cz', 1, NULL, 0, 0, 'vydelavejnakupem', 1, '2017-02-09 00:00:00', NULL),
(4851160, NULL, 4384608, 'Active', '', '', 'Šperky.cz', '', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12092555', '1484507970.jpg', '<p>Port&aacute;l &Scaron;perky.cz je největ&scaron;&iacute;m internetov&yacute;m &nbsp;klenovnictv&iacute;m v ČR.</p>\n', '<p>Port&aacute;l &Scaron;perky.cz je největ&scaron;&iacute;m internetov&yacute;m &nbsp;klenovnictv&iacute;m v ČR. Nab&iacute;z&iacute; 66 světovch značek za velice př&iacute;zniv&eacute; ceny.</p>\n', NULL, 7, 3, 1, 1, 'sperky-cz', 1, NULL, 0, 0, 'vydelavejnakupem', 1, '2017-02-09 00:00:00', NULL),
(4851161, 4596081, NULL, 'Active', 'en', 'Bata - DACH', '', 'http://www.bata.de', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12341307-1443783237000', '', '', '', NULL, 0, 10, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851162, 4780307, NULL, 'Active', 'en', 'SCDKey', '', 'https://www.scdkey.com/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12723759-1484637641000', '', '', '', NULL, 0, 8, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851163, 4161047, NULL, 'Deactive', 'en', 'Willsoor.cz', '', 'http://www.willsoor.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11584263-1480520749000', '', '', '', NULL, 0, 10, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'cz'),
(4851164, 4322334, NULL, 'Active', 'en', 'Nazuby.eu', '', 'http://www.nazuby.eu', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11883455-1479142652000', '', '', '', NULL, 0, 3, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851165, 4352968, NULL, 'Active', 'en', 'Footshop.sk', '', 'https://www.footshop.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11910056-1407847354000', '1488233542.jpg', '<p>Na port&aacute;li Footshop.sk&nbsp;n&aacute;jdete d&aacute;msku, p&aacute;nsku i detsk&uacute; obuv.</p>\n', '<p>Na port&aacute;li Footshop.sk&nbsp;n&aacute;jdete d&aacute;msku, p&aacute;nsku i detsk&uacute; obuv, ďalej značkov&eacute; skate, hip-hop a sneakers top&aacute;nky, &scaron;iltovky, ruksaky, peňaženky a mnoho ďal&scaron;ieho. V&scaron;etok tovar je skladom a pripraven&yacute;&nbsp;k okamžit&eacute;mu odoslaniu!</p>\n', NULL, 7.5, 15, 1, 1, 'footshop-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-27 23:12:22', 'sk'),
(4851166, 4482726, NULL, 'Active', 'en', 'Triola.sk', '', 'http://eshop.triola.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12150577-1444641196000', '', '', '', NULL, 0, 8, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851167, 4689087, NULL, 'Active', 'en', 'Eyerim.sk', '', 'https://www.eyerim.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12506024-1455705919000', '1487716655.jpg', '<p>Eyerim.sk pred&aacute;va dizajn&eacute;rsku očn&uacute; optiku - tis&iacute;cky autentick&yacute;ch modelov slnečn&yacute;ch, dioptrick&yacute;ch a lyžiarskych okuliarov.</p>\n', '<p>Eyerim.sk pred&aacute;va dizajn&eacute;rsku očn&uacute; optiku - tis&iacute;cky autentick&yacute;ch modelov slnečn&yacute;ch, dioptrick&yacute;ch a lyžiarskych okuliarov. Na&scaron;&iacute;m cieľom je pon&uacute;knuť &ldquo;v&scaron;etko pod jednou strechou&rdquo;. Od prest&iacute;žnych svetov&yacute;ch značiek, ako napr&iacute;klad Ray-Ban, cez ručne vyr&aacute;ban&eacute; kolekcie men&scaron;&iacute;ch nez&aacute;visl&yacute;ch v&yacute;robcov a funkčn&eacute; &scaron;portov&eacute; r&aacute;my, až po v&yacute;berov&eacute; dizajn&eacute;rske k&uacute;sky. Na Eyerim je po&scaron;tovn&eacute; zadarmo do v&scaron;etk&yacute;ch kraj&iacute;n EU. V&scaron;etky na&scaron;e produkty s&uacute; 100% origin&aacute;lne a maj&uacute; 2-ročn&uacute; z&aacute;ruku.</p>\n', NULL, 4, 8, 1, 1, 'eyerim-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 23:37:35', 'sk'),
(4851168, 4733566, NULL, 'Active', 'en', 'Vasa-Moda.sk', '', 'https://www.vasa-moda.sk/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12623013-1466776350000', '', '', '', NULL, 0, 11, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851169, 3376485, NULL, 'Active', 'en', 'Hej.sk Affiliate Program', '', 'http://www.hej.sk', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-10936645-1466168837000', '', '', '', NULL, 0, 1, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851170, 3767115, NULL, 'Deactive', 'en', 'Setriza3.sk', '', 'http://www.setriza3.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11201090-1398162222000', '', '', '', NULL, 0, 3, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851171, 3773522, NULL, 'Active', 'en', 'Relaxero.sk', '', 'https://www.relaxero.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11119359-1486070053000', '', '', '', NULL, 0, 11, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851172, 3773524, NULL, 'Active', 'en', 'Zlavadna.SK', '', 'http://www.zlavadna.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11128901-1480932953000', '', '', '', NULL, 0, 8, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851173, 3782732, NULL, 'Active', 'en', 'Morezliav.sk', '', 'http://www.morezliav.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-11123221-1447426657000', '', '', '', NULL, 0, 5, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851174, 3849659, NULL, 'Active', 'en', 'Couponzone.SK', '', 'http://www.couponzone.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11184520-1459508711000', '', '', '', NULL, 0, 5, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851175, 4535290, NULL, 'Active', 'en', 'Conrad.sk', '', 'http://www.conrad.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12247907-1434104139000', '1491946616.jpg', '<p>Conrad je jedna z najv&auml;č&scaron;&iacute;ch zasielateľsk&yacute;ch spoločnost&iacute; na poli elektroniky v Eur&oacute;pe.</p>\n', '<p>Conrad je jedna z najv&auml;č&scaron;&iacute;ch zasielateľsk&yacute;ch spoločnost&iacute; na poli elektroniky v Eur&oacute;pe. Jedn&aacute; sa o rodinn&uacute; firmu, ktor&aacute; bola založen&aacute; v Berl&iacute;ne v roku 1923. Pon&uacute;ka &scaron;irok&uacute; &scaron;k&aacute;lu produktov, ktor&eacute; pokr&yacute;vaj&uacute; takmer v&scaron;etko, čo m&aacute; čo do činenia s elektronikou a technikou. Logistick&aacute; kapacita je až 100 000 bal&iacute;kov denne. Objem z&aacute;sielok čin&iacute; 8,3 mili&oacute;na ročne so 150 mili&oacute;nmi v&yacute;robkov.</p>\n', '', 4, 8, 0, 1, 'conrad-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-11 23:36:56', 'sk'),
(4851176, 4612042, NULL, 'Active', 'en', 'Vivantis.sk', '', 'https://www.vivantis.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12380414-1467205675000', '', '', '', NULL, 0, 5, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851177, 4786085, NULL, 'Active', 'en', 'Eukanuba-shop.sk', '', 'https://www.eukanuba-shop.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12654682-1481100073000', '1487717240.png', '<p>Eukanuba-shop.sk je ofici&aacute;lny e-shop s krmivom Eukanuba na Slovensku.</p>\n', '<p>Eukanuba-shop.sk je ofici&aacute;lny e-shop s krmivom Eukanuba na Slovensku. V ponuke n&aacute;jdete granule pre psov a mačky, pre dospel&eacute; zvierat&aacute; aj &scaron;teniatka, pre zdravie a fyzick&yacute; v&yacute;kon.</p>\n\n<p>Doprava je vždy zadarmo, ku každej objend&aacute;vce nad 500, - dostanete darček podľa vlastn&eacute;ho v&yacute;beru. Tovar doručujeme už druh&yacute; deň!</p>\n', NULL, 6, 12, 1, 1, 'eukanuba-shop-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 23:47:20', 'sk'),
(4851178, 4795556, NULL, 'Active', 'en', 'Vivre.cz', '', 'https://www.vivre.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12672047-1482329215000', '1489439133.png', '<p>Kr&aacute;sn&eacute; věci pro bydlen&iacute; a život v origin&aacute;ln&iacute;m designu v časově omezen&yacute;ch kampan&iacute;ch.</p>\n', '<p>Vivre.cz nab&iacute;z&iacute;&nbsp;&scaron;irok&yacute; sortiment bytov&yacute;ch doplňků, n&aacute;bytku, textili&iacute; a dal&scaron;&iacute;ch produktů pro z&uacute;tulněn&iacute; va&scaron;eho domova. Kr&aacute;sn&eacute; věci pro bydlen&iacute; a život v origin&aacute;ln&iacute;m designu v časově omezen&yacute;ch kampan&iacute;ch.</p>\n', NULL, 4, 8, 1, 1, '', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 21:59:09', 'cz'),
(4851179, 4795557, NULL, 'Active', 'en', 'Vivrehome.sk', '', 'https://www.vivrehome.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12672048-1482403252000', '', '', '', NULL, 0, 8, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851180, 4836009, NULL, 'Active', 'en', 'Temponabytok.sk', '', 'http://www.temponabytok.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12796703-1482318416000', '1491771421.jpg', '<p>Temponabytok.sk - predaj n&aacute;bytku a bytov&yacute;ch doplnkov za bezkonkurenčn&eacute; ceny.</p>\n', '<p>Venujeme sa maloobchodn&eacute;mu a veľkoobchodn&eacute;mu predaju n&aacute;bytku a bytov&yacute;ch doplnkov za bezkonkurenčn&eacute; ceny už cel&yacute;ch 24 rokov. Prev&aacute;dzkujeme 22 vlastn&yacute;ch maloobchodn&yacute;ch (kamenn&yacute;ch) predajn&iacute; pod značkou TEMPO-KONDELA N&Aacute;BYTOK a okrem toho v r&aacute;mci n&aacute;&scaron;ho veľkoobchodu z&aacute;sobujeme na&scaron;&iacute;m sortimentom 24 partnersk&yacute;ch predajn&iacute; n&aacute;bytku v SR a ďal&scaron;&iacute;ch 222 predajn&iacute; n&aacute;bytku v ČR.</p>\n\n<p>N&aacute;&scaron; tovar si m&ocirc;žte zak&uacute;piť na jednej z 22 maloobchodn&yacute;ch predajn&iacute; alebo priamo na na&scaron;om e-shopoch www.temponabytok.sk alebo www.temponabytek.cz</p>\n', NULL, 2, 4, 1, 1, 'temponabytok-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 22:57:01', 'sk'),
(4851181, 4835954, NULL, 'Active', 'en', 'Temponabytek.cz', '', 'http://www.temponabytek.cz', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12804232-1483447107000', '1491771319.png', '<p>Temponabytek.cz - &scaron;irok&aacute; &scaron;k&aacute;la n&aacute;bytku a bytov&yacute;ch doplňků.</p>\n', '<p>V nab&iacute;dce Temponabytek.cz najdete &scaron;irokou &scaron;k&aacute;lu n&aacute;bytku a bytov&yacute;ch doplňků. Velk&eacute;mu z&aacute;jmu se tě&scaron;&iacute; oddělen&iacute; čalouněn&eacute;ho n&aacute;bytku. Kromě čalouněn&yacute;ch sedac&iacute;ch souprav v kůži i l&aacute;tce najdete na v eshopu&nbsp;zaj&iacute;mavou nab&iacute;dku v&iacute;cefunkčn&iacute;ch pohovek zaj&iacute;mav&yacute;ch předev&scaron;&iacute;m pro mlad&eacute; rodiny, kter&eacute; překvap&iacute; designem a atraktivn&iacute; cenou. Nab&iacute;z&iacute;me i&nbsp;j&iacute;deln&iacute; sestavy, ob&yacute;vac&iacute; stěny, kuchyně a židle.</p>\n', NULL, 2, 4, 1, 1, 'temponabytek-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 22:55:19', 'cz'),
(4851182, 4851136, NULL, 'Active', 'en', 'VimVic.cz', '', 'https://www.vimvic.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12803975-1485277692000', '1489011142.png', '<p>VimVic.cz je&nbsp;dynamicky se rozv&iacute;jej&iacute;c&iacute; projekt, kter&yacute; nab&iacute;z&iacute;&nbsp;des&iacute;tky kvalitn&iacute;ch online video kurzů.</p>\n', '<p>VimVic.cz je&nbsp;dynamicky se rozv&iacute;jej&iacute;c&iacute; projekt, kter&yacute; nab&iacute;z&iacute;&nbsp;des&iacute;tky kvalitn&iacute;ch online video kurzů.</p>\n\n<p>Od programov&aacute;n&iacute; a ovl&aacute;d&aacute;n&iacute; kancel&aacute;řsk&yacute;ch aplikac&iacute; jako MS Excel, přes jazykov&eacute; kurzy až po kurzy osobn&iacute;ho rozvoje - vybere si každ&yacute;.</p>\n\n<p>Kromě kurzů zde najdete tak&eacute; představen&iacute;&nbsp;řady zaj&iacute;mav&yacute;ch pracovn&iacute;ch př&iacute;ležitost&iacute; a možnost&iacute; spolupr&aacute;ce s firmami.</p>\n\n<p>Společnost V&iacute;mV&iacute;c.cz patř&iacute; s nyněj&scaron;&iacute;mi 40 000 registrovan&yacute;mi uživateli patř&iacute; mezi největ&scaron;&iacute; prodejce online kurzů v Česk&eacute; republice.&nbsp;</p>\n', NULL, 15, 25, 1, 1, 'vimvic-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-08 23:09:58', 'cz'),
(4851183, 4605417, NULL, 'Active', 'en', 'Deichmann.cz', '', 'http://www.deichmann.com/CZ/cs/shop/welcome.html', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12411607-1477056299000', '1485891170.gif', '<p>M&oacute;dn&iacute; obuv pro v&scaron;echny věkov&eacute; kategorie v dobr&eacute; kvalitě za bezkonkurenčn&iacute; cenu.</p>\n', '<p>Na str&aacute;nk&aacute;ch Deichmann.cz&nbsp;mohou z&aacute;jemci vyhled&aacute;vat v &scaron;irok&eacute; nab&iacute;dce sortimentu d&aacute;msk&eacute;, p&aacute;nsk&eacute;, dětsk&eacute; a sportovn&iacute; obuvi. K online sortimentu patř&iacute; vedle DEICHMANN obuvi a doplňků i speci&aacute;ln&iacute; internetov&eacute; nab&iacute;dky.</p>\n', NULL, 4, 8, 1, 1, 'deichmann-cz', 1, NULL, 0, 0, 'cj', 1, '2017-02-09 00:00:00', NULL),
(4851184, 4640980, NULL, 'Active', 'en', 'Deichmann.sk', '', 'http://www.deichmann.com/SK/sk/shop/welcome.html', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12405387-1453727717000', '1487546816.jpg', '<p>Deichmann obuv a doplnky dopĺňaj&uacute; v&nbsp;r&aacute;mci on-line sortimentu &scaron;peci&aacute;lne internetov&eacute; ponuky.</p>\n', '<p>&bdquo;M&oacute;dna obuv pre v&scaron;etky vekov&eacute; kateg&oacute;rie s dobrou kvalitou za bezkonkurenčn&uacute; cenu&ldquo;. Vďaka tomuto princ&iacute;pu sa skupina DEICHMANN vypracovala do poz&iacute;cie najv&auml;č&scaron;ieho maloobchodn&iacute;ka s obuvou v Eur&oacute;pe. Celosvetovo predala skupina DEICHMANN v roku 2015 viac ako 172 mili&oacute;nov p&aacute;rov obuvi. Spoločnosť DEICHMANN pon&uacute;ka obuv aj on-line, a to 24 hod&iacute;n denne. Na str&aacute;nke www.deichmann.com m&ocirc;žu z&aacute;ujemcovia prezerať &scaron;irok&uacute; ponuku d&aacute;mskej, p&aacute;nskej, detskej a &scaron;portovej obuvi. Aktu&aacute;lnu m&oacute;dnu obuv, ako aj m&oacute;dne doplnky si z&aacute;kazn&iacute;ci m&ocirc;žu objednať s doručen&iacute;m zadarmo.<br />\nDeichmann obuv a doplnky dopĺňaj&uacute; v&nbsp;r&aacute;mci on-line sortimentu &scaron;peci&aacute;lne internetov&eacute; ponuky.</p>\n', NULL, 4, 8, 1, 1, 'deichmann-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-20 00:26:56', NULL),
(4851185, 3784989, NULL, 'Active', 'en', 'Zamenej.sk', '', 'http://www.zamenej.sk', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-11117305-1454419648000', '', '', '', NULL, 0, 2, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851186, 4856236, NULL, 'Active', 'en', 'BENU.CZ', '', 'https://www.benu.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12788839-1481721204000', '1487526954.jpg', '<p>L&eacute;k&aacute;rny BENU.cz jsou souč&aacute;st&iacute; l&eacute;k&aacute;rensk&eacute;ho velkoobchodu PHOENIX, největ&scaron;&iacute;ho distributora l&eacute;ků v Evropě.</p>\n', '<p>L&eacute;k&aacute;rny BENU.cz jsou souč&aacute;st&iacute; l&eacute;k&aacute;rensk&eacute;ho velkoobchodu PHOENIX, největ&scaron;&iacute;ho distributora l&eacute;ků v Evropě. L&eacute;k&aacute;rny BENU působ&iacute; v cel&eacute; Evropě na česk&eacute;m trhu působ&iacute; 1990. Internetov&yacute; obchod BENU.cz patř&iacute; k největ&scaron;&iacute;m l&eacute;k&aacute;rensk&yacute;m eshopům&nbsp; na česk&eacute;m trhu. Prioritou jsou dokonal&eacute; služby z&aacute;kazn&iacute;kům. V&scaron;e zač&iacute;n&aacute; odborn&yacute;m poradenstv&iacute;m prostřednictv&iacute;m online poradny se zku&scaron;en&yacute;mi l&eacute;k&aacute;rn&iacute;ky, n&aacute;sleduje v&yacute;běrem z 15 000 tis&iacute;c produktů skladem a konč&iacute; rychl&yacute;m dod&aacute;n&iacute;m. K oceňovan&yacute;m v&yacute;hod&aacute;m je osobn&iacute; odběr zdarma na vice než 150 l&eacute;k&aacute;rn&aacute;ch po cel&eacute; ČR.</p>\n', '', 2, 3, 1, 1, 'benu-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4851187, 4510889, NULL, 'Active', 'en', 'Notino.cz', '', 'http://www.notino.cz', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12197703-1485167062000', '1486575866.jpg', '<p>Notino.cz - jednička v online prodeji parf&eacute;mů a kosmetiky ve středn&iacute; a v&yacute;chodn&iacute; Evropě. M&aacute; velk&yacute; v&yacute;běr parf&eacute;mů a kosmetiky za bezkonkurenčn&iacute; ceny.</p>\n', '<p>Notino.cz - jednička v online prodeji parf&eacute;mů a kosmetiky ve středn&iacute; a v&yacute;chodn&iacute; Evropě. M&aacute; velk&yacute; v&yacute;běr parf&eacute;mů a kosmetiky za bezkonkurenčn&iacute; ceny.</p>\n', '', 5, 10, 0, 1, 'notino-cz', 1, NULL, 1, 1, 'cj', 1, '2017-02-18 00:00:00', 'cz'),
(4851188, 4498040, NULL, 'Active', 'en', 'Banggood CJ Affiliate Program', '', 'http://www.banggood.com', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12280881-1436939256000', '', '', '', NULL, 0, 3, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851189, 3866918, NULL, 'Active', 'en', 'Newgo.SK', '', 'http://www.newgo.sk', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11201689-1467888799000', '', '', '', NULL, 0, 8, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851190, 4856736, NULL, 'Active', 'en', 'BOLF.CZ', '', 'https://www.bolf.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12795126-1482236962000', '1488479118.jpg', '<p>Bolf.cz se již řadu let zab&yacute;v&aacute; prodejem p&aacute;nsk&eacute;ho a&nbsp;d&aacute;msk&eacute;ho oblečen&iacute;.</p>\n', '<p>Bolf.cz se již řadu let zab&yacute;v&aacute; prodejem p&aacute;nsk&eacute;ho a&nbsp;d&aacute;msk&eacute;ho oblečen&iacute; a dynamicky se rozv&iacute;j&iacute; v oblasti e-commerce. Po letech zku&scaron;enost&iacute; v oblasti m&oacute;dy a online prodeje jsme si jisti, že spln&iacute;me v&scaron;echna va&scaron;e oček&aacute;v&aacute;n&iacute;.</p>\n\n<p>Proč si vybrat Bolf.cz<br />\n- v&iacute;ce než 250 origin&aacute;ln&iacute;ch m&oacute;dn&iacute;ch značek<br />\n- sez&oacute;nn&iacute; v&yacute;prodeje a&nbsp;dal&scaron;&iacute; pravideln&eacute; akce<br />\n- v&scaron;echno skladem<br />\n-&nbsp;doprava nad 599 Kč ZDARMA<br />\n- vr&aacute;cen&iacute; zbož&iacute; do&nbsp;30 dnů<br />\n- množstevn&iacute; slevy &ndash; č&iacute;m v&iacute;c nakoup&iacute;te, t&iacute;m v&iacute;c z&iacute;sk&aacute;v&aacute;te!</p>\n', NULL, 4, 8, 1, 1, 'bolf-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-02 19:25:18', 'cz'),
(4851191, 4510893, NULL, 'Active', 'en', 'Notino.sk', '', 'http://www.notino.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12197744-1485167151000', '1487632584.jpg', '<p>E-shop Notino p&ocirc;sob&iacute; na trhu od r. 2004 a je bez preh&aacute;ňania jednička v online predaji parfumov a kozmetiky v strednej a v&yacute;chodnej Eur&oacute;pe.</p>\n', '<p>E-shop Notino p&ocirc;sob&iacute; na trhu od r. 2004 a je bez preh&aacute;ňania jednička v online predaji parfumov a kozmetiky v strednej a v&yacute;chodnej Eur&oacute;pe. Pon&uacute;kame veľk&yacute; v&yacute;ber parfumov a kozmetiky za bezkonkurenčn&eacute; ceny, denne expedujeme 12 000 objedn&aacute;vok a ročn&yacute; obrat prevy&scaron;uje 110 mil. Eur! V&scaron;etok tovar m&aacute;me na sklade. V Bratislave je možn&yacute; aj osobn&yacute; odber.</p>\n', NULL, 5, 10, 0, 1, 'notino-sk', 1, NULL, 0, 0, 'cj', 1, '2017-02-21 00:16:24', 'sk'),
(4851192, 4856788, NULL, 'Active', 'en', 'NABYTOK-BOGART.SK', '', 'http://www.nabytok-bogart.sk/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12796701-1482317843000', '', '', '', NULL, 0, 5, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851193, 4883722, NULL, 'Active', 'en', 'DRMAX.CZ', '', 'https://www.drmax.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12838566-1486541322000', '1487527108.jpg', '<p>L&eacute;karny DrMax.cz patř&iacute; mezi nejzn&aacute;měj&scaron;&iacute; a největ&scaron;&iacute; s&iacute;t l&eacute;k&aacute;ren v Česk&eacute; Republice.</p>\n', '<p>L&eacute;karny DrMax.cz patř&iacute; mezi nejzn&aacute;měj&scaron;&iacute; a největ&scaron;&iacute; s&iacute;t l&eacute;k&aacute;ren v Česk&eacute; Republice. S Drmax.cz m&aacute;te možnost osobn&iacute;ho odběru zdarma v 396 l&eacute;k&aacute;rn&aacute;ch po cel&eacute; česk&eacute; republice. E-Shop Dr.Max je jedn&iacute;m z nejrychleji rostouc&iacute;ch na trhu a denně si na něm prohl&eacute;dne zbož&iacute; několik des&iacute;tek tis&iacute;c lid&iacute;.<br />\nNa E-Shopu lze čerpat z v&yacute;hody &scaron;ir&scaron;&iacute;ho v&yacute;běru zbož&iacute; oproti běžn&yacute;m l&eacute;k&aacute;rn&aacute;m - sortiment obsahuje přes 10 000 položek.</p>\n', '', 3, 6, 1, 1, 'drmax-cz', 1, NULL, 0, 1, 'cj', 1, '2017-02-19 22:00:00', 'cz'),
(4851194, 2612819, NULL, 'Active', 'en', 'Hotels.com APAC', '', 'http://www.hotels.com', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-10685346-1460478000000', '', '', '', NULL, 0, 6, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL);
INSERT INTO `advertiser` (`id`, `advertiser_id`, `parent_id`, `account_status`, `language`, `advertiser_name`, `advertiser_alias`, `program_url`, `relationship_status`, `mobile_supported`, `mobile_tracking_certified`, `commission_link`, `logo`, `desc_short`, `desc_full`, `desc_extra`, `cashback_customer`, `cashback_value`, `cashback_const`, `cashback_percent`, `url`, `active`, `category_id`, `is_favourite`, `show_in_menu`, `source`, `browser_panel`, `first_published`, `site_domain`) VALUES
(4851195, 3966020, NULL, 'Active', 'en', 'Lufthansa - CZ', '', 'http://lufthansa.com', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-11339850-1398256897000', '', '', '', NULL, 0, 1.5, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851196, 4802831, NULL, 'Active', 'en', 'Art Naturals', '', 'http://www.artnaturals.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851197, 4864007, NULL, 'Active', 'en', 'Naked Zebra', '', 'http://www.naked-zebra.com/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851198, 4427449, NULL, 'Active', 'en', 'Booking.com US (Private Program)', 'Booking.com', 'http://www.booking.com', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12010984', '1488844903.gif', '<p>Booking.com je bohat&aacute; nab&iacute;dka ubytov&aacute;n&iacute; po cel&eacute;m světe. R&aacute;j pro cetovatele.</p>\n', '<p>Vyb&iacute;rejte z rozs&aacute;hl&eacute; nab&iacute;dky hotelů, penzionů, hostelů a dal&scaron;&iacute;ch druhů ubytov&aacute;n&iacute; po cel&eacute;m světě. Využijte Booking.com pro prodloužen&yacute; v&iacute;kend v Česk&eacute;m r&aacute;ji nebo exotickou dovolenou.</p>\n', '<p>Cashback odměna je založena pouze na ceně&nbsp;ubytov&aacute;n&iacute;. Nevztahuje se na ž&aacute;dn&eacute; daně,&nbsp;dph, poplatky za služby nebo na ostatn&iacute; hotelov&eacute; n&aacute;klady&nbsp;(včetně restaurac&iacute; a l&aacute;zn&iacute;). Cashback odměna bude vyplacena měs&iacute;c pot&eacute;, co jste ukončili svůj pobyt.</p>\n', 2, 4, 1, 1, 'booking-com', 1, NULL, 1, 1, 'cj', 0, '2017-03-07 11:32:03', NULL),
(4851199, 4856841, NULL, 'Active', 'en', 'Bux.cz', '', 'http://www.bux.cz', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12838633', '1489051089.png', '<p>Internetov&eacute; knihkupectv&iacute; s dlouholetou prax&iacute; na česk&eacute;m trhu.</p>\n', '<p>Internetov&eacute; knihkupectv&iacute; s dlouholetou prax&iacute; na česk&eacute;m trhu. Z&aacute;kazn&iacute;ci si u n&aacute;s mohou vybrat ze &scaron;irok&eacute;ho portfolia knih, kter&eacute; obsahuje v&iacute;ce jak 55 000 titulů z různ&yacute;ch ž&aacute;nrů od beletrie až po učebnice. Denně aktualizujeme ceny u vybran&yacute;ch titulů, abychom z&aacute;kazn&iacute;kům přinesli maxim&aacute;ln&iacute; spokojenost a zaj&iacute;mav&eacute; cenov&eacute; nab&iacute;dky.<br />\nNab&iacute;z&iacute;me nejen knižn&iacute; zbož&iacute;, ale tak&eacute; doplňkov&yacute; sortiment jako jsou kalend&aacute;ře, di&aacute;ře, DVD a dal&scaron;&iacute;.<br />\nZ&aacute;kazn&iacute;ci oceňuj&iacute; předev&scaron;&iacute;m &scaron;irok&yacute; sortiment, n&iacute;zk&eacute; ceny, dopravu od 29 Kč, přehledn&yacute; web a komunikaci na&scaron;eho eshopu.</p>\n', NULL, 3.5, 7, 1, 1, 'bux-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-09 10:18:09', 'cz'),
(4851200, 4878402, NULL, 'Active', 'en', 'DIGITAL24.CZ', '', 'http://www.digital24.cz/', 'joined', 0, 0, 'http://www.dpbolvw.net/click-8106089-12851361', '1491170775.png', '<p>Digital24.cz jsou pecialist&eacute; na prodej digit&aacute;ln&iacute;ch fotoapar&aacute;tů, objektivů a dom&aacute;c&iacute;ch spotřebičů.</p>\n', '<p>Digital24.cz jsou pecialist&eacute; na prodej digit&aacute;ln&iacute;ch fotoapar&aacute;tů, objektivů a dom&aacute;c&iacute;ch spotřebičů. Velk&eacute; množstv&iacute; v&yacute;robků skladem, nejniž&scaron;&iacute; ceny na trhu, doprava a hodnotn&yacute; d&aacute;rek zdarma.&nbsp; Možnost osobn&iacute;ho odběru po cel&eacute; ČR.</p>\n', NULL, 0.75, 1.5, 1, 1, 'digital24-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-03 00:06:15', 'cz'),
(4851201, 4882378, NULL, 'Active', 'en', 'DECODOMA.CZ', '', 'https://www.decodoma.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12863120', '1489361211.jpg', '<p>decoDoma je tu pro každ&eacute;ho, kdo r&aacute;d zvelebuje svůj domov.</p>\n', '<p>decoDoma je tu pro každ&eacute;ho, kdo r&aacute;d zvelebuje svůj domov. Nab&iacute;z&iacute; produkty pro dům, zahradu i jen tak pro radost. Exkluzivně dov&aacute;ž&iacute;me z It&aacute;lie a &Scaron;panělska nap&iacute;nac&iacute; elastick&eacute; potahy na sedačky a křesla, kter&eacute; dok&aacute;ž&iacute; během minutky proměnit star&yacute; okoukan&yacute; n&aacute;bytek v &uacute;plně nov&yacute;. Nakupovat můžou z&aacute;kazn&iacute;ci z ti&scaron;těn&eacute;ho katalogu, na e-shopu nebo v kamenn&yacute;ch prodejn&aacute;ch decoDoma.</p>\n', NULL, 3, 5, 1, 1, 'decodoma-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-13 00:26:51', 'cz'),
(4851202, 4894323, NULL, 'Active', 'en', 'ROGELLI.CZ', '', 'http://www.rogelli.cz/', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12859168', '1489498434.png', '<p>Oblečen&iacute; značky&nbsp;Rogelli&nbsp;je vhodn&eacute; pro cyklistiku, spinning, běh a dal&scaron;&iacute; sporty.</p>\n', '<p>Sportovn&iacute; oblečen&iacute; od ofici&aacute;ln&iacute;ho dealera t&eacute;to značky. Sportovn&iacute; oblečen&iacute;&nbsp;ROGELLI&nbsp;je určen&eacute; pro jogging, skating, běžky, fitness, outdoor, běh, cyklistiku a voln&yacute; čas, dal&scaron;&iacute;mi doplňky jsou cyklo rukavice a cyklo br&yacute;le.</p>\n', NULL, 4, 8, 1, 1, 'rogelli-cz', 1, NULL, 0, 0, 'cj', 1, '2017-03-14 14:43:45', 'cz'),
(4851203, 4860942, NULL, 'Active', 'en', 'HAWAJ.CZ', '', 'http://www.hawaj.cz', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12853883', '1491171681.png', '<p>Hawaj.cz - baz&eacute;ny, v&iacute;řivky, trampol&iacute;ny, alt&aacute;ny, sklen&iacute;ky, sol&aacute;rn&iacute; sprchy, plynov&eacute; z&aacute;řiče, grily a dal&scaron;&iacute;</p>\n', '<p>Hawaj.cz - baz&eacute;ny, v&iacute;řivky, trampol&iacute;ny, alt&aacute;ny, sklen&iacute;ky, sol&aacute;rn&iacute; sprchy, plynov&eacute; z&aacute;řiče, grily a dal&scaron;&iacute;. Působ&iacute; na česk&eacute;m trhu již 11 let dnes je jedn&iacute;m z největ&scaron;&iacute;ch internetov&yacute;ch obchodů v dan&eacute;m segmentu zbož&iacute; v ČR. M&aacute;me zbož&iacute; v dostatečn&eacute;m množstv&iacute; na vlastn&iacute;ch skladech 2000 m2 a za jedny z nejlep&scaron;&iacute;ch cen na trhu.</p>\n', '<p>Provize nejsou vypl&aacute;ceny za n&aacute;sleduj&iacute;c&iacute; kategorie zbož&iacute;: trampol&iacute;ny, sklen&iacute;ky, alt&aacute;ny, hlin&iacute;kov&eacute; žebř&iacute;ky, nafukovac&iacute; z&aacute;bava, sportovn&iacute; potřeby, uměl&eacute; v&aacute;nočn&iacute; stromečky</p>\n', 3, 6, 0, 1, 'hawaj-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-13 00:00:13', 'cz'),
(4851204, 4878405, NULL, 'Active', 'en', 'DIGITAL24.SK', '', 'http://www.digital24.sk', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, 'sk'),
(4851205, 3812192, NULL, 'Active', 'en', '(IS) Interserver Webhosting and VPS ', '', 'https://www.interserver.net', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851206, 4731409, NULL, 'Active', 'en', 'Weekend in Italy', '', 'http://www.weekendinitaly.com/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851207, 3022407, NULL, 'Deactive', 'en', '(eUK) eUKhost Ltd', '', 'http://www.eukhost.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851208, 4332652, NULL, 'Active', 'en', '(WHUK) WebHosting UK COM Ltd.', '', 'http://www.webhosting.uk.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851209, 4777644, NULL, 'Active', 'en', 'Vitalvibe.eu', '', 'http://www.vitalvibe.eu', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851210, 4643855, NULL, 'Active', 'en', 'Snackhost - CZ-PL', '', 'http://snackhost.com/cs', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851211, 4851178, NULL, 'Active', 'en', 'Kytary.sk', '', 'https://kytary.sk/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851212, 4869035, NULL, 'Active', 'en', 'Anosd.cz', '', 'http://www.anosd.cz/', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12855452', '1491169397.jpg', '<p>ANO spořiteln&iacute; družstvo je siln&aacute;, modern&iacute; a inovativn&iacute; finančn&iacute; instituce.</p>\n', '<p>ANO spořiteln&iacute; družstvo je siln&aacute;, modern&iacute; a inovativn&iacute; finančn&iacute; instituce, kter&aacute; nab&iacute;z&iacute; sv&yacute;m členům z řad občanů, živnostn&iacute;ků a firem v Česk&eacute; republice srozumiteln&eacute; produkty. Nejv&yacute;znamněj&scaron;&iacute;m produktem je term&iacute;novan&yacute; vklad ANO PRO zhodnocen&iacute; s garanc&iacute; nejvy&scaron;&scaron;&iacute;ho &uacute;rokov&eacute;ho zhodnocen&iacute;. ANO spořiteln&iacute; družstvo obh&aacute;jilo v roce 2016 1. m&iacute;sto za Nejlep&scaron;&iacute; term&iacute;novan&yacute; vklad od serveru Banky.cz a obdrželo i oceněn&iacute; Nejlep&scaron;&iacute; finančn&iacute; produkt - 1. m&iacute;sto v kategorii Term&iacute;novan&yacute; vklad - od serveru Finparada.cz.</p>\n', NULL, 250, 500, 0, 0, 'anosd-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-02 23:43:17', 'cz'),
(4851213, 4892025, NULL, 'Active', 'en', 'LACNYJANKO.SK', '', 'http://www.lacnyjanko.sk', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851214, 4894829, NULL, 'Active', 'en', 'LAVALIERE.CZ', '', 'https://www.lavaliere.cz/', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-12877860', '1491758437.jpg', '<p>Lavaliere.cz - ručně vyr&aacute;běn&eacute; luxusn&iacute; n&aacute;ramky a n&aacute;hrdeln&iacute;ky z kvalitn&iacute;ch př&iacute;rodn&iacute;ch kamenů, polodrahokamů a prvků z chirurgick&eacute; oceli, stř&iacute;bra a zlata.</p>\n', '<p>Česk&aacute; značka Lavaliere nab&iacute;z&iacute; ručně vyr&aacute;běn&eacute; luxusn&iacute; n&aacute;ramky na ruku a n&aacute;hrdeln&iacute;ky z kvalitn&iacute;ch př&iacute;rodn&iacute;ch kamenů, polodrahokamů a prvků z chirurgick&eacute; oceli, stř&iacute;bra a zlata. Vytv&aacute;ř&iacute;me vlastn&iacute; p&aacute;nsk&eacute; a d&aacute;msk&eacute; designov&eacute; kolekce - kor&aacute;lkov&eacute; n&aacute;ramky, macram&eacute; n&aacute;ramky, n&aacute;ramky s kotvami (anchor) a n&aacute;ramky ve stylu bangles. Nejsme přeprodejci č&iacute;nsk&yacute;ch napodobenin. V&yacute;roba na&scaron;ich &scaron;perků prob&iacute;h&aacute; v chr&aacute;něn&eacute; d&iacute;lně v Jablonci nad Nisou a navazuje na tradici &scaron;perkařstv&iacute; v Jablonci nad Nisou</p>\n', NULL, 3, 5, 1, 1, 'lavaliere-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 19:20:37', 'cz'),
(4851215, 1911025, NULL, 'Active', 'en', 'Accorhotels.com Europe & ROW', '', 'http://www.accorhotels.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851216, 1056026, NULL, 'Active', 'en', 'Fox Rent A Car', '', 'https://www.foxrentacar.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851217, 1097361, NULL, 'Active', 'en', 'CyberLink Affiliate Program', '', 'http://www.cyberlink.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851218, 1475632, NULL, 'Active', 'en', 'EntirelyPets', '', 'http://www.entirelypets.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851219, 1971416, NULL, 'Active', 'en', 'Vital Imagery', '', 'http://www.vitalimagery.com/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851220, 2326712, NULL, 'Active', 'en', 'Trusted Tours and Attractions', '', 'http://www.trustedtours.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851221, 3088068, NULL, 'Active', 'en', 'Best of Orlando', '', 'http://www.bestoforlando.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851222, 3323178, NULL, 'Active', 'en', 'Liquid Web Preferred Partner Program', '', 'http://www.liquidweb.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851223, 3386711, NULL, 'Active', 'en', 'Green Man Gaming', '', 'http://www.greenmangaming.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851224, 3656277, NULL, 'Active', 'en', 'eEuroparts.com', '', 'http://www.eEuroparts.com', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-11302446?cm_mmc=CJ-_-3425505-_-8106089-_-Saab%20900%20Parts', '1492032928.png', '<p>eEuroparts.com - rychle rostouc&iacute; online prodejce autod&iacute;lů</p>\n', '<p>eEuroparts.com je rychle rostouc&iacute; online prodejce autod&iacute;lů s t&eacute;měř 20 000 d&iacute;ly v nab&iacute;dce. Nab&iacute;z&iacute; d&iacute;ly pro BMW, Saab, Volvo and Mini. Chyst&aacute; se brzy roz&scaron;&iacute;řit nab&iacute;dku o dal&scaron;&iacute; evropsk&eacute; značky. Nab&iacute;z&iacute; skvěl&yacute; z&aacute;kaznick&yacute; servis,&nbsp; rychl&eacute; a bezplatn&eacute; dod&aacute;n&iacute;, produkty s vysokou kvalitou a n&iacute;zk&eacute; ceny.</p>\n', '', 3, 6, 1, 1, 'eeuroparts-com', 1, NULL, 0, 0, 'cj', 1, '2017-04-12 23:35:28', NULL),
(4851225, 4502328, NULL, 'Active', 'de', 'OTTO - SK', '', 'http://otto.sk', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851226, 4518745, NULL, 'Active', 'en', 'Kinguin', '', 'http://www.kinguin.net/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851227, 4625944, NULL, 'Active', 'de', 'Sheego SK', '', 'http://www.sheego.sk', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12530765', '1491771946.png', '<p>SHEEGO - d&aacute;msk&aacute; m&oacute;da od velikosti 40</p>\n', '<p>SHEEGO nen&iacute; jen online obchod. S inspiracemi můžete sledovat aktu&aacute;ln&iacute; m&oacute;dn&iacute; trendy a naj&iacute; styl, kter&yacute; v&aacute;m nejl&eacute;pe vyhoduvje od velikosti 40 až do velikosti 58.</p>\n', NULL, 6, 12, 1, 1, 'sheego-sk', 1, NULL, 0, 0, 'cj', 1, '2017-04-09 23:05:46', 'sk'),
(4851228, 4686326, NULL, 'Active', 'de', 'Paragon Software Group', '', 'http://www.paragon-software.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851229, 4727627, NULL, 'Active', 'en', 'Suntransfers.com', '', 'http://http://www.suntransfers.com/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851230, 4765232, NULL, 'Active', 'en', 'TRAVELIST.sk', '', 'https://travelist.sk/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851231, 4802700, NULL, 'Active', 'en', 'Vivid Racing', '', 'https://www.vividracing.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851232, 4811174, NULL, 'Active', 'en', 'VIP Electronic Cigarette', '', 'https://www.vipelectroniccigarette.co.uk', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851233, 4887633, NULL, 'Active', 'en', 'PARYS.CZ', '', 'http://www.parys.cz/', 'joined', 0, 0, 'http://www.kqzyfj.com/click-8106089-12889240', '1491947660.png', '<p>Parys.cz - ryb&aacute;řsk&eacute; potřeby</p>\n', '<p>E-shop Parys.cz působ&iacute; na česk&eacute;m trhu již 15 let. D&iacute;ky tomu patř&iacute; mezi největ&scaron;&iacute; a nejobl&iacute;beněj&scaron;&iacute; ryb&aacute;řsk&eacute; e-shopy v Česk&eacute; republice. Nab&iacute;z&iacute;me ryb&aacute;řsk&eacute; potřeby t&eacute;měř pro v&scaron;echny druhy rybolovu (kaprařina, př&iacute;vlač, feeder, plavačka, mořsk&yacute; rybolov a sumcařina). Vět&scaron;inu sortimentu drž&iacute;me skladem, proto drtivou vět&scaron;inu objedn&aacute;vek doruč&iacute;me nejpozději do 2. pracovn&iacute;ho dne. V na&scaron;em sortimentu naleznete v&scaron;echny obl&iacute;ben&eacute; ryb&aacute;řsk&eacute; značky. Celkem v&iacute;ce než 30 000 produktů a přes 170 značek.</p>\n', '', 4, 7, 0, 1, 'parys-cz', 1, NULL, 0, 0, 'cj', 1, '2017-04-11 23:54:20', 'cz'),
(4851234, 4887634, NULL, 'Active', 'en', 'PARYS.SK', '', 'http://www.parys.sk', 'joined', 0, 0, 'http://www.tkqlhce.com/click-8106089-12890812', '1491947791.png', '<p>Parys.sk - ryb&aacute;rsk&yacute; e-shop</p>\n', '<p>E-shop Parys.sk bol spusten&yacute; v septembri 2016. Po 15 rokoch p&ocirc;sobenia v Českej republike sme sa rozhodli expandovať na Slovensko. V Českej republike patr&iacute; n&aacute;&scaron; e-shop medzi najv&auml;č&scaron;ie a najobľ&uacute;benej&scaron;ie ryb&aacute;rske e-shopy.<br />\nVstup na Slovensko prebehol nad na&scaron;e očak&aacute;vania a z&aacute;kazn&iacute;ci si n&aacute;&scaron; e-shop veľmi obľ&uacute;bili. Pon&uacute;kame ryb&aacute;rske potreby takmer pre v&scaron;etky druhy rybolovu (kaprarina, pr&iacute;vlač, feeder, plavačku, morsk&yacute; rybolov a lov sumcov).<br />\nV&auml;č&scaron;inu sortimentu drž&iacute;me skladom, preto drviv&uacute; v&auml;č&scaron;inu objedn&aacute;vok doruč&iacute;me najnesk&ocirc;r do 2. pracovn&eacute;ho dňa. V na&scaron;om sortimente n&aacute;jdete v&scaron;etky obľ&uacute;ben&eacute; ryb&aacute;rske značky. Viac ako 30 000 produktov a cez 170 značiek.</p>\n', '', 4, 7, 0, 1, '', 1, NULL, 0, 0, 'cj', 1, '2017-04-11 23:56:31', 'sk'),
(4851235, 904879, NULL, 'Active', 'en', 'eBooks.com', '', 'http://www.ebooks.com', 'joined', 0, 0, 'http://www.jdoqocy.com/click-8106089-10752759', '1491947330.jpg', '<p>eBooks.com - popul&aacute;rn&iacute; obchod s eknihami</p>\n', '<p>eBooks.com je popul&aacute;rn&iacute; obchod s eknihami s velk&yacute;m v&yacute;běrem s předn&iacute;mi světov&yacute;mi autory.</p>\n', '', 4, 8, 1, 1, 'ebooks-com', 1, NULL, 0, 0, 'cj', 1, '2017-04-11 23:48:50', NULL),
(4851236, 4257305, NULL, 'Active', 'en', 'AVAST Software', '', 'https://www.avast.com', 'joined', 0, 0, 'http://www.anrdoezrs.net/click-8106089-12901051', '1491860790.jpg', '<p>Avast Software je nejdůvěryhodněj&scaron;&iacute; v&yacute;robce mobiln&iacute; a PC ochrany na světě.</p>\n', '<p>Avast Software je nejdůvěryhodněj&scaron;&iacute; v&yacute;robce mobiln&iacute; a PC ochrany na světě. Ochraňuje v&iacute;ce než 230 mili&oacute;nů lid&iacute;. Již přes 25 let patř&iacute; mezi &scaron;pičky v oboru. Nab&iacute;z&iacute; ochranu pro PC, Mac i Android.</p>\n', '', 30, 50, 0, 1, 'avast', 1, NULL, 0, 0, 'cj', 1, '2017-04-10 23:46:30', NULL),
(4851237, 2538056, NULL, 'Active', 'en', 'SPAMfighter', '', 'http://www.spamfighter.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851238, 4799066, NULL, 'Active', 'en', 'Oberlo Affiliate program', '', 'https://www.oberlo.com/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851239, 2239542, NULL, 'Active', 'en', 'Logo Design - The Logo Company', '', 'http://thelogocompany.net', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851240, 2725075, NULL, 'Active', 'en', 'OneTravel', '', 'http://www.onetravel.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851241, 2288710, NULL, 'Active', 'en', 'TicketNetwork', '', 'http://www.ticketnetwork.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851242, 4009935, NULL, 'Active', 'en', 'ParisCityVision.com', '', 'http://www.pariscityvision.com/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851243, 1561202, NULL, 'Active', 'en', 'Encore Software - Broderbund, Punch, Hoyle & more', '', 'http://www.punchsoftware.com, http://broderbund.com; http://hoylegaming.com; http://punchCAD.com;  http://viva-media.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851244, 3252578, NULL, 'Active', 'en', 'HalloweenCostumes.com', '', 'http://www.halloweencostumes.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851245, 1599858, NULL, 'Active', 'en', 'SuperJeweler', '', 'http://www.superjeweler.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851246, 3300852, NULL, 'Active', 'en', 'Lightake.com', '', 'http://www.lightake.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851247, 3773223, NULL, 'Active', 'en', 'SheIn', '', 'http://www.SheIn.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851248, 1154360, NULL, 'Active', 'en', 'fabric.com', '', 'http://www.fabric.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851249, 3084302, NULL, 'Active', 'en', 'Contiki', '', 'http://contiki.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851250, 3273383, NULL, 'Active', 'en', 'SOS Online Backup', '', 'http://www.sosonlinebackup.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851251, 4740515, NULL, 'Active', 'en', 'RoseGal', '', 'http://www.rosegal.com/', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851252, 4895962, NULL, 'Active', 'en', 'HealthmateForever', '', 'http://www.healthmateforever.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851253, 2005415, NULL, 'Active', 'en', 'Parallels', '', 'http://www.parallels.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL),
(4851254, 1530444, NULL, 'Active', 'en', 'Hotel Group Reservations by HotelPlanner.com', '', 'http://www.HotelPlanner.com', 'joined', 0, 0, '', '', '', '', NULL, 0, 0, 1, 1, '', 0, NULL, 0, 0, 'cj', 1, NULL, NULL);

--
-- Spouště `advertiser`
--
DELIMITER $$
CREATE TRIGGER `advertiser_bi` BEFORE INSERT ON `advertiser`
 FOR EACH ROW BEGIN

 IF (NEW.active = 1 AND NEW.first_published IS NULL) THEN
      SET NEW.first_published = NOW();
 END IF;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `advertiser_bu` BEFORE UPDATE ON `advertiser`
 FOR EACH ROW BEGIN

 IF (NEW.active = 1 AND NEW.first_published IS NULL) THEN
      SET NEW.first_published = NOW();
 END IF;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabulky `advertiser_category`
--

CREATE TABLE IF NOT EXISTS `advertiser_category` (
  `advertiser_id` int(10) unsigned NOT NULL,
  `category_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `advertiser_category`
--

INSERT INTO `advertiser_category` (`advertiser_id`, `category_id`) VALUES
(2270432, 2),
(2270432, 3),
(2270432, 6),
(2270432, 7),
(2270432, 9),
(2270432, 10),
(2538890, 34),
(2983194, 2),
(2983194, 12),
(2983194, 13),
(2983194, 16),
(2983194, 26),
(3007908, 2),
(3007908, 10),
(3007908, 12),
(3007908, 13),
(3007908, 14),
(3278515, 11),
(3304249, 13),
(3304249, 26),
(3342166, 3),
(3342166, 6),
(3342166, 7),
(3342166, 9),
(3342166, 10),
(3342166, 39),
(3409221, 11),
(3662652, 5),
(3662652, 30),
(3662652, 31),
(3662813, 8),
(3662813, 22),
(3707193, 5),
(3716953, 2),
(3716953, 4),
(3716953, 5),
(3716953, 8),
(3716953, 10),
(3716953, 30),
(3716953, 31),
(3716953, 36),
(3769367, 5),
(3772579, 5),
(3773527, 5),
(3781746, 5),
(3792995, 5),
(3803104, 8),
(3803104, 41),
(3804135, 5),
(3846628, 5),
(3855115, 5),
(3857051, 5),
(3924040, 5),
(3930502, 5),
(3936405, 5),
(3936405, 31),
(3946019, 5),
(3949914, 2),
(3949914, 15),
(4017417, 5),
(4017417, 31),
(4038908, 3),
(4038908, 10),
(4038908, 32),
(4038908, 33),
(4038908, 34),
(4051258, 2),
(4051258, 15),
(4052024, 4),
(4065356, 7),
(4065356, 16),
(4065356, 27),
(4067754, 8),
(4067754, 41),
(4098645, 6),
(4098645, 36),
(4120215, 2),
(4120215, 12),
(4120215, 13),
(4120215, 14),
(4132083, 2),
(4132083, 7),
(4132083, 16),
(4132084, 7),
(4132084, 16),
(4132084, 27),
(4136467, 2),
(4136467, 15),
(4155694, 6),
(4201179, 3),
(4201179, 32),
(4201179, 34),
(4213975, 10),
(4230368, 8),
(4230368, 41),
(4238401, 3),
(4238401, 33),
(4267436, 6),
(4267436, 29),
(4285555, 5),
(4285555, 31),
(4285556, 5),
(4285556, 8),
(4285556, 19),
(4285556, 22),
(4285556, 31),
(4307021, 10),
(4307021, 35),
(4316775, 11),
(4316775, 32),
(4322332, 6),
(4327375, 2),
(4327375, 12),
(4327375, 13),
(4327375, 14),
(4327375, 15),
(4327375, 16),
(4327375, 17),
(4344860, 2),
(4344860, 10),
(4344860, 13),
(4344860, 17),
(4344860, 37),
(4352963, 2),
(4352963, 15),
(4356323, 10),
(4356323, 39),
(4371373, 2),
(4371373, 10),
(4371373, 12),
(4371373, 13),
(4371373, 26),
(4371373, 28),
(4379461, 8),
(4379461, 18),
(4384608, 2),
(4384608, 6),
(4384608, 12),
(4384608, 13),
(4384608, 14),
(4384608, 15),
(4384608, 17),
(4384608, 36),
(4385202, 2),
(4385202, 8),
(4385202, 10),
(4385202, 15),
(4385202, 35),
(4386089, 11),
(4400192, 2),
(4400192, 12),
(4400192, 13),
(4400192, 14),
(4400192, 15),
(4400192, 37),
(4429232, 2),
(4429232, 12),
(4429232, 13),
(4429232, 15),
(4429232, 26),
(4429232, 28),
(4429877, 4),
(4429877, 6),
(4429877, 9),
(4429877, 24),
(4429877, 40),
(4432968, 35),
(4432968, 37),
(4432968, 39),
(4438849, 6),
(4438849, 29),
(4442115, 4),
(4444762, 2),
(4444762, 12),
(4444762, 13),
(4444762, 15),
(4444863, 7),
(4460474, 6),
(4460474, 10),
(4460474, 29),
(4460474, 35),
(4460474, 36),
(4460474, 37),
(4460474, 39),
(4460474, 42),
(4460474, 44),
(4482699, 2),
(4482699, 12),
(4482699, 13),
(4482699, 26),
(4489910, 4),
(4489910, 10),
(4502358, 2),
(4502358, 10),
(4502358, 12),
(4502358, 13),
(4502358, 14),
(4502358, 15),
(4502358, 16),
(4502358, 17),
(4502358, 26),
(4502358, 37),
(4502358, 39),
(4503047, 2),
(4503047, 13),
(4503047, 17),
(4506402, 4),
(4513714, 2),
(4513714, 12),
(4513714, 13),
(4513714, 14),
(4513714, 24),
(4513714, 28),
(4514887, 2),
(4514887, 15),
(4515416, 6),
(4515416, 36),
(4520838, 8),
(4520838, 18),
(4556917, 10),
(4556917, 39),
(4571294, 2),
(4571294, 10),
(4571294, 39),
(4577514, 8),
(4577514, 18),
(4597444, 7),
(4597444, 35),
(4601714, 36),
(4608876, 3),
(4608876, 32),
(4608876, 33),
(4608876, 34),
(4609486, 6),
(4609486, 10),
(4609486, 36),
(4609486, 39),
(4612000, 2),
(4612000, 6),
(4612000, 10),
(4612000, 12),
(4612000, 13),
(4612000, 14),
(4612000, 15),
(4612000, 16),
(4612000, 17),
(4612000, 26),
(4612000, 36),
(4612000, 37),
(4612007, 2),
(4612007, 6),
(4612007, 12),
(4612007, 13),
(4612007, 14),
(4612007, 15),
(4612007, 16),
(4612007, 17),
(4612007, 26),
(4612007, 27),
(4612007, 36),
(4612012, 2),
(4612012, 3),
(4612012, 7),
(4612012, 8),
(4612012, 9),
(4612012, 10),
(4612012, 12),
(4612012, 13),
(4612012, 14),
(4612012, 15),
(4612012, 16),
(4612012, 24),
(4612012, 27),
(4612012, 28),
(4612012, 32),
(4612012, 33),
(4612012, 34),
(4616713, 21),
(4620184, 10),
(4620184, 11),
(4620184, 35),
(4625942, 2),
(4625942, 13),
(4625942, 15),
(4625942, 16),
(4631233, 2),
(4631233, 7),
(4631233, 12),
(4631233, 13),
(4631233, 14),
(4631233, 15),
(4631233, 16),
(4631322, 2),
(4631322, 12),
(4631322, 13),
(4631322, 14),
(4631322, 15),
(4631322, 16),
(4631322, 28),
(4634072, 2),
(4634072, 12),
(4634072, 13),
(4634072, 14),
(4634072, 15),
(4634072, 26),
(4634072, 28),
(4634073, 2),
(4634073, 12),
(4634073, 13),
(4634073, 14),
(4634073, 15),
(4634073, 24),
(4634073, 26),
(4634073, 28),
(4639051, 2),
(4639051, 4),
(4639051, 9),
(4639051, 14),
(4639051, 15),
(4639051, 24),
(4639051, 44),
(4643822, 2),
(4643822, 12),
(4643822, 13),
(4643822, 15),
(4643822, 16),
(4643822, 17),
(4645120, 10),
(4648080, 2),
(4648080, 10),
(4648080, 37),
(4648084, 2),
(4648084, 10),
(4687529, 3),
(4687529, 32),
(4687529, 33),
(4687529, 34),
(4687529, 37),
(4687530, 3),
(4687530, 32),
(4687530, 33),
(4687530, 34),
(4687530, 37),
(4687536, 10),
(4687536, 37),
(4689086, 2),
(4691071, 2),
(4691071, 12),
(4691071, 13),
(4691071, 14),
(4691071, 15),
(4691071, 17),
(4691071, 37),
(4694431, 10),
(4694431, 35),
(4697950, 11),
(4711521, 11),
(4711524, 11),
(4727030, 12),
(4727030, 13),
(4727030, 17),
(4727030, 36),
(4731601, 8),
(4731601, 18),
(4731700, 2),
(4731700, 3),
(4731700, 6),
(4731700, 10),
(4731700, 17),
(4731700, 35),
(4731700, 38),
(4733564, 2),
(4733564, 12),
(4733564, 13),
(4733564, 17),
(4733564, 28),
(4736194, 2),
(4736194, 3),
(4736194, 7),
(4736194, 8),
(4736194, 9),
(4736194, 10),
(4736194, 12),
(4736194, 13),
(4736194, 14),
(4736194, 15),
(4736194, 16),
(4736194, 17),
(4736194, 24),
(4736194, 27),
(4736194, 28),
(4736194, 32),
(4736194, 33),
(4736194, 34),
(4738082, 2),
(4738082, 12),
(4738082, 13),
(4738082, 28),
(4742199, 3),
(4742199, 9),
(4742199, 10),
(4742199, 32),
(4742199, 33),
(4742199, 34),
(4742199, 35),
(4742200, 9),
(4745707, 12),
(4745707, 26),
(4747013, 8),
(4747013, 41),
(4754392, 9),
(4754392, 10),
(4754392, 44),
(4754401, 3),
(4754401, 32),
(4754411, 36),
(4754411, 40),
(4754417, 7),
(4754417, 16),
(4754441, 2),
(4754441, 12),
(4754441, 13),
(4754441, 14),
(4754441, 24),
(4754441, 28),
(4754451, 2),
(4754451, 12),
(4754451, 13),
(4754451, 14),
(4754451, 28),
(4754455, 2),
(4754455, 12),
(4754455, 13),
(4754455, 14),
(4754455, 15),
(4754455, 28),
(4754462, 10),
(4754462, 35),
(4754462, 37),
(4754462, 39),
(4754470, 2),
(4754470, 12),
(4754470, 13),
(4754470, 14),
(4754470, 15),
(4754470, 28),
(4754475, 2),
(4754475, 12),
(4754475, 13),
(4754475, 14),
(4754475, 24),
(4754475, 28),
(4758292, 10),
(4758292, 37),
(4758296, 10),
(4758296, 42),
(4766423, 20),
(4769945, 6),
(4769945, 36),
(4770057, 19),
(4770057, 22),
(4770601, 8),
(4786083, 10),
(4786083, 42),
(4786110, 7),
(4786110, 16),
(4786110, 35),
(4805149, 4),
(4805149, 10),
(4805198, 10),
(4805198, 38),
(4806885, 10),
(4806885, 11),
(4806885, 35),
(4828144, 37),
(4829964, 4),
(4829964, 40),
(4836093, 2),
(4836093, 3),
(4836093, 6),
(4836093, 7),
(4836093, 10),
(4836093, 35),
(4836093, 39),
(4846352, 2),
(4846352, 12),
(4846352, 13),
(4846352, 17),
(4846352, 28),
(4846918, 2),
(4846918, 7),
(4846918, 12),
(4846918, 13),
(4846918, 14),
(4846918, 15),
(4846918, 16),
(4851127, 7),
(4851131, 7),
(4851152, 2),
(4851152, 10),
(4851152, 12),
(4851152, 13),
(4851152, 17),
(4851152, 36),
(4851152, 37),
(4851152, 39),
(4851153, 2),
(4851153, 6),
(4851153, 7),
(4851153, 9),
(4851153, 10),
(4851153, 12),
(4851153, 13),
(4851153, 14),
(4851153, 15),
(4851153, 16),
(4851153, 17),
(4851153, 26),
(4851153, 27),
(4851153, 35),
(4851153, 36),
(4851153, 37),
(4851153, 38),
(4851153, 39),
(4851154, 2),
(4851154, 12),
(4851154, 13),
(4851154, 14),
(4851154, 15),
(4851154, 17),
(4851157, 6),
(4851157, 36),
(4851158, 2),
(4851158, 17),
(4851159, 6),
(4851159, 29),
(4851160, 2),
(4851160, 17),
(4851165, 2),
(4851165, 12),
(4851165, 13),
(4851165, 14),
(4851165, 15),
(4851167, 2),
(4851175, 3),
(4851175, 10),
(4851175, 32),
(4851175, 33),
(4851175, 34),
(4851175, 39),
(4851177, 10),
(4851177, 42),
(4851178, 10),
(4851178, 35),
(4851178, 37),
(4851178, 39),
(4851181, 37),
(4851183, 2),
(4851183, 15),
(4851184, 2),
(4851184, 15),
(4851186, 29),
(4851187, 2),
(4851187, 36),
(4851190, 2),
(4851190, 12),
(4851190, 13),
(4851190, 28),
(4851191, 36),
(4851193, 29),
(4851198, 8),
(4851198, 19),
(4851198, 22),
(4851199, 38),
(4851200, 3),
(4851200, 10),
(4851200, 33),
(4851200, 35),
(4851201, 10),
(4851201, 35),
(4851201, 37),
(4851201, 39),
(4851202, 7),
(4851202, 10),
(4851202, 16),
(4851202, 35),
(4851203, 7),
(4851203, 10),
(4851203, 35),
(4851212, 11),
(4851214, 2),
(4851214, 13),
(4851214, 17),
(4851227, 2),
(4851227, 13),
(4851227, 15),
(4851227, 16),
(4851233, 10),
(4851233, 35),
(4851234, 10),
(4851234, 35),
(4851235, 38),
(4851236, 34);

-- --------------------------------------------------------

--
-- Struktura tabulky `advertiser_event`
--

CREATE TABLE IF NOT EXISTS `advertiser_event` (
  `id` int(10) unsigned NOT NULL,
  `advertiser_id` int(10) unsigned NOT NULL,
  `url` varchar(1024) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `published_from` datetime DEFAULT NULL,
  `published_to` datetime DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(49) NOT NULL,
  `url` varchar(1024) CHARACTER SET utf32 DEFAULT NULL,
  `desc` varchar(199) DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `category`
--

INSERT INTO `category` (`id`, `title`, `url`, `desc`, `parent_id`, `active`) VALUES
(2, 'Móda', 'moda', NULL, NULL, 1),
(3, 'Elektro', 'elektro', NULL, NULL, 1),
(4, 'Jídlo', 'jidlo', NULL, NULL, 1),
(5, 'Slevové portály', 'slevove-portaly', NULL, NULL, 1),
(6, 'Krása a zdraví', 'krasa-a-zdravi', NULL, NULL, 1),
(7, 'Sport', 'sport', NULL, NULL, 1),
(8, 'Cestování', 'cestovani', NULL, NULL, 1),
(9, 'Děti', 'deti', NULL, NULL, 1),
(10, 'Domácnost a hobby', 'domacnost-a-hobby', '', NULL, 1),
(11, 'Finance a služby', 'finance-a-sluzby', NULL, NULL, 1),
(12, 'Pánská móda', 'panska-moda', '', 2, 1),
(13, 'Dámská móda', 'damska-moda', '', 2, 1),
(14, 'Dětská móda', 'detska-moda', '', 2, 1),
(15, 'Obuv', 'obuv', '', 2, 1),
(16, 'Sportovní oblečení', 'sportovni-obleceni', '', 2, 1),
(17, 'Šperky a doplňky', 'sperky-a-doplnky', '', 2, 1),
(18, 'Cestovní kanceláře', 'cestovni-kancelare', '', 8, 1),
(19, 'Hotely a ubytování', 'hotely-a-ubytovani', '', 8, 1),
(20, 'Letenky', 'letenky', '', 8, 1),
(21, 'Půjčovny aut', 'pujcovny-aut', '', 8, 1),
(22, 'Ubytování a hotely', 'ubytovani-a-hotely', '', 8, 1),
(24, 'Dětská móda', 'detska-moda', '', 2, 1),
(26, 'Spodní prádlo', 'spodni-pradlo', '', 2, 1),
(27, 'Sportovní oblečení', 'sportovni-obleceni', '', 2, 1),
(28, 'Dámská a pánská móda', 'damska-a-panska-moda', '', 2, 1),
(29, 'Lékárny', 'lekarny', '', 6, 1),
(30, 'Wellness pobyty', 'wellness-pobyty', '', 8, 1),
(31, 'Slevové portály', 'slevove-portaly', '', 8, 1),
(32, 'Mobilní telefony', 'mobilni-telefony', '', 3, 1),
(33, 'Domácí spotřebiče', 'domaci-spotrebice', '', 3, 1),
(34, 'Počítače', 'pocitace', '', 3, 1),
(35, 'Hobby', 'hobby', '', 10, 1),
(36, 'Kosmetika a parfémy', 'kosmetika-a-parfemy', '', 6, 1),
(37, 'Nábytek a bytové doplňky', 'nabytek-a-bytove-doplnky', '', 10, 1),
(38, 'Knihy a hudba', 'knihy-a-hudba', '', 10, 1),
(39, 'Domácí potřeby', 'www.vydelavejnakupem.cz/kategorie/domaci-potreby', '', 10, 1),
(40, 'Potraviny', 'potraviny', '', 4, 1),
(41, 'Zážitky', 'zazitky', '', 8, 1),
(42, 'Domácí mazlíčci', 'domaci-mazlicci', '', 10, 1),
(43, 'Ostatní', 'ostatni', '', NULL, 0),
(44, 'Hracky', 'hracky', '', 9, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `commission_log`
--

CREATE TABLE IF NOT EXISTS `commission_log` (
  `id` int(10) unsigned NOT NULL,
  `action_status` enum('new','locked','extended','closed') NOT NULL,
  `action_type` enum('bonus','click','impression','sale','lead','advanced sale','advanced lead','performance incentive') NOT NULL,
  `aid` int(10) unsigned NOT NULL,
  `commission_id` bigint(20) unsigned NOT NULL COMMENT 'id provize',
  `country` varchar(16) NOT NULL COMMENT 'kód země',
  `event_date` datetime NOT NULL,
  `locking_date` date NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL COMMENT 'číslo objednávky',
  `original` tinyint(1) NOT NULL,
  `original_action_id` bigint(20) unsigned NOT NULL,
  `posting_date` datetime NOT NULL,
  `website_id` int(10) unsigned NOT NULL COMMENT 'id stránky, kde je reklama použita',
  `action_tracker_id` int(10) unsigned NOT NULL,
  `action_tracker_name` varchar(512) NOT NULL,
  `advertiser_id` int(10) unsigned DEFAULT NULL COMMENT 'ID advertiser z nasi DB',
  `advertiser_name` varchar(512) NOT NULL COMMENT 'jméno inzerenta',
  `commission_amount` double NOT NULL COMMENT 'výše provize',
  `order_discount` double NOT NULL COMMENT 'sleva na objednávce',
  `sid` varchar(250) NOT NULL COMMENT 'náš identifikátor',
  `sale_amount` double NOT NULL COMMENT 'cena celé objednávky',
  `cid` int(10) unsigned NOT NULL COMMENT 'ID advertiser z CJ'
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `commission_log`
--

INSERT INTO `commission_log` (`id`, `action_status`, `action_type`, `aid`, `commission_id`, `country`, `event_date`, `locking_date`, `order_id`, `original`, `original_action_id`, `posting_date`, `website_id`, `action_tracker_id`, `action_tracker_name`, `advertiser_id`, `advertiser_name`, `commission_amount`, `order_discount`, `sid`, `sale_amount`, `cid`) VALUES
(27, 'closed', 'sale', 11422636, 2018101494, 'CZ', '2016-07-20 02:59:59', '2016-09-10', 1026793139, 0, 1739777803, '2016-07-20 04:00:19', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.28, 0, 'test', 5.73, 0),
(28, 'closed', 'sale', 11422636, 2018107294, 'CZ', '2016-07-20 03:10:27', '2016-09-10', 1026793635, 0, 1739783182, '2016-07-20 04:00:59', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.14, 0, 'test', 2.85, 0),
(31, 'closed', 'sale', 11422636, 2025202377, 'CZ', '2016-08-08 03:06:41', '2016-09-10', 1027467829, 0, 1746359885, '2016-08-08 04:00:19', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.23, 0, '', 4.77, 0),
(32, 'closed', 'sale', 12001535, 2050705229, 'CZ', '2016-10-18 01:51:57', '2016-11-10', 1030473339, 0, 1769911897, '2016-10-18 02:30:29', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0, 0, 'test', 0, 0),
(33, 'closed', 'sale', 11422636, 2053879596, 'CZ', '2016-10-27 01:39:04', '2016-11-10', 1030911963, 0, 1772911111, '2016-10-27 02:30:13', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.23, 0, 'petrik', 4.63, 0),
(34, 'locked', 'sale', 10881559, 2058179385, '', '2016-10-31 00:00:00', '2016-12-30', 79515553758591, 0, 1776903468, '2016-11-07 19:02:11', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.15, 0, 'test', 2.08, 0),
(35, 'new', 'sale', 12574854, 2061894919, '', '2016-11-10 00:00:00', '2017-01-09', 79530342418591, 0, 1780378571, '2016-11-16 23:31:10', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.11, 0, 'tom', 1.62, 0),
(36, 'closed', 'sale', 11417684, 2068355784, 'CZ', '2016-11-28 02:11:25', '2016-12-10', 1032611681, 0, 1786623236, '2016-11-28 03:00:47', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.27, 0, '741cc9583e98a547e6e3be757fdca8c1d6dcfa0e', 5.3, 0),
(37, 'new', 'sale', 10578899, 2070871737, '', '2016-11-26 00:00:00', '2017-01-25', 80226786318591, 0, 1788989050, '2016-12-01 00:01:11', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.27, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 3.91, 0),
(38, 'new', 'sale', 10578899, 2070895116, '', '2016-11-28 00:00:00', '2017-01-27', 80226786338591, 0, 1788994855, '2016-12-01 00:04:05', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.06, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 0.81, 0),
(39, 'new', 'sale', 10578899, 2072102990, '', '2016-12-02 00:00:00', '2017-01-31', 80116484250370, 0, 1790008344, '2016-12-03 01:30:23', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.56, 0, '477ffc2291dd5745d63f4772393fa1d21ea24ca5', 7.94, 0),
(40, 'new', 'sale', 10578899, 2072102992, '', '2016-12-02 00:00:00', '2017-01-31', 80116484260370, 0, 1790008346, '2016-12-03 01:30:23', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.06, 0, '477ffc2291dd5745d63f4772393fa1d21ea24ca5', 0.89, 0),
(41, 'new', 'sale', 11417684, 2075288248, 'CZ', '2016-12-08 03:56:51', '2017-01-10', 1033105011, 0, 1792739571, '2016-12-08 05:00:07', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.23, 0, 'bcef6cc02dcb2eb931747664393bb7a8841fefb7', 4.63, 0),
(42, 'new', 'sale', 10578899, 2075891611, '', '2016-12-08 00:00:00', '2017-02-06', 80226786378591, 0, 1793215419, '2016-12-09 01:30:29', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.46, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 6.51, 0),
(43, 'new', 'sale', 10578899, 2075891614, '', '2016-12-08 00:00:00', '2017-02-06', 80226786358591, 0, 1793215422, '2016-12-09 01:30:29', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.29, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 4.12, 0),
(44, 'new', 'sale', 10578899, 2076453538, '', '2016-12-09 00:00:00', '2017-02-07', 80226786348591, 0, 1793688092, '2016-12-10 01:30:13', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.63, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 8.94, 0),
(45, 'new', 'sale', 10578899, 2077080683, '', '2016-12-10 00:00:00', '2017-02-08', 80172214078591, 0, 1794133868, '2016-12-11 01:30:18', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 2.56, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 36.59, 2270432),
(46, 'new', 'sale', 10578899, 2078809062, '', '2016-12-13 00:00:00', '2017-02-11', 80226786408591, 0, 1795753769, '2016-12-14 01:31:19', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.83, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 11.84, 2270432),
(47, 'locked', 'sale', 11417684, 2080146709, 'CZ', '2016-12-16 10:49:12', '2017-01-10', 1033603443, 0, 1797019688, '2016-12-16 11:31:43', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.16, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 3.19, 4052024),
(48, 'new', 'sale', 10578899, 2080426922, '', '2016-12-16 00:00:00', '2017-02-14', 80226786388591, 0, 1797277719, '2016-12-17 01:30:18', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.11, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 1.54, 2270432),
(49, 'new', 'sale', 10578899, 2080427074, '', '2016-12-16 00:00:00', '2017-02-14', 80226786418591, 0, 1797277935, '2016-12-17 01:30:20', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.6, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 8.54, 2270432),
(51, 'new', 'sale', 12574854, 2080881865, '', '2016-12-17 00:00:00', '2017-02-15', 79642285944161, 0, 1797714037, '2016-12-18 01:30:24', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.1, 0, 'filiphlad', 1.38, 2270432),
(53, 'new', 'sale', 10578899, 2081348770, '', '2016-12-18 00:00:00', '2017-02-16', 80116484240370, 0, 1798163397, '2016-12-19 01:31:31', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.86, 0, '477ffc2291dd5745d63f4772393fa1d21ea24ca5', 12.3, 2270432),
(54, 'new', 'sale', 10578899, 2081879885, '', '2016-12-19 00:00:00', '2017-02-17', 80282950478591, 0, 1798643443, '2016-12-20 01:30:35', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.4, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 5.76, 2270432),
(55, 'new', 'sale', 10578899, 2081879886, '', '2016-12-19 00:00:00', '2017-02-17', 80282950488591, 0, 1798643442, '2016-12-20 01:30:35', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.52, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 7.37, 2270432),
(56, 'closed', 'sale', 11417684, 2085162229, 'CZ', '2016-12-28 04:26:25', '2017-01-10', 1034254691, 0, 1801618832, '2016-12-28 05:30:08', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.35, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 7.06, 4052024),
(57, 'new', 'sale', 10578899, 2086013463, '', '2016-12-29 00:00:00', '2017-02-27', 80226786328591, 0, 1802402543, '2016-12-30 01:30:32', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.31, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 4.4, 2270432),
(58, 'new', 'sale', 10578899, 2086013492, '', '2016-12-29 00:00:00', '2017-02-27', 80226786368591, 0, 1802402544, '2016-12-30 01:30:32', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.52, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 7.42, 2270432),
(96, 'new', 'sale', 10881559, 2080427570, '', '2016-12-16 00:00:00', '2017-02-14', 79441853255037, 0, 1797276559, '2016-12-17 01:30:24', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.51, 0, 'nejjasnejsi-princezna', 7.26, 2270432),
(97, 'new', 'sale', 10881559, 2080882690, '', '2016-12-17 00:00:00', '2017-02-15', 79441853265037, 0, 1797715370, '2016-12-18 01:30:30', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.1, 0, 'nejjasnejsi-princezna', 1.4, 2270432),
(116, 'new', 'sale', 11417684, 2089111062, 'CZ', '2017-01-06 12:45:22', '2017-02-10', 1034870053, 0, 1805227950, '2017-01-06 13:32:06', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.57, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 11.3, 4052024),
(214, 'new', 'sale', 11417684, 2090828606, 'CZ', '2017-01-10 04:06:25', '2017-02-10', 1035104745, 0, 1806631206, '2017-01-10 05:00:22', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.27, 0, '741cc9583e98a547e6e3be757fdca8c1d6dcfa0e', 5.34, 4052024),
(247, 'new', 'sale', 10578899, 2091262350, '', '2017-01-10 00:00:00', '2017-03-11', 80226786398591, 0, 1807003437, '2017-01-11 01:30:33', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.06, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 0.88, 2270432),
(248, 'closed', 'sale', 11417684, 2094002990, 'CZ', '2017-01-18 02:08:44', '2017-02-10', 1035621287, 0, 1809581659, '2017-01-18 03:00:22', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.23, 0, 'b1809327264d1f0c52493a22e0d0ca84e037422e', 4.59, 4052024),
(249, 'new', 'sale', 10578899, 2094839525, '', '2017-01-19 00:00:00', '2017-03-20', 80150965128591, 0, 1810329085, '2017-01-20 01:30:52', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 2.72, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 38.81, 2270432),
(250, 'closed', 'advanced sale', 11919999, 2095050065, 'CZ', '2017-01-20 11:42:29', '2017-02-10', 0, 0, 1810531630, '2017-01-20 12:31:16', 8106089, 373568, 'Tescoma.cz_itemsale', 4356323, 'Tescoma.cz', 5.28, 2.81, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 65.97, 4356323),
(251, 'locked', 'lead', 12294952, 2097955561, 'CZ', '2017-01-28 04:59:01', '2017-02-10', 125024651, 0, 1813153160, '2017-01-28 06:00:17', 8106089, 381554, ' EXIM Tours.cz_simple lead', 4577514, 'EXIM Tours.cz', 0, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 2219.99, 4577514),
(252, 'closed', 'sale', 11417680, 2098078100, 'CZ', '2017-01-28 11:34:23', '2017-02-10', 1036248139, 0, 1813274079, '2017-01-28 12:31:02', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.41, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 8.18, 4052024),
(257, 'new', 'advanced sale', 11028633, 2098329578, 'CZ', '2017-01-29 08:43:25', '2017-03-30', 5591336760, 0, 1813515098, '2017-01-29 09:31:27', 8106089, 384088, 'Zoot.cz_new_customer', 4851154, 'ZOOT.cz', 6.57, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 82.12, 3600617),
(258, 'closed', 'sale', 11417680, 2098628064, 'CZ', '2017-01-30 05:56:11', '2017-02-10', 1036365603, 0, 1813804718, '2017-01-30 07:00:18', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 0, 4052024),
(259, 'new', 'advanced sale', 12467478, 2098750484, 'CZ', '2017-01-30 10:52:34', '2017-03-31', 1700660771, 0, 1813919318, '2017-01-30 12:01:02', 8106089, 382382, 'Euronics.cz_itemsale', 4608876, 'Euronics.cz', 0.09, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 4.41, 4608876),
(260, 'closed', 'advanced sale', 11332299, 2098997197, 'CZ', '2017-01-31 01:49:04', '2017-02-10', 2044820, 0, 1814153342, '2017-01-31 02:31:28', 8106089, 359877, ' Bata.cz - itemsale', 3949914, 'Bata - CZ', 1.56, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 15.64, 3949914),
(261, 'new', 'advanced sale', 12086036, 2099008799, 'CZ', '2017-01-31 03:15:43', '2017-04-01', 4475825190, 0, 1814162742, '2017-01-31 04:00:38', 8106089, 374139, 'Krasa.cz_itemsale', 4384608, 'Vivantis.cz', 6.47, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 64.73, 4384608),
(262, 'new', 'advanced sale', 11028633, 2099202138, 'CZ', '2017-01-31 12:32:22', '2017-04-01', 5591365698, 0, 1814346831, '2017-01-31 13:31:03', 8106089, 384088, 'Zoot.cz_new_customer', 4851154, 'ZOOT.cz', 8.52, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 106.46, 3600617),
(263, 'new', 'sale', 11417680, 2099995026, 'CZ', '2017-02-02 09:39:41', '2017-03-10', 1036498147, 0, 1815067430, '2017-02-02 10:31:05', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.46, 0, '8dca0557ee002c7113aadac3c5d2c29c5421b9c6', 9.26, 4052024),
(264, 'new', 'sale', 11417680, 2100009869, 'CZ', '2017-02-02 10:05:38', '2017-03-10', 1036500327, 0, 1815081436, '2017-02-02 11:01:40', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.99, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 19.74, 4052024),
(265, 'new', 'advanced sale', 11028633, 2100262803, 'CZ', '2017-01-29 08:43:25', '2017-03-30', 5591336760, 0, 1813515098, '2017-02-03 03:55:27', 8106089, 384088, 'Zoot.cz_new_customer', 4851154, 'ZOOT.cz', -6.57, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', -82.12, 3600617),
(266, 'new', 'sale', 11417680, 2101925001, 'CZ', '2017-02-07 01:53:40', '2017-03-10', 1036681635, 0, 1816802473, '2017-02-07 02:30:35', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.46, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 9.1, 4052024),
(267, 'new', 'advanced sale', 11028633, 2102030646, 'CZ', '2017-02-07 07:45:46', '2017-04-08', 5591441061, 0, 1816898128, '2017-02-07 08:32:11', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', 6.08, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 75.99, 3600617),
(268, 'locked', 'sale', 12294952, 2102386000, '', '2017-01-28 04:59:01', '2017-02-10', 125024651, 0, 1817226592, '2017-02-08 05:30:49', 8106089, 381617, 'EXIM Tours.cz_batch ', 4577514, 'EXIM Tours.cz', 77.7, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 2219.99, 4577514),
(269, 'closed', 'advanced sale', 12723859, 2105408666, 'CZ', '2017-02-15 07:54:46', '2017-03-10', 1170215018, 0, 1819953879, '2017-02-15 09:00:31', 8106089, 388346, 'countrylife.cz_itemsale', 4829964, 'Countrylife.cz', 2.75, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 27.53, 4829964),
(270, 'new', 'advanced sale', 11028633, 2105736377, 'CZ', '2017-02-16 05:34:48', '2017-04-17', 5591564163, 0, 1820265188, '2017-02-16 06:30:31', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', 29.04, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 362.97, 3600617),
(271, 'closed', 'sale', 11417680, 2105888840, 'CZ', '2017-02-16 11:37:46', '2017-03-10', 1037119057, 0, 1820402159, '2017-02-16 12:30:56', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.32, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 6.39, 4052024),
(272, 'new', 'advanced sale', 11028633, 2107172003, 'CZ', '2017-02-20 04:02:30', '2017-04-21', 5591605659, 0, 1821641342, '2017-02-20 05:00:19', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', 21.09, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 263.63, 3600617),
(273, 'closed', 'advanced sale', 12838566, 2107832957, 'CZ', '2017-02-21 14:15:45', '2017-03-10', 517059926, 0, 1822276228, '2017-02-21 15:01:50', 8106089, 389929, 'DRMAX.CZ_itemsale', 4851193, 'DRMAX.CZ', 0.89, 0, '0330d47190ac50e2dd01958d291f9c94e950dc8d', 14.87, 4883722),
(274, 'new', 'advanced sale', 11028633, 2107971452, 'CZ', '2017-02-07 07:45:46', '2017-04-08', 5591441061, 0, 1816898128, '2017-02-22 00:35:13', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', -6.08, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', -75.99, 3600617),
(275, 'new', 'advanced sale', 11028633, 2107976276, 'CZ', '2017-02-16 05:34:48', '2017-04-17', 5591564163, 0, 1820265188, '2017-02-22 00:55:05', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', -22.53, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', -281.7, 3600617),
(276, 'closed', 'sale', 11417680, 2108794996, 'CZ', '2017-02-24 02:08:26', '2017-03-10', 1037465715, 0, 1823190509, '2017-02-24 03:00:45', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.57, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 11.45, 4052024),
(277, 'closed', 'advanced sale', 12613630, 2108808925, 'CZ', '2017-02-24 03:24:40', '2017-03-10', 823602, 0, 1823204336, '2017-02-24 04:01:54', 8106089, 386270, 'inSPORTline.cz_itemsale', 4754417, 'inSPORTline.cz', 15.14, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 302.73, 4754417),
(278, 'closed', 'sale', 11417680, 2109211761, 'CZ', '2017-02-25 05:42:32', '2017-03-10', 1037544859, 0, 1823578689, '2017-02-25 06:31:19', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.34, 0, 'bcef6cc02dcb2eb931747664393bb7a8841fefb7', 6.89, 4052024),
(279, 'closed', 'sale', 11417680, 2109505522, 'CZ', '2017-02-26 03:28:38', '2017-03-10', 1037593605, 0, 1823864882, '2017-02-26 04:30:18', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.78, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 15.54, 4052024),
(280, 'closed', 'sale', 11417680, 2109524758, 'CZ', '2017-02-26 05:32:38', '2017-03-10', 1037601443, 0, 1823883898, '2017-02-26 06:30:26', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.52, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 10.34, 4052024),
(281, 'closed', 'advanced sale', 12838566, 2109569728, 'CZ', '2017-02-26 08:01:34', '2017-03-10', 517064681, 0, 1823925004, '2017-02-26 09:01:26', 8106089, 389929, 'DRMAX.CZ_itemsale', 4851193, 'DRMAX.CZ', 0.39, 0, '0330d47190ac50e2dd01958d291f9c94e950dc8d', 6.49, 4883722),
(282, 'new', 'sale', 10578899, 2110215766, '', '2017-02-27 00:00:00', '2017-04-28', 80081086215037, 0, 1824551264, '2017-02-28 01:30:36', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.46, 0, 'bcef6cc02dcb2eb931747664393bb7a8841fefb7', 6.57, 2270432),
(283, 'closed', 'sale', 11417680, 2110222319, 'CZ', '2017-02-28 01:09:02', '2017-03-10', 1037690031, 0, 1824560269, '2017-02-28 02:00:28', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.22, 0, 'ec172baee90432bffc00c6f4f435c91ef67e89af', 4.48, 4052024),
(284, 'new', 'sale', 11417680, 2111057382, 'CZ', '2017-03-02 02:42:42', '2017-04-10', 1037791213, 0, 1825349186, '2017-03-02 03:30:35', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.53, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 10.69, 4052024),
(285, 'new', 'sale', 11417680, 2111498174, 'CZ', '2017-03-03 04:39:47', '2017-04-10', 1037845239, 0, 1825754072, '2017-03-03 05:30:34', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 1.8, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 36.03, 4052024),
(286, 'new', 'advanced sale', 11028633, 2112650415, 'CZ', '2017-02-20 04:02:30', '2017-04-21', 5591605659, 0, 1821641342, '2017-03-06 08:05:48', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', -14.13, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', -176.65, 3600617),
(287, 'new', 'sale', 11417680, 2113406520, 'CZ', '2017-03-08 01:01:34', '2017-04-10', 1038154285, 0, 1827523999, '2017-03-08 02:00:24', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.23, 0, 'ec172baee90432bffc00c6f4f435c91ef67e89af', 4.63, 4052024),
(288, 'new', 'advanced sale', 11316236, 2113741638, 'CZ', '2017-03-08 12:57:09', '2017-04-10', 39157481, 0, 1827768115, '2017-03-08 14:00:13', 8106089, 337438, 'Bonatex_item_Sale', 3007908, 'Bonatex - CZ', 4.65, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 46.46, 3007908),
(289, 'new', 'advanced sale', 11781997, 2114121511, 'CZ', '2017-03-09 10:44:59', '2017-05-08', 417636819, 0, 1828102801, '2017-03-09 12:02:51', 8106089, 368024, 'Pilulka.cz_itemsale', 4267436, 'Pilulka.cz', 0.37, 0, 'e2b6277be612f1daf0969602e364d6f9e64afd97', 7.32, 4267436),
(290, 'new', 'sale', 11417680, 2114955488, 'CZ', '2017-03-11 09:16:09', '2017-04-10', 1038351015, 0, 1828808666, '2017-03-11 10:01:41', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.6, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 11.99, 4052024),
(291, 'new', 'advanced sale', 12788839, 2115209588, 'CZ', '2017-03-12 05:05:03', '2017-05-11', 9077030871, 0, 1829056154, '2017-03-12 06:01:02', 8106089, 389025, 'BENU.CZ- itemsale', 4851186, 'BENU.CZ', 2.59, 0, 'e2b6277be612f1daf0969602e364d6f9e64afd97', 51.75, 4856236),
(292, 'locked', 'sale', 11417680, 2115530873, 'CZ', '2017-03-13 03:32:14', '2017-04-10', 1038519637, 0, 1829369405, '2017-03-13 04:30:13', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.96, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 19.16, 4052024),
(293, 'new', 'advanced sale', 11028633, 2115576724, 'CZ', '2017-03-13 07:01:47', '2017-05-12', 5591877453, 0, 1829417391, '2017-03-13 08:02:49', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', 19.76, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 247.01, 3600617),
(294, 'locked', 'advanced sale', 12733241, 2115617448, 'CZ', '2017-03-13 08:45:41', '2017-04-10', 569814, 0, 1829449027, '2017-03-13 11:00:38', 8106089, 387637, 'KnihyDobrovsky.cz_itemsale', 4805198, 'KnihyDobrovsky.cz', 0.23, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 3.77, 4805198),
(295, 'closed', 'sale', 11417680, 2116439282, 'CZ', '2017-03-15 03:57:07', '2017-04-10', 1038664529, 0, 1830223812, '2017-03-15 05:00:09', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.46, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 9.26, 4052024),
(296, 'closed', 'advanced sale', 11466391, 2116523411, 'CZ', '2017-03-15 08:48:32', '2017-04-10', 1417583396, 0, 1830303648, '2017-03-15 09:31:48', 8106089, 364303, 'Parfemy-Elnino.cz_itemsale', 4098645, 'Parfemy-Elnino.cz', 2.94, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 58.87, 4098645),
(297, 'closed', 'sale', 11417680, 2117277039, 'CZ', '2017-03-17 07:11:08', '2017-04-10', 1038830963, 0, 1831021512, '2017-03-17 08:00:59', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.63, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 12.49, 4052024),
(298, 'closed', 'sale', 11417680, 2117696272, 'CZ', '2017-03-18 08:21:46', '2017-04-10', 1038902577, 0, 1831415427, '2017-03-18 09:01:41', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.45, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 8.9, 4052024),
(299, 'closed', 'sale', 11417680, 2118401070, 'CZ', '2017-03-20 04:55:05', '2017-04-10', 1039060181, 0, 1832095958, '2017-03-20 06:00:53', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.57, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 11.43, 4052024),
(300, 'new', 'sale', 10578899, 2118748360, '', '2017-03-20 00:00:00', '2017-05-19', 82275466740370, 0, 1832425767, '2017-03-21 01:31:52', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.64, 0, '477ffc2291dd5745d63f4772393fa1d21ea24ca5', 9.12, 2270432),
(301, 'new', 'sale', 10578899, 2118750226, '', '2017-03-20 00:00:00', '2017-05-19', 82515077438591, 0, 1832425041, '2017-03-21 01:32:05', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.07, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 1.03, 2270432),
(302, 'new', 'sale', 10578899, 2118750227, '', '2017-03-20 00:00:00', '2017-05-19', 82515077448591, 0, 1832425042, '2017-03-21 01:32:05', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.25, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 3.6, 2270432),
(303, 'new', 'sale', 10578899, 2119176457, '', '2017-03-21 00:00:00', '2017-05-20', 82275466730370, 0, 1832835073, '2017-03-22 01:30:49', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.07, 0, '477ffc2291dd5745d63f4772393fa1d21ea24ca5', 1.06, 2270432),
(304, 'closed', 'sale', 11417680, 2119656082, 'CZ', '2017-03-23 04:47:10', '2017-04-10', 1039246479, 0, 1833298016, '2017-03-23 05:30:41', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.53, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 10.66, 4052024),
(305, 'closed', 'lead', 12010984, 2119827252, 'CZ', '2017-03-23 13:38:24', '2017-04-10', 1599098691, 0, 1833462813, '2017-03-23 14:30:55', 8106089, 375414, 'Hotel Booking (US)', 4851198, 'Booking.com US (Private Program)', 0, 0, 'b1809327264d1f0c52493a22e0d0ca84e037422e', 198.83, 4427449),
(306, 'closed', 'lead', 12010984, 2119828974, 'CZ', '2017-03-23 13:33:40', '2017-04-10', 1151447821, 0, 1833461575, '2017-03-23 14:31:07', 8106089, 375414, 'Hotel Booking (US)', 4851198, 'Booking.com US (Private Program)', 0, 0, 'b1809327264d1f0c52493a22e0d0ca84e037422e', 191.46, 4427449),
(307, 'closed', 'lead', 12010984, 2119829950, 'CZ', '2017-03-23 13:41:58', '2017-04-10', 1616181208, 0, 1833463759, '2017-03-23 14:31:26', 8106089, 375414, 'Hotel Booking (US)', 4851198, 'Booking.com US (Private Program)', 0, 0, 'b1809327264d1f0c52493a22e0d0ca84e037422e', 130.94, 4427449),
(308, 'closed', 'sale', 11417680, 2120038035, 'CZ', '2017-03-24 04:38:59', '2017-04-10', 1039311581, 0, 1833663907, '2017-03-24 05:30:21', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.21, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 4.27, 4052024),
(309, 'new', 'advanced sale', 11028633, 2121109156, 'CZ', '2017-03-13 07:01:47', '2017-05-12', 5591877453, 0, 1829417391, '2017-03-27 04:05:09', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', -16.27, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', -203.38, 3600617),
(310, 'new', 'advanced sale', 11028633, 2121123702, 'CZ', '2017-03-27 04:58:54', '2017-05-26', 5592072942, 0, 1834698104, '2017-03-27 06:00:08', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', 36.4, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 454.97, 3600617),
(311, 'closed', 'sale', 11417680, 2121156209, 'CZ', '2017-03-27 06:40:27', '2017-04-10', 1039615681, 0, 1834725562, '2017-03-27 07:31:08', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.26, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 5.2, 4052024),
(312, 'closed', 'sale', 11417680, 2121548832, 'CZ', '2017-03-28 06:39:09', '2017-04-10', 1039671366, 0, 1835097806, '2017-03-28 07:30:56', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.32, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 6.42, 4052024),
(313, 'new', 'advanced sale', 11781997, 2121899012, 'CZ', '2017-03-29 05:42:09', '2017-05-28', 417699159, 0, 1835435177, '2017-03-29 06:30:52', 8106089, 368024, 'Pilulka.cz_itemsale', 4267436, 'Pilulka.cz', 0.96, 0, 'b1809327264d1f0c52493a22e0d0ca84e037422e', 19.31, 4267436),
(314, 'new', 'advanced sale', 12086036, 2122246180, 'CZ', '2017-03-30 00:25:44', '2017-05-29', 4475955130, 0, 1835765570, '2017-03-30 01:30:35', 8106089, 374139, 'Krasa.cz_itemsale', 4384608, 'Vivantis.cz', 2.79, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 27.85, 4384608),
(315, 'new', 'sale', 10578899, 2122638069, '', '2017-03-30 00:00:00', '2017-05-29', 81605003201835, 0, 1836127603, '2017-03-31 01:31:19', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.1, 0, 'db7916c25ccd6953d0eec2133df8c0dc8a316648', 1.42, 2270432),
(316, 'closed', 'sale', 11417680, 2122695789, 'CZ', '2017-03-31 03:02:55', '2017-04-10', 1039864522, 0, 1836186237, '2017-03-31 04:00:30', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.36, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 7.14, 4052024),
(317, 'new', 'sale', 10578899, 2123034712, '', '2017-03-31 00:00:00', '2017-05-30', 81810184415807, 0, 1836508743, '2017-04-01 01:30:18', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.34, 0, '741cc9583e98a547e6e3be757fdca8c1d6dcfa0e', 4.77, 2270432),
(318, 'new', 'sale', 10578899, 2123717827, '', '2017-04-02 00:00:00', '2017-06-01', 81605003191835, 0, 1837163984, '2017-04-03 01:31:20', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.15, 0, 'db7916c25ccd6953d0eec2133df8c0dc8a316648', 2.19, 2270432),
(319, 'new', 'sale', 11417680, 2123767603, 'CZ', '2017-04-03 03:00:27', '2017-05-10', 1040094368, 0, 1837211902, '2017-04-03 04:01:13', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.39, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 7.8, 4052024),
(320, 'new', 'sale', 11417680, 2124611478, 'CZ', '2017-04-05 03:42:57', '2017-05-10', 1040231674, 0, 1837996042, '2017-04-05 04:30:31', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.49, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 9.82, 4052024),
(321, 'new', 'advanced sale', 11781997, 2124648132, 'CZ', '2017-04-05 05:52:56', '2017-06-04', 417457169, 0, 1838026810, '2017-04-05 07:00:28', 8106089, 368024, 'Pilulka.cz_itemsale', 4267436, 'Pilulka.cz', 0.66, 0, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', 13.27, 4267436),
(322, 'new', 'sale', 10578899, 2124992648, '', '2017-04-05 00:00:00', '2017-06-04', 82442904218591, 0, 1838317401, '2017-04-06 01:30:38', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.16, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 2.25, 2270432),
(323, 'new', 'sale', 10578899, 2125542631, '', '2017-04-06 00:00:00', '2017-06-05', 81810184405807, 0, 1838722887, '2017-04-07 01:30:33', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.19, 0, '741cc9583e98a547e6e3be757fdca8c1d6dcfa0e', 2.72, 2270432),
(324, 'new', 'sale', 11417680, 2126028770, 'CZ', '2017-04-08 04:39:40', '2017-05-10', 1040475540, 0, 1839106402, '2017-04-08 05:30:18', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.27, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 5.38, 4052024),
(325, 'new', 'sale', 11417680, 2126131856, 'CZ', '2017-04-08 11:27:07', '2017-05-10', 1040517674, 0, 1839206784, '2017-04-08 12:30:09', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.51, 0, '96835dd8bfa718bd6447ccc87af89ae1675daeca', 10.1, 4052024),
(326, 'new', 'sale', 11417680, 2126352070, 'CZ', '2017-04-09 06:35:02', '2017-05-10', 1040579988, 0, 1839417878, '2017-04-09 07:30:48', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.27, 0, 'ec172baee90432bffc00c6f4f435c91ef67e89af', 5.43, 4052024),
(327, 'new', 'sale', 11417680, 2126671550, 'CZ', '2017-04-10 02:38:32', '2017-05-10', 1040638950, 0, 1839728583, '2017-04-10 03:30:17', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.73, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 14.53, 4052024),
(328, 'new', 'advanced sale', 12838566, 2126673397, 'CZ', '2017-04-10 03:00:25', '2017-06-09', 517117111, 0, 1839729987, '2017-04-10 04:00:10', 8106089, 389929, 'DRMAX.CZ_itemsale', 4851193, 'DRMAX.CZ', 1, 0, '0330d47190ac50e2dd01958d291f9c94e950dc8d', 16.72, 4883722),
(329, 'new', 'sale', 10578899, 2127197339, '', '2017-04-10 00:00:00', '2017-06-09', 82275466760370, 0, 1840095205, '2017-04-11 01:30:31', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.12, 0, '477ffc2291dd5745d63f4772393fa1d21ea24ca5', 1.73, 2270432),
(330, 'new', 'advanced sale', 11028633, 2127232486, 'CZ', '2017-03-27 04:58:54', '2017-05-26', 5592072942, 0, 1834698104, '2017-04-11 03:45:00', 8106089, 350162, 'zoot_sale', 4851154, 'ZOOT.cz', -27.88, 0, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', -348.47, 3600617),
(331, 'new', 'sale', 11417680, 2127664830, 'CZ', '2017-04-12 03:35:45', '2017-05-10', 1040788964, 0, 1840504050, '2017-04-12 04:30:17', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.31, 0, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', 6.17, 4052024),
(332, 'new', 'sale', 11417680, 2127668553, 'CZ', '2017-04-12 03:52:36', '2017-05-10', 1040790610, 0, 1840507955, '2017-04-12 04:30:57', 8106089, 363192, 'Damejidlo.cz_sale', 4052024, 'Damejidlo.cz', 0.17, 0, '0330d47190ac50e2dd01958d291f9c94e950dc8d', 3.47, 4052024),
(333, 'new', 'advanced sale', 12487996, 2127690654, 'CZ', '2017-04-12 05:11:46', '2017-05-17', 301793, 0, 1840529527, '2017-04-12 06:01:57', 8106089, 382536, ' Rozbaleno.cz_itemsale', 4612012, 'Rozbaleno.cz', 0.93, 0, '0330d47190ac50e2dd01958d291f9c94e950dc8d', 13.54, 4612012),
(334, 'new', 'sale', 10578899, 2128750299, '', '2017-04-14 00:00:00', '2017-06-13', 82761275545807, 0, 1841515200, '2017-04-15 01:30:26', 8106089, 373788, 'Completed Payment (NEW) ', 2270432, 'AliExpress by Alibaba.com', 0.82, 0, '741cc9583e98a547e6e3be757fdca8c1d6dcfa0e', 11.65, 2270432);

--
-- Spouště `commission_log`
--
DELIMITER $$
CREATE TRIGGER `commission_log_ai` AFTER INSERT ON `commission_log`
 FOR EACH ROW BEGIN

	DECLARE user_id 			INT(11);
	DECLARE parent_id 			INT(11);
	DECLARE cashback_customer 	FLOAT; 			-- zakanikova odmena
	DECLARE cashback_value 		FLOAT; 			-- nase odmena
	DECLARE cashback_percent 		INT;			-- 0 procenta | 1 pevna hodnota
	DECLARE cashback_u_amount	FLOAT;			-- konecna castka ktera se zapise nakupujicimu
	DECLARE cashback_p_amount	FLOAT;			-- konecna castka ktera se zapise nadrazenemu
  DECLARE payment_type VARCHAR(120);

  SET @payment_type = 'payment';

	SELECT u.id, u.parent_id 
		INTO @user_id, @parent_id 
		FROM user u 
		WHERE u.sid = NEW.sid;

	SELECT a.cashback_percent, a.cashback_customer, a.cashback_value 
		INTO @cashback_percent, @cashback_customer, @cashback_value 
		FROM advertiser a 
		WHERE a.id = NEW.advertiser_id;
		

	SET @cashback_u_amount = @cashback_customer;

	IF @cashback_percent = 1 THEN
			
		SET @cashback_u_amount = NEW.sale_amount * @cashback_customer * 0.01; 
		
		-- 1. lepsi takto, dochazi k chybe po zakrouhleni a trojclenkou by delalo binec
		
	END IF;

        -- 2. radeji kontrola, kdyby se zadala chybne hodnota (abysme meli zisk odecist nejakou hodnotu?)
        IF (@cashback_u_amount > NEW.commission_amount AND (@cashback_u_amount > 0 AND NEW.commission_amount > 0)) THEN
		
            SET @cashback_u_amount = NEW.commission_amount; 			
    
        END IF;


  IF (NEW.sale_amount < 0) THEN
      SET @payment_type = 'storno';
  END IF;

	INSERT INTO user_account (commission_log_id, user_id, commission_value, created, updated, type) 
  VALUES (NEW.id, @user_id, @cashback_u_amount, NEW.posting_date, NEW.posting_date, @payment_type);

	IF @parent_id > 0 THEN 
		BEGIN
		
			SET @cashback_p_amount = @cashback_u_amount * 0.5;
			
			INSERT INTO user_account (commission_log_id, user_id, commission_value, created, updated, type) 
				VALUES (NEW.id, @parent_id, @cashback_p_amount, NEW.posting_date, NEW.posting_date, @payment_type);
			
		END;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktura tabulky `login_log`
--

CREATE TABLE IF NOT EXISTS `login_log` (
  `id` int(10) unsigned NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned DEFAULT NULL,
  `login` varchar(1000) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `user_agent` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `login_log`
--

INSERT INTO `login_log` (`id`, `date`, `user_id`, `login`, `success`, `ip`, `user_agent`) VALUES
(1, '2016-11-10 11:07:21', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(2, '2016-11-10 16:14:54', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),
(3, '2016-11-10 16:45:20', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),
(4, '2016-11-11 09:54:54', 13, 't.franckova@gmail.com', 1, '37.48.45.217', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),
(5, '2016-11-11 10:00:25', 15, 'mlha-martin@seznam.cz', 1, '37.48.6.5', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(6, '2016-11-11 10:51:17', 14, 'tomashuda@gmail.com', 1, '37.48.37.54', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(7, '2016-11-11 14:37:19', NULL, 'karofast@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),
(8, '2016-11-11 14:41:19', 16, 'karofast@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),
(9, '2016-11-11 20:08:06', 15, 'mlha-martin@seznam.cz', 1, '89.103.250.6', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36'),
(10, '2016-11-11 21:39:12', NULL, 'tomashuda@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(11, '2016-11-11 21:39:16', NULL, 'tomashuda@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(12, '2016-11-11 21:40:05', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(13, '2016-11-13 12:28:58', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(14, '2016-11-13 12:31:42', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(15, '2016-11-14 16:15:30', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(16, '2016-11-15 19:29:38', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:e029:7c3d:e5fb:3e3f', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(17, '2016-11-15 19:30:20', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:e029:7c3d:e5fb:3e3f', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(18, '2016-11-15 19:30:29', NULL, 'kirchmanova', 0, '2a00:1028:83d6:2e9a:e029:7c3d:e5fb:3e3f', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(19, '2016-11-15 19:33:02', NULL, 'marketing@rtsoft.cz', 0, '2a00:1028:83d6:2e9a:e029:7c3d:e5fb:3e3f', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(20, '2016-11-15 19:34:10', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:e029:7c3d:e5fb:3e3f', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(21, '2016-11-15 19:39:04', NULL, 'martina', 0, '2a00:1028:83d6:2e9a:e029:7c3d:e5fb:3e3f', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(22, '2016-11-15 20:49:33', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(23, '2016-11-15 20:51:14', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(24, '2016-11-15 20:51:24', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(25, '2016-11-15 20:51:26', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(26, '2016-11-15 20:51:27', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(27, '2016-11-15 20:51:32', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(28, '2016-11-15 20:51:50', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(29, '2016-11-15 20:53:16', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(30, '2016-11-15 20:53:18', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(31, '2016-11-15 20:53:35', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(32, '2016-11-15 20:53:58', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(33, '2016-11-15 20:54:21', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(34, '2016-11-15 20:54:55', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(35, '2016-11-15 20:55:01', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(36, '2016-11-15 20:55:39', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(37, '2016-11-15 20:55:58', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(38, '2016-11-15 21:29:21', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(39, '2016-11-15 21:29:27', NULL, 'kirchmanova@gmail.com', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(40, '2016-11-15 21:29:50', 17, 'kirchmanova@gmai.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(41, '2016-11-15 21:30:31', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(42, '2016-11-15 21:30:35', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(43, '2016-11-15 21:31:20', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(44, '2016-11-15 21:31:27', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(45, '2016-11-15 21:31:31', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(46, '2016-11-15 21:31:36', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(47, '2016-11-15 21:32:04', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(48, '2016-11-15 21:32:13', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(49, '2016-11-15 21:32:36', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(50, '2016-11-15 21:33:22', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(51, '2016-11-15 21:36:24', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(52, '2016-11-15 21:37:06', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(53, '2016-11-15 21:38:33', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5d72:cb7f:a784:a16d', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(54, '2016-11-15 21:39:02', NULL, 'kirchmanova@gmail.com', 0, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(55, '2016-11-16 09:42:27', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(56, '2016-11-16 09:42:49', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(57, '2016-11-16 09:43:03', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(58, '2016-11-16 09:43:10', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(59, '2016-11-16 09:43:26', NULL, 'kirchmanova@gmai.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(60, '2016-11-16 09:43:34', 17, 'kirchmanova@gmai.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(61, '2016-11-16 09:43:37', NULL, 'marketing@rtsoft.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(62, '2016-11-16 09:43:51', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(63, '2016-11-16 09:44:36', 17, 'kirchmanova@gmai.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'),
(64, '2016-11-16 09:44:50', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(65, '2016-11-16 09:44:59', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(66, '2016-11-16 09:45:18', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(67, '2016-11-16 09:45:50', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(68, '2016-11-16 09:46:00', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(69, '2016-11-16 09:46:18', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(70, '2016-11-16 09:46:33', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(71, '2016-11-16 09:47:42', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(72, '2016-11-16 09:48:33', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(73, '2016-11-16 09:48:42', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(74, '2016-11-17 08:12:51', 17, 'kirchmanova@gmail.com', 1, '109.81.210.164', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(75, '2016-11-21 15:35:18', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(76, '2016-11-23 12:23:09', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(77, '2016-11-23 12:23:09', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(78, '2016-11-26 12:18:39', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(79, '2016-11-28 09:58:51', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(80, '2016-11-28 09:59:24', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(81, '2016-11-29 22:41:49', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(83, '2016-12-06 10:15:44', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(84, '2016-12-06 14:25:38', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(85, '2016-12-06 16:48:51', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(86, '2016-12-08 11:23:58', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
(87, '2016-12-08 22:56:40', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(88, '2016-12-15 21:22:30', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(89, '2016-12-16 18:44:10', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(90, '2016-12-18 18:27:38', 18, 'kurdik@rtsoft.cz', 1, '88.146.98.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(91, '2016-12-18 20:22:08', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(92, '2016-12-20 15:28:50', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(93, '2016-12-23 21:24:51', NULL, 'kurdik', 0, '88.146.98.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(94, '2016-12-23 21:24:58', 18, 'kurdik@rtsoft.cz', 1, '88.146.98.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(95, '2016-12-23 21:43:43', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(96, '2016-12-26 10:19:40', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(97, '2016-12-28 12:14:36', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(98, '2016-12-28 12:24:52', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:c57a:5a10:eac4:1f7c', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(99, '2017-01-02 19:23:46', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:598e:2748:fb71:dfd', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(100, '2017-01-03 16:22:40', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(101, '2017-01-04 11:26:33', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(102, '2017-01-04 11:26:34', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(103, '2017-01-04 19:41:40', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:34b6:ba8:ae85:3f43', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(104, '2017-01-05 21:08:16', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(105, '2017-01-05 22:17:50', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(106, '2017-01-06 07:54:32', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(107, '2017-01-06 20:10:32', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(108, '2017-01-08 11:53:39', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:1578:1fa7:c7ee:f446', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(109, '2017-01-08 17:31:34', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(110, '2017-01-08 17:43:54', 18, 'kurdik@rtsoft.cz', 1, '88.146.98.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(111, '2017-01-08 18:43:59', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(112, '2017-01-09 20:54:27', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:a8dd:156f:8681:bec8', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(113, '2017-01-09 20:56:08', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(114, '2017-01-09 21:23:58', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(115, '2017-01-09 22:18:36', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Linux; Android 5.0.1; GT-I9505 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36'),
(116, '2017-01-10 12:01:31', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(117, '2017-01-10 19:49:40', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:7dff:ec81:248d:c168', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(118, '2017-01-10 20:55:13', 19, '19', 1, '88.146.98.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(119, '2017-01-10 20:58:42', 14, '14', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(120, '2017-01-11 09:11:11', 20, '20', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(121, '2017-01-11 12:22:20', 21, '21', 1, '46.23.58.100', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G900F Build/MMB29M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/MESSENGER;FBAV/100.0.0.29.61;]'),
(122, '2017-01-11 12:29:12', 21, '21', 1, '46.23.58.100', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(123, '2017-01-11 15:51:01', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(124, '2017-01-11 19:06:35', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:172:30a3:df9f:b1f', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(125, '2017-01-11 19:47:22', 22, '22', 1, '109.81.210.112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(126, '2017-01-11 22:46:15', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(127, '2017-01-11 22:46:50', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(128, '2017-01-12 17:50:55', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(129, '2017-01-13 07:57:51', 23, 'filip.hlad@gmail.com', 1, '46.167.203.122', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(130, '2017-01-13 07:58:11', 23, 'filip.hlad@gmail.com', 1, '46.167.203.122', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(131, '2017-01-13 07:59:23', 23, 'filip.hlad@gmail.com', 1, '46.167.203.122', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(132, '2017-01-13 08:02:16', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(133, '2017-01-13 08:08:52', 24, '24', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(134, '2017-01-13 14:13:44', 15, 'mlha-martin@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(135, '2017-01-14 16:12:20', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:ed69:3a77:90a6:63d9', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(136, '2017-01-15 12:56:27', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(137, '2017-01-15 18:51:15', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:996e:2f01:a1db:f2e1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(138, '2017-01-18 19:24:47', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:996e:2f01:a1db:f2e1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(139, '2017-01-19 11:49:02', 15, 'mlha-martin@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(140, '2017-01-19 11:49:28', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(141, '2017-01-19 11:51:12', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(142, '2017-01-19 16:10:47', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(143, '2017-01-19 16:12:41', 25, 'r.franckova@gmail.com', 1, '89.190.54.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(144, '2017-01-19 23:43:01', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(145, '2017-01-20 08:16:04', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(146, '2017-01-20 10:30:32', 26, '26', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(147, '2017-01-20 14:14:34', NULL, 'm.kirchmanova@centrum.cz', 0, '212.79.110.74', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94'),
(148, '2017-01-20 14:14:54', NULL, 'm.kirchmanova@centrum.cz', 0, '212.79.110.74', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94'),
(149, '2017-01-20 14:15:19', NULL, 'kirchman@email.cz', 0, '212.79.110.74', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94'),
(150, '2017-01-20 14:38:57', NULL, 'm.kirchmanova@centrum.cz', 0, '212.79.110.74', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94'),
(151, '2017-01-20 14:39:08', NULL, 'kirchman@email.cz', 0, '212.79.110.74', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 OPR/42.0.2393.94'),
(152, '2017-01-21 18:42:13', 14, 'tomashuda@gmail.com', 1, '2a00:1028:83d6:96b6:303f:c85d:210d:d7a', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(153, '2017-01-24 09:26:37', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(154, '2017-01-24 09:26:41', NULL, 'kirchmanova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(155, '2017-01-24 09:26:51', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(156, '2017-01-24 17:43:26', 19, '19', 1, '88.146.98.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(157, '2017-01-24 17:45:13', 19, '19', 1, '88.146.98.128', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(158, '2017-01-25 12:04:08', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(159, '2017-01-25 14:14:31', 27, 'dena.sramkova@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(160, '2017-01-25 14:42:25', 27, 'dena.sramkova@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(161, '2017-01-25 18:43:52', NULL, 'hudova.lucka@seznam.cz', 0, '2a00:1028:83d6:96b6:141f:d3d6:9e93:1ada', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(162, '2017-01-25 18:44:50', NULL, 'hudova.lucka@seznam.cz', 0, '2a00:1028:83d6:96b6:141f:d3d6:9e93:1ada', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(163, '2017-01-25 18:45:51', NULL, 'hudova.lucka@seznam.cz', 0, '2a00:1028:83d6:96b6:141f:d3d6:9e93:1ada', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(164, '2017-01-25 18:50:33', 28, 'hudova.lucka@seznam.cz', 1, '2a00:1028:83d6:96b6:141f:d3d6:9e93:1ada', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(165, '2017-01-26 10:25:09', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(166, '2017-01-27 08:31:11', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(167, '2017-01-27 21:27:17', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(168, '2017-01-28 15:43:37', NULL, 'M.kirchmanova@centrum.cz', 0, '37.188.236.177', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1'),
(169, '2017-01-28 15:44:10', NULL, 'M.kirchmanova@centrum.cz', 0, '37.188.236.177', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1'),
(170, '2017-01-28 15:45:07', NULL, 'm.kirchmanova@centrum.cz', 0, '37.188.236.177', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1'),
(171, '2017-01-28 19:39:14', NULL, 'm.kirchmanova@centrum.cz', 0, '109.81.210.51', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1'),
(172, '2017-01-30 11:40:52', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(173, '2017-01-30 13:54:27', 14, '14', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'),
(174, '2017-01-30 19:50:24', 17, 'kirchmanova@gmail.com', 1, '109.81.210.187', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(175, '2017-01-31 08:51:59', NULL, 'm.spiritova@seznam.cz', 0, '109.81.210.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(176, '2017-01-31 08:52:30', NULL, 'm.spiritova@centrum.cz', 0, '109.81.210.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(177, '2017-01-31 08:52:43', NULL, 'm.spiritova@centrum.cz', 0, '109.81.210.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(178, '2017-01-31 08:53:30', NULL, 'm.spiritova@centrum.cz', 0, '109.81.210.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(179, '2017-01-31 10:35:39', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(180, '2017-02-01 09:05:19', 22, 'mariekirchmanova@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(181, '2017-02-02 17:31:46', 14, '14', 1, '37.48.32.91', 'Mozilla/5.0 (Linux; Android 5.0.1; GT-I9505 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36'),
(182, '2017-02-02 17:34:15', 30, '30', 1, '37.48.44.67', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1'),
(183, '2017-02-03 11:38:53', 17, 'kirchmanova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(184, '2017-02-06 16:54:09', NULL, 'martinzika370@gmail.com', 0, '93.99.166.158', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(185, '2017-02-06 16:54:20', NULL, 'martinzika370@gmail.com', 0, '93.99.166.158', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(186, '2017-02-06 16:54:44', 31, '31', 1, '93.99.166.158', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(187, '2017-02-06 23:18:05', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(188, '2017-02-07 11:23:46', 32, '32', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(189, '2017-02-07 11:23:56', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(190, '2017-02-07 14:52:18', 33, 'ladyzaba@gmail.com', 1, '2a00:1028:8b40:a7f2:65f0:8537:2e6b:1436', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(191, '2017-02-07 15:36:43', 25, 'r.franckova@gmail.com', 1, '89.190.54.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(192, '2017-02-07 17:12:09', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(193, '2017-02-07 17:15:30', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(194, '2017-02-07 23:34:15', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(196, '2017-02-10 11:58:12', 35, 'cashback@centrum.cz', 1, '194.79.55.130', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(197, '2017-02-11 20:40:47', 13, 't.franckova@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(198, '2017-02-13 09:39:56', 22, 'mariekirchmanova@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(199, '2017-02-13 12:07:53', 25, 'r.franckova@gmail.com', 1, '89.190.54.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(200, '2017-02-13 17:10:20', 17, 'kirchmanova@gmail.com', 1, '109.81.210.18', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(201, '2017-02-14 18:59:35', 13, 't.franckova@gmail.com', 1, '212.11.105.250', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'),
(202, '2017-02-15 15:13:28', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(203, '2017-02-15 16:04:48', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(204, '2017-02-15 22:15:44', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(205, '2017-02-15 22:18:41', 13, 't.franckova@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(206, '2017-02-15 23:11:07', 31, 'martin.zika@desinsekta.cz', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(207, '2017-02-15 23:21:40', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(208, '2017-02-17 08:05:09', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(209, '2017-02-17 08:06:04', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(210, '2017-02-17 10:53:18', NULL, 'helcad82@seznam.cz', 0, '212.79.110.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(211, '2017-02-17 10:53:40', 38, '38', 1, '212.79.110.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(212, '2017-02-17 11:45:06', NULL, 'lukasholy1@gmail.com', 0, '89.102.115.188', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(213, '2017-02-17 16:43:08', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(214, '2017-02-18 07:18:32', 39, 'lexbrychta@gmail.com', 1, '94.112.123.8', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(215, '2017-02-18 19:53:43', NULL, 'helcad82@seznam.cz', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(216, '2017-02-18 19:53:52', NULL, 'helcad82@seznam.cz', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(217, '2017-02-19 10:57:39', 19, '19', 1, '46.13.150.57', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(218, '2017-02-19 12:58:21', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(219, '2017-02-20 15:37:06', NULL, 'chott@rtsoft.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(220, '2017-02-20 15:37:11', NULL, 'chott@rtsoft.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(221, '2017-02-20 15:37:17', NULL, 'chott@rtsoft.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(222, '2017-02-20 15:37:25', NULL, 'chott@rtsoft.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(223, '2017-02-20 15:37:31', NULL, 'chott@rtsoft.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(224, '2017-02-20 15:41:08', NULL, 'mlha-martin@seznam.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(225, '2017-02-20 15:41:13', 15, 'mlha-martin@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(226, '2017-02-20 15:44:54', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(227, '2017-02-20 15:45:35', 40, '40', 1, '93.99.39.209', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/602.4.8 (KHTML, like Gecko) Version/10.0.3 Safari/602.4.8'),
(228, '2017-02-20 19:59:19', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(230, '2017-02-21 09:55:59', 42, '42', 1, '94.112.52.222', 'Mozilla/5.0 (Linux; Android 5.0.2; Redmi Note 3 Build/LRX22G; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/107.0.0.19.337;]'),
(231, '2017-02-21 10:59:26', 28, 'Hudova.lucka@seznam.cz', 1, '109.81.209.181', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G935F Build/MMB29K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/111.0.0.18.69;]'),
(232, '2017-02-21 11:35:56', 43, '43', 1, '46.167.201.246', 'Mozilla/5.0 (Linux; Android 4.4.2; Cirrus 5 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(233, '2017-02-21 11:49:47', 13, 't.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(234, '2017-02-21 13:32:32', NULL, 'atamandi@seznam.cz', 0, '46.167.205.242', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(235, '2017-02-21 13:32:38', NULL, 'atamandi@seznam.cz', 0, '46.167.205.242', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(236, '2017-02-21 13:32:43', NULL, 'atamandi@seznam.cz', 0, '46.167.205.242', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(237, '2017-02-21 13:32:48', NULL, 'atamandi@seznam.cz', 0, '46.167.205.242', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(238, '2017-02-21 17:01:38', 38, '38', 1, '37.48.47.5', 'Mozilla/5.0 (Windows NT 6.2; ARM; Trident/7.0; Touch; rv:11.0; WPDesktop; Lumia 630 Dual SIM) like Gecko'),
(239, '2017-02-21 17:09:43', 44, 'atamandi@seznam.cz', 1, '46.167.205.242', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(240, '2017-02-22 10:46:50', NULL, 'mlha-martin@seznam.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(241, '2017-02-22 10:46:56', NULL, 'mlha-martin@seznam.cz', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(242, '2017-02-22 10:47:17', 15, 'mlha-martin@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(243, '2017-02-22 17:33:29', 45, '45', 1, '46.135.44.253', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-A510F Build/MMB29K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/56.0.2924.87 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/111.0.0.18.69;]'),
(244, '2017-02-22 18:41:09', 46, 'nova.pavla@volny.cz', 1, '88.102.107.86', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(245, '2017-02-22 18:55:50', 47, 'mir.kovarik@gmail.com', 1, '213.192.58.58', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(246, '2017-02-22 19:01:25', 38, '38', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(247, '2017-02-22 19:22:48', NULL, '48', 0, '83.208.205.205', 'Mozilla/5.0 (Linux; Android 6.0; HUAWEI VNS-L21 Build/HUAWEIVNS-L21; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/MESSENGER;FBAV/105.0.0.16.69;]'),
(248, '2017-02-22 19:23:02', NULL, '48', 0, '83.208.205.205', 'Mozilla/5.0 (Linux; Android 6.0; HUAWEI VNS-L21 Build/HUAWEIVNS-L21; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/MESSENGER;FBAV/105.0.0.16.69;]'),
(249, '2017-02-22 19:23:36', NULL, '49', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(250, '2017-02-22 19:24:05', NULL, 'pohlova29@seznam.cz', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(251, '2017-02-22 19:24:09', NULL, '48', 0, '83.208.205.205', 'Mozilla/5.0 (Linux; Android 6.0; HUAWEI VNS-L21 Build/HUAWEIVNS-L21; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/MESSENGER;FBAV/105.0.0.16.69;]'),
(252, '2017-02-22 19:24:12', NULL, '48', 0, '83.208.205.205', 'Mozilla/5.0 (Linux; Android 6.0; HUAWEI VNS-L21 Build/HUAWEIVNS-L21; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/MESSENGER;FBAV/105.0.0.16.69;]'),
(253, '2017-02-22 19:25:07', NULL, '49', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(254, '2017-02-22 19:25:44', NULL, 'pohlova299@seznam.cz', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(255, '2017-02-22 19:25:50', NULL, '49', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(256, '2017-02-22 19:26:27', NULL, '49', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(257, '2017-02-22 19:26:41', NULL, '49', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(258, '2017-02-22 19:26:47', NULL, '49', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(259, '2017-02-22 19:26:58', NULL, '49', 0, '2001:1ae9:13:8300:1df2:9e33:ed8:3b12', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(260, '2017-02-22 20:13:22', 48, 'pepulka@seznam.cz', 1, '83.208.205.205', 'Mozilla/5.0 (Linux; Android 6.0; HUAWEI VNS-L21 Build/HUAWEIVNS-L21) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36'),
(261, '2017-02-22 20:19:40', 52, 'Dana.zalabakova@seznam.cz', 1, '194.228.13.145', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Mobile/14D27 SznProhlizec/4.2i'),
(262, '2017-02-22 20:39:50', 38, '38', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(263, '2017-02-23 05:31:05', 46, 'nova.pavla@volny.cz', 1, '88.102.107.86', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(264, '2017-02-23 08:16:29', 57, '57', 1, '37.188.187.207', 'Mozilla/5.0 (Linux; Android 5.0; SM-N9005 Build/LRX21V; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/55.0.2883.91 Mobile Safari/537.36 [FB_IAB/MESSENGER;FBAV/103.0.0.12.69;]'),
(265, '2017-02-23 09:31:04', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(266, '2017-02-23 09:39:56', 25, 'r.franckova@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(267, '2017-02-23 09:41:04', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(268, '2017-02-23 09:43:03', 22, 'mariekirchmanova@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(269, '2017-02-23 15:38:08', 38, '38', 1, '212.79.110.123', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(270, '2017-02-23 16:45:25', 58, 'risa.muck@seznam.cz', 1, '62.209.230.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(271, '2017-02-23 17:12:05', 59, 'Simona.Sitorova@seznam.cz', 1, '85.135.55.98', 'Mozilla/5.0 (Linux; Android 4.4.4; E2105 Build/24.0.A.5.14) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36'),
(272, '2017-02-23 18:24:55', 46, 'nova.pavla@volny.cz', 1, '88.102.107.86', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(273, '2017-02-23 20:52:27', 25, 'r.franckova@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(274, '2017-02-23 21:16:10', 22, 'mariekirchmanova@seznam.cz', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(275, '2017-02-23 21:50:44', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(276, '2017-02-24 10:02:45', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(277, '2017-02-24 12:28:37', NULL, 't.franckova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(278, '2017-02-24 12:29:23', NULL, 't.franckova@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(279, '2017-02-24 15:53:11', 46, 'nova.pavla@volny.cz', 1, '88.102.107.86', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(280, '2017-02-25 11:22:36', 38, 'helcad82@seznam.cz', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0');
INSERT INTO `login_log` (`id`, `date`, `user_id`, `login`, `success`, `ip`, `user_agent`) VALUES
(281, '2017-02-25 13:31:11', 13, 't.franckova@gmail.com', 1, '212.11.105.250', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(282, '2017-02-26 09:41:31', 58, 'risa.muck@seznam.cz', 1, '62.209.230.66', 'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36'),
(283, '2017-02-26 10:23:55', NULL, 'mlha-martin@seznam.cz', 0, '89.103.250.6', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(284, '2017-02-26 10:24:03', NULL, 'mlha-martin@seznam.cz', 0, '89.103.250.6', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(285, '2017-02-26 10:24:11', 15, 'mlha-martin@seznam.cz', 1, '89.103.250.6', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(286, '2017-02-26 14:03:03', 58, 'risa.muck@seznam.cz', 1, '62.209.230.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(287, '2017-02-27 11:06:00', 31, '31', 1, '212.79.110.106', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(288, '2017-02-27 13:09:22', 15, 'mlha-martin@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(289, '2017-02-27 21:32:10', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(290, '2017-02-28 09:53:28', 31, '31', 1, '212.79.110.106', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(291, '2017-02-28 10:30:24', 13, 't.franckova@gmail.com', 1, '212.11.105.250', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(292, '2017-02-28 15:49:44', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(293, '2017-03-02 20:05:16', 17, 'kirchmanova@gmail.com', 1, '109.81.214.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(294, '2017-03-03 10:26:52', NULL, 'medvec.milan@gmail.com', 0, '31.47.102.52', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36'),
(295, '2017-03-03 10:28:29', 65, 'medvec.milan@gmail.com', 1, '31.47.102.52', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36'),
(296, '2017-03-04 11:00:57', 66, 'Marketadolezalova87@gmail.com', 1, '80.95.103.252', 'Mozilla/5.0 (Linux; Android 5.1; HUAWEI TIT-L01 Build/HUAWEITIT-L01) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(297, '2017-03-05 20:31:46', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(298, '2017-03-05 22:06:25', 32, '32', 1, '2a00:1028:96d4:779a:a83f:8b20:b264:735b', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(299, '2017-03-06 21:27:59', 14, '14', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(300, '2017-03-07 06:34:01', 17, 'kirchmanova@gmail.com', 1, '109.81.214.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(301, '2017-03-07 18:30:02', 36, 'Lukasholy1@gmail.com', 1, '46.135.72.253', 'Mozilla/5.0 (Linux; Android 5.1.1; D5503 Build/14.6.A.1.236) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36'),
(302, '2017-03-07 20:59:01', 64, 'jiri.kunstmuller@arcadis.com', 1, '46.235.153.43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(303, '2017-03-08 08:05:43', 17, 'kirchmanova@gmail.com', 1, '109.81.214.59', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(304, '2017-03-09 09:15:22', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(305, '2017-03-09 18:36:42', 67, 'Mgr.matyasova@email.cz', 1, '82.100.48.8', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1'),
(306, '2017-03-10 08:59:28', 17, 'kirchmanova@gmail.com', 1, '109.81.214.59', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(307, '2017-03-10 09:59:31', 17, 'kirchmanova@gmail.com', 1, '109.81.214.59', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(308, '2017-03-11 09:39:43', 68, 'petrulc@seznam.cz', 1, '212.79.110.65', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(309, '2017-03-11 14:59:42', NULL, 'barrunkaa@seznam.cz', 0, '212.79.110.28', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(310, '2017-03-11 15:00:12', NULL, 'barrunkaa@seznam.cz', 0, '212.79.110.28', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(311, '2017-03-11 15:01:05', 69, 'barrunkaa@seznam.cz', 1, '212.79.110.28', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(312, '2017-03-13 06:25:52', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(313, '2017-03-13 13:33:48', 70, '70', 1, '89.190.54.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(314, '2017-03-13 13:34:08', 25, 'r.franckova@gmail.com', 1, '89.190.54.200', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(315, '2017-03-13 15:42:55', 14, 'tomashuda@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(316, '2017-03-13 19:15:19', 17, 'kirchmanova@gmail.com', 1, '109.81.214.59', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(317, '2017-03-14 10:20:01', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(318, '2017-03-15 19:46:21', 24, '24', 1, '37.48.40.147', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2 like Mac OS X) AppleWebKit/602.3.12 (KHTML, like Gecko) Version/10.0 Mobile/14C92 Safari/602.1'),
(319, '2017-03-18 12:45:55', 17, 'kirchmanova@gmail.com', 1, '2a00:1028:83d6:2e9a:bc93:6ab3:392e:72d', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(320, '2017-03-18 15:18:29', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(321, '2017-03-19 15:37:13', 65, 'medvec.milan@gmail.com', 1, '46.135.22.13', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(322, '2017-03-20 09:21:09', 71, '71', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36'),
(323, '2017-03-20 12:19:23', 32, 'alcyonecorp@gmail.com', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(324, '2017-03-20 23:05:56', 71, '71', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(325, '2017-03-21 08:50:21', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(326, '2017-03-21 10:54:06', 72, 'dada_sudova@centrum.cz', 1, '62.240.171.50', 'Mozilla/5.0 (X11; CrOS x86_64 9000.91.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.110 Safari/537.36'),
(327, '2017-03-23 09:05:55', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:5959:e8ee:1870:3201', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(328, '2017-03-23 09:06:02', 24, '24', 1, '2a00:1028:83d6:2e9a:5959:e8ee:1870:3201', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(329, '2017-03-24 06:58:06', NULL, 'lukasholy1@gmail.com', 0, '147.228.135.88', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(330, '2017-03-24 06:58:13', NULL, 'lukasholy1@gmail.com', 0, '147.228.135.88', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(331, '2017-03-24 07:00:51', 36, 'lukasholy1@gmail.com', 1, '147.228.135.88', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(332, '2017-03-24 07:31:46', 73, 'mnoukilpoirot@gmail.com', 1, '94.113.252.18', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(333, '2017-03-24 15:59:01', NULL, 'tomashuda@gmail.com', 0, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(334, '2017-03-24 15:59:08', 14, '14', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(335, '2017-03-24 15:59:26', 30, '30', 1, '93.99.39.209', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.110 Safari/537.36'),
(336, '2017-03-25 01:32:06', NULL, 'helcad82@seznam.cz', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(337, '2017-03-25 01:32:25', NULL, 'helcad82@seznam.cz', 0, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(338, '2017-03-25 01:32:43', 38, '38', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(339, '2017-03-25 06:06:36', 46, 'nova.pavla@volny.cz', 1, '88.102.107.86', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(340, '2017-03-25 18:36:51', NULL, 'tomashuda@gmail.com', 0, '2a00:1028:83d6:96b6:edba:eda0:6231:7dcb', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(341, '2017-03-25 18:43:29', 14, 'tomashuda@gmail.com', 1, '2a00:1028:83d6:96b6:edba:eda0:6231:7dcb', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:51.0) Gecko/20100101 Firefox/51.0'),
(342, '2017-03-26 07:55:22', NULL, 'Lukasholy1@gmail.com', 0, '89.102.115.188', 'Mozilla/5.0 (Linux; Android 5.1.1; D5503 Build/14.6.A.1.236) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(343, '2017-03-26 07:55:35', 36, 'Lukasholy1@gmail.com', 1, '89.102.115.188', 'Mozilla/5.0 (Linux; Android 5.1.1; D5503 Build/14.6.A.1.236) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(344, '2017-03-26 17:47:23', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(345, '2017-03-26 21:12:48', 32, '32', 1, '2a00:1028:96d4:779a:e92e:5d3f:bc64:b6c5', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(346, '2017-03-27 10:43:58', NULL, 'Lukasholy1@gmail.com', 0, '147.228.135.199', 'Mozilla/5.0 (Linux; Android 5.1.1; D5503 Build/14.6.A.1.236) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(347, '2017-03-27 10:44:12', 36, 'Lukasholy1@gmail.com', 1, '147.228.135.199', 'Mozilla/5.0 (Linux; Android 5.1.1; D5503 Build/14.6.A.1.236) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(348, '2017-03-27 14:40:08', 69, 'barrunkaa@seznam.cz', 1, '212.79.110.28', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(349, '2017-03-28 18:39:06', 74, 'Tandema@seznam.cz', 1, '46.135.68.245', 'Mozilla/5.0 (Linux; Android 5.1; HUAWEI LYO-L01 Build/HUAWEILYO-L01) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.126 Mobile Safari/537.36'),
(350, '2017-03-28 20:51:22', 14, 'tomashuda@gmail.com', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(351, '2017-03-29 06:23:49', 69, 'barrunkaa@seznam.cz', 1, '212.79.110.28', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(352, '2017-03-29 11:50:14', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:64e2:5545:68a2:ca64', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(353, '2017-03-29 11:50:23', 24, '24', 1, '2a00:1028:83d6:2e9a:64e2:5545:68a2:ca64', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(354, '2017-03-30 19:33:26', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:70a6:c048:543c:2476', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(355, '2017-03-30 19:33:39', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:70a6:c048:543c:2476', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(356, '2017-03-30 19:34:02', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:70a6:c048:543c:2476', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(357, '2017-03-30 19:35:34', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:70a6:c048:543c:2476', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(358, '2017-03-30 19:54:38', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:70a6:c048:543c:2476', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(359, '2017-03-31 06:16:47', 28, 'hudova.lucka@seznam.cz', 1, '109.81.213.213', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-G935F Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(360, '2017-03-31 11:42:30', 24, '24', 1, '2a00:1028:83d6:2e9a:70db:1eb4:455a:71bc', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(361, '2017-04-01 14:06:05', 14, '14', 1, '37.48.35.191', 'Mozilla/5.0 (Linux; Android 5.0.1; GT-I9505 Build/LRX22C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Mobile Safari/537.36'),
(362, '2017-04-02 20:34:23', 14, '14', 1, '212.79.110.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0'),
(363, '2017-04-03 14:00:35', 75, 'h_p_b@centrum.cz', 1, '185.153.195.86', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'),
(364, '2017-04-03 17:29:26', 76, 'Pejsovamonika@seznam.cz', 1, '89.24.174.84', 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1'),
(365, '2017-04-07 04:57:05', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(366, '2017-04-07 05:25:22', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(367, '2017-04-07 05:26:26', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(368, '2017-04-07 05:37:03', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(369, '2017-04-07 05:47:16', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(370, '2017-04-07 05:58:08', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(371, '2017-04-07 06:10:41', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(372, '2017-04-07 07:09:51', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:b529:769:17b2:5def', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(373, '2017-04-07 07:12:52', NULL, 'kirchmanova@gmail.com', 0, '2a00:1028:83d6:2e9a:b529:769:17b2:5def', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(374, '2017-04-07 07:48:35', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(375, '2017-04-07 08:13:06', NULL, 'j.reitinger@centrum.cz', 0, '147.228.125.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36 OPR/44.0.2510.857'),
(376, '2017-04-07 08:14:09', 77, 'j.reitinger@centrum.cz', 1, '147.228.125.122', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.98 Safari/537.36 OPR/44.0.2510.857'),
(377, '2017-04-07 08:20:33', 78, '78', 1, '37.48.62.203', 'Mozilla/5.0 (Linux; Android 6.0.1; Redmi 4 Build/MMB29M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.132 Mobile Safari/537.36 [FB_IAB/MESSENGER;FBAV/112.0.0.17.70;]'),
(378, '2017-04-10 11:34:31', 15, 'mlha-martin@seznam.cz', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(379, '2017-04-11 07:11:19', 32, '32', 1, '93.99.39.209', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(380, '2017-04-11 08:11:45', 69, 'barrunkaa@seznam.cz', 1, '194.228.216.126', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(381, '2017-04-12 14:25:07', 58, 'risa.muck@seznam.cz', 1, '62.209.230.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'),
(382, '2017-04-13 13:32:59', 42, '42', 1, '94.112.52.222', 'Mozilla/5.0 (Linux; U; Android 5.0.2; cs-cz; Redmi Note 3 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.146 Mobile Safari/537.36 XiaoMi/MiuiBrowser/8.5.9'),
(383, '2017-04-17 17:27:55', 79, 'mail@janvanecek.biz', 1, '194.228.79.150', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.1 Safari/603.1.30');

-- --------------------------------------------------------

--
-- Struktura tabulky `register_log`
--

CREATE TABLE IF NOT EXISTS `register_log` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `message` varchar(49) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `role`
--

INSERT INTO `role` (`id`, `name`, `title`, `active`) VALUES
(1, 'admin', 'Admin', 1),
(2, 'user', 'Uživatel', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` mediumtext
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'aaa', 'aaaaaaaaaaaa'),
(2, 'bbb', 'bbbbb'),
(3, 'text_help', '<p>Aaaa bbb ccc.</p>\n\n<p>Dddd eee fff.</p>\n');

-- --------------------------------------------------------

--
-- Struktura tabulky `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `id` smallint(5) unsigned NOT NULL COMMENT 'id',
  `shortcut` varchar(2) NOT NULL COMMENT 'zkratka',
  `name` varchar(100) NOT NULL COMMENT 'název',
  `phone` varchar(5) DEFAULT NULL COMMENT 'tel. předvolba',
  `not_deleted` tinyint(1) unsigned DEFAULT '1' COMMENT 'aktivní'
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8 COMMENT='státy';

--
-- Vypisuji data pro tabulku `state`
--

INSERT INTO `state` (`id`, `shortcut`, `name`, `phone`, `not_deleted`) VALUES
(1, 'AF', 'Afghánistán', NULL, 1),
(2, 'AL', 'Albánie', NULL, 1),
(3, 'DZ', 'Alžírsko', '213', 1),
(4, 'AS', 'Americká Samoa', NULL, 1),
(5, 'VI', 'Americké Panenské ostrovy', '84', 1),
(6, 'AD', 'Andorra', '376', 1),
(7, 'AO', 'Angola', '244', 1),
(8, 'AI', 'Anguilla', '1264', 1),
(9, 'AQ', 'Antarktida', NULL, 1),
(10, 'AG', 'Antigua a Barbuda', '1268', 1),
(11, 'AR', 'Argentina', '54', 1),
(12, 'AM', 'Arménie', '374', 1),
(13, 'AW', 'Aruba', '297', 1),
(14, 'AU', 'Austrálie', '61', 1),
(15, 'AZ', 'Ázerbájdžán', '994', 1),
(16, 'BS', 'Bahamy', '1242', 1),
(17, 'BH', 'Bahrajn', '973', 1),
(18, 'BD', 'Bangladéš', '880', 1),
(19, 'BB', 'Barbados', '1246', 1),
(20, 'BE', 'Belgie', '32', 1),
(21, 'BZ', 'Belize', '501', 1),
(22, 'BJ', 'Benin', '229', 1),
(23, 'BM', 'Bermudy', '1441', 1),
(24, 'BY', 'Bělorusko', '375', 1),
(25, 'BT', 'Bhútán', '975', 1),
(26, 'BO', 'Bolívie', '591', 1),
(27, 'BA', 'Bosna a Hercegovina', '387', 1),
(28, 'BW', 'Botswana', '267', 1),
(29, 'BV', 'Bouvetův ostrov', NULL, 1),
(30, 'BR', 'Brazílie', '55', 1),
(31, 'IO', 'Britské Indickooceánské území', NULL, 1),
(32, 'VG', 'Britské Panenské ostrovy', '84', 1),
(33, 'BN', 'Brunej', '673', 1),
(34, 'BG', 'Bulharsko', '359', 1),
(35, 'BF', 'Burkina Faso', '226', 1),
(36, 'BI', 'Burundi', '257', 1),
(37, 'CK', 'Cookovy ostrovy', '682', 1),
(38, 'TD', 'Čad', NULL, 1),
(39, 'CZ', 'Česká republika', '420', 1),
(40, 'CN', 'Čína', '86', 1),
(41, 'DK', 'Dánsko', '45', 1),
(42, 'CD', 'Demokratická republika Kongo', NULL, 1),
(43, 'DM', 'Dominika', '1809', 1),
(44, 'DO', 'Dominikánská republika', '1809', 1),
(45, 'DJ', 'Džibutsko', '253', 1),
(46, 'EG', 'Egypt', '20', 1),
(47, 'EC', 'Ekvádor', '593', 1),
(48, 'ER', 'Eritrea', '291', 1),
(49, 'EE', 'Estonsko', '372', 1),
(50, 'ET', 'Etiopie', '251', 1),
(51, 'FO', 'Faerské ostrovy', '298', 1),
(52, 'FK', 'Falklandy', '500', 1),
(53, 'FJ', 'Fidži', '679', 1),
(54, 'PH', 'Filipíny', '63', 1),
(55, 'FI', 'Finsko', '358', 1),
(56, 'FR', 'Francie', '33', 1),
(57, 'GF', 'Francouzská Guyana', '594', 1),
(58, 'TF', 'Francouzská jižní území', NULL, 1),
(59, 'PF', 'Francouzská Polynésie', '689', 1),
(60, 'GA', 'Gabon', '241', 1),
(61, 'GM', 'Gambie', '220', 1),
(62, 'GZ', 'Gaza', NULL, 1),
(63, 'GH', 'Ghana', '233', 1),
(64, 'GI', 'Gibraltar', '350', 1),
(65, 'GD', 'Grenada', '1473', 1),
(66, 'GL', 'Grónsko', '299', 1),
(67, 'GE', 'Gruzie', '7880', 1),
(68, 'GP', 'Guadeloupe', '590', 1),
(69, 'GU', 'Guam', '671', 1),
(70, 'GT', 'Guatemala', '502', 1),
(71, 'GG', 'Guernsey', NULL, 1),
(72, 'GN', 'Guinea', '224', 1),
(73, 'GW', 'Guinea-Bissau', '245', 1),
(74, 'GY', 'Guyana', '592', 1),
(75, 'HT', 'Haiti', '509', 1),
(76, 'HM', 'Heardův ostrov a McDonaldovy ostrovy', NULL, 1),
(77, 'HN', 'Honduras', '504', 1),
(78, 'HK', 'Hongkong', '852', 1),
(79, 'CL', 'Chile', '56', 1),
(80, 'HR', 'Chorvatsko', '385', 1),
(81, 'IN', 'Indie', '91', 1),
(82, 'ID', 'Indonésie', '62', 1),
(83, 'IQ', 'Irák', '964', 1),
(84, 'IR', 'Írán', '98', 1),
(85, 'IE', 'Irsko', '353', 1),
(86, 'IS', 'Island', '354', 1),
(87, 'IT', 'Itálie', '39', 1),
(88, 'IL', 'Izrael', '972', 1),
(89, 'JM', 'Jamajka', '1876', 1),
(90, 'JP', 'Japonsko', '81', 1),
(91, 'YE', 'Jemen', '967', 1),
(92, 'JE', 'Jersey', NULL, 1),
(93, 'ZA', 'Jihoafrická republika', '27', 1),
(94, 'GS', 'Jižní Georgie a Jižní Sandwichovy ostrovy', NULL, 1),
(95, 'KR', 'Jižní Korea', '82', 1),
(96, 'JO', 'Jordánsko', '962', 1),
(97, 'KY', 'Kajmanské ostrovy', '1345', 1),
(98, 'KH', 'Kambodža', '855', 1),
(99, 'CM', 'Kamerun', '237', 1),
(100, 'CA', 'Kanada', '1', 1),
(101, 'CV', 'Kapverdy', '238', 1),
(102, 'QA', 'Katar', '974', 1),
(103, 'KZ', 'Kazachstán', '7', 1),
(104, 'KE', 'Keňa', '254', 1),
(105, 'KI', 'Kiribati', '686', 1),
(106, 'CC', 'Kokosové ostrovy', NULL, 1),
(107, 'CO', 'Kolumbie', '57', 1),
(108, 'KM', 'Komory', '269', 1),
(109, 'CG', 'Kongo', '242', 1),
(110, 'CR', 'Kostarika', '506', 1),
(111, 'CU', 'Kuba', '53', 1),
(112, 'KW', 'Kuvajt', '965', 1),
(113, 'CY', 'Kypr', '357', 1),
(114, 'KG', 'Kyrgyzstán', '996', 1),
(115, 'LA', 'Laos', '856', 1),
(116, 'LS', 'Lesotho', '266', 1),
(117, 'LB', 'Libanon', '961', 1),
(118, 'LR', 'Libérie', '231', 1),
(119, 'LY', 'Libye', '218', 1),
(120, 'LI', 'Lichtenštejnsko', '417', 1),
(121, 'LT', 'Litva', '370', 1),
(122, 'LV', 'Lotyšsko', '371', 1),
(123, 'LU', 'Lucebursko', '352', 1),
(124, 'MO', 'Macao', '853', 1),
(125, 'MG', 'Madagaskar', '261', 1),
(126, 'HU', 'Maďarsko', '36', 1),
(127, 'MK', 'Makedonie', '389', 1),
(128, 'MY', 'Malajsie', '60', 1),
(129, 'MW', 'Malawi', '265', 1),
(130, 'MV', 'Maledivy', '960', 1),
(131, 'ML', 'Mali', '223', 1),
(132, 'MT', 'Malta', '356', 1),
(133, 'MA', 'Maroko', '212', 1),
(134, 'MH', 'Marshallovy ostrovy', '692', 1),
(135, 'MQ', 'Martinik', '596', 1),
(136, 'MU', 'Mauricius', NULL, 1),
(137, 'MR', 'Mauritánie', '222', 1),
(138, 'YT', 'Mayotte', '269', 1),
(139, 'MX', 'Mexiko', '52', 1),
(140, 'FM', 'Mikronésie', '691', 1),
(141, 'MD', 'Moldavsko', '373', 1),
(142, 'MC', 'Monako', '377', 1),
(143, 'MN', 'Mongolsko', '95', 1),
(144, 'MS', 'Montserrat', '1664', 1),
(145, 'MZ', 'Mosambik', '258', 1),
(146, 'MM', 'Myanmar', NULL, 1),
(147, 'NA', 'Namibie', '264', 1),
(148, 'NR', 'Nauru', '674', 1),
(149, 'NP', 'Nepál', '670', 1),
(150, 'DE', 'Německo', '49', 1),
(151, 'NE', 'Niger', '227', 1),
(152, 'NG', 'Nigérie', '234', 1),
(153, 'NI', 'Nikaragua', '505', 1),
(154, 'NU', 'Niue', '683', 1),
(155, 'AN', 'Nizozemské Antily', NULL, 1),
(156, 'NL', 'Nizozemsko', '31', 1),
(157, 'NF', 'Norfolk', '672', 1),
(158, 'NO', 'Norsko', '47', 1),
(159, 'NC', 'Nová Kaledonie', '687', 1),
(160, 'NZ', 'Nový Zéland', '64', 1),
(161, 'OM', 'Omán', '968', 1),
(162, 'IM', 'Ostrov Man', NULL, 1),
(163, 'PK', 'Pákistán', NULL, 1),
(164, 'PW', 'Palau', '680', 1),
(165, 'PA', 'Panama', '507', 1),
(166, 'PG', 'Papua Nová Guinea', '675', 1),
(167, 'PY', 'Paraguay', '595', 1),
(168, 'PE', 'Peru', '51', 1),
(169, 'PN', 'Pitcairn', NULL, 1),
(170, 'CI', 'Pobřeží slonoviny', NULL, 1),
(171, 'PL', 'Polsko', '48', 1),
(172, 'PR', 'Portoriko', '1787', 1),
(173, 'PT', 'Portugalsko', '351', 1),
(174, 'AT', 'Rakousko', '43', 1),
(175, 'RE', 'Réunion', '262', 1),
(176, 'GQ', 'Rovníková Guinea', '240', 1),
(177, 'RU', 'Rusko', '7', 1),
(178, 'RO', 'Rumunsko', '40', 1),
(179, 'RW', 'Rwanda', '250', 1),
(180, 'GR', 'Řecko', '30', 1),
(181, 'PM', 'Saint Pierre a Miquelon', NULL, 1),
(182, 'SV', 'Salvador', '503', 1),
(183, 'WS', 'Samoa', NULL, 1),
(184, 'SM', 'San Marino', '378', 1),
(185, 'SA', 'Saúdská Arábie', '966', 1),
(186, 'SN', 'Senegal', '221', 1),
(187, 'KP', 'Severní Korea', '850', 1),
(188, 'MP', 'Severní Mariany', NULL, 1),
(189, 'SC', 'Seychely', '1758', 1),
(190, 'SL', 'Sierra Leone', '232', 1),
(191, 'SG', 'Singapur', '65', 1),
(192, 'SK', 'Slovensko', '421', 1),
(193, 'SI', 'Slovinsko', '963', 1),
(194, 'SO', 'Somálsko', '252', 1),
(195, 'AE', 'Spojené arabské emiráty', '971', 1),
(196, 'US', 'Spojené státy', '1', 1),
(197, 'UM', 'Spojené státy – menší odlehlé ostrovy', NULL, 1),
(198, 'CS', 'Srbsko', '381', 1),
(199, 'LK', 'Srí Lanka', '94', 1),
(200, 'CF', 'Středoafrická republika', '236', 1),
(201, 'SR', 'Surinam', '597', 1),
(202, 'SD', 'Súdán', '249', 1),
(203, 'SJ', 'Svalbard', NULL, 1),
(204, 'SH', 'Svatá Helena', '290', 1),
(205, 'KN', 'Svatý Kryštof a Nevis', '1869', 1),
(206, 'LC', 'Svatá Lucie', NULL, 1),
(207, 'ST', 'Svatý Tomáš', '239', 1),
(208, 'VC', 'Svatý Vincenc a Grenadiny', NULL, 1),
(209, 'SZ', 'Svazijsko', '268', 1),
(210, 'SY', 'Sýrie', NULL, 1),
(211, 'SB', 'Šalamounovy ostrovy', '677', 1),
(212, 'ES', 'Španělsko', '34', 1),
(213, 'SE', 'Švédsko', '46', 1),
(214, 'CH', 'Švýcarsko', '41', 1),
(215, 'TJ', 'Tádžikistán', '7', 1),
(216, 'TZ', 'Tanzanie', NULL, 1),
(217, 'TH', 'Thajsko', '66', 1),
(218, 'TW', 'Tchaj-wan', '886', 1),
(219, 'TG', 'Togo', '228', 1),
(220, 'TK', 'Tokelau', NULL, 1),
(221, 'TO', 'Tonga', '676', 1),
(222, 'TT', 'Trinidad a Tobago', '1868', 1),
(223, 'TN', 'Tunisko', '216', 1),
(224, 'TR', 'Turecko', '90', 1),
(225, 'TM', 'Turkmenistán', '993', 1),
(226, 'TC', 'Turks a Caicos', '1649', 1),
(227, 'TV', 'Tuvalu', '688', 1),
(228, 'UG', 'Uganda', '256', 1),
(229, 'UA', 'Ukrajina', '380', 1),
(230, 'UY', 'Uruguay', '598', 1),
(231, 'UZ', 'Uzbekistán', '7', 1),
(232, 'CX', 'Vánoční ostrov', NULL, 1),
(233, 'VA', 'Vatikán', '379', 1),
(234, 'VU', 'Vanuatu', '678', 1),
(235, 'VE', 'Venezuela', '58', 1),
(236, 'GB', 'Velká Británie', '44', 1),
(237, 'VN', 'Vietnam', '84', 1),
(238, 'TP', 'Východní Timor', NULL, 1),
(239, 'WF', 'Wallis a Futuna', '681', 1),
(240, 'ZM', 'Zambie', '260', 1),
(241, 'EH', 'Západní Sahara', NULL, 1),
(242, 'ZW', 'Zimbabwe', '263', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `supported_organization`
--

CREATE TABLE IF NOT EXISTS `supported_organization` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` tinyint(3) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL,
  `password` varchar(64) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `sid` varchar(250) DEFAULT NULL COMMENT 'identifikator do plateb',
  `parent_id` int(11) unsigned DEFAULT NULL,
  `fb_id` bigint(20) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `user`
--

INSERT INTO `user` (`id`, `password`, `firstname`, `lastname`, `email`, `created`, `active`, `sid`, `parent_id`, `fb_id`) VALUES
(1, '$2y$10$KL8GuoGgs2g47E32IOfLK.N6QpvsTiN5Qos0hfaHqlUJ7nHQSqFCi', 'Adminssss', 'Admin', 'admin@rtsoft.cz', '2015-01-14 16:30:10', 0, 'identifikatorplateb', NULL, NULL),
(13, '$2y$10$ySOUAPQldTY9lTciYgFGRuqXOiPK5hvnfceV4t3bzQ8BSXgSQoC96', '', '', 't.franckova@gmail.com', '2016-11-10 08:24:01', 1, 'bcef6cc02dcb2eb931747664393bb7a8841fefb7', NULL, NULL),
(14, '$2y$10$d9hHxWW4bM85esWFUJYCHu5LWvsmWQi3W1FSo0ctd5JqsptNSb002', '', '', 'tomashuda@gmail.com', '2016-11-10 10:53:22', 1, '96835dd8bfa718bd6447ccc87af89ae1675daeca', NULL, 10207663325013559),
(15, '$2y$10$gyWqo3mgaKrYHvhdfxJfHepSMGObgFi4W/M4b9YfUKiSSrT6Xg7fa', '', '', 'mlha-martin@seznam.cz', '2016-11-11 09:59:03', 1, '477ffc2291dd5745d63f4772393fa1d21ea24ca5', 13, NULL),
(16, '$2y$10$paOzlFJ13Rszd6YvXAt13Ol0sjCK58PTZpg8rKcPReinlyYJPdL6e', '', '', 'karofast@gmail.com', '2016-11-11 14:37:01', 1, '746610bf3cdcbe3be3ea9b1ae94fbd3a0217d830', NULL, NULL),
(17, '$2y$10$s7WEAvqVpmCf2UQU.yeaqufS16iFCx4xV/JJ6rq/zkU5kO98PYIRu', '', '', 'kirchmanova@gmail.com', '2016-11-13 13:38:30', 1, '741cc9583e98a547e6e3be757fdca8c1d6dcfa0e', NULL, NULL),
(18, '$2y$10$IZi1DQVx0c0YOxoIGNO9peORNdkIG/b8wl9hfMKIVarGRuzo6NzV2', '', '', 'kurdik@rtsoft.cz', '2016-12-18 18:26:18', 1, '243966f8917aff622cea5dd0c06a372766aea221', NULL, NULL),
(19, '', '', '', 'michael.kurdik@seznam.cz', '2017-01-10 20:55:13', 1, 'da65ef7f70cb58c41332ac0f25d063157bafeeed', NULL, 10208031503008775),
(20, '', '', '', 'radek@tancouz.com', '2017-01-11 09:11:11', 1, '1e3534dee4d291f8502a6178cf1e24fa9f37474e', NULL, 10211366078258353),
(21, '', '', '', 'sailorp@centrum.cz', '2017-01-11 12:22:20', 1, '8606cd3e054c7559c581971a3f2ad1ead2459967', NULL, 10208072474754653),
(22, '$2y$10$/qGqv8WDIg.wuvnERvyNVeDXGhCD6hHCDwNwHun/S.eQR57fPcVJm', '', '', 'mariekirchmanova@seznam.cz', '2017-01-11 19:47:22', 1, 'ff2aba6a2ef142a668f0096439a0281dbf51e19f', NULL, 10208103444012522),
(23, '$2y$10$kuWwm36/QVX9Cc/mUhHibOlVpvWKuXi32gUjP7G2aXMQ0F4Yfgn1m', '', '', 'filip.hlad@gmail.com', '2017-01-13 07:57:05', 1, '158aec06a66da73c04406db61e41b76cce2a7b7f', NULL, NULL),
(24, '', '', '', 'marti.kir@seznam.cz', '2017-01-13 08:08:52', 1, 'b1809327264d1f0c52493a22e0d0ca84e037422e', NULL, 10212255241362739),
(25, '$2y$10$uAmeA8zXYJAGWR4X09w6H.vTHxVL1Hw7KZxBsqo.gXwNWyxuK1hza', '', '', 'r.franckova@gmail.com', '2017-01-19 16:12:01', 1, 'ffa9a0a2f48d93c0380700ba99ae6c286e24c75c', 13, NULL),
(26, '', '', '', 'bedrich@4c.cz', '2017-01-20 10:30:31', 1, '47c8dcedf518711483be6a2c52dfd1a779905144', NULL, 1281362615258561),
(27, '$2y$10$iaaQY0rL7zlzPiiJ2YJui.HyLGQRcxNigAGKmi4JdyA5ncwJydoam', '', '', 'dena.sramkova@seznam.cz', '2017-01-25 14:11:48', 1, 'db7916c25ccd6953d0eec2133df8c0dc8a316648', NULL, NULL),
(28, '$2y$10$Pc9J1/alUv0N6wcEs4x/B.gAfcDV3rMWZU0SDIdw5J99919y1rsMa', '', '', 'hudova.lucka@seznam.cz', '2017-01-25 18:40:27', 1, 'ed2c96dc3bc963b4dea2dc996fef013757241ff8', NULL, NULL),
(30, '', '', '', '8luckyluk8@seznam.cz', '2017-02-02 17:34:15', 1, '8dca0557ee002c7113aadac3c5d2c29c5421b9c6', NULL, 10208115546507081),
(31, '$2y$10$S.JmyOHYP54WwWelmWgZTOQQ//lAnB88rABIRackFCZ41cl9EOEe2', '', '', 'martin.zika@desinsekta.cz', '2017-02-06 16:54:44', 1, '16f36b0e7f4869c2ab925c977fad5bc3f58cf571', NULL, 953888194746059),
(32, '$2y$10$nKMm/OuwZZm4Ac.fCj6lOe02TvgtNOX8wqdrE2L5yZCzq42vX2YxW', '', '', 'alcyonecorp@gmail.com', '2017-02-07 11:23:46', 1, 'ec172baee90432bffc00c6f4f435c91ef67e89af', NULL, 1035861689892192),
(33, '$2y$10$3MoAmGn3sqpc.ZjEGFWVLura8UeV/A3x/KB4yALIchw/cGwgxBrFa', '', '', 'ladyzaba@gmail.com', '2017-02-07 14:51:37', 1, 'ec52c5c605979443f8b1073b1fa85768e7ff3982', NULL, NULL),
(35, '$2y$10$abEZyZkPW3eqj0OLD/FfXOZVXxy1Md.AJKi5zKRyUIIqPCeH9KQve', '', '', 'cashback@centrum.cz', '2017-02-10 11:57:45', 1, '178c7e8b9dcb62e88a7aa46aae52d995410cf5e8', NULL, NULL),
(36, '$2y$10$XSBwDmFDXdiab.5hC9MLm.g.tvebnG2QUlpcGuYfn1NR5QwWVdPXy', '', '', 'Lukasholy1@gmail.com', '2017-02-17 06:57:09', 1, '961e8d7a3fc3612477d5e16b8267bae5c819cb5a', NULL, NULL),
(37, '$2y$10$AitUGTTL0CMHxpu4RqXDEe6hl2nTSlfTfgwgZDbQaZ0p54PovQgS2', '', '', 'tttpapi@gmail.com', '2017-02-17 08:57:13', 1, '9e8bc93ffa2c64747321df6e7eb9900d7c6c9553', NULL, NULL),
(38, '$2y$10$ZH//qJpVidNI5UJFO9DbreSE2R9CTnH1lsW0pWlsGwsIL5xJp1DAi', '', '', 'helcad82@seznam.cz', '2017-02-17 10:53:40', 1, '0330d47190ac50e2dd01958d291f9c94e950dc8d', NULL, 1687415611272458),
(39, '$2y$10$3FtlObSKFSqz91cZPpqFXu2qTZFzU0hLHIdwTmyErVZ4WoAHj/BVy', '', '', 'lexbrychta@gmail.com', '2017-02-18 07:16:52', 1, 'b678ed9f6fcec06291d8b859d0c23bed8c8dcded', NULL, NULL),
(40, '', '', '', 'michael@pavlista.cz', '2017-02-20 15:45:35', 1, '81109d4cf0c4cc2e5163725de593b8cfdd5d8f1e', NULL, 10208312289912839),
(42, '', '', '', NULL, '2017-02-21 09:55:59', 1, '3f1e2233412c5787d3960aa7c4042e0fd1466e6a', 38, 597720767088260),
(43, '', '', '', 'stecova.dirn@seznam.cz', '2017-02-21 11:35:56', 1, '3477beede8cca6f5c74db907f58465b280a0e01e', 28, 1360095094012625),
(44, '$2y$10$O4sudNgTEAMXCwK5C17P4u7gxkv.0lhPFNdIkw.cX3KmkkoDp7a/i', '', '', 'atamandi@seznam.cz', '2017-02-21 13:32:23', 1, 'aed38b9c0840145cf711654023a4c9f4e1704069', 28, NULL),
(45, '', '', '', 'petr.myslikovjan@seznam.cz', '2017-02-22 17:33:29', 1, '444ce4ac95f3d5b17701d8b3dd0aa762b6779666', 38, 1575930412421518),
(46, '$2y$10$LD2My/wD/jdjBoFfIje11uUQuf9znR/nH/bgyoHCs5lG6C/VzA3Q2', '', '', 'nova.pavla@volny.cz', '2017-02-22 18:40:43', 1, 'c64c94ba49656b7b814554091d3a94256bdb5022', 38, NULL),
(47, '$2y$10$aci2fHUXq3K0oEr7tRTTUeCr930uJq6C30r52bw.eOpU00Woslm2q', '', '', 'mir.kovarik@gmail.com', '2017-02-22 18:44:26', 1, 'f1e883b0b730dcaaf28b22f329750bb11875c297', 38, NULL),
(48, '$2y$10$c3DlO3k6/a4PxUZYbHDIPepI0QfSPU655t51qTyg1DVn5LKACb74u', '', '', 'pepulka@seznam.cz', '2017-02-22 19:22:33', 1, '37e6c00b935ecc9137bb2a593258b13a82ea5a48', 38, 1228287110625066),
(49, '$2y$10$l4p/eHylqFPc8qFpONYSF.h/MhUdsCLEoE0QiRjhu23HnCjvia3mq', '', '', 'pohlova29@seznam.cz', '2017-02-22 19:23:23', 0, '398f8fd1a3c43d302ec4571be1b472aee5c74b09', 38, 1362050947150867),
(51, '$2y$10$GXyyK6SvI49jmXgJEiLZ0uFfH/AAFX6VkO2ki8iZ9TMXvIhOjvv.K', '', '', 'pohlova299@seznam.cz', '2017-02-22 19:24:46', 0, '087dfb9ff89a445f90baff2db8a48f57da28b582', 38, NULL),
(52, '$2y$10$hvFaDOKy/w/3i9XDRXCZyerXhuhoiG97Au9.wXgvjyA0vBVjM3W/y', '', '', 'Dana.zalabakova@seznam.cz', '2017-02-22 20:17:57', 1, 'fbc5b6bad29202df0f692ddc4051e18bb5cc1e04', 38, NULL),
(57, '', '', '', 'danesovab@seznam.cz', '2017-02-23 08:16:29', 1, '85365f86d6deca0d16c2f6fe4949996d5dcabfae', 38, 1590079801006757),
(58, '$2y$10$8JF/69DKZ7NpmmOjiyRYZOEC6QviuyR2lrPponwCBuYqm/SavgoXq', '', '', 'risa.muck@seznam.cz', '2017-02-23 16:44:27', 1, 'db1ab01670319e102e95c0cd91c58fd366acd79d', NULL, NULL),
(59, '$2y$10$RvCr.L0.XFu20s0GRPOiceR6F8h25gYDGdffhvYloXeUwOJQZ3qeO', '', '', 'Simona.Sitorova@seznam.cz', '2017-02-23 17:11:13', 1, '2f1e1c088b08a947ace8bfb37a8973e4f552da9c', 38, NULL),
(60, '$2y$10$oBcaQ3Hf9OoKb9CB4xHNVOb/ZvEPbGBs5EZOcEveBjAHTRaZYDYdi', '', '', 'rackova@seznam.cz', '2017-02-23 20:27:55', 0, 'f4df3b384136532899a62eb549e349703b70f555', 38, NULL),
(61, '$2y$10$jGnW8.JefiOpdqj8GXzNwu5h8H4yedTIhFnjaF9ZO7ONN6UOz.VRS', '', '', 'marek.mj@seznam.cz', '2017-02-25 07:16:23', 1, '4c43c19230e661445cca315579e78fa8e57d2a2b', 38, NULL),
(63, '$2y$10$tKFeESskVunh1RiY3nPV6OImL5LhX473uqxFxXc5o9TcAS7g04F9.', '', '', 'ian.666@seznam.cz', '2017-02-25 07:19:35', 0, '1966fb41b5277118b65f52c65085ef7f99a18cae', 38, NULL),
(64, '$2y$10$Vdy7rKQsRR.oeg2xBMXxBONg9T9p4WYGRf8ebXY5O9A7MwsbaDiju', '', '', 'jiri.kunstmuller@arcadis.com', '2017-03-02 10:21:47', 1, '035e19ed95bc76f559e26ab3eb9ea542729538e7', 14, NULL),
(65, '$2y$10$zjpUd5TL8SOA3W3PGem3b.9vAmM9aewcl6rTl/hIUN/bWje5hcC.6', '', '', 'medvec.milan@gmail.com', '2017-03-03 10:26:40', 1, '469df9759636cd31b72632465ecc96326812c581', NULL, NULL),
(66, '$2y$10$9ygTXmJ6QZzTKij6LapLY.HK.NlWHduX5oRFCMh8anXFxCLyEu0CC', '', '', 'Marketadolezalova87@gmail.com', '2017-03-04 11:00:06', 1, '790cc084e0b93735959509e0f6a46d4f0b31c0d6', NULL, NULL),
(67, '$2y$10$7KSTVWpfwviWdfNv4cWzQ.I9ih7Nlctjw69qx2mmlk7gTOahFJTTW', '', '', 'Mgr.matyasova@email.cz', '2017-03-09 18:34:39', 1, 'e2b6277be612f1daf0969602e364d6f9e64afd97', NULL, NULL),
(68, '$2y$10$XubdAVliRxDUG5FFby/9OOrvD.S9JWRWkGVlXELPXwcU.Z58HubuC', '', '', 'petrulc@seznam.cz', '2017-03-11 09:38:17', 1, '95ce327c6e44b3279dc84dcfcd6efdaf7751fa35', 14, NULL),
(69, '$2y$10$uTRcAeb3Cqgki3xRmnZjLemyLSA6XPmtqHD1gfhg258YGKDjuKBMG', '', '', 'barrunkaa@seznam.cz', '2017-03-11 15:00:01', 1, '74f58d8910dd0009e04652ae0c3c00f2b93fe0c2', NULL, NULL),
(70, '', '', '', 'r.franckova@seznam.cz', '2017-03-13 13:33:48', 1, '64002d667f31266368fd514eccaa42cd9b6b28e4', NULL, 1505079882836183),
(71, '', '', '', 'facelady11@gmail.com', '2017-03-20 09:21:08', 1, '67b702b101204e603ea754d5bf2a1bce595a39e9', NULL, 1206149752813572),
(72, '$2y$10$EWlimQ/F9GQGoUvR51Yxjuk.FjQq8Xvf06UrVR38Via2V4/NxgEO6', '', '', 'dada_sudova@centrum.cz', '2017-03-21 10:53:44', 1, 'c53fa5d47f3dd1dadf61a5b267aabf562ab02a11', NULL, NULL),
(73, '$2y$10$hLKZQBWq37I9GnXiC1Rg2eUNUttVqLOMJpM5VDXVpig3HRzuYUHYi', '', '', 'mnoukilpoirot@gmail.com', '2017-03-24 07:30:52', 1, '56db4c1c37e3a2783b46d908d07a80f6d4245655', 36, NULL),
(74, '$2y$10$UNDIMS3iqNs6SoCSIPyyQecCZar83dbSEAx1TlAHEg4Oots7Ezhhu', '', '', 'Tandema@seznam.cz', '2017-03-28 18:38:05', 1, '5a2a15a86bac33416ea36b0824b87bf5f513e792', NULL, NULL),
(75, '$2y$10$AiKjEhFXb74sFHndQJI/J.xe429I1gDPcKsw7qm9FODR3nj8Fugyy', '', '', 'h_p_b@centrum.cz', '2017-04-03 13:41:23', 1, 'f41c92aceec25ec1a00e414016c0561108125e37', NULL, NULL),
(76, '$2y$10$m.VZW3rOcPClVmfu1ZTpf.DKJf/c/hLtMsLks2F3E0ZPL/PTy6PoG', '', '', 'Pejsovamonika@seznam.cz', '2017-04-03 17:28:27', 1, '990d2c02f54a5c3533a25f1eb19fb6ffd9aa46f5', NULL, NULL),
(77, '$2y$10$bN89ResWIatP7LM1LI54W.Bw.c0fiRgQ2Tyw0OHCp9Kb0Fp.PbXbe', '', '', 'j.reitinger@centrum.cz', '2017-04-07 08:13:42', 1, '8bbff2760ceef3bfcfa6e4e1642ecba8876bbca6', NULL, NULL),
(78, '', '', '', 'lubomir.kristek@email.cz', '2017-04-07 08:20:33', 1, '3cd24223fff4db5b07208cf7cc173378df2ae618', 77, 784067985082706),
(79, '$2y$10$OIZ3FgKmt761gIBavPU3UuNsW2qFHHdDENMYkpSp3cAJYZrzNMv4O', '', '', 'mail@janvanecek.biz', '2017-04-17 17:27:12', 1, '4aabce8b7c27ced3fa0948f41199dcf1fd3306ab', 38, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(10) unsigned NOT NULL,
  `commission_log_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `commission_value` decimal(9,2) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '1',
  `type` enum('payment','storno','payout') COLLATE utf8_czech_ci NOT NULL DEFAULT 'payment'
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `user_account`
--

INSERT INTO `user_account` (`id`, `commission_log_id`, `user_id`, `commission_value`, `created`, `updated`, `confirmed`, `type`) VALUES
(1, 27, NULL, 0.17, '2016-07-20 02:59:59', '0000-00-00 00:00:00', 1, 'payment'),
(2, 28, NULL, 0.09, '2016-07-20 03:10:27', '0000-00-00 00:00:00', 1, 'payment'),
(3, 31, NULL, 0.14, '2016-08-08 03:06:41', '0000-00-00 00:00:00', 1, 'payment'),
(4, 32, NULL, 0.00, '2016-10-18 01:51:57', '0000-00-00 00:00:00', 1, 'payment'),
(5, 33, NULL, 0.14, '2016-10-27 01:39:04', '0000-00-00 00:00:00', 1, 'payment'),
(6, 34, NULL, 0.10, '2016-10-31 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(7, 35, NULL, 0.08, '2016-11-10 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(8, 36, 17, 0.16, '2016-11-28 02:11:25', '0000-00-00 00:00:00', 1, 'payment'),
(9, 37, 14, 0.20, '2016-11-26 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(10, 38, 14, 0.04, '2016-11-28 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(11, 39, 15, 0.40, '2016-12-02 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(12, 39, 13, 0.02, '2016-12-02 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(13, 40, 15, 0.04, '2016-12-02 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(14, 40, 13, 0.00, '2016-12-02 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(15, 41, 13, 0.14, '2016-12-08 03:56:51', '0000-00-00 00:00:00', 1, 'payment'),
(16, 42, 14, 0.33, '2016-12-08 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(17, 43, 14, 0.21, '2016-12-08 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(18, 44, 14, 0.45, '2016-12-09 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(19, 45, 14, 1.83, '2016-12-10 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(20, 46, 14, 0.59, '2016-12-13 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(21, 47, 14, 0.10, '2016-12-16 10:49:12', '0000-00-00 00:00:00', 1, 'payment'),
(22, 48, 14, 0.08, '2016-12-16 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(23, 49, 14, 0.43, '2016-12-16 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(25, 51, 14, 0.07, '2016-12-17 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(27, 53, 15, 0.62, '2016-12-18 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(28, 53, 13, 0.03, '2016-12-18 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(29, 54, 14, 0.29, '2016-12-19 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(30, 55, 14, 0.37, '2016-12-19 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(31, 56, 14, 0.21, '2016-12-28 04:26:25', '0000-00-00 00:00:00', 1, 'payment'),
(32, 57, 14, 0.22, '2016-12-29 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(33, 58, 14, 0.37, '2016-12-29 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(34, 96, 13, 0.36, '2016-12-16 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(35, 97, 13, 0.07, '2016-12-17 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(36, 116, 14, 0.34, '2017-01-06 12:45:22', '0000-00-00 00:00:00', 1, 'payment'),
(37, 214, 17, 0.16, '2017-01-10 04:06:25', '0000-00-00 00:00:00', 1, 'payment'),
(38, 247, 14, 0.04, '2017-01-10 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(39, 248, 24, 0.14, '2017-01-18 02:08:44', '0000-00-00 00:00:00', 1, 'payment'),
(40, 249, 14, 1.94, '2017-01-19 00:00:00', '0000-00-00 00:00:00', 1, 'payment'),
(41, 250, 22, 2.64, '2017-01-20 11:42:29', '0000-00-00 00:00:00', 1, 'payment'),
(42, 251, 22, 0.00, '2017-01-28 04:59:01', '0000-00-00 00:00:00', 1, 'payment'),
(43, 252, 14, 0.25, '2017-01-28 11:34:23', '0000-00-00 00:00:00', 1, 'payment'),
(44, 257, 22, 3.28, '2017-01-29 08:43:25', '0000-00-00 00:00:00', 1, 'payment'),
(45, 258, 14, 0.00, '2017-01-30 05:56:11', '0000-00-00 00:00:00', 1, 'payment'),
(46, 259, 22, 0.09, '2017-01-30 10:52:34', '0000-00-00 00:00:00', 1, 'payment'),
(47, 260, 22, 0.78, '2017-01-31 01:49:04', '0000-00-00 00:00:00', 1, 'payment'),
(48, 261, 22, 4.53, '2017-01-31 03:15:43', '0000-00-00 00:00:00', 1, 'payment'),
(49, 262, 22, 4.26, '2017-01-31 12:32:22', '0000-00-00 00:00:00', 1, 'payment'),
(50, 263, 30, 0.28, '2017-02-02 09:39:41', '0000-00-00 00:00:00', 1, 'payment'),
(51, 264, 14, 0.59, '2017-02-02 10:05:38', '0000-00-00 00:00:00', 1, 'payment'),
(52, 265, 22, -3.28, '2017-01-29 08:43:25', '0000-00-00 00:00:00', 1, 'storno'),
(53, 266, 31, 0.27, '2017-02-07 01:53:40', '0000-00-00 00:00:00', 1, 'payment'),
(54, 267, 25, 3.04, '2017-02-07 07:45:46', '0000-00-00 00:00:00', 1, 'payment'),
(55, 267, 13, 0.15, '2017-02-07 07:45:46', '0000-00-00 00:00:00', 1, 'payment'),
(56, 268, 22, 44.40, '2017-01-28 04:59:01', '0000-00-00 00:00:00', 1, 'payment'),
(57, 269, 25, 1.38, '2017-02-15 07:54:46', '2017-02-15 07:54:46', 1, 'payment'),
(58, 269, 13, 0.07, '2017-02-15 07:54:46', '2017-02-15 07:54:46', 1, 'payment'),
(59, 270, 25, 14.52, '2017-02-16 06:30:31', '2017-02-16 06:30:31', 1, 'payment'),
(60, 270, 13, 0.73, '2017-02-16 06:30:31', '2017-02-16 06:30:31', 1, 'payment'),
(61, 271, 14, 0.19, '2017-02-16 12:30:56', '2017-02-16 12:30:56', 1, 'payment'),
(62, 272, 25, 10.55, '2017-02-20 05:00:19', '2017-02-20 05:00:19', 1, 'payment'),
(63, 272, 13, 0.53, '2017-02-20 05:00:19', '2017-02-20 05:00:19', 1, 'payment'),
(64, 273, 38, 0.45, '2017-02-21 15:01:50', '2017-02-21 15:01:50', 1, 'payment'),
(65, 274, 25, -3.04, '2017-02-22 00:35:13', '2017-02-22 00:35:13', 1, 'storno'),
(66, 274, 13, -0.15, '2017-02-22 00:35:13', '2017-02-22 00:35:13', 1, 'storno'),
(67, 275, 25, -11.30, '2017-02-22 00:55:05', '2017-02-22 00:55:05', 1, 'storno'),
(68, 275, 13, -0.57, '2017-02-22 00:55:05', '2017-02-22 00:55:05', 1, 'storno'),
(69, 276, 31, 0.34, '2017-02-24 03:00:45', '2017-02-24 03:00:45', 1, 'payment'),
(70, 277, 31, 6.05, '2017-02-24 04:01:54', '2017-02-24 04:01:54', 1, 'payment'),
(71, 278, 13, 0.21, '2017-02-25 06:31:19', '2017-02-25 06:31:19', 1, 'payment'),
(72, 279, 25, 0.47, '2017-02-26 04:30:18', '2017-02-26 04:30:18', 1, 'payment'),
(73, 279, 13, 0.02, '2017-02-26 04:30:18', '2017-02-26 04:30:18', 1, 'payment'),
(74, 280, 25, 0.31, '2017-02-26 06:30:26', '2017-02-26 06:30:26', 1, 'payment'),
(75, 280, 13, 0.02, '2017-02-26 06:30:26', '2017-02-26 06:30:26', 1, 'payment'),
(76, 281, 38, 0.19, '2017-02-26 09:01:26', '2017-02-26 09:01:26', 1, 'payment'),
(77, 282, 13, 0.33, '2017-02-28 01:30:36', '2017-02-28 01:30:36', 1, 'payment'),
(78, 283, 32, 0.13, '2017-02-28 02:00:28', '2017-02-28 02:00:28', 1, 'payment'),
(79, 284, 31, 0.32, '2017-03-02 03:30:35', '2017-03-02 03:30:35', 1, 'payment'),
(80, 285, 31, 1.08, '2017-03-03 05:30:34', '2017-03-03 05:30:34', 1, 'payment'),
(81, 286, 25, -7.07, '2017-03-06 08:05:48', '2017-03-06 08:05:48', 1, 'storno'),
(82, 286, 13, -0.35, '2017-03-06 08:05:48', '2017-03-06 08:05:48', 1, 'storno'),
(83, 287, 32, 0.14, '2017-03-08 02:00:24', '2017-03-08 02:00:24', 1, 'payment'),
(84, 288, 22, 2.32, '2017-03-08 14:00:13', '2017-03-08 14:00:13', 1, 'payment'),
(85, 289, 67, 0.22, '2017-03-09 12:02:51', '2017-03-09 12:02:51', 1, 'payment'),
(86, 290, 14, 0.36, '2017-03-11 10:01:41', '2017-03-11 10:01:41', 1, 'payment'),
(87, 291, 67, 1.29, '2017-03-12 06:01:02', '2017-03-12 06:01:02', 1, 'payment'),
(88, 292, 31, 0.57, '2017-03-13 04:30:13', '2017-03-13 04:30:13', 1, 'payment'),
(89, 293, 25, 9.88, '2017-03-13 08:02:49', '2017-03-13 08:02:49', 1, 'payment'),
(90, 293, 13, 0.49, '2017-03-13 08:02:49', '2017-03-13 08:02:49', 1, 'payment'),
(91, 294, 22, 0.13, '2017-03-13 11:00:38', '2017-03-13 11:00:38', 1, 'payment'),
(92, 295, 31, 0.28, '2017-03-15 05:00:09', '2017-03-15 05:00:09', 1, 'payment'),
(93, 296, 25, 1.18, '2017-03-15 09:31:48', '2017-03-15 09:31:48', 1, 'payment'),
(94, 296, 13, 0.06, '2017-03-15 09:31:48', '2017-03-15 09:31:48', 1, 'payment'),
(95, 297, 31, 0.37, '2017-03-17 08:00:59', '2017-03-17 08:00:59', 1, 'payment'),
(96, 298, 14, 0.27, '2017-03-18 09:01:41', '2017-03-18 09:01:41', 1, 'payment'),
(97, 299, 31, 0.34, '2017-03-20 06:00:53', '2017-03-20 06:00:53', 1, 'payment'),
(98, 300, 15, 0.46, '2017-03-21 01:31:52', '2017-03-21 01:31:52', 1, 'payment'),
(99, 300, 13, 0.02, '2017-03-21 01:31:52', '2017-03-21 01:31:52', 1, 'payment'),
(100, 301, 14, 0.05, '2017-03-21 01:32:05', '2017-03-21 01:32:05', 1, 'payment'),
(101, 302, 14, 0.18, '2017-03-21 01:32:05', '2017-03-21 01:32:05', 1, 'payment'),
(102, 303, 15, 0.05, '2017-03-22 01:30:49', '2017-03-22 01:30:49', 1, 'payment'),
(103, 303, 13, 0.00, '2017-03-22 01:30:49', '2017-03-22 01:30:49', 1, 'payment'),
(104, 304, 31, 0.32, '2017-03-23 05:30:41', '2017-03-23 05:30:41', 1, 'payment'),
(105, 305, 24, 3.98, '2017-03-23 14:30:55', '2017-03-23 14:30:55', 1, 'payment'),
(106, 306, 24, 3.83, '2017-03-23 14:31:07', '2017-03-23 14:31:07', 1, 'payment'),
(107, 307, 24, 2.62, '2017-03-23 14:31:26', '2017-03-23 14:31:26', 1, 'payment'),
(108, 308, 31, 0.13, '2017-03-24 05:30:21', '2017-03-24 05:30:21', 1, 'payment'),
(109, 309, 25, -8.14, '2017-03-27 04:05:09', '2017-03-27 04:05:09', 1, 'storno'),
(110, 309, 13, -0.41, '2017-03-27 04:05:09', '2017-03-27 04:05:09', 1, 'storno'),
(111, 310, 25, 18.20, '2017-03-27 06:00:08', '2017-03-27 06:00:08', 1, 'payment'),
(112, 310, 13, 0.91, '2017-03-27 06:00:08', '2017-03-27 06:00:08', 1, 'payment'),
(113, 311, 31, 0.16, '2017-03-27 07:31:08', '2017-03-27 07:31:08', 1, 'payment'),
(114, 312, 31, 0.19, '2017-03-28 07:30:56', '2017-03-28 07:30:56', 1, 'payment'),
(115, 313, 24, 0.58, '2017-03-29 06:30:52', '2017-03-29 06:30:52', 1, 'payment'),
(116, 314, 22, 1.95, '2017-03-30 01:30:35', '2017-03-30 01:30:35', 1, 'payment'),
(117, 315, 27, 0.07, '2017-03-31 01:31:19', '2017-03-31 01:31:19', 1, 'payment'),
(118, 316, 31, 0.21, '2017-03-31 04:00:30', '2017-03-31 04:00:30', 1, 'payment'),
(119, 317, 17, 0.24, '2017-04-01 01:30:18', '2017-04-01 01:30:18', 1, 'payment'),
(120, 318, 27, 0.11, '2017-04-03 01:31:20', '2017-04-03 01:31:20', 1, 'payment'),
(121, 319, 31, 0.23, '2017-04-03 04:01:13', '2017-04-03 04:01:13', 1, 'payment'),
(122, 320, 31, 0.29, '2017-04-05 04:30:31', '2017-04-05 04:30:31', 1, 'payment'),
(123, 321, 22, 0.40, '2017-04-05 07:00:28', '2017-04-05 07:00:28', 1, 'payment'),
(124, 322, 14, 0.11, '2017-04-06 01:30:38', '2017-04-06 01:30:38', 1, 'payment'),
(125, 323, 17, 0.14, '2017-04-07 01:30:33', '2017-04-07 01:30:33', 1, 'payment'),
(126, 324, 14, 0.16, '2017-04-08 05:30:18', '2017-04-08 05:30:18', 1, 'payment'),
(127, 325, 14, 0.30, '2017-04-08 12:30:09', '2017-04-08 12:30:09', 1, 'payment'),
(128, 326, 32, 0.16, '2017-04-09 07:30:48', '2017-04-09 07:30:48', 1, 'payment'),
(129, 327, 31, 0.44, '2017-04-10 03:30:17', '2017-04-10 03:30:17', 1, 'payment'),
(130, 328, 38, 0.50, '2017-04-10 04:00:10', '2017-04-10 04:00:10', 1, 'payment'),
(131, 329, 15, 0.09, '2017-04-11 01:30:31', '2017-04-11 01:30:31', 1, 'payment'),
(132, 329, 13, 0.04, '2017-04-11 01:30:31', '2017-04-11 01:30:31', 1, 'payment'),
(133, 330, 25, -13.94, '2017-04-11 03:45:00', '2017-04-11 03:45:00', 1, 'storno'),
(134, 330, 13, -6.97, '2017-04-11 03:45:00', '2017-04-11 03:45:00', 1, 'storno'),
(135, 331, 31, 0.19, '2017-04-12 04:30:17', '2017-04-12 04:30:17', 1, 'payment'),
(136, 332, 38, 0.10, '2017-04-12 04:30:57', '2017-04-12 04:30:57', 1, 'payment'),
(137, 333, 38, 0.41, '2017-04-12 06:01:57', '2017-04-12 06:01:57', 1, 'payment'),
(138, 334, 17, 0.58, '2017-04-15 01:30:26', '2017-04-15 01:30:26', 1, 'payment');

-- --------------------------------------------------------

--
-- Struktura tabulky `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `user_role`
--

INSERT INTO `user_role` (`id`, `user_id`, `role_id`) VALUES
(77, 1, 1),
(78, 1, 2),
(87, 13, 2),
(88, 14, 2),
(89, 15, 2),
(90, 16, 2),
(91, 14, 1),
(96, 17, 1),
(97, 17, 2),
(98, 18, 2),
(99, 18, 1),
(100, 19, 2),
(101, 20, 2),
(102, 21, 2),
(103, 22, 2),
(104, 23, 2),
(105, 24, 2),
(106, 25, 2),
(107, 26, 2),
(108, 27, 2),
(109, 28, 2),
(110, 30, 2),
(111, 31, 2),
(112, 32, 2),
(113, 33, 2),
(115, 35, 2),
(116, 36, 2),
(117, 37, 2),
(118, 38, 2),
(119, 39, 2),
(120, 40, 2),
(122, 42, 2),
(123, 43, 2),
(124, 44, 2),
(125, 45, 2),
(126, 46, 2),
(127, 47, 2),
(128, 48, 2),
(129, 49, 2),
(130, 51, 2),
(131, 52, 2),
(132, 57, 2),
(133, 58, 2),
(134, 59, 2),
(135, 60, 2),
(136, 61, 2),
(137, 63, 2),
(138, 64, 2),
(139, 65, 2),
(140, 66, 2),
(141, 67, 2),
(142, 68, 2),
(143, 69, 2),
(144, 70, 2),
(145, 71, 2),
(146, 72, 2),
(147, 73, 2),
(148, 74, 2),
(149, 75, 2),
(150, 76, 2),
(151, 77, 2),
(152, 78, 2),
(153, 79, 2);

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `advertiser`
--
ALTER TABLE `advertiser`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `advertiser-id` (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `advertiser_id` (`advertiser_id`),
  ADD KEY `advertiser_id_2` (`advertiser_id`);

--
-- Klíče pro tabulku `advertiser_category`
--
ALTER TABLE `advertiser_category`
  ADD PRIMARY KEY (`advertiser_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Klíče pro tabulku `advertiser_event`
--
ALTER TABLE `advertiser_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `advertiser_id` (`advertiser_id`);

--
-- Klíče pro tabulku `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Klíče pro tabulku `commission_log`
--
ALTER TABLE `commission_log`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `commison_unique` (`commission_id`,`event_date`),
  ADD KEY `cid` (`advertiser_id`),
  ADD KEY `sid` (`sid`);

--
-- Klíče pro tabulku `login_log`
--
ALTER TABLE `login_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Klíče pro tabulku `register_log`
--
ALTER TABLE `register_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Klíče pro tabulku `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Klíče pro tabulku `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `shortcut` (`shortcut`);

--
-- Klíče pro tabulku `supported_organization`
--
ALTER TABLE `supported_organization`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `sid` (`sid`),
  ADD UNIQUE KEY `fb_id` (`fb_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Klíče pro tabulku `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `commission_log_id` (`commission_log_id`);

--
-- Klíče pro tabulku `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_role_user1_idx` (`user_id`),
  ADD KEY `fk_user_role_role1_idx` (`role_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `advertiser`
--
ALTER TABLE `advertiser`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4851255;
--
-- AUTO_INCREMENT pro tabulku `advertiser_event`
--
ALTER TABLE `advertiser_event`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT pro tabulku `commission_log`
--
ALTER TABLE `commission_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=335;
--
-- AUTO_INCREMENT pro tabulku `login_log`
--
ALTER TABLE `login_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=384;
--
-- AUTO_INCREMENT pro tabulku `register_log`
--
ALTER TABLE `register_log`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pro tabulku `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pro tabulku `state`
--
ALTER TABLE `state`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',AUTO_INCREMENT=243;
--
-- AUTO_INCREMENT pro tabulku `supported_organization`
--
ALTER TABLE `supported_organization`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT pro tabulku `user_account`
--
ALTER TABLE `user_account`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT pro tabulku `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=154;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `advertiser`
--
ALTER TABLE `advertiser`
  ADD CONSTRAINT `advertiser_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `advertiser` (`id`);

--
-- Omezení pro tabulku `advertiser_category`
--
ALTER TABLE `advertiser_category`
  ADD CONSTRAINT `advertiser_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `advertiser_category_ibfk_3` FOREIGN KEY (`advertiser_id`) REFERENCES `advertiser` (`id`);

--
-- Omezení pro tabulku `advertiser_event`
--
ALTER TABLE `advertiser_event`
  ADD CONSTRAINT `advertiser_event_ibfk_1` FOREIGN KEY (`advertiser_id`) REFERENCES `advertiser` (`id`) ON DELETE CASCADE;

--
-- Omezení pro tabulku `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `category_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`);

--
-- Omezení pro tabulku `commission_log`
--
ALTER TABLE `commission_log`
  ADD CONSTRAINT `commission_log_ibfk_1` FOREIGN KEY (`advertiser_id`) REFERENCES `advertiser` (`id`);

--
-- Omezení pro tabulku `login_log`
--
ALTER TABLE `login_log`
  ADD CONSTRAINT `login_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Omezení pro tabulku `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `user` (`id`);

--
-- Omezení pro tabulku `user_account`
--
ALTER TABLE `user_account`
  ADD CONSTRAINT `user_account_ibfk_1` FOREIGN KEY (`commission_log_id`) REFERENCES `commission_log` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_account_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Omezení pro tabulku `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `fk_user_role_role1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_role_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
