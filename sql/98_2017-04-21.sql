-- add sloupec 
ALTER TABLE `user_account`
ADD `commission_rate` float NULL AFTER `commission_value`;

-- uprava stavajicich odmen
UPDATE user_account ua 
INNER JOIN commission_log cl ON ua.commission_log_id = cl.id 
INNER JOIN advertiser a ON cl.advertiser_id = a.id 
SET ua.commission_rate = 0.01 * a.cashback_customer 
WHERE a.cashback_percent = 1 AND ua.commission_rate IS NULL

-- update trigeru pro nove odmeny
DROP TRIGGER `commission_log_ai`;
DELIMITER ;;
CREATE TRIGGER `commission_log_ai` AFTER INSERT ON `commission_log` FOR EACH ROW
BEGIN

	DECLARE user_id 			INT(11);
	DECLARE parent_id 			INT(11);
	DECLARE cashback_customer 	FLOAT; 			-- zakanikova odmena
	DECLARE cashback_value 		FLOAT; 			-- nase odmena
	DECLARE cashback_percent 	INT;			-- 0 procenta | 1 pevna hodnota
	DECLARE cashback_u_amount	FLOAT;			-- konecna castka ktera se zapise nakupujicimu
	DECLARE cashback_p_amount	FLOAT;			-- konecna castka ktera se zapise nadrazenemu
        DECLARE slave_reward_percentage FLOAT;                  -- odmena v % ktera se dava za otroky (bude nahrazena z hodnoty v tabulce)

  DECLARE payment_type VARCHAR(120);

  SET @payment_type = 'payment';

	SELECT u.id, u.parent_id 
		INTO @user_id, @parent_id 
		FROM user u 
		WHERE u.sid = NEW.sid;

	SELECT a.cashback_percent, a.cashback_customer, a.cashback_value 
		INTO @cashback_percent, @cashback_customer, @cashback_value 
		FROM advertiser a 
		WHERE a.id = NEW.advertiser_id;
		

	SET @cashback_u_amount = @cashback_customer;
	SET @cashback_value = NULL;

	IF @cashback_percent = 1 THEN
			
		-- promenna cashback pro nas se nahradi tou co bere uzivatel (jen abych nemusel delat dalsi promennou)
		SET @cashback_value = @cashback_customer * 0.01; 
		SET @cashback_u_amount = NEW.sale_amount * @cashback_value; 
		
		-- 1. lepsi takto, dochazi k chybe po zakrouhleni a trojclenkou by delalo binec
		
	END IF;

        -- 2. radeji kontrola, kdyby se zadala chybne hodnota (abysme meli zisk odecist nejakou hodnotu?)
        IF (@cashback_u_amount > NEW.commission_amount AND (@cashback_u_amount > 0 AND NEW.commission_amount > 0)) THEN
		
            SET @cashback_u_amount = NEW.commission_amount; 			
    
        END IF;


  IF (NEW.sale_amount < 0) THEN
      SET @payment_type = 'storno';
  END IF;

  INSERT INTO user_account (commission_log_id, user_id, commission_value, commission_rate, created, updated, type) 
  VALUES (NEW.id, @user_id, @cashback_u_amount, @cashback_value, NEW.posting_date, NEW.posting_date, @payment_type);

	IF @parent_id > 0 THEN 
		BEGIN
		
                        SET @slave_reward_percentage = 0.5;
			SET @cashback_p_amount = @cashback_u_amount * @slave_reward_percentage;
			

			INSERT INTO user_account (commission_log_id, user_id, commission_value, commission_rate, created, updated, type) 
				VALUES (NEW.id, @parent_id, @cashback_p_amount, @cashback_p_amount, NEW.posting_date, NEW.posting_date, @payment_type);
			
		END;
	END IF;
END
;;