<?php

namespace RTsoft\Model;

use Nette;

/**
 * Provádí operace nad databázovou tabulkou.
 */
class Repository extends Nette\Object
{
    /** @var string Table name */
    protected $tableName;

    /** @var Nette\Database\Context */
    protected $context;

    /** @var Nette\Database\SelectionFactory */
    protected $selectionFactory;

    // doplnek k nazvum v selectboxech pro neaktivni zaznamy (POZOR - je to znovu v scripts.js)
    public static $textAddonForInactiveOption = "(neaktivní)";


    public function __construct(\Nette\Database\Context $context)
    {
        $this->context = $context;
    }

    /**
     * Vraci nazev tabulky
     * @return string
     */
    public function getTableName()
    {
        if (empty($this->tableName))
        {
            // název tabulky odvodíme z názvu třídy
            preg_match('#(\w+)Repository$#', get_class($this), $m);

            $this->tableName = strtolower($m[1]);
        }
    
        return $this->tableName;
    }

    /**
     * Vrací objekt reprezentující databázovou tabulku.
     * @return Nette\Database\Table\Selection
     */
    protected function getTable()
    {
        if (empty($this->tableName))
        {
            $this->getTableName();
        }

        return $this->context->table($this->tableName);
    }

    /**
     * Vrací všechny řádky z tabulky.
     * @return Nette\Database\Table\Selection
     */
    public function findAll()
    {
        return $this->getTable();
    }

    /**
     * Vrací řádky podle filtru, např. array('name' => 'John').
     * @return Nette\Database\Table\Selection
     */
    public function findBy(array $by)
    {
        return $this->getTable()->where($by);
    }

    /**
     * Vrací záznamu podle INT primary key
     * @param $id
     */
    public function findRow($id)
    {
        return $this->getTable()->get((is_numeric($id) ? (int) $id : $id));
    }

    /**
     * Vkládá data do tabulky
     * @param $data
     */
    public function insert($data)
    {
        return $this->getTable()->insert($data);
    }

    /**
     * Vymaže záznam podle primárního klíče
     * @param $id
     */
    public function delete($id)
    {
        return $this->findBy(array($this->getTable()->getPrimary() => (is_numeric($id) ? (int) $id : $id)))->delete();
    }

    /*
     * Ulozi nebo updatne zaznam
     */
    public function save($data, $id = NULL)
    {
        if (NULL === $id) 
        {
            $record = $this->insert($data);
        } 
        else 
        {
            $id = (is_numeric($id) ? (int) $id : $id);
            $record = $this->findRow($id);
            $record->update($data);
        }

        return $record;
    }

    /**
     *
     *
     * @return array kolekce sloupcu databaze
     */
    public function getColumns($tableName = NULL)
    {
        if (!$tableName)
        {
            $tableName = $this->getTableName();
        }
    
        $columns = $this->context->connection->getSupplementalDriver()->getColumns($tableName);

        $columnsResult = array();

        foreach ($columns as $column) 
        {
            $columnsResult[] = $column['name'];
        }

        return $columnsResult;
    }

    /**
     * Vrati odfiltrovana data tak, ze obsahuji indexy jen existujicich sloupcu
     * v tabulce
     *
     * @return array data jen s indexy existujich sloupcu v tabulce
     */
    protected function getFilteredData($data)
    {
        $columns = $this->getColumns();

        foreach ($data as $key => $value) 
        {
            if (!in_array($key, $columns)) 
            {
                unset($data[$key]);
            }
        }

        return $data;
    }

    public function insertFiltered($data)
    {
        $data = $this->getFilteredData($data);

        return $this->insert($data);
    }

    public function saveFiltered($data, $id = NULL)
    {
        $data = (array) $data;
        $data = $this->getFilteredData($data);

        return $this->save($data, $id);
    }

    /**
     * Spouští sadu SQL příkazů.
     * Pro každý vrací v poli result.
     * @param array $queries pole SQL příkazů
     */
    public function execQueries(array $queries)
    {
        $results = array();

        foreach ($queries as $q)
        {
            $results[] = $this->context->query($q);
        }

        return $results;
    }

    /**
     * Spouští sadu SQL příkazů v transakci.
     * Pro každý vrací v poli result.
     * @param array $queries pole SQL příkazů
     * @return mixed pole resultů, FALSE při chybě
     * @author LV
     * @todo Dopsat podporu parametrů.
     */
    public function execInTransaction(array $queries)
    {
        try
        {
            if ($this->context->beginTransaction())
            {
                $results = $this->execQueries($queries);
                $this->context->commit();
                return $results;
            }
        }
        catch (PDOException $e)
        {
            $this->context->rollBack();
            $this->lastError = $e->getMessage();
        }

        return FALSE;
    }

    /**
     * Vrací text poslední chyby.
     * @return type
     */
    public function getLastError()
    {
        return $this->context->errorCode();
    }

    /**
     * Univerzalni metoda pro filtrovani metodou =.
     */
    protected function rowsEqualFilter($rows, array $filters, array $options)
    {
        foreach ($filters as $f)
        {
            if (!empty($options["filter_{$f}"]))
            {
                $rows->where("{$f} = ?", $options["filter_{$f}"]);
            }
        }
    }

    /**
     * Univerzalni metoda pro filtrovani metodou LIKE.
     */
    protected function rowsLikeFilter($rows, array $filters, array $options)
    {
        foreach ($filters as $f)
        {
            if (!empty($options["filter_{$f}"]))
            {
                $rows->where("{$f} LIKE ?", "%{$options["filter_{$f}"]}%");
            }
        }
    }

    /**
     * Univerzální měnič sloupce "active" nebo "not_deleted".
     * @param int $id ID primary key
     */
    public function switchActive($id)
    {
        $row = $this->findRow($id);
        
        if (isset($row->active))
        {
            $a = !$row->active;
            $row->update(array("active" => $a));
        }
        if (array_key_exists("not_deleted", $row->toArray()))
        {
            $a = ($row->not_deleted ? NULL : 1);
            $row->update(array("not_deleted" => $a));
        }
        
        return $a;
    }
    
}
