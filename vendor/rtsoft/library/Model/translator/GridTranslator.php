<?php

namespace RTsoft\Model\Translator;

class GridTranslator implements \Nette\Localization\ITranslator
{
    private $t = array(
        "First" => "Začátek"
      , "Last"  => "Konec"
      , "Previous" => "Předchozí"
      , "Next" => "Další"
      , "Filter" => "F" //"Filtrovat"
      , "Cancel" => "X" //"Vyčistit"
    );
    
    /**
     * Translates the given string.
     * @param  string   message
     * @param  int      plural count
     * @return string
     */
    public function translate($message, $count = NULL)
    {
        if (isset($this->t[$message]))
        {
            return $this->t[$message];
        }
        
        //\Tracy\Debugger::barDump($message);
        return $message;
    }
}