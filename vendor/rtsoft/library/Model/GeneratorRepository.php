<?php

namespace RTsoft\Model;

class GeneratorRepository extends Repository
{
    public function getTableDescription($name)
    {
        $data = $this->context->query("SHOW FULL COLUMNS FROM `{$name}`");

        $return = array();
        foreach ($data as $row)
        {
            $return[$row->Field] = $row;
        }

        return $return;
    }
}
