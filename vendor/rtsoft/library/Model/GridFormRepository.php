<?php

namespace RTsoft\Model;

class GridFormRepository extends Repository implements IGridFormRepository
{
    // atributy u konfiguratoru sloupcu
    const COLUMN_TITLE             = "title";             // titulek v zahlavi tabulky u daneho sloupce a ve formulari label vlevo
    const COLUMN_TITLE_GRID        = "title_grid";        // titulek v zahlavi tabulky (neni-li, pouzije se COLUMN_TITLE)
    const COLUMN_TYPE              = "type";              // typ hodnot - jedna z TYPE_XYZ
    const COLUMN_FILTER_TYPE       = "filter_type";       // typ filtru - pokud nic, filtr se vygeneruje podle typu hodnot (COLUMN_TYPE)
    const COLUMN_DEFAULT           = "default";           // defaultni hodnota polozky ve formulari
    const COLUMN_FILTER_DEFAULT    = "filter_default";    // defaultni hodnota polozky ve filtru
    const COLUMN_VALIDATORS        = "validators";        // pole validatoru Form::XYZ (pole klic => hodnota)
    const COLUMN_OPTIONS           = "options";           // dalsi volby - pole polozek OPTION_XXX_YYY_ZZZ
    const COLUMN_ITEMS             = "items";             // polozky pro choice contrl
    const COLUMN_REPOSITORY        = "repository";        // instance repository, ze ktere se daji tahat data pro dany sloupec
    const COLUMN_ATTRIBUTES        = "attributes";        // dalsi atributy, ktere se pridaji k polozce ve formulari (pole klic => hodnota)
    const COLUMN_FILTER_ATTRIBUTES = "filter_attributes"; // dalsi atributy, ktere se pridaji k polozce ve filtru (pole klic => hodnota)
    const COLUMN_FILTER_BY         = "filter_by";         // co se použije při filtrování místo názvu sloupce
    const COLUMN_ORDER_BY          = "order_by";          // co se použije při řazení místo názvu sloupce
    const COLUMN_TOOLTIP           = "tooltip";           // nápověda ke sloupcům a políčkům formuláře

    // moznosti k COLUMN_OPTIONS
    const OPTION_NOT_IN_GRID       = "not_in_list";       // nezobrazovat v gridu
    const OPTION_NOT_IN_FORM       = "not_in_form";       // nezobrazovat ve formu
    const OPTION_SORTING_DISABLED  = "sorting_disabled";  // zakazat razeni
    const OPTION_DISABLED          = "disabled";          // custom
    const OPTION_MULTIPLE          = "multiple";          // pro multiple selectboxy a uploadboxy
    const OPTION_CKEDITOR          = "ckeditor";          // podpora CK editoru

    // typy sloupcu                                       // V GRIDU / VE FILTRU GRIDU  / VE FORMULÁŘI - DEFAULTNĚ
    const TYPE_TEXT            = "text";                  // text    / textbox          / textbox
    const TYPE_PASSWORD        = "password";              // text    / textbox          / textbox na heslo
    const TYPE_TEXTAREA        = "textarea";              // text    / textbox          / textarea
    const TYPE_INT             = "int";                   // text    / 2 txt políčka    / textbox + validátory na celé číslo
    const TYPE_FLOAT           = "float";                 // text    / 2 txt políčka    / textbox + validátory na desetinné číslo
    const TYPE_DATE            = "date";                  // text    / 2 txt políčka    / textbox s datepickerem, validátory na datum
    const TYPE_DATETIME        = "datetime";              // text    / 2 txt políčka    / textbox s datetimepickerem, validátory na datum a čas
    const TYPE_TIME            = "time";                  // text    / 2 txt políčka    / textbox s timepickerem, validátory na čas
    const TYPE_CHECKBOX        = "checkbox";              // text    / selectbox ano-ne / checkbox
    const TYPE_REF             = "ref";                   // text    / textbox          / selectbox     \
    const TYPE_SELECT          = "select";                // text    / selectbox        / selectbox      |
    const TYPE_CHECKBOXLIST    = "checkboxlist";          // text    / neimplementováno / checkboxlist   | nutné zadat COLUMN_ITEMS nebo COLUMN_REPOSITORY
    const TYPE_RADIOLIST       = "radiobuttonlist";       // text    / neimplementováno / radiolist     /
    const TYPE_UPLOAD          = "upload";                // text    / neimplementováno / uploadbox
    const TYPE_IMAGE_UPLOAD    = "image";                 // text    / obrázek datastr. / uploadbox pro nový obrázek, zobrazený obrázek, checkbox na smazání
    const TYPE_LABEL           = "label";                 // nezobr. / neimplementováno / řádek s textem zadaným v COLUMN_TITLE jenom jako oddělovač
    // aliasy
    const TYPE_BLOB = self::TYPE_UPLOAD;
    const TYPE_BOOL = self::TYPE_CHECKBOX;

    // typy filtru
    const FILTER_TYPE_ACCORDING_TO_THE_TYPE                     = "ft_according_to_type";      // filtr generovat nejaky defaultni podle typu, porovnavani taky defaultni podle typu
    const FILTER_TYPE_NONE                                      = "ft_none";                   // filtr zadny
    const FILTER_TYPE_ONE_TEXTBOX_COMPARING_EXACT_MATCH         = "ft_1_textbox_exact";        // filtr jeden textbox, porovnavani presna shoda (= 'abc')
    const FILTER_TYPE_ONE_TEXTBOX_COMPARING_SUBSTRING_MATCH     = "ft_1_textbox_substr";       // filtr jeden textbox, porovnavani podretezec (LIKE '%abc%')
    const FILTER_TYPE_ONE_TEXTBOX_COMPARING_SUBSTRING_BEGINNING = "ft_1_textbox_substr_begin"; // filtr jeden textbox, porovnavani podretezec zacinajici na neco (LIKE 'abc%')
    const FILTER_TYPE_ONE_TEXTBOX_COMPARING_SUBSTRING_ENDING    = "ft_1_textbox_substr_end";   // filtr jeden textbox, porovnavani podretezec koncici na neco (LIKE '%abc')
    const FILTER_TYPE_TWO_TEXTBOXES_COMPARING_RANGE             = "ft_2_textbox_range";        // filtr dva textboxy na porovnavani rozmezi od-do
    const FILTER_TYPE_BOOL                                      = "ft_bool";                   // filtr selectbox ano, ne
    const FILTER_TYPE_OWN                                       = 'filter_type_own';

    // atributy
    const ATTRIBUTE_SELECTBOX        = "selectbox"; // pro definici atributu selectboxu
    const ATTRIBUTE_SELECTBOX_NORMAL = "normal";    // nepředělávat selectbox na selectpicker
    const ATTRIBUTE_SELECTBOX_AJAX   = "ajax";      // udělat ajaxový selectpicker

    // aby mel validator defaultni hlasku podle sveho typu
    const DEFAULT_MESSAGE = "default_message";

    // moznosti u typu bool
    const BOOL_POSITIVE = "yes";
    const BOOL_NEGATIVE = "no";

    // doplnky k nazvum prvku pro filtrovani od-do
    const NAME_SUFFIX_FROM = "from";
    const NAME_SUFFIX_TO = "to";

    // regularni vyrazy
    const REGEXP_PHONE = '^[+(]{0,2}[0-9 ().-]{9,}';

    // je separator, pro pouziti v atributech a nastaveni separatoru v checkboxlistu/radiolistu
    const SEPARATOR = 'sep';

    // seznamu typu, ktere maji mit filtr od-do
    public static $columnTypesWithDefaultRangeFilter = array(self::TYPE_INT, self::TYPE_FLOAT, self::TYPE_DATE, self::TYPE_DATETIME, self::TYPE_TIME);

    // seznam typu, ktere defaultne nemaji byt v gridu
    public static $columnTypesNotInGrid = array(self::TYPE_LABEL);

    // seznam typu, ktere se ve formu pri ulozeni nemaji standardne ukladat
    public static $columnTypesNotSaveInForm = array(self::TYPE_LABEL);

    /** @var string Nazev primarniho klice */
    protected $primaryKeyColumnName = "id";

    /** @var string Název sloupce, ktery se použije jako hodnota ve fetchItemsForChoiceControl() - pokud nezadá programátor, vezme se primární klíč. */
    protected $columnNameForValue;

    /** @var string Název sloupce, který se použije jako text ve fetchItemsForChoiceControl() - pokud nezadá programátor, vezme se první sloupec z $columns. */
    protected $columnNameForText;

    /** @var string Název sloupce, který se použije jako příznak aktivity ve fetchItemsForChoiceControl(). */
    protected $columnNameForActiveFlag = "not_deleted";


    /**
     * Konstruktor.
     * Kontroly a nastavení některých parametrů.
     * @param \Nette\Database\Context $context
     */
    public function __construct(\Nette\Database\Context $context)
    {
        parent::__construct($context);

        if (!isset($this->columns) || !is_array($this->columns))
        {
            throw new Exception("Repository neobsahuje \$columns!");
        }

        $this->getTableName();

        // nastavení primárního klíče jako sloupec, ze kterého se berou hodnoty
        if (empty($this->columnNameForValue))
        {
            // takhle by to asi melo byt, jenze neco v Nette se zacykluje
            // Allowed memory size of 134217728 bytes exhausted (tried to allocate 20480 bytes)
            // File: .../vendor/nette/database/src/Database/ResultSet.php:163
            // $this->columnNameForValue = "`{$this->tableName}`.`{$this->primaryKeyColumnName}`";
            // funkcni verze bez nazvu tabulky:

            $this->columnNameForValue = "{$this->primaryKeyColumnName}";
        }

        // nastavení prvního sloupce z $columns jako text (mimo jiné pro fetchItemsForChoiceControl())
        if (empty($this->columnNameForText))
        {
            $this->columnNameForText = "`{$this->tableName}`.`" . array_keys($this->columns)[0] . "`";
        }
    }

    /**
     * Univerzalni filtrovani.
     * Prochazi vsechny sloupce v $columns a podle typu polozek a typu filtru generuje podminky pomoci $rows->where().
     *
     * Programator muze tuto metodu prepsat a napsat si vlastni filtrovani.
     * Pokud chce spustit filtrovani podle sloupcu v $columns, zavola parenta.
     *
     * @param object $rows
     * @param array $options
     */
    public function filterRows($rows, array $options = null)
    {
        //\Tracy\Debugger::log($options);
        //\Tracy\Debugger::barDump($this->columns);

        // filtr dle primarniho klice, pokud neni v columns a je v options
        if (!isset($this->columns[$this->primaryKeyColumnName]) && isset($options[$this->primaryKeyColumnName]))
        {
            $rows->where($this->primaryKeyColumnName, $options[$this->primaryKeyColumnName]);
        }
        if (!isset($this->columns[$this->columnNameForValue]) && isset($options[$this->columnNameForValue]))
        {
            $rows->where($this->columnNameForValue, $options[$this->columnNameForValue]);
        }

        $optionsKeys = array_keys($options);
        
        // prochazeni sloupcu a generovani filtrovacich podminek
        foreach($this->columns as $n => &$c)
        {
            //\Tracy\Debugger::log($n);

            if (!isset($c[self::COLUMN_FILTER_BY]))
            {
                $c[self::COLUMN_FILTER_BY] = $this->tableName . "." . $n;
            }

            // pokud nebyl zadan typ filtru, nastavi se konstanta urcujici vytvoreni filtru podle typu
            if (!isset($c[self::COLUMN_FILTER_TYPE]))
            {
                $c[self::COLUMN_FILTER_TYPE] = self::FILTER_TYPE_ACCORDING_TO_THE_TYPE;
            }

            // filtry od-do
            if ($c[self::COLUMN_FILTER_TYPE] == self::FILTER_TYPE_TWO_TEXTBOXES_COMPARING_RANGE
            || ($c[self::COLUMN_FILTER_TYPE] == self::FILTER_TYPE_ACCORDING_TO_THE_TYPE && in_array($c[self::COLUMN_TYPE], self::$columnTypesWithDefaultRangeFilter)))
            {
                // podminka od
                $colNameFrom = $n . "_" . self::NAME_SUFFIX_FROM;
                if (isset($options[$colNameFrom]))
                {
                    $x = $options[$colNameFrom];

                    // kdyz nejde pozadavek pres ajax, je tam pole obsahujici date, timezone_type, timezone, tak se do podminky musi dat to "date"
                    if (is_array($options[$colNameFrom]) && isset($options[$colNameFrom]["date"]))
                    {
                        $x = $options[$colNameFrom]["date"];
                    }

                    $rows->where("{$c[self::COLUMN_FILTER_BY]} >= ?", $x);
                }

                // podminka do - musi se zamenit cas z 00:00:00 na 23:59:59
                $colNameTo = $n . "_" . self::NAME_SUFFIX_TO;
                if (isset($options[$colNameTo]))
                {
                    $x = $options[$colNameTo];

                    // kdyz nejde pozadavek pres ajax, je tam pole obsahujici date, timezone_type, timezone, tak se do podminky musi dat to "date"
                    if (is_array($options[$colNameTo]) && isset($options[$colNameTo]["date"]))
                    {
                        $x = str_replace("00:00:00", "23:59:59", $options[$colNameTo]["date"]);
                    }

                    // jako objekt DateTime to chodi, kdyz pozadavek jde pres ajax
                    if ($options[$colNameTo] instanceof \DateTime)
                    {
                        $x = clone $options[$colNameTo];
                        $x->add(new \DateInterval("PT86399S"));
                    }

                    $rows->where("{$c[self::COLUMN_FILTER_BY]} <= ?", $x);
                }
            }

            // filtry normalni
            if (in_array($n, $optionsKeys))
            { 
                if ($c[self::COLUMN_TYPE] == self::TYPE_BOOL)
                {
                    if ($options[$n] === self::BOOL_POSITIVE)
                    {
                        $rows->where($c[self::COLUMN_FILTER_BY], 1);
                    }
                    if ($options[$n] === self::BOOL_NEGATIVE)
                    {
                        $rows->where($c[self::COLUMN_FILTER_BY], (($n == $this->columnNameForActiveFlag) ? NULL : 0));
                    }
                }
                else if ($c[self::COLUMN_TYPE] == self::TYPE_SELECT || $c[self::COLUMN_TYPE] == self::TYPE_RADIOLIST)
                {
                    // filtrujem jen kdyz neni vybrana ta prvni prompt polozka, aby to negenerovalo WHERE sloupecek = ''
                    if ($options[$n] !== "")
                    {
                        $rows->where($c[self::COLUMN_FILTER_BY], $options[$n]);
                    }
                }
                else if ($c[self::COLUMN_FILTER_TYPE] == self::FILTER_TYPE_OWN)
                {
                    $rows->where($c[self::COLUMN_FILTER_BY], $options[$n]);
                }
                else
                {
                    switch ($c[self::COLUMN_FILTER_TYPE])
                    {
                        case self::FILTER_TYPE_ONE_TEXTBOX_COMPARING_EXACT_MATCH:
                            $rows->where($c[self::COLUMN_FILTER_BY], $options[$n]);
                            break;

                        case self::FILTER_TYPE_ONE_TEXTBOX_COMPARING_SUBSTRING_BEGINNING:
                            $rows->where("{$c[self::COLUMN_FILTER_BY]} LIKE ?", "{$options[$n]}%");
                            break;

                        case self::FILTER_TYPE_ONE_TEXTBOX_COMPARING_SUBSTRING_ENDING:
                            $rows->where("{$c[self::COLUMN_FILTER_BY]} LIKE ?", "%{$options[$n]}");
                            break;

                        default:
                            $rows->where("{$c[self::COLUMN_FILTER_BY]} LIKE ?", "%{$options[$n]}%");
                    }
                }
            }
        }
    }

    /**
     * Univerzalni metoda pro ziskavani poctu zaznamu.
     * Pouziti hlavne pro paginator a pro DataGrid.
     * Pouziva metodu filterRows(), kterou by mel implementovat potomek.
     * @param array $options kriteria pro filtrovani ve filterRows()
     * @return int
     */
    public final function getRowsCount(array $options = null, $order = null)
    {
        $rows = $this->findAll();

        $this->filterRows($rows, $options);

        return $rows->count("`{$this->getTableName()}`.`{$this->primaryKeyColumnName}`");
    }

    /**
     * Univerzalni metoda pro ziskavani radku tabulky pro zadanou stranku.
     * Cislo stranky urci typicky paginator nebo DataGrid.
     * Pouziva metodu filterRows(), kterou by mel implementovat potomek.
     * @param array $options kriteria pro filtrovani ve filterRows()
     * @param type $order podle ceho se ma radit
     * @param type $length pocet zaznamu na jedne strance
     * @param type $offset kolik zaznamu od zacatku se ma preskocit
     * @return object
     */
    public final function findRows(array $options = null, $order = null, $limit = null, $offset = null)
    {
        $rows = $this->findAll();

        $this->filterRows($rows, $options);

        if (NULL !== $limit && NULL !== $offset) // po staru
        {
            $rows->limit($limit, $offset);
        }

        if (is_object($limit)) // pro DataGrid
        {
            /* @var $limit \Nette\Utils\Paginator */
            $rows->limit($limit->itemsPerPage, $limit->offset);
        }

        if (!empty($order))
        {
            if (is_array($order) && isset($order[0])) // pro DataGrid
            {
                if (isset($this->columns[$order[0]][self::COLUMN_ORDER_BY]))
                {
                    $rows->order($this->columns[$order[0]][self::COLUMN_ORDER_BY] . (isset($order[1]) ? " " . $order[1] : ""));
                }
                else
                {
                    $rows->order(implode(' ', $order));
                }
            }
            else // po staru
            {
                $rows->order($order);
            }
        }

        return $rows;
    }

    /**
     * Univerzalni metoda pro ziskani dat pro selectboxy, checkboxlisty, radiobuttony.
     * Programator muze metodu prepsat a napsat si vlastni.
     * @param array $options
     * @return array Pole klíč => hodnota
     */
    public function fetchItemsForChoiceControl($options = array())
    {
        $rows = $this->findAll();

        // select value a text
        $rows->select("{$this->columnNameForValue} AS `value`");
        if ($this->columnNameForActiveFlag)
        {
            $rows->select("CONCAT_WS(' ', {$this->columnNameForText}, CASE WHEN {$this->tableName}.{$this->columnNameForActiveFlag} THEN '' ELSE ? END) AS `text`", self::$textAddonForInactiveOption);
        }
        else
        {
            $rows->select("{$this->columnNameForText} AS `text`");
        }

        // když byly zadány nějaké parametry, tak se na to pustí univerzální filtrování.
        if (!empty($options))
        {
            $this->filterRows($rows, $options);
        }

        // napřed aktivní, ty podle abecedy, pak neaktivní a taky podle abecedy
        $rows->order($this->columnNameForActiveFlag ? "{$this->tableName}.{$this->columnNameForActiveFlag} DESC, text ASC" : "text ASC");

        //\Tracy\Debugger::log($rows->getSql(), "fetchItemsForChoiceControl");
        //\Tracy\Debugger::log("COUNT: " . $rows->count("*"), "fetchItemsForChoiceControl");

        // vrátí se pole klíč => hodnota
        return $rows->fetchPairs('value', 'text');
    }

    /**
     * Vrati polozky pro naseptavac.
     * @param string $q Hledany vyraz.
     * @param string $name Nazev policka, ve kterem se naseptava.
     * @param string $options podminka
     * @return array Pole pro naseptavac.
     */
    public function fetchItemsForWhisperer($q, $name, $options = NULL)
    {
        $rows = $this->findAll();

        // select value a text
        $rows->select("{$this->columnNameForValue} AS `value`");
        if ($this->columnNameForActiveFlag)
        {
            $rows->select("CONCAT_WS(' ', {$this->columnNameForText}, CASE WHEN {$this->tableName}.{$this->columnNameForActiveFlag} THEN '' ELSE ? END) AS `text`", self::$textAddonForInactiveOption);
        }
        else
        {
            $rows->select("{$this->columnNameForText} AS `text`");
        }

        if ($q !== NULL)
        {
            $rows->where("{$this->columnNameForText} LIKE ?", "%{$q}%");
        }
        
        if ($this->columnNameForActiveFlag)
        {
            $rows->where("{$this->tableName}.{$this->columnNameForActiveFlag} = 1");
        }

        //\Tracy\Debugger::log($options, "debug_fetchItemsForWhisperer");
        
        if (!empty($options))
        {    
            while ($con_name = current($options))
            {
                $rows->where(key($options), $con_name);
                next($options);
            }
        }    

        $rows->limit(10);
        
        $this->changeSelectionBeforeGettingItemsForWhisperer($q, $name, $rows);

        //\Tracy\Debugger::log($rows->getSql(), "debug_fetchItemsForWhisperer");

        $data = array();

        foreach ($rows as $r)
        {
            $data[] = array(
                "text" => $r->text,
                "value" => $r->value,
            );
        }

        return $data;
    }
    
    /**
     * Zde muze ve zdedene tride programator zasahnout do dat, ktere se ziskavaji pro naseptavace.
     * @param string $q hledany vyraz
     * @param string $name policko ve kterem se naseptava
     * @param \Nette\Database\Table\Selection $rows
     */
    protected function changeSelectionBeforeGettingItemsForWhisperer(string $q, string $name, \Nette\Database\Table\Selection $rows)
    {
        
    }

    /**
     * Overi, zda je u daneho columnu dana option.
     * @param array $c jeden column z pole $columns
     * @param string $option hledana option
     */
    protected final function hasOption($c, $option)
    {
        return (isset($c[self::COLUMN_OPTIONS]) && is_array($c[self::COLUMN_OPTIONS]) && in_array($option, $c[self::COLUMN_OPTIONS]));
    }


    // <editor-fold defaultstate="collapsed" desc=" GET, SET">

    function getSelectboxFilterPromptText()
    {
        return $this->selectboxFilterPromptText;
    }

    function getPrimaryKeyColumnName()
    {
        return $this->primaryKeyColumnName;
    }

    function getColumnNameForValue()
    {
        return $this->columnNameForValue;
    }

    function getColumnNameForText()
    {
        return $this->columnNameForText;
    }

    function getColumnNameForActiveFlag()
    {
        return $this->columnNameForActiveFlag;
    }

    // </editor-fold>
}
