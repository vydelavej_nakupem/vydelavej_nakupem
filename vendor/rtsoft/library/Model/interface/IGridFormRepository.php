<?php

namespace RTsoft\Model;

/**
 * Tohle rozhrani musi repository trida implementovat, aby ji mohly pouzivat komponenty:
 * - FormComponent pro ukladani formulare a vypisovani polozek v choice controlech
 * - GridComponent jako zdroj dat
 * 
 * @author vrana
 */
interface IGridFormRepository
{
    public function fetchItemsForChoiceControl($options = array());
    public function filterRows($rows, array $options);
    public function save($data, $id = 0);
}
