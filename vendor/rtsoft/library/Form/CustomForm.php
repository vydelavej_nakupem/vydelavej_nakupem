<?php

namespace RTsoft\Form;

use Nette\Application\UI\Form;

/**
 * Form s metodami pro vkladani vlastnich komponent.
 *
 * @author vrana
 */
class CustomForm extends \Nette\Application\UI\Form
{
    private $antiSpamControlRegistered = FALSE;
    
    /**
     * Přidá ochranu proti robotům.
     */
    public function addAntiSpamProtection()
    {
        if (!$this->antiSpamControlRegistered)
        {
            \RTsoft\Control\AntispamControl::register();
            $this->antiSpamControlRegistered = TRUE;
        }
        
        $this->addAntispam();
    }
    
    /**
     * Přidá komponentu Label.
     * @param string $name
     * @param string $text
     */
    public function addLabel($name, $text)
    {
        $this[$name] = new \RTsoft\Control\Label($text);
        return $this[$name];
    }
    
    
    /**
     * Přidá komponentu Img.
     * @param string $name
     * @param string $text
     */
    public function addImg($name, $src)
    {
        $this[$name] = new \RTsoft\Control\Img($src);
        return $this[$name];
    }

}
