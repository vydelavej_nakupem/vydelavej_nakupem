<?php

namespace RTsoft\Form;

use Nette\Application\UI\Form;
use Vodacek\Forms\Controls\DateInput;
use Nette\Forms\Controls;

/**
 * Zmeny rendereru kvuli bootstrapu 3.
 * @property string $labelClass Název class u labelu, default: col-sm-4.
 * @property string $controlClass Název class u labelu, default: col-sm-8.
 * @property string $formClass Název class celého formuláře, default: form-horizontal form-normal.
 */
Class BootstrapForm extends CustomForm
{
    private $labelClass = "col-sm-4";
    private $controlClass = "col-sm-8";
    private $formClass = 'form-horizontal form-normal';
    
    public function render()
    {
        $renderer = $this->getRenderer();
        
        $renderer->wrappers['controls']['container'] = NULL;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=' . $this->controlClass;
        $renderer->wrappers['label']['container'] = 'div class="' . $this->labelClass . ' control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        
        $this->bootstrap();
        return parent::render();
    }
    
    /**
     * Přidá do formuláře správné class, aby se formulář vykreslil v bootstrap-like.
     * @param type $size
     */
    public function makeBootstrap($size = null)
    {
        Switch($size)
        {
            case 'small':
                $this->labelClass   = 'col-sm-2';
                $this->controlClass = 'col-sm-4';
                break;
            case 'full':
            default:
                break;
        }
        $this->bootstrap();
    }
    
    /**
     * Přidá další třídu do formuláře.
     * @param string $formClass
     */
    public function addFormClass($formClass)
    {
        $this->formClass .= (empty($this->formClass) ? "" : " ") . $formClass;
    }
    
    function getLabelClass()
    {
        return $this->labelClass;
    }

    function getControlClass()
    {
        return $this->controlClass;
    }

    function setLabelClass($labelClass)
    {
        $this->labelClass = $labelClass;
    }

    function setControlClass($controlClass)
    {
        $this->controlClass = $controlClass;
    }
    
    function getFormClass()
    {
        return $this->formClass;
    }

    function setFormClass($formClass)
    {
        $this->formClass = $formClass;
    }
    
    /**
     * Nastaví správné bootstrap class všem prvkům.
     */
    private function bootstrap()
    {
        $this->getElementPrototype()
             ->class($this->formClass)
             ->role('form');

        foreach ($this->getControls() as $control)
        { 
            if ($control instanceof Controls\Button)
            {
                $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                $usedPrimary = TRUE;
            }
            elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox
                    || $control instanceof DateInput)
            {
                $control->getControlPrototype()->addClass('form-control');
            }
            elseif ($control instanceof Controls\Checkbox || $control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList)
            {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }
        
    }
}
