<?php

namespace RTsoft\Utils;

/**
 * Trida na posilani mailu.
 * 
 * Priklad pouziti:
 * 
 * Oddedit a nastavit parametry (viz komentar u konstruktoru).
 * Pro odeslani mailu napsat takovouhle funkci:
 * 
 *  public function sendTest()
 *  {
 *      // dalsi extra parametry, ktere se nacpou do latte
 *      $this->params["aaa"] = "bbb";
 *      
 *      // nastavi se prijemce, zaroven se do params daji email a hash
 *      // nutne udelat driv, nez setTemplate()
 *      $this->setRecipients("test@rtsoft.cz");
 *      
 *      // nastavi se predmet
 *      $this->setSubject("Test");
 *      
 *      // zada se nazev sablony v adresari app/templates/Mailer
 *      // tim se zaroven vygeneruje html obsah mailu
 *      $this->setTemplate(__DIR__ . "/cesta/k/obsahu/emailu.latte");
 *      
 *      // a mail uz se jenom odesle
 *      $this->send();
 *  }
 */
class Mailer extends \Nette\Object
{
    /** @var \Nette\Mail\Message */
    private $mail;

    /** @var \Nette\Mail\SmtpMailer */
    private $mailer;
    
    /** @var bool Jestli je debug mod. */
    private $debugMode = FALSE;
    
    /** @var string Zakladni url (musi se brat z konfiguraku, protoze se muze Mailer pouzivat pri CLI volani). */
    private $baseUri;
    
    /** @var string Nazev aplikace */
    private $appTitle;
    
    /** @var array Parametry, ktere se posilaji do latte. */
    protected $params;
    
    /** @var array Sekce 'mailer' z neonu. */
    private $config;
    
    /** @var bool Zda je email urcen pro zakaznika, defaultne ano, email obsahuje zapati s odhlasovacim odkazem */
    private $emailIsForCustomer = TRUE;

    /**
     * Do $config potřebuje nainjectovat parametry z parameters.neon, které vypadají takhle:
     *     mailer:
     *         from: "info@rtsoft.cz"
     *         fromName: "Projekt"
     *
     *         smtp:
     *             host: "posta.rtsoft.cz"
     *             username: 'odchozi@odchozi.cz'
     *             password: 'odchozi'
     * 
     * Do $baseUri potřebuje nastavit URL, pod kterým je web dostupný, dost často je v emailech potřeba a pokud se PHP skript pouští přes CLI, tak není dostupný http request a skript se to nemůže zjistit sám.
     * 
     * Do $appTitle se zadá název aplikace.
     *
     * @param array $config
     * @param string $baseUri
     * @param string $appTitle
     */
    function __construct($config, $baseUri, $appTitle)
    {
        $this->config = $config;
        $this->baseUri = $baseUri;
        $this->appTitle = $appTitle;

        $this->reset();
    }

    /**
     * Nastaveni vseho do vychozi podoby.
     */
    protected function reset()
    {
        // smtp mailer
        $this->mailer = new \Nette\Mail\SmtpMailer($this->config["smtp"]);

        // mail message
        $this->mail = new \Nette\Mail\Message();
        $this->mail->setFrom($this->config["from"], $this->config["fromName"]);
        $this->mail->addReplyTo($this->config["from"], $this->config["fromName"]);
        $this->mail->setReturnPath($this->config["from"]);

        // debug rezim - vse se posila na jeden email
        if (!empty($this->config["debug_emailTo"]))
        {
            $this->debugMode = TRUE;
            $this->mail->addTo($this->config["debug_emailTo"]);
        }
        
        // zalozi se pole pro parametry, ktere se pak daji do latte
        $this->params = array();
        
        // vychozi parametry
        $this->params["baseUri"] = $this->baseUri;
        $this->params["appTitle"] = $this->appTitle;
        $this->params["config"] = $this->config;
    }

    /**
     * Nastaveni predmetu.
     * @param string $subject
     */
    protected function setSubject($subject)
    {
        $this->mail->setSubject($subject);
    }

    /**
     * Pridani prilohy k mailu.
     *
     * @param string $file Nazev souboru v pripade, ze je content nenulovy, jinak cesta k souboru
     * @param null|string $content Soubor prilohy
     * @param null|string $contentType MIME typ souboru
     */
    protected function setAttachment($file, $content = null, $contentType = null)
    {
        $this->mail->addAttachment($file, $content, $contentType);
    }

    /**
     * Udela text mailu, vytvori latte, da mu parametry a necha vygenerovat html.
     * 
     * Krome jineho je nutne, aby v $params byly polozky 'email' a 'hash'.
     * To ale muze zaridit metoda setRecipients() pri zadani jednoho emailu, coz
     * bude typicke pouziti.
     * 
     * @param string $latteFileName
     */
    protected function setTemplate($latteFileName)
    {
        // doplneni dalsich parametru
        $this->params["emailForCustomer"] = $this->emailIsForCustomer;
        
        // vyrendrovani emailu
        $latte = new \Latte\Engine();
        $body = $latte->renderToString($latteFileName, $this->params);

        $this->changeEmailBody($body);

        $this->mail->setHtmlBody($body);
        $this->mail->setBody(strip_tags($body));
    }

    /**
     * Možnost zasáhnout do textu mailu stringově.
     */
    protected function changeEmailBody(&$body)
    {
        
    }

    /**
     * Nastavi prijemce, kopie a skryte kopie.
     * Vsechny parametry muzou byt:
     * - string s jednim emailem
     * - string s vice emaily oddelenymi carkou
     * - pole s emaily
     * Pokud se zada pouze jeden email, zaroven se hodi do pole $params email
     * prijemce a hash.
     * 
     * @param string|array $to prijemci
     * @param string|array $cc kopie
     * @param string|array $bcc skryte kopie
     */
    protected function setRecipients($to, $cc = NULL, $bcc = NULL)
    {
        if (empty($this->config["debug_emailTo"]))
        {
            // adresati
            if (!is_array($to) && strpos($to, ",") !== FALSE)
            {
                $to = explode(",", $to);
            }

            if (is_array($to))
            {
                foreach ($to as $rcpt)
                {
                    $this->mail->addTo(trim($rcpt));
                }
            }
            else
            {
                $this->mail->addTo($to);
                
                if ($this->emailIsForCustomer)
                {
                    // kdyz je jenom jeden, tak se rovnou do sablony vlozi jeho email a hash
                    $this->params["email"] = $to;
                    $this->params["hash"] = $this->calculateHash($to);
                }
            }

            // kopie
            if (!empty($cc))
            {
                if (!is_array($cc) && strpos($cc, ",") !== FALSE)
                {
                    $cc = explode(",", $cc);
                }

                if (is_array($cc))
                {
                    foreach ($cc as $rcpt)
                    {
                        $this->mail->addCc(trim($rcpt));
                    }
                }
                else
                {
                    $this->mail->addCc($cc);
                }
            }

            // slepa kopie
            if (!empty($bcc))
            {
                if (!is_array($bcc) && strpos($bcc, ",") !== FALSE)
                {
                    $bcc = explode(",", $bcc);
                }

                if (is_array($bcc))
                {
                    foreach ($bcc as $rcpt)
                    {
                        $this->mail->addBcc(trim($rcpt));
                    }
                }
                else
                {
                    $this->mail->addBcc($bcc);
                }
            }
        }
    }

    /**
     * Odeslani emailu. V mailu uz musi byt zadany text, predmet a prijemci.
     * @return boolean Uspesnost odeslani.
     */
    protected function send()
    {
        try
        {
            $this->mailer->send($this->mail);
            return TRUE;
        }
        catch (\Nette\Mail\SmtpException $e)
        {
            \Tracy\Debugger::log($e, "mailer");
            return FALSE;
        }
    }
    
    /**
     * Vypocte hash ze zadaneho retezce.
     * @param string $text
     * @return string
     */
    protected function calculateHash($text)
    {
        return \Nette\Security\Passwords::hash($text);
    }
    
    /**
     * Overi hash.
     * @param string $text
     * @param string $hash
     * @return string
     */
    public function verifyHash($text, $hash)
    {
        return \Nette\Security\Passwords::verify($text, $hash);
    }

    
    
}