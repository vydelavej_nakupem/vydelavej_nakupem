<?php

namespace RTsoft\Utils;

/**
 * ConfigParameters - trida na nacitani parametru z configu ze sekce parameters.
 *
 * Pouziti:
 * 
 * config.neon:
 * services:
 *     - RTsoft\Utils\ConfigParameters
 *
 * v nejake tride:
 * public function __construct(\RTsoft\Utils\ConfigParameters $parameters)
 * {
 *     $this->parameters = $parameters;
 * }
 *
 * nacteni parametru:
 * $this->parameters->xyz
 *
 * @author Standa Smitka, Marek Susicky
 */
class ConfigParameters extends \Nette\Utils\ArrayHash
{
    public function __construct(\Nette\DI\Container $container)
    {
        // Nacteni parametru
        foreach($container->parameters as $key => $val)
        {
            $this->$key = is_array($val)? $this->addLevel($val) : $val;
        }
    }

    protected function addLevel($val)
    {
        // Dalsi uroven
        $level = new \Nette\Utils\ArrayHash();

        // Vypreparovani jedne urovne
        foreach ($val as $key => $val)
        {
            $level->$key = is_array($val)? $this->addLevel($val) : $val;
        }

        // Vraceni vysledku
        return $level;
    }

}
