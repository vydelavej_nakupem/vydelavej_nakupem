<?php

namespace RTsoft\Utils;

use Nette\Database\Table\ActiveRow;
use \Nette\Utils\ArrayHash;

class Tools
{
    /**
     * Rekurzivni mazani adresare.
     * @param string $dir
     */
    public static function rrmdir($dir) 
    { 
       if (is_dir($dir)) 
       { 
         $objects = scandir($dir); 
         foreach ($objects as $object) 
         {       

            if ($object != "." && $object != "..")
            { 
                if (filetype($dir."/".$object) == "dir") 
                {       
                    self::rrmdir($dir."/".$object);    
                }    
                else 
                { 
                    unlink($dir."/".$object); 
                }
            }
         } 
         reset($objects); 
         rmdir($dir); 
       } 
    }
    
    /**
     * Převede ActiveRow na ArrayHash.
     * @param Nette\Database\Table\ActiveRow $row
     * @return ArrayHash
     */
    public static function activeRowToArrayHash(ActiveRow $row)
    {
        $arrayHash = new ArrayHash();
        
        foreach($row AS $key => $val)
        {
            $arrayHash[$key] = $val;
        }
        
        return $arrayHash;
    }
}
