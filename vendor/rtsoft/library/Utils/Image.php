<?php

namespace RTsoft\Utils;

/**
 * Rozsireni tridy pro praci s obrazky z Nette.
 *
 * Priklad pouziti:
 * $image = \RTsoft\Utils\Image::fromFile("obrazek.jpg");
 * $image = \RTsoft\Utils\Image::fromBlank(100, 200);
 * Diky tomu, ze ve tride \Nette\Utils\Image je napsano "new static", 
 * vytvori se instance \RTsoft\Utils\Image a ne pouze \Nette\Utils\Image.
 */ 
class Image extends \Nette\Utils\Image
{
    public function writeText($img, $font, $x, $y, $text, $color)
    {
        $color = imagecolorallocate($img->imageResource, $color[0], $color[1], $color[2]);
        
        $text = iconv("UTF-8", "ISO-8859-2", $text);
        
        imagestring($img->imageResource, $font, $x, $y, $text, $color);
    }            
}
