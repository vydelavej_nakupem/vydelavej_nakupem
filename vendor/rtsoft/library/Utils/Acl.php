<?php

namespace RTsoft\Utils;

use Nette;

/**
 * Class Acl
 * @package Model
 */
class Acl extends Nette\Security\Permission
{

    /**
     * Automatické přiřazení práv k rolím do backendu
     * @param array $permission Práva definovaná v permission.neon
     */
    public function __construct($permission)
    {
        // Pole obsahující již přidané Resources (s kterými můžeme pracovat v rámci rolí)
        $existResources = [];

        // Projedeme všechny role
        foreach ($permission as $role => $resources)
        {
            $this->addRole($role);

            // Definice, kam která role může
            foreach ($resources as $resource)
            {
                // Pokud budeme mít presenter s nadefinovanými akcemi, tak je záznam vždy pole
                if (is_array($resource))
                {
                    // Máme nadefinované jednotlivé akce v presenteru (role má přístup pouze do nich a nikam jinam)
                    foreach ($resource as $presenter => $actions)
                    {
                        $allowActions = [];

                        foreach ($actions as $action)
                        {
                            $allowActions[] = $action;
                        }

                        //$resourceName = "Admin:{$presenter}";
                        $resourceName = $presenter;

                        // V případě, že nemáme přidaný presenter do resources, přidáme ho (kvůli oprávnění)
                        if (!in_array($presenter, $existResources))
                        {
                            $existResources[] = $presenter;

                            $this->addResource($resourceName);
                        }

                        $this->allow($role, $resourceName, empty($allowActions) ? self::ALL : $allowActions);
                    }
                }
                else
                {
                    $resourceName = $resource;

                    // V případě, že nemáme přidaný presenter do resources, přidáme ho (kvůli oprávnění)
                    if (!in_array($resource, $existResources))
                    {
                        $existResources[] = $resource;

                        $this->addResource($resourceName);
                    }

                    // Pokud není pole, má role přístup do celého presenteru (do všech akcí)
                    $this->allow($role, $resourceName, self::ALL);
                }
            }
        }
    }


    /**
     * Returns TRUE if and only if the Role has access to [certain $privileges upon] the Resource.
     *
     * This method checks Role inheritance using a depth-first traversal of the Role list.
     * The highest priority parent (i.e., the parent most recently added) is checked first,
     * and its respective parents are checked similarly before the lower-priority parents of
     * the Role are checked.
     *
     * @param  string|Permission::ALL|IRole  role
     * @param  string|Permission::ALL|IResource  resource
     * @param  string|Permission::ALL  privilege
     * @throws Nette\InvalidStateException
     * @return bool
     */
    public function isAllowed($role = self::ALL, $resource = self::ALL, $privilege = self::ALL) {

        // Zajištění toho, aby se nevyhazovala vyjímka 'Resource does not exist.'
        if ($resource !== self::ALL)
        {
            if ($resource instanceof Nette\Security\IResource)
            {
                $resource = $resource->getResourceId();
            }

            // Neexistující resource přidáme do registru
            if(!in_array($resource, $this->resources))
            {
                $this->addResource($resource);
            }
        }

        return parent::isAllowed($role, $resource, $privilege);
    }

}