<?php

namespace RTsoft\Utils\String;

/**
 * Třída pro práci s řetězci
 *
 * @author Tomas Huda <tomas.huda@rtsoft.cz>
 */
class String
{
    /**
     * Převádí řetězec z camelCase na under_score
     * @param string $camelCaseString
     * @return string underscore case string
     */
    public function camelCaseToUnderscore($camelCaseString)
    {
        $stringFirstLower = lcfirst($camelCaseString);
        
        $stringArray = str_split($stringFirstLower);
        
        $resultArray = [];
        foreach ($stringArray as $char)
        {
            $lowerChar = strtolower($char);
            
            if ($lowerChar === $char)
            {
                $resultArray[] = $char;
            }
            else
            {
                $resultArray[] = '_';
                $resultArray[] = $lowerChar;
            }
        }
        
        return implode('', $resultArray);
    }
}
