<?php

namespace RTsoft\Presenter;

use Nette;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /**
     * @var array presentery s veřejným přístupem
     */
    protected $presenterNamesWithPublicAccess = ['Sign', 'Error', 'Admin:Sign', 'Admin:Error'];


    /**
     * @throws Nette\Security\AuthenticationException
     */
    protected function startup()
    {
        parent::startup();

        // POKUD NA TENTO PREZENTER NENÍ POVOLEN VEŘEJNÝ PŘÍSTUP
        if (!in_array($this->name, $this->presenterNamesWithPublicAccess))
        {
            $isLoggedIn = $this->user->isLoggedIn();

            // pokud neni prihlasen...
            if (empty($this->user) || !$isLoggedIn)
            {
                // ulozi se pozdavek pro pozdejsi pouziti a presmeruje se na prihlasovaci stranku
                $key = $this->storeRequest();
                $this->redirect('Sign:in', array('returnKey' => $key));
            }
            else
            {
                // pokud je prihlasen, kontroluje se, jestli ma prava na zobrazeni presentru a akce podle permission.neon (ale pouze pokud existuje)
                if (isset($this->context->parameters["permission"]) && !$this->user->isAllowed($this->getName(), $this->action))
                {
                    throw new Nette\Security\AuthenticationException("Nemáte dostatečná oprávnění [" . $this->getName() . ", $this->action]");
                }
            }
        }
    }

    /**
     * Before render :)
     */
    public function beforeRender()
    {
        parent::beforeRender();

        if (isset($this->context->parameters['appTitle']))
        {
            $this->template->title = $this->context->parameters['appTitle'];
        }
    }

    /**
     * Univerzální metoda pro vytváření komponent.
     * Pokud se vyskytne v latte {control xyz}, tato metoda je zavolána s parametrem $name = "xyz".
     * V metodě se otestuje, zda náhodou neexistuje metoda createComponentXyz,
     * - pokud ano, zavolá se parent z Nette, ve kterém se zavolá createComponentXyz,
     * - pokud ne, tak se zavolá vytvoření služby, jejíž název musí být v config.neon
     *   (pokud není, spadne to).
     *
     * @param string $name
     * @return mixed
     */
    protected function createComponent($name)
    {
        if (method_exists($this, "createComponent" . ucfirst($name)))
        {
            return parent::createComponent($name);
        }

        // Komponenta pro sluzbu Xyz se muze jmenovat xyz nebo Xyz
        try
        {
            // Nazev komponenty se shoduje presene
            $c = $this->context->createService($name);
        }
        catch (\Nette\DI\MissingServiceException $e)
        {
            // Zkusit "ucfirst" nazev
            try
            {
                // Pokus o prevod xyz na Xyz
                $c = $this->context->createService(ucfirst($name));
            }
            catch (\Nette\DI\MissingServiceException $e)
            {
                // "Vlastni" hlaska pro informaci o 2 pokusech
                throw new \Nette\DI\MissingServiceException("Services '$name' or '" . ucfirst($name) . "' not found.");
            }
        }
        
        // predani http requestu, pokud se v komponente vyskytuje setter pro nej
        if (method_exists($c, "setHttpRequest"))
        {
            $c->httpRequest = $this->getHttpRequest();
        }
        
        return $c;
    }
    
    /**
     * Určí, zda se vyžadován zabezpečný login, ne pokud:
     * - je to vypnuté nebo hodnoty nejsou v konfigu
     * - uživatel přichází z IP ze seznamu
     * - uživatelovo IP obsahuje řetězec
     * @return boolean
     */
    public function secureLoginRequired()
    {
        if (!isset($this->context->parameters["secureLogin"]["enabled"]) || !$this->context->parameters["secureLogin"]["enabled"])
        {
            return NULL;
        }
        
        if (in_array($_SERVER['REMOTE_ADDR'], $this->context->parameters["secureLogin"]["notRequiredFromIP"]))
        {
            return FALSE;
        }
        
        if (is_array($this->context->parameters["secureLogin"]["notRequiredFromIPparts"]))
        {
            foreach($this->context->parameters["secureLogin"]["notRequiredFromIPparts"] as $ipPart)
            {
                if (FALSE !== strpos($_SERVER['REMOTE_ADDR'], $ipPart))
                {
                    return FALSE;
                }
            }
        }
        
        return TRUE;
    }
    
    /**
     * Aby se zobrazily flash messages, kdyz jdeme pres ajax.
     * 
     * @param string $message
     * @param string $type
     * @return mixed
     */
    public function flashMessage($message, $type = 'info')
    {
        $f = parent::flashMessage($message, $type);

        $this->redrawControl("flashMessagesWrapper");
        $this->redrawControl("flashMessages");
        
        return $f;
    }
    
    /**
     * Akce pro zavření fancyboxu.
     *
     * @param array $redirect Kam se má přesměrovat parent - ve stejném formátu jako to chce $this->redirect() z Nette.
     * @param string $redirectTo Url, kam se má přesměrovat parent - jako alternativa k parametru $redirect.
     * @param bool|string $refreshGrid Zda se má zavolat v parentovi funkce refreshGrid(). Pokud je TRUE, zavolá se bez parametru (kvůli zpětné kompatibilitě). Pokud je string, zavolá se s parametrem - nutné uvádět name gridu (to se zjistí v atributu data-grid-name u divu s classem 'grid').
     * @param bool $reload
     */
    public function actionCloseFancybox($redirect = NULL, $redirectTo = NULL, $refreshGrid = NULL, $reload = FALSE)
    {
        //\Tracy\Debugger::log($refreshGrid, "closeFancybox");

        $this->template->setFile(__DIR__ . "/../Templates/closeFancybox.latte");
        
        if (!empty($redirect) && empty($redirectTo))
        {
            $redirectTo = $this->link($redirect);
        }
        
        if (!empty($redirectTo))
        {
            if (isset($this->request->parameters["_fid"]))
            {
                $hash = "";
                
                if (($posHash = strpos($redirectTo, "#")))
                {
                    $hash = substr($redirectTo, $posHash);
                    $redirectTo = substr($redirectTo, 0, $posHash);
                }
                
                $redirectTo .= (strpos($redirectTo, "?") !== FALSE ? "&" : "?") . "_fid=" . $this->request->parameters["_fid"] . $hash;
            }
        
            // redirect udela javascript pote, co zavre fancybox
            $this->template->redirectTo = $redirectTo;
            
            // trik - aby se flash messages zobrazily na dalsi strance
            $this->flashSession->setExpiration(time() + 5);
        }
        
        $this->template->refreshGrid = $refreshGrid;
        
        $this->template->reload = $reload;
    }   
}
