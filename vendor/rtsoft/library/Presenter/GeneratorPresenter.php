<?php

namespace RTsoft\Presenter;

use \App\Model;
use \Nette\Forms\Form;

class GeneratorPresenter extends BasePresenter
{
    /** @var \RTsoft\Model\GeneratorRepository */
    private $generatorRepository;

    /** @persistent */
    public $path;

    /** @persistent */
    public $name;

    /** @persistent */
    public $Name;

    /** @persistent */
    public $naMe;

    private $types = array(
    'TEXT'            => "Text",
    'PASSWORD'        => "Password",
    'TEXTAREA'        => "Textarea",
    'INT'             => "Int",
    'FLOAT'           => "Float",
    'DATE'            => "Date",
    'DATETIME'        => "Datetime",
    'TIME'            => "Time",
    'BOOL'            => "Bool",
    'REF'             => "Ref",
    'SELECT'          => "Selectbox",
    'CHECKBOXLIST'    => "Checkbox list",
    'RADIOLIST'       => "Radio list",
    'BLOB'            => "Upload box",
    'IMAGE_UPLOAD'    => "Image upload",
    'LABEL'           => "Label");

    private $filterTypes = array(
    'ACCORDING_TO_THE_TYPE'                     => "According to the type",
    'NONE'                                      => "None",
    'ONE_TEXTBOX_COMPARING_EXACT_MATCH'         => "Exact match",
    'ONE_TEXTBOX_COMPARING_SUBSTRING_MATCH'     => "Substring",
    'ONE_TEXTBOX_COMPARING_SUBSTRING_BEGINNING' => "Substring start",
    'ONE_TEXTBOX_COMPARING_SUBSTRING_ENDING'    => "Substring end",
    'TWO_TEXTBOXES_COMPARING_RANGE'             => "Range",
    'BOOL'                                      => "Bool");

    private $options = array(
    'NOT_IN_GRID'       => "Not in list",
    'NOT_IN_FORM'       => "Not in form",
    'SORTING_DISABLED'  => "Do not sort",
    'DISABLED'          => "Disabled",
    'MULTIPLE'          => "Multiple",
    'CKEDITOR'          => "Ckeditor");

    private $whitespace = array(
    'spaces'            => "Mezery",
    'tabs'              => "Taby");

    public function __construct(\RTsoft\Model\GeneratorRepository $generatorRepository)
    {
        $this->generatorRepository = $generatorRepository;

        parent::__construct();
    }

    public function actionDefault()
    {
        $this->template->setFile(__DIR__ . "/../Templates/Generator/default.latte");
    }

    public function actionStep2()
    {
        $this->template->setFile(__DIR__ . "/../Templates/Generator/step2.latte");
    }

    public function createComponentForm()
    {
        $form = new \RTsoft\Form\BootstrapForm();

        $form->addGroup('Základní nastavení');
        $form->addText('name', "Jméno tabulky")
                ->addRule(Form::FILLED);
        $form->addText('path', "Cesta kam generovat soubory")
                ->setDefaultValue('temp/generator/')
                ->addRule(Form::FILLED);

        $form->addSubmit('submit', 'Pokračovat');
        $form->onSuccess[] = array($this, 'formSucceeded');

        return $form;
    }

    public function createComponentForm2()
    {
        $form = new \RTsoft\Form\BootstrapForm();

        $this->createPresenterForm($form);
        $this->createModelForm($form);
        $this->createConfigForm($form);

        $form->addSubmit('submit', 'Vygenerovat');
        $form->onSuccess[] = array($this, 'form2Succeeded');

        return $form;
    }

    public function formSucceeded($form, $values)
    {
        $data = $form->getValues();

        $this->name = $data['name'];
        $this->Name = implode(array_map('ucfirst', explode('_', $data['name'])));
        $this->naMe = lcfirst($this->Name);

        if (substr($data['path'], -1) != '/')
        {
            $this->path = $data['path'].'/';
        }
        else
        {
            $this->path = $data['path'];
        }

        $this->redirect('Generator:step2');
    }

    public function form2Succeeded($form, $values)
    {
        $data = $form->getValues();

        try
        {
            $this->generatePresenter($data);
            $this->generatePresenterTemplates($data);
            $this->generateModel($data);
            $this->generateComponentList($data);
            $this->generateComponentForm($data);
            $this->generateConfigModel($data);
            $this->generateConfigComponent($data);
        }
        catch (\Exception $e)
        {
            $this->flashMessage('Soubory nebyly vygenerovány.', 'error');
            return;
        }

        $this->flashMessage('Soubory úspěšně vygenerovány.', 'success');
        return;
    }

    private function createPresenterForm($form)
    {
        $form->addGroup('Nastavení presenteru');
        $form->addText('presenter_header_default', 'Nadpis')
                ->addRule(Form::FILLED);
        $form->addText('presenter_verb_suffix', 'Koncovka slovesa');
        $form->addText('presenter_header_edit', 'Nadpis u editace')
                ->setDefaultValue('Editace záznamu')
                ->addRule(Form::FILLED);
        $form->addText('presenter_header_add', 'Nadpis u přidání')
                ->setDefaultValue('Přidání záznamu')
                ->addRule(Form::FILLED);
        $form->addText('presenter_button_edit', 'Tlačítko editace')
                ->setDefaultValue('Editovat záznam')
                ->addRule(Form::FILLED);
        $form->addText('presenter_button_add', 'Tlačítko přídání')
                ->setDefaultValue('Přidat záznam')
                ->addRule(Form::FILLED);
        $form->addText('presenter_button_delete', 'Tlačítko smazání')
                ->setDefaultValue('Smazat záznam')
                ->addRule(Form::FILLED);
        $form->addText('presenter_confirm_delete', 'Ověřovací zpráva u smazání')
                ->setDefaultValue('Opravdu chcete smazat záznam?')
                ->addRule(Form::FILLED);
        $form->addText('presenter_button_undelete', 'Tlačítko obnovení')
                ->setDefaultValue('Obnovit záznam')
                ->addRule(Form::FILLED);
        $form->addText('presenter_confirm_undelete', 'Ověřovací zpráva u obnovení')
                ->setDefaultValue('Opravdu chcete obnovit záznam?')
                ->addRule(Form::FILLED);
    }

    private function generatePresenter($data)
    {
        $presenter = $this->getFile($this->path.'presenter/', $this->Name.'Presenter.php');
        $txt =
         "<?php\n\n"
        ."namespace App\\Presenter;\n\n"
        ."use App\\Model;\n\n"
        ."/**\n"
        ." * @persistent(list)\n"
        ." */\n"
        ."class {$this->Name}Presenter extends BaseDashboardPresenter\n"
        ."{\n"
        ."    /** @var Model\\{$this->Name}Repository */\n"
        ."    protected \${$this->naMe}Repository;\n\n"
        ."    function __construct(Model\\{$this->Name}Repository \${$this->naMe}Repository)\n"
        ."    {\n"
        ."        \$this->repository = \${$this->naMe}Repository;\n"
        ."        parent::__construct();\n"
        ."    }\n\n"
        ."    protected function startup()\n"
        ."    {\n"
        ."        parent::startup();\n"
        ."        \$this->itemName = '{$data['presenter_header_default']}';\n"
        ."        \$this->verbSuffix = '{$data['presenter_verb_suffix']}';\n"
        ."    }\n"
        ."}";
        $this->writeFile($presenter, $txt);
    }

    private function generatePresenterTemplates($data)
    {
        $default = $this->getFile("{$this->path}templates/{$this->Name}/", 'default.latte');
        $txt =
         "{block content}\n\n"
        ."<a id=\"add\" n:href=\"add\" class=\"btn btn-primary top-right-button open-in-fancybox\"><span class=\"glyphicon glyphicon-plus-sign\"></span> {$data['presenter_button_add']}</a>\n\n"
        ."<h2 n:block=\"title\">{$data['presenter_header_default']}</h2>\n\n"
        .'{control list}';
        $this->writeFile($default, $txt);

        $edit = $this->getFile("{$this->path}templates/{$this->Name}/", 'edit.latte');
        $txt =
         "{layout '../@iframe.latte'}\n\n"
        ."{block fancy-header}{$data['presenter_header_edit']}{/block}\n\n"
        ."{block content}\n\n"
        .'{control form}';
        $this->writeFile($edit, $txt);

        $add = $this->getFile("{$this->path}templates/{$this->Name}/", 'add.latte');
        $txt =
         "{layout '../@iframe.latte'}\n\n"
        ."{block fancy-header}{$data['presenter_header_add']}{/block}\n\n"
        ."{block content}\n\n"
        .'{control form}';
        $this->writeFile($add, $txt);
    }

    private function createModelForm($form)
    {
        $columns = $this->generatorRepository->getColumns($this->name);
        $tableData = $this->generatorRepository->getTableDescription($this->name);

        $form->addGroup('Nastavení modelu');
        if (is_array($columns))
        {
            foreach ($columns as $key => $item)
            {
                $form->addGroup('Nastavení sloupečku \''.$item.'\'');
     $enabled = $form->addCheckbox('model_column_enabled_'.$key, 'Vygenerovat do columns');
                $form->addText('model_column_title_'.$key, 'Popisek pro form i grid')
                        ->setDefaultValue(ucfirst($tableData[$item]['Comment']));
                $form->addText('model_column_title_grid_'.$key, 'Jiný popisek pro grid')
                        ->setAttribute('placeholder', 'Použije se popisek pokud nevyplněno.');
        $type = $form->addSelect('model_column_type_'.$key, 'Typ', $this->types)
                        ->setPrompt('Vyberte typ');
                $form->addSelect('model_column_filter_type_'.$key, 'Typ filtru', $this->filterTypes)
                        ->setPrompt('Vyberte typ');
     $default = $form->addText('model_column_default_'.$key, 'Default hodnota');
    $fDefault = $form->addText('model_column_filter_default_'.$key, 'Default hodnota filtru');
    $required = $form->addCheckbox('model_column_required_'.$key, 'Required ve formuláři');
  $validators = $form->addTextArea('model_column_validators_'.$key, 'Validátory')
                     ->setAttribute('placeholder', 'Form::MAX_LENGTH => array(self::DEFAULT_MESSAGE, 2)');
                $form->addCheckboxList('model_column_options_'.$key, 'Nastavení', $this->options);
                $form->addTextArea('model_column_items_'.$key, 'Položky pro selectbox')
                        ->setAttribute('placeholder', 'Pro enum se vygeneruje automaticky.');
                $form->addTextArea('model_column_attributes_'.$key, 'Atributy')
                        ->setAttribute('placeholder', '"placeholder" => "one", "onclick" => "two"');
                $form->addTextArea('model_column_filter_attributes_'.$key, 'Atributy filtru')
                        ->setAttribute('placeholder', '"placeholder" => "one", "onclick" => "two"');
                $form->addText('model_column_filter_by_'.$key, 'Co se použije při filtrování')
                        ->setAttribute('placeholder', $this->name.'.'.$item);
                $form->addText('model_column_order_by_'.$key, 'Co se použije při řazení')
                        ->setAttribute('placeholder', $this->name.'.'.$item);
                $form->addText('model_column_tooltip_'.$key, 'Tooltip')
                        ->setDefaultValue(ucfirst($tableData[$item]['Comment']));

                if ($item != 'id')
                {
                    $enabled->setValue(true);
                }
                if ($tableData[$item]['Key'] == 'MUL')
                {
                    $type->setDefaultValue('REF');
                }
                else if (preg_match('/varchar\(([0-9]+)\)/', $tableData[$item]['Type'], $match) == 1)
                {
                    $type->setDefaultValue('TEXT');
                    $validators->setDefaultValue("Form::MAX_LENGTH => array(self::DEFAULT_MESSAGE, {$match[1]})");
                }
                else
                {
                    $this->assignType($type, $tableData[$item]['Type']);
                }
                if ($item == 'active' || $item == 'not_deleted')
                {
                    $default->setDefaultValue("TRUE");
                    $fDefault->setDefaultValue("self::BOOL_POSITIVE");
                }
                if ($tableData[$item]['Null'] == 'NO')
                {
                    $required->setValue(true);
                }
            }
        }
    }

    private function generateModel($data)
    {
        $enums = array();
        $columns = $this->generatorRepository->getColumns($this->name);
        $tableData = $this->generatorRepository->getTableDescription($this->name);
        $file = $this->getFile($this->path.'model/', $this->Name.'Repository.php');
        $txt =
         "<?php\n\n"
        ."namespace App\\Model;\n\n"
        ."use Nette\\Application\\UI\\Form;\n"
        ."use RTsoft\\Model\\GridFormRepository as R;\n\n"
        ."class {$this->Name}Repository extends R\n"
        ."{\n"
        ."    protected \$tableName = '{$this->name}';\n\n"
        ."    public \$columns = array(\n";
        if (is_array($columns))
        {
            foreach ($columns as $key => $item)
            {
                if ($data['model_column_enabled_'.$key] == false)
                {
                    continue;
                }
                $txt = $txt."        \n";
                $txt = $txt."        '{$item}' => array(\n";
                if ($data['model_column_title_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_TITLE             => '{$data['model_column_title_'.$key]}',\n";
                }
                if ($data['model_column_title_grid_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_TITLE_GRID        => '{$data['model_column_title_grid_'.$key]}',\n";
                }
                if ($data['model_column_type_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_TYPE              => self::TYPE_{$data['model_column_type_'.$key]},\n";
                }
                if (strpos($tableData[$item]['Type'], 'enum') !== false)
                {
                    $enums[] = $item;
                    $this->generateEnum($item, $tableData);
                }
                if ($data['model_column_filter_type_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_FILTER_TYPE       => self::FILTER_TYPE_{$data['model_column_filter_type_'.$key]},\n";
                }
                if ($data['model_column_default_'.$key] != null)
                {
                    /*if (strpos($data['model_column_default_'.$key], 'self::') === false)
                    {
                        $data['model_column_default_'.$key] = "'{$data['model_column_default_'.$key]}'";
                    }*/
                    $txt = $txt."            self::COLUMN_DEFAULT           => {$data['model_column_default_'.$key]},\n";
                }
                if ($data['model_column_filter_default_'.$key] != null)
                {
                    /*if (strpos($data['model_column_filter_default_'.$key], 'self::') === false)
                    {
                        $data['model_column_filter_default_'.$key] = "'{$data['model_column_filter_default_'.$key]}'";
                    }*/
                    $txt = $txt."            self::COLUMN_FILTER_DEFAULT    => {$data['model_column_filter_default_'.$key]},\n";
                }
                if ($data['model_column_validators_'.$key] != null && $data['model_column_required_'.$key] == true)
                {
                    $txt = $txt."            self::COLUMN_VALIDATORS        => array(Form::REQUIRED => self::DEFAULT_MESSAGE, {$data['model_column_validators_'.$key]}),\n";
                }
                else if ($data['model_column_required_'.$key] == true)
                {
                    $txt = $txt."            self::COLUMN_VALIDATORS        => array(Form::REQUIRED => self::DEFAULT_MESSAGE),\n";
                }
                else if ($data['model_column_validators_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_VALIDATORS        => array({$data['model_column_validators_'.$key]}),\n";
                }
                if (is_array($data['model_column_options_'.$key]) && count($data['model_column_options_'.$key]) > 0)
                {
                    $txt = $txt."            self::COLUMN_OPTIONS           => array(";
                    foreach ($data['model_column_options_'.$key] as $value)
                    {
                        $txt = $txt." self::OPTION_{$value},";
                    }
                    $txt = $txt."),\n";
                }
                if ($data['model_column_items_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_ITEMS             => array({$data['model_column_items_'.$key]}),\n";
                }
                if ($data['model_column_attributes_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_ATTRIBUTES        => array({$data['model_column_attributes_'.$key]}),\n";
                }
                if ($data['model_column_filter_attributes_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_FILTER_ATTRIBUTES => array({$data['model_column_filter_attributes_'.$key]}),\n";
                }
                if ($data['model_column_filter_by_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_FILTER_BY         => '{$data['model_column_filter_by_'.$key]}',\n";
                }
                if ($data['model_column_order_by_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_ORDER_BY          => '{$data['model_column_order_by_'.$key]}',\n";
                }
                if ($data['model_column_tooltip_'.$key] != null)
                {
                    $txt = $txt."            self::COLUMN_TOOLTIP           => '{$data['model_column_tooltip_'.$key]}',\n";
                }
                $txt = $txt."        ),\n";
            }
        }
        $txt = $txt."    );\n";
        if (count($enums) > 0)
        {
            $txt = $txt.
            "    );\n\n"
            ."    public function __construct(\\Nette\\Database\\Context \$context)\n"
            ."    {\n"
            ."        parent::__construct(\$context);\n"
            ."        \n";
            foreach ($enums as $item)
            {
                $Item = implode(array_map('ucfirst', explode('_', $item)));
                $txt = $txt."        \$this->columns[\"{$item}\"][self::COLUMN_ITEMS] = \\App\\Enum\\E{$Item}::\$items;\n";
            }
            $txt = $txt."    }\n}";
        }
        $txt = $txt."}";

        $this->writeFile($file, $txt);
    }

    private function generateComponentList($data)
    {
        $php = $this->getFile($this->path.'component/'.$this->Name.'/', $this->Name.'List.php');
        $txt =
         "<?php\n\n"
        ."namespace App\\Component;\n\n"
        ."use App\\Model;\n"
        ."use RTsoft\\Model\\GridFormRepository as R;\n\n"
        ."/**\n"
        ." * @persistent(list)\n"
        ." */\n"
        ."class {$this->Name}List extends ListComponent\n"
        ."{\n"
        ."    /** @var Model\\{$this->Name}Repository */\n"
        ."    protected \${$this->naMe}Repository;\n\n"
        ."    function __construct(Model\\{$this->Name}Repository \${$this->naMe}Repository)\n"
        ."    {\n"
        ."        \$this->{$this->naMe}Repository = \${$this->naMe}Repository;\n"
        ."        parent::__construct(\${$this->naMe}Repository);\n"
        ."    }\n\n"
        ."}";
        $this->writeFile($php, $txt);

        $latte = $this->getFile($this->path.'component/'.$this->Name.'/', $this->Name.'List.latte');
        $txt =
        '{control dataGrid}';
        $this->writeFile($latte, $txt);

        $grid = $this->getFile($this->path.'component/'.$this->Name.'/', $this->Name.'List_grid.latte');
        $txt =
        "{define row-actions}\n"
        ."    <a href=\"{plink edit \$row->id}\" class=\"glyphicon glyphicon-edit open-in-fancybox\" title=\"{$data['presenter_button_edit']}\"></a>\n"
        ."    <a href=\"#\" title=\"{\$row->not_deleted ? '{$data['presenter_button_delete']}' : '{$data['presenter_button_undelete']}'}\" n:class=\"glyphicon, \$row->not_deleted ? glyphicon-remove : glyphicon-ok\" onclick=\"if (window.confirm('{if \$row->not_deleted}{$data['presenter_confirm_delete']}{else}{$data['presenter_confirm_undelete']}{/if}')) $(this).next().click(); return false;\"></a>\n"
        ."    <a href=\"{plink switch! \$row->id}\" class=\"ajax hidden\"></a>\n"
        ."{/define}";
        $this->writeFile($grid, $txt);
    }

    private function generateComponentForm($data)
    {
        $php = $this->getFile($this->path.'component/'.$this->Name.'/', $this->Name.'Form.php');
        $txt =
         "<?php\n\n"
        ."namespace App\\Component;\n\n"
        ."use App\\Model;\n"
        ."use RTsoft\\Component\\GridForm\\FormComponent;\n"
        ."use RTsoft\\Model\\GridFormRepository as R;\n"
        ."use Nette\\Application\\UI\\Form;\n\n"
        ."class {$this->Name}Form extends FormComponent\n"
        ."{\n"
        ."    protected \$redirectAfterSavingTo = array(\"closeFancybox\", array(\"refreshGrid\" => TRUE));\n"
        ."    \n"
        ."    \n"
        ."    /** @var Model\\{$this->Name}Repository */\n"
        ."    protected \${$this->naMe}Repository;\n\n"
        ."    function __construct(Model\\{$this->Name}Repository \${$this->naMe}Repository)\n"
        ."    {\n"
        ."        \$this->{$this->naMe}Repository = \${$this->naMe}Repository;\n"
        ."        parent::__construct(\${$this->naMe}Repository);\n"
        ."    }\n\n"
        ."}";
        $this->writeFile($php, $txt);

        $latte = $this->getFile($this->path.'component/'.$this->Name.'/', $this->Name.'Form.latte');
        $txt =
        "{control form}";
        $this->writeFile($latte, $txt);
    }

    private function createConfigForm($form)
    {
        $form->addGroup('Nastavení configu');
        $form->addSelect('config_whitespace', 'Neon formát používá', $this->whitespace)->setDefaultValue("spaces");
    }

    private function generateConfigModel($data)
    {
        $file = $this->getFile($this->path.'config/', 'model.neon');
        $ws = $data['config_whitespace'] == 'tabs' ? "\t" : "    ";
        $line = "- App\\Model\\{$this->Name}Repository\n";
        $orig  = $temp = file_get_contents(getcwd().'/app/config/model.neon');
        while (preg_match('/- App\\\Model\\\([\S]+)Repository/', $temp, $match) == 1)
        {
            if (strcmp($this->Name, $match[1]) < 0)
            {
                $pos = strpos($orig, $match[0]);
                $txt = substr($orig, 0, $pos).$line.$ws.substr($orig, $pos);
                return $this->writeFile($file, $txt);
            }
            else if (strcmp($this->Name, $match[1]) == 0)
            {
                $this->flashMessage('Záznam v model.neon již existuje.');
                return $this->writeFile($file, $orig);
            }
            else
            {
                $temp = substr($temp, strlen($match[0]));
            }
        }
        $this->writeFile($file, $orig.$ws.$line);
    }

    private function generateConfigComponent($data)
    {
        $file = $this->getFile($this->path.'config/', 'component.neon');
        $ws = $data['config_whitespace'] == 'tabs' ? "\t" : "    ";
        $line =
         "{$this->Name}Form:\n"
        ."{$ws}{$ws}class: App\\Component\\{$this->Name}Form\n\n"
        ."{$ws}{$this->Name}List:\n"
        ."{$ws}{$ws}class: App\\Component\\{$this->Name}List\n\n";
        $orig = $temp = file_get_contents(getcwd().'/app/config/component.neon');
        while (preg_match('/([\S]+)Form:/', $temp, $match) == 1)
        {
            if (strcmp($this->Name, $match[1]) < 0)
            {
                $pos = strpos($orig, $match[0]);
                $txt = substr($orig, 0, $pos).$line.$ws.substr($orig, $pos);
                return $this->writeFile($file, $txt);
            }
            else if (strcmp($this->Name, $match[1]) == 0)
            {
                $this->flashMessage('Záznam v component.neon již existuje.');
                return $this->writeFile($file, $orig);
            }
            else
            {
                $current =
                "{$match[0]}\n"
               ."{$ws}{$ws}class: App\\Component\\{$match[1]}Form\n\n"
               ."{$ws}{$match[1]}List:\n"
               ."{$ws}{$ws}class: App\\Component\\{$match[1]}List\n\n";
                $temp = substr($temp, strlen($current) - 1);
            }
        }
        $this->writeFile($file, $orig.$ws.$line);
    }

    private function getFile($path, $name, $option = 'w')
    {
        if (!file_exists($path))
        {
            mkdir($path, 0777, true);
        }
        return fopen($path.$name, $option);
    }

    private function writeFile($file, $text)
    {
        fwrite($file, $text);
        fclose($file);
    }

    private function assignType($control, $type)
    {
        if (strpos($type, 'varchar') !== false)
        {
            return $control->setDefaultValue('TEXT');
        }
        else if (strpos($type, 'tinyint(1)') !== false)
        {
            return $control->setDefaultValue('BOOL');
        }
        else if (strpos($type, 'int') !== false)
        {
            return $control->setDefaultValue('INT');
        }
        else if (strpos($type, 'text') !== false)
        {
            return $control->setDefaultValue('TEXTAREA');
        }
        else if (strpos($type, 'decimal') !== false)
        {
            return $control->setDefaultValue('FLOAT');
        }
        else if (strpos($type, 'datetime') !== false || strpos($type, 'timestamp') !== false)
        {
            return $control->setDefaultValue('DATETIME');
        }
        else if (strpos($type, 'time') !== false)
        {
            return $control->setDefaultValue('TIME');
        }
        else if (strpos($type, 'date') !== false)
        {
            return $control->setDefaultValue('DATE');
        }
        else if (strpos($type, 'enum') !== false)
        {
            return $control->setDefaultValue('SELECT');
        }
        else if (strpos($type, 'blob') !== false)
        {
            return $control->setDefaultValue('BLOB');
        }
    }

    private function generateEnum($item, $tableData)
    {
        $Item = implode(array_map('ucfirst', explode('_', $item)));
        $file = $this->getFile("{$this->path}enum/", "E{$Item}.php");
        $txt =
        "<?php\n\n"
        ."namespace App\\Enum;\n\n"
        ."class E{$Item} extends \\MabeEnum\\Enum\n"
        ."{\n";
        $types = str_replace("'", '', explode(',', substr($tableData[$item]['Type'], 5, -1)));
        foreach ($types as $type)
        {
            $TYPE = strtoupper($type);
            $txt = $txt."    const {$TYPE} = '{$type}';\n";
        }
        $txt = $txt."\n    public static \$items = array(\n";
        foreach ($types as $type)
        {
            $TYPE = strtoupper($type);
            $txt = $txt."        self::{$TYPE} => '{$type}',\n";
        }
        $txt = $txt."    );\n}";
        $this->writeFile($file, $txt);
    }
}
