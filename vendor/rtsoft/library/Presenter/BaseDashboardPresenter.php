<?php

namespace RTsoft\Presenter;

use Nette;

abstract class BaseDashboardPresenter extends BasePresenter
{
    /** @var string Část názvu komponenty */
    protected $componentName;
    
    /** @var string Název položky */
    protected $itemName;
    
    /** @var string Koncovka u sloves ve vetach typu: Záznam byl{koncovka} smazán{koncovka} */
    protected $verbSuffix = "";

    /** @var bool Zda je povolena obnova stavu ze session. */
    protected $saveAndRestoreStateFromSession = FALSE;
    
    /** @var \RTsoft\Model\Repository Hlavní repozitář */
    protected $repository;
    
    
    /**
     * Akce pro přehled nějakých položek obsahující grid.
     * Pokud je povoleno ukládání a obnovování stavu a jde o první načtení,
     * spustí se obnova stavu ze session.
     * Programátor může akci přepsat, ale musí zavolat parenta.
     */
    public function actionDefault()
    {
        if ($this->saveAndRestoreStateFromSession && $this->stateShouldBeRestored())
        {
            $this->restoreStateFromSession();
        }
    }

    /**
     * Akce pro přidávání nové položky obsahující formulář.
     * Programátor může akci přepsat, ale musí zavolat parenta.
     * @param mixed $returnTo
     */
    public function actionAdd($returnTo = NULL)
    {
        
    }
    
    /**
     * Akce pro editaci položky obsahující formulář.
     * Programátor může akci přepsat, ale musí zavolat parenta.
     * @param mixed $returnTo
     */
    public function actionEdit($id, $returnTo = NULL)
    {
        
    }

    /**
     * Akce pro aktivaci/deaktivaci položky.
     * Programátor může akci přepsat, ale musí zavolat parenta.
     * @param mixed $returnTo
     */
    public function actionSwitch($id)
    {
        
    }
    
    /**
     * Render pro aktivaci/deaktivaci položky.
     * Programátor může metodu přepsat, ale musí zavolat parenta.
     * @param mixed $returnTo
     */
    public function renderSwitch($id)
    {
        $this->switchRowWithId($id);
        $this->redirect("default");
    }
    
    /**
     * Signál pro aktivaci/deaktivaci položky.
     * Programátor může handle přepsat, ale musí zavolat parenta.
     * @param mixed $returnTo
     */
    public function handleSwitch($id)
    {
        $this->switchRowWithId($id);
        $this->redrawControl("refreshGridWrapper");
        $this->redrawControl("refreshGrid");
    }
    
    /*public function handleRemove($id)
    {
        $this->repository->remove($id);
        $this->flashMessage("{$this->itemName} byl{$this->verbSuffix} smazán{$this->verbSuffix}.", 'success');
        //$this->redirect("{$this->name}:default");
        $this->redrawControl("refreshGrid");
    }*/

    /**
     * Vytváření XyzList komponenty.
     * @return \RTsoft\Component\GridForm\GridComponent
     */
    protected function createComponentList()
    {
        /* @var $c \RTsoft\Component\GridForm\GridComponent */
        $c = $this->context->createService($this->getComponentName() . "List");
        
        if ($this->saveAndRestoreStateFromSession)
        {
            $c->saveStateInSession = TRUE;
        }
        
        return $c;
    }
    
    /**
     * Vytváření XyzForm komponenty.
     * @return \RTsoft\Component\GridForm\FormComponent
     */
    protected function createComponentForm()
    {
        /* @var $c \RTsoft\Component\GridForm\FormComponent */
        $c = $this->context->createService($this->getComponentName() . "Form");
        
        if (isset($this->request->parameters["id"]))
        {
            $c->id = $this->request->parameters["id"];
        }
        
        if (isset($this->request->parameters["returnTo"]))
        {
            $c->returnTo = $this->presenter->request->parameters["returnTo"];
        }

        $c->httpRequest = $this->getHttpRequest();
        
        return $c;
    }
    
    /**
     * Jenom vraci nazev, ktery zrejme maji komponenty.
     * Nazev odvodi z nazvu presenteru (CustomerPresenter -> Customer).
     * @return string
     */
    protected function getComponentName()
    {
        if (empty($this->componentName))
        {
            $this->componentName = str_replace("Presenter", "", $this->reflection->shortName);
        }
        
        return $this->componentName;
    }
    
    /**
     * Vrátí ActiveRow s daným ID, nebo pokud neexistuje, hodí exception nebo vrátí FALSE.
     * @param int $id
     * @param bool $throwExceptionIfNotExists Zda hodit exception v případě neexistence.
     * @return ActiveRow|bool
     * @throws \Nette\Application\BadRequestException
     */
    protected function getRowById($id, $throwExceptionIfNotExists = TRUE)
    {
        $row = $this->repository->findRow($id);
        
        if (!$row && $throwExceptionIfNotExists)
        {
            throw new \Nette\Application\BadRequestException();
        }
        
        return $row;
    }
    
    /**
     * Aktivace deaktivace.
     * @param int $id
     */
    protected function switchRowWithId($id, $repository = NULL)
    {
        $b = ($repository ? $repository->switchActive($id) : $this->repository->switchActive($id));
        $op = $b ? "obnoven" : "smazán";
        
        $itemName = isset($repository->itemName) ? $repository->itemName : $this->itemName;
        $verbSuffix = isset($repository->verbSuffix) ? $repository->verbSuffix : $this->verbSuffix;
        
        $this->flashMessage("{$itemName} byl{$verbSuffix} {$op}{$verbSuffix}.", 'success');
    }
    
    /**
     * Obnova stavu gridu z dat v session tak, ze se provede redirect, ve kterem
     * budou obsazeny vsechny parametry.
     * Url muze vypadat treba takto:
     * http://l-nette-project/customer/?list-dataGrid-filter[user_id]=1&list-dataGrid-filter[not_deleted]=yes&list-dataGrid-orderColumn=name&list-dataGrid-orderType=desc&list-dataGrid-page=2
     */
    protected function restoreStateFromSession()
    {
        $redirParams = $this->getParamsFromSessionForRedirect();
            
        //\Tracy\Debugger::log("REDIR", "redir");
        //\Tracy\Debugger::log($redirParams, "redir");
            
        if (!empty($redirParams))
        {
            $this->redirect("this", $redirParams);
        }
    }
    
    /**
     * Vraci, jestli se jedna o prvni nacteni stranky nebo ne.
     * Je to dulezite pro rozhodnuti, zda se bude obnovovat stav gridu ze
     * session nebo ne.
     * 
     * @todo Asi neni uplne idealni.
     * @return bool
     */
    protected function stateShouldBeRestored($listAndDataGridComponentName = array("list-dataGrid"))
    {
        if ($this->isAjax() || !empty($_POST))
        {
            return FALSE;
        }
        
        $req = $this->getHttpRequest()->getQuery();
        //\Tracy\Debugger::log($req, "req");
        
        if (!empty($req["dontRestoreState"]))
        {
            return FALSE;
        }
            
        foreach ($listAndDataGridComponentName as $n)
        {
            if (!empty($req["{$n}-orderColumn"]) || !empty($req["{$n}-orderType"]) || !empty($req["{$n}-filter"]) || !empty($req["{$n}-page"]))
            {
                return FALSE;
            }
        }
        
        return TRUE;
    }
    
    /**
     * Ze session, kam ulozila stav komponenta, vytaha parametry a sestavi pole 
     * obsahujici data pro redirect.
     * 
     * @param string $componentName
     * @param string $componentNameSuffix
     * @param string $listAndDataGridComponentName
     * @return string
     */
    protected function getParamsFromSessionForRedirect($componentName = NULL, $listAndDataGridComponentName = "list-dataGrid")
    {
        // kdyz nebyl zadan nazev komponenty, tak ji odvodi od nazvu presenteru a prida suffix (tzn. CustomerPresenter => CustomerList)
        if (NULL === $componentName)
        {
            $componentName = $this->getComponentName() . "List";
        }
        
        // vytahne sekci ze session
        $section = $this->session->getSection($componentName);
        
        // pokud neexistuje, tak se zadny stav obnovovat nebude
        if (!$section)
        {
            return NULL;
        }
            
        // pole pro parametry do redirectu
        $redirParams = array();

        // vytahne ze sekce session polozky, ktere byly vyplneny ve filtru
        $filterOptions = $section->filterOptions;
        
        //\Tracy\Debugger::log("filterOptions", "section");
        //\Tracy\Debugger::log($filterOptions, "section");

        // do pole pro redirect da polozky filtru
        if ($filterOptions)
        {
            foreach($filterOptions as $k => $v)
            {
                $redirParams["{$listAndDataGridComponentName}-filter[{$k}]"] = $v;
            }
        }

        // vytahne ze sekce session cislo stranky
        $page = $section->page;
        
        //\Tracy\Debugger::log("page", "section");
        //\Tracy\Debugger::log($page, "section");

        // pokud tam bylo a nebylo 1, prida se do pole pro redirect cislo stranky
        if ($page && $page != 1)
        {
            $redirParams["{$listAndDataGridComponentName}-page"] = $page;
        }

        // vytahne ze sekce session razeni
        $order = $section->order;
        
        //\Tracy\Debugger::log("order", "section");
        //\Tracy\Debugger::log($order, "section");

        // pokud tam bylo razeni
        if ($order)
        {
            // a bylo tam pole o 2 polozkach, ktere vzniklo z paginatoru (1. polozka obsahuje sloupec, 2. polozka ASC/DESC)
            if (is_array($order) && count($order) == 2)
            {
                $redirParams["{$listAndDataGridComponentName}-orderColumn"] = $order[0];
                if ($order[1] != "ASC")
                {   
                    $redirParams["{$listAndDataGridComponentName}-orderType"] = strtolower($order[1]);
                }
            }
            
            // pokud bylo razeni jen jako string
            if (is_string($order))
            {
                $redirParams["{$listAndDataGridComponentName}-orderColumn"] = $order;
                $redirParams["{$listAndDataGridComponentName}-orderType"] = "";
            }
        }
        
        return $redirParams;
    }
}
