<?php

namespace RTsoft\Control;

class Label extends \Nette\Forms\Controls\BaseControl
{
    public function getControl()
    {
        return "<div class='text'>{$this->getValue()}</div>";
    }
}
