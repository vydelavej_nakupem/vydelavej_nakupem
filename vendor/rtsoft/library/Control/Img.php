<?php

namespace RTsoft\Control;

use Nette\Utils\Html;

/**
 * Vlastni formularovy prvek - zobrazeni obrazku.
 */
class Img extends \Nette\Forms\Controls\BaseControl
{
    public function getControl()
    {
        $src = $this->getValue();
        $name = $this->getHtmlName();
        
        $img = Html::el("img");
        $img->name($name);
        
        if (empty($src))
        {
            $img->alt("Není");
        }
        else
        {
            $img->src(\Latte\Runtime\Filters::dataStream($src));
        }
        
        return $img;
    }
}
