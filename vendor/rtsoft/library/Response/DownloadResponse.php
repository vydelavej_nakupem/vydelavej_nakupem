<?php

namespace RTsoft\Response;

use Nette;

/**
 * Vlastni response pro stahovani souboru.
 * Narozdil od FileResponse nevyzaduje existenci souboru na disku, staci kdyz
 * obsah je v $content, takze se hodi pro stahovani blobu z databaze.
 */
class DownloadResponse extends Nette\Object implements Nette\Application\IResponse
{
    /** @var string */
    private $content;
    
    /** @var string */
	private $contentType;
    
    /** @var string */
	private $name;
    
    /** @var bool */
	private $forceDownload;
    
    /**
     * Konstruktor :)
     * @param string $content Obsah stahovaneho souboru.
     * @param string $name Nazev souboru, ktery se objevi uzivateli jako stahovany.
     * @param string $contentType Typ obsahu, defaultne obecny octet-stream.
     * @param bool $forceDownload Zda vynutit stazeni souboru narozdil od zobrazeni v browseru.
     */
    function __construct($content, $name, $contentType = NULL, $forceDownload = TRUE)
    {
        $this->content = $content;
        $this->name = $name;
        $this->contentType = $contentType ? $contentType : 'application/octet-stream';
		$this->forceDownload = $forceDownload;
    }

    /**
	 * Sends response to output.
     * It doesn't accept resuming. 
     * TODO: to accept resuming :) inspiration in FileResponse
	 * @return void
	 */
    public function send(Nette\Http\IRequest $httpRequest, Nette\Http\IResponse $httpResponse)
    {
        $httpResponse->setContentType($this->contentType);
		$httpResponse->setHeader('Content-Disposition',
			($this->forceDownload ? 'attachment' : 'inline')
				. '; filename="' . $this->name . '"'
				. '; filename*=utf-8\'\'' . rawurlencode($this->name));
        
        $filesize = $length = strlen($this->content);
        
        $httpResponse->setHeader('Content-Range', 'bytes 0-' . ($filesize - 1) . '/' . $filesize);
        $httpResponse->setHeader('Content-Length', $length);
        echo $this->content;
    }
}
