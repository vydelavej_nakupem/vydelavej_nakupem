<?php

namespace RTsoft\Response;

use Nette\Application\IResponse;
use Nette\Object;
use Nette;

/**
 * Výstup ve formě ODS/XLS/XLSX/PDF/HTML.
 * Defaultně ODS.
 * 
 * @uses Pro použití je nutné dát do composer.json:
 * "phpoffice/phpexcel": "*"
 * 
 * @example Příklad použití:
 * $rows = $this->userRepository->fetchForExport();
 * $response = new \RTsoft\Response\SpreadSheetResponse($rows, "Uživatelé.xlsx", \RTsoft\Response\SpreadSheetResponse::FORMAT_XLSX);
 * $this->sendResponse($response);
 * 
 * @version 1.0; 2015-10-30
 * @author vrana@rtsoft.cz
 * 
 * @property-read \PHPExcel $excel Objekt PHPExcel
 */
class SpreadSheetResponse extends Object implements IResponse 
{
    const FORMAT_ODS = "ods"; // default
    const FORMAT_XLS = "xls";
    const FORMAT_XLSX = "xlsx"; 
    const FORMAT_PDF = "pdf";
    const FORMAT_HTML = "html";
    
    /** @var \PHPExcel */
    protected $spreadSheet;
    
    /** @var string */
    protected $filename;
    
    /** @var bool */
	protected $addHeading;
    
    /** @var string */
    protected $format;
    
    /** @var Nette\Database\IRowContainer */
    private $rows;

    public function __construct(Nette\Database\IRowContainer $rows, $filename = "output.ods", $format = self::FORMAT_ODS, $addHeading = TRUE)
    {
        $this->rows = $rows;
        $this->filename = $filename;
        $this->addHeading = $addHeading;
        $this->format = $format;
        
        $this->spreadSheet = new \PHPExcel();
        $this->spreadSheet->getActiveSheet()->setTitle('Data');
        
        if (!empty($this->rows))
        {
            $this->fillExcelSheet();
        }
    }
        
    /**
     * Projde selection a naplni sesit excelu daty.
     */
    private function fillExcelSheet()
    {
        // najde aktualni sesit
        $sheet = $this->spreadSheet->getActiveSheet();
        
        // prvni radek s daty
        $rowNumber = ($this->addHeading ? 2 : 1);
        
        // prochazeni selection a plneni sesitu
        foreach ($this->rows as $row)
        {
            // zacatek ve sloupci A
            $col = 'A'; 
            
            foreach ($row as $k => $cell)
            {
                //\Tracy\Debugger::log($k);\Tracy\Debugger::log($cell);
                
                // zahlavi sloupcu se vyplni pri prochazeni prvniho radku ze selection
                if ($this->addHeading && $rowNumber == 2)
                {
                    $sheet->setCellValue($col . "1", $k);
                }
                
                // vyplneni bunky hodnotou
                $sheet->setCellValue($col . $rowNumber, $cell);
                $col++;
            }
            
            $rowNumber++;
        }
    }

    /**
     * Sends response to output.
     * @return void
     */
    public function send(Nette\Http\IRequest $httpRequest, Nette\Http\IResponse $httpResponse)
    {
        $httpResponse->setContentType('application/force-download');
        $httpResponse->setHeader('Content-Disposition', 'attachment;filename="' . $this->filename . '"');
        $httpResponse->setHeader('Content-Transfer-Encoding', 'binary');

        switch ($this->format)
        {
            default:
            case self::FORMAT_ODS:
                $writer = new \PHPExcel_Writer_OpenDocument($this->spreadSheet);
                break;
            
            case self::FORMAT_PDF:
                $writer = new \PHPExcel_Writer_PDF($this->spreadSheet);
                break;
            
            case self::FORMAT_HTML:
                $writer = new \PHPExcel_Writer_HTML($this->spreadSheet);
                break;
            
            case self::FORMAT_XLS:
                $writer = new \PHPExcel_Writer_Excel5($this->spreadSheet);
                break;
            
            case self::FORMAT_XLSX:
                $writer = new \PHPExcel_Writer_Excel2007($this->spreadSheet);
                break;
                
        }
        
        $writer->save('php://output');
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="GET, SET">

    function getExcel()
    {
        return $this->spreadSheet;
    }
    
    function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    function setAddHeading($addHeading)
    {
        $this->addHeading = $addHeading;
        return $this;
    }

    function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }
    
    function setRows(\Nette\Database\Table\Selection $rows)
    {
        $this->rows = $rows;
        $this->fillExcelSheet();
        return $this;
    }
        
    // </editor-fold>
}