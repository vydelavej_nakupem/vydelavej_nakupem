<?php

namespace RTsoft\Response;

use Nette;
use Nette\Application;
use Nette\Http;

class ImageResponse extends Nette\Object implements Application\IResponse
{
	/** @var \Nette\Image|string */
    private $image;

	/**
     * @param \Nette\Image|string
     */
    public function __construct($image)
    {
        if (!$image instanceof \Nette\Image && !file_exists($image))
        {
            throw new \Nette\InvalidArgumentException('Image must be and instance of \Nette\Image or a filepath to an existing file!');
        }
        
        $this->image = $image;
    }

    /**
     * Odesle response s obrazkem
     * 
     * @param \Nette\Http\IRequest $httpRequest
     * @param \Nette\Http\IResponse $httpResponse
     */
	public function send(\Nette\Http\IRequest $httpRequest, \Nette\Http\IResponse $httpResponse)
	{
        // pokud je to instance obrazku, tak se posle, ma na to svou metodu
		if ($this->image instanceof \Nette\Image)
        {
            $this->image->send();
            return;
        }

        // jinak pokud je to soubor, odesila se rucne
        // napred se urci mime typ
        $type = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $this->image);
		$type = strpos($type, '/') ? $type : 'application/octet-stream';
		$httpResponse->setContentType($type);
        
        // nastavi se delka souboru obrazku
		$length = filesize($this->image);
        $httpResponse->setHeader('Content-Length', $length);

		// soubor se precte a hodi na vystup
		readfile($this->image);
	}
}