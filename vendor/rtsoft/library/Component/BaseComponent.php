<?php

namespace RTsoft\Component;

/**
 * Zakladni komponenta.
 * Metoda render() automaticky nastavi latte se stejnym nazvem souboru jako
 * php soubor a zaregistruje vsechny filtry.
 *
 * @author vrana
 */
class BaseComponent extends \Nette\Application\UI\Control
{
    private $componentName;
    private $componentNameWithPath;
    protected $latteFile;
    
    public function getComponentName()
    {
        if (NULL === $this->componentName)
        {
            $this->componentName = $this->reflection->shortName;
        }
        
        return $this->componentName;
    }

    public function getComponentNameWithPath()
    {
        if (NULL === $this->componentNameWithPath)
        {
            $this->componentNameWithPath = str_replace(".php", "", $this->reflection->fileName);
        }
        
        return $this->componentNameWithPath;
    }
    
    /**
     * Pro tuto komponentu vrati sekci v session.
     */
    protected function getSessionSectionForThisComponent()
    {
        $section = $this->presenter->session->getSection($this->componentName);
        return $section;
    }
    
    /**
     * Samo si zjisti cestu a nazev latte a nastavi ho do templaty.
     * Dale zaregistruje vsechny filtry.
     */
    public function render($params = NULL)
    {
        if (empty($this->latteFile))
        {
            $this->latteFile = $this->getComponentNameWithPath();
        }
        
        $this->template->setFile($this->latteFile . ".latte");
        //\RTsoft\Utils\Filters::registerAll($this->template);
        $this->template->componentName = $this->getComponentName();
        $this->template->render();
    }
}
