<?php

namespace RTsoft\Component\GridForm;

use Nette\Application\UI\Form;
use Nette\Forms\Controls;
use Nette\Utils\Html;
use RTsoft\Model\GridFormRepository as R;

/**
 * Trida pro vypisovani editacnich formularu.
 *
 * @author vrana
 * @copyright (c) 2016, RTsoft
 *
 * @property int $id
 * @property string $returnTo
 * @property string $nameOfColumnForAnnouncement
 * @property mixed $redirectAfterSavingTo
 * @property string $selectBoxPrompt
 * @property array $editActions
 * @property array $addActions
 * @property \Nette\Http\Request $httpRequest
 */
abstract class FormComponent extends BaseComponent
{
    /**
     * ID záznamu při editaci.
     * Nastavovat ho musí BaseDashboardPresenter.
     * @var int
     */
    protected $id = NULL;

    /**
     * Pomocny parametr na urceni, kam se ma po ulozeni uzivatel vracet.
     * @var string
     */
    protected $returnTo = NULL;

    /**
     * Hodnota kterého sloupce se má vypsat při zobrazení hlášky:
     * "Záznam 'XYZ' byl uložen."
     * Standardně se vypisuje ID.
     *
     * Může ale obsahovat i pole, například:
     * array("lastname", " ", "firstname")
     * pak se vypíše: Záznam 'Opršálek Franta' byl uložen.
     * Pokud daný řetězec není v $row jako sloupeček, tak se vloží tak, jak je, takže " " se dá použít jako oddělovač.
     *
     * @var string|array
     */
    protected $nameOfColumnForAnnouncement = "id";

    /**
     * Kam přesměrovat uživatele po uložení.
     * Může být string, ale pokud je array, pak v [0] je presenter:akce a v [1] jsou parametry.
     * @var mixed
     */
    protected $redirectAfterSavingTo;

    /**
     * Default text první položky na selectboxech.
     * @var string
     */
    protected $selectBoxPrompt = "-- Vyberte --";

    /**
     * Hlavní repozitář, ze kterého čerpá data.
     * @var \RTsoft\Model\IFormRepository
     */
    protected $repository;

    /**
     * Seznam akci, ktere se povazuji za editaci.
     * @var array
     */
    protected $editActions = array("edit");

    /**
     * Seznam akci, ktere se povazuji za pridavani.
     * @var array
     */
    protected $addActions = array("add", "addForm");

    /**
     * Http request.
     * @var \Nette\Http\Request
     */
    protected $httpRequest;


    /**
     * Konstruktor - predani hlavniho repozitare, pres ktery bude brat data.
     * @param \RTsoft\Component\GridForm\IGridFormRepository $repository
     */
    public function __construct(\RTsoft\Model\IGridFormRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Render
     */
    public function render($params = NULL)
    {
        // pokud se nesubmituje form
        if (!$this["form"]->isSubmitted())
        {
            // pri editaci a kdyz se nesubmituje form
            if (in_array($this->presenter->action, $this->editActions))
            {
                if (NULL === $this->id)
                {
                    $this->id = $this->presenter->request->parameters["id"]; // zjisteni ID z requestu (z duvodu zpetne kompatibility)
                }

                /* @var $row \Nette\Database\Table\ActiveRow */
                $row = $this->repository->findRow($this->id);

                if (!$row)
                {
                    throw new \Nette\Application\BadRequestException();
                }
                
                $this->template->row = $row;

                if (!$this->checkPermissionToEdit($row, $this->presenter->user))
                {
                    throw new \Nette\Application\ForbiddenRequestException();
                }

                foreach($this->repository->columns as $n => &$c)
                {
                    if ($this->isThisAjaxSelectbox($c) && !empty($c[R::COLUMN_REPOSITORY]) && $row->offsetExists($n) && NULL !== $row[$n])
                    {
                        $options = array($c[R::COLUMN_REPOSITORY]->columnNameForValue => $row[$n]);

                        $this["form"][$n]->items = $c[R::COLUMN_REPOSITORY]->fetchItemsForChoiceControl($options);

                        /*\Tracy\Debugger::log($n, "ff");
                        \Tracy\Debugger::log(count($this["form"][$n]->items), "ff");
                        \Tracy\Debugger::log($options, "ff");*/
                    }
                }

                $this->setFormDefaults($row);
            }

            // pri pridavani a kdyz se nesubmituje form
            if (in_array($this->presenter->action, $this->addActions))
            {
                foreach($this->repository->columns as $n => &$c)
                {
                    if ($this->isThisAjaxSelectbox($c) && !empty($c[R::COLUMN_REPOSITORY]) && isset($c[R::COLUMN_DEFAULT]))
                    {
                        $this["form"][$n]->items = $c[R::COLUMN_REPOSITORY]->fetchItemsForChoiceControl(array($c[R::COLUMN_REPOSITORY]->columnNameForValue => $c[R::COLUMN_DEFAULT]));
                    }
                }

                $this->setFormDefaults(NULL);
            }
        }

        parent::render();
    }

    /**
     * Kontrola oprávnění k editaci.
     * Programator ma moznost metodu prepsat a napsat kontrolu opravneni.
     *
     * @param \Nette\Database\Table\ActiveRow $row
     * @param \Nette\Security\User $user
     */
    protected function checkPermissionToEdit(\Nette\Database\Table\ActiveRow $row, \Nette\Security\User $user)
    {
        return TRUE;
    }

    /**
     * Nastaveni default hodnot formulare pri pridavani a editaci.
     * Ale vola se to jen kdyz se nesubmituje form.
     *
     * Programator ma moznost metodu prepsat a pridat do $row dalsi hodnoty.
     * Pri editaci je $row nastaven na editovany zaznam.
     * Pri pridavani je $row NULL.
     *
     * @param array $row
     * @throws \Nette\Application\BadRequestException
     */
    protected function setFormDefaults($row)
    {
        if ($row != NULL)
        {
            $this["form"]->setDefaults($row);
        }
    }


    /**
     * Vytvoreni editacniho formulare.
     *
     * Programator muze metodu prepsat a neco si do formulare pridat, ale pak
     * musi zavolat tuto nadrazenou metodu!
     * Tlacitko na ukladani s name 'save' se  vytvori, pokud neexistuje.
     *
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentForm()
    {
        $form = new \RTsoft\Form\BootstrapForm();
        return $this->createForm($form);
    }

    /**
     * Vytvoreni editacniho formulare.
     *
     * Stejné chování jako metoda createComponentForm, jen ukládá prvky do neviditelné skupiny.
     * Použitelné pokud poté přidáváte další prvky a checete je groupovat.
     *
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentFormGroup()
    {
        $form = new \RTsoft\Form\BootstrapForm();
        $form->addGroup('default', true)
                ->setOption('container', \Nette\Utils\Html::el('div')->class("group-default"))
                ->setOption('label', null);
        return $this->createForm($form);
    }

    protected function createForm($form)
    {
        foreach($this->repository->columns as $n => &$c)
        {
            if ($this->hasOption($c, R::OPTION_NOT_IN_FORM))
            {
                continue;
            }

            if (!isset($c[R::COLUMN_VALIDATORS]) || !is_array($c[R::COLUMN_VALIDATORS]))
            {
                $c[R::COLUMN_VALIDATORS] = array();
            }

            $input = NULL;

            if (!isset($c[R::COLUMN_TYPE]))
            {
                \Tracy\Debugger::barDump($c);
            }

            // vytvoreni inputu podle typu
            switch ($c[R::COLUMN_TYPE])
            {
                case R::TYPE_BOOL:
                    $input = $form->addCheckbox($n, $c[R::COLUMN_TITLE]);
                    break;

                case R::TYPE_DATE:
                    $input = $form->addDate($n, $c[R::COLUMN_TITLE], \Vodacek\Forms\Controls\DateInput::TYPE_DATE);
                    $c[R::COLUMN_VALIDATORS]['\Vodacek\Forms\Controls\DateInput::validateDateInputValid'] = R::DEFAULT_MESSAGE;
                    break;

                case R::TYPE_DATETIME:
                    $input = $form->addDate($n, $c[R::COLUMN_TITLE], \Vodacek\Forms\Controls\DateInput::TYPE_DATETIME);
                    $c[R::COLUMN_VALIDATORS]['\Vodacek\Forms\Controls\DateInput::validateDateInputValid'] = R::DEFAULT_MESSAGE;
                    break;

                case R::TYPE_TIME:
                    $input = $form->addDate($n, $c[R::COLUMN_TITLE], \Vodacek\Forms\Controls\DateInput::TYPE_TIME);
                    //$c[R::COLUMN_VALIDATORS]['\Vodacek\Forms\Controls\DateInput::validateDateInputValid'] = R::DEFAULT_MESSAGE; // nefunguje
                    break;

                case R::TYPE_PASSWORD:
                    $input = $form->addPassword($n, $c[R::COLUMN_TITLE]);
                    break;

                case R::TYPE_TEXTAREA:
                    $input = $form->addTextArea($n, $c[R::COLUMN_TITLE]);
                    break;

                case R::TYPE_REF:
                case R::TYPE_SELECT:
                    if ($this->hasOption($c, R::OPTION_MULTIPLE))
                    {
                        $input = $form->addMultiSelect($n, $c[R::COLUMN_TITLE]);
                    }
                    else
                    {
                        $input = $form->addSelect($n, $c[R::COLUMN_TITLE]);
                        $input->setPrompt($this->selectBoxPrompt);
                    }
                    break;

                case R::TYPE_CHECKBOXLIST:
                    $input = $form->addCheckboxList($n, $c[R::COLUMN_TITLE]);
                    break;

                case R::TYPE_RADIOLIST:
                    $input = $form->addRadioList($n, $c[R::COLUMN_TITLE]);
                    break;

                case R::TYPE_UPLOAD:
                    if ($this->hasOption($c, R::OPTION_MULTIPLE))
                    {
                        $input = $form->addMultiUpload($n, $c[R::COLUMN_TITLE]);
                    }
                    else
                    {
                        $input = $form->addUpload($n, $c[R::COLUMN_TITLE]);
                    }
                    break;

                case R::TYPE_IMAGE_UPLOAD:

                    // vlastni prvek pro zobrazeni obrazku
                    $form->addImg($n, $c[R::COLUMN_TITLE]);

                    // prvek pro upload noveho obrazku, nad timto se daji aplikovat validatory
                    $input = $form->addUpload($n . "_upload", $c[R::COLUMN_TITLE] . " - nový");

                    // prvek pro smazani obrazku
                    $form->addCheckbox($n . "_delete", $c[R::COLUMN_TITLE] . " - smazat");

                    break;

                case R::TYPE_LABEL:
                    $input = $form->addLabel($n, $c[R::COLUMN_TITLE]);
                    break;

                default:
                    $input = $form->addText($n, $c[R::COLUMN_TITLE]);

                    if ($c[R::COLUMN_TYPE] == R::TYPE_INT && !array_key_exists(Form::INTEGER, $c[R::COLUMN_VALIDATORS]))
                    {
                        $c[R::COLUMN_VALIDATORS][Form::INTEGER] = R::DEFAULT_MESSAGE;
                    }

                    if ($c[R::COLUMN_TYPE] == R::TYPE_FLOAT && !array_key_exists(Form::FLOAT, $c[R::COLUMN_VALIDATORS]))
                    {
                        $c[R::COLUMN_VALIDATORS][Form::FLOAT] = R::DEFAULT_MESSAGE;
                    }

                    break;
            }

            if (!empty($input))
            {
                // itemy - bud zadane pole nebo zavola metodu z repository
                if ($input instanceof \Nette\Forms\Controls\ChoiceControl || $input instanceof \Nette\Forms\Controls\MultiChoiceControl)
                {
                    // pokud se jedna o ajaxovy selectbox
                    if ($this->isThisAjaxSelectbox($c))
                    {
                        // a byl nastaven repository
                        if (!empty($c[R::COLUMN_REPOSITORY]))
                        {
                            // vytahneme si hodnotu z postu, tedy to, co bylo odeslano
                            $postValue = $this->httpRequest->getPost($n);
                            if ($postValue)
                            {
                                $input->setItems($c[R::COLUMN_REPOSITORY]->fetchItemsForChoiceControl(array($c[R::COLUMN_REPOSITORY]->columnNameForValue => $postValue)));
                            }
                        }

                        $input->checkAllowedValues = FALSE;
                    }
                    else
                    {
                        // v ostatnich pripadech klasicke plneni items, ...
                        if (!empty($c[R::COLUMN_ITEMS]))
                        {
                            // bud se zadaneho pole
                            $input->setItems($c[R::COLUMN_ITEMS]);
                        }
                        elseif (!empty($c[R::COLUMN_REPOSITORY]))
                        {
                            // nebo zavola fetchItemsForChoiceControl() ze zadaneho repository
                            $input->setItems($c[R::COLUMN_REPOSITORY]->fetchItemsForChoiceControl());
                        }
                    }
                }

                // default hodnota
                if (isset($c[R::COLUMN_DEFAULT]))
                {
                    if ($c[R::COLUMN_TYPE] != R::TYPE_SELECT || array_key_exists($c[R::COLUMN_DEFAULT], $input->items) || $input->checkAllowedValues === FALSE)
                    {
                        $input->setDefaultValue($c[R::COLUMN_DEFAULT]);
                    }
                }
                
                // validatory
                if (isset($c[R::COLUMN_VALIDATORS]) && is_array($c[R::COLUMN_VALIDATORS]))
                {
                    // prochazi a nastavuje validatory
                    foreach ($c[R::COLUMN_VALIDATORS] as $operation => $messageArg)
                    {
                        // zpracovani hlasky a argumentu - bud je jen hlaska, nebo oboji v poli
                        if (!is_array($messageArg))
                        {
                            $message = $messageArg;
                            $arg = NULL;
                        }
                        else
                        {
                            list($message, $arg) = $messageArg;
                        }

                        // hlaska - bud custom nebo defaultni podle typu sloupce
                        if ($message == R::DEFAULT_MESSAGE)
                        {
                            switch ($operation)
                            {
                                case Form::REQUIRED:
                                    $message = "%label je povinná položka.";
                                    break;

                                case Form::INTEGER:
                                    $message = "%label musí být celé číslo.";
                                    break;

                                case Form::FLOAT:
                                    $message = "%label musí být desetinné číslo.";
                                    break;

                                case Form::EMAIL:
                                    $message = "%label musí obsahovat platnou emailovou adresu.";
                                    break;

                                case Form::MIN_LENGTH:
                                    $message = "%label nesmí být kratší než %d znaky.";
                                    break;

                                case Form::MAX_LENGTH:
                                    $message = "%label nesmí být delší než %d znaky.";
                                    break;

                                case Form::RANGE:
                                    $message = "%label musí být v rozmezí %d a %d.";

                                    if (is_array($arg) && count($arg) == 2)
                                    {
                                        // různé hlášky, pokud první z argumentů je NULL
                                        if ($arg[0] === NULL)
                                        {
                                            if ($arg[1] === 0)
                                            {
                                                $message = "%label musí být nekladné číslo.";
                                            }
                                            else if ($arg[1] === 1)
                                            {
                                                $message = "%label musí být záporné číslo.";
                                            }
                                            else
                                            {
                                                $message = "%label musí být menší nebo rovno %d%d.";
                                            }
                                        }

                                        // různé hlášky, pokud druhý z argumentů je NULL
                                        if ($arg[1] === NULL)
                                        {
                                            if ($arg[0] === 0)
                                            {
                                                $message = "%label musí být nezáporné číslo.";
                                            }
                                            else if ($arg[0] === 1)
                                            {
                                                $message = "%label musí být kladné číslo.";
                                            }
                                            else
                                            {
                                                $message = "%label musí být větší nebo rovno %d.";
                                            }
                                        }
                                    }
                                    break;

                                case Form::VALID:
                                case '\Vodacek\Forms\Controls\DateInput::validateDateInputValid':

                                    // nutne rozlisit podle typu sloupce
                                    switch ($c[R::COLUMN_TYPE])
                                    {
                                        case R::TYPE_DATE:
                                            $message = "%label musí obsahovat datum ve formátu 'den.měsíc.rok'.";
                                            break;

                                        case R::TYPE_DATETIME:
                                            $message = "%label musí obsahovat datum ve formátu 'den.měsíc.rok hodina:minuta:sekunda'.";
                                            break;

                                        case R::TYPE_TIME:
                                            $message = "%label musí obsahovat čas ve formátu 'hodina:minuta:sekunda'.";
                                            break;
                                    }
                                    break;

                                case Form::PATTERN:

                                    // podle regularniho vyrazu hlaska
                                    switch($arg)
                                    {
                                        case R::REGEXP_PHONE:
                                            $message = "%label musí obsahovat telefonní číslo ve správném formátu.";
                                            break;

                                        default:
                                            $message = "%label nebyl správně vyplněn.";
                                    }

                                    break;

                                case Form::IMAGE:
                                    $message = "%label musí být JPEG, PNG nebo GIF.";
                                    break;

                                default:
                                    $message = NULL;
                            }
                        }

                        // pokud se mezi validatory nenachazi REQUIRED, prida se navic podminka na vyplnenost
                        if (!array_key_exists(Form::REQUIRED, $c[R::COLUMN_VALIDATORS]) && $c[R::COLUMN_TYPE] != R::TYPE_BOOL)
                        {
                            $input->addCondition(Form::REQUIRED)
                                  ->addRule($operation, $message, $arg);
                        }
                        else
                        {
                            // pridani validatoru bez podminky
                            $input->addRule($operation, $message, $arg);
                        }
                    }
                }

                // tooltip
                if (isset($c[R::COLUMN_TOOLTIP]))
                {
                    $input->setAttribute("tooltip", str_replace("%label", $c[R::COLUMN_TITLE], $c[R::COLUMN_TOOLTIP]));
                }

                // atributy
                if (isset($c[R::COLUMN_ATTRIBUTES]) && is_array($c[R::COLUMN_ATTRIBUTES]))
                {
                    foreach ($c[R::COLUMN_ATTRIBUTES] as $attrKey => $attrVal)
                    {
                        if($attrKey === R::SEPARATOR)
                        {
                            // je atribut pro nastaveni separatoru v checkboxlistu
                            $input->getSeparatorPrototype()->setName($attrVal);
                            continue;
                        }

                        $input->setAttribute($attrKey, $attrVal);
                    }
                }

                // disabled
                if ($this->hasOption($c, R::OPTION_DISABLED))
                {
                    $input->setDisabled(TRUE);
                }

                // ckeditor
                if ($this->hasOption($c, R::OPTION_CKEDITOR))
                {
                    $input->getControlPrototype()->class('ckeditor');
                    $form->getElementPrototype()->onsubmit('CKEDITOR.instances["' . $input->getHtmlId() . '"].updateElement()');
                }
            }
        }

        $form->onSuccess[] = $this->formSubmitted;

        $this->addSubmitButton($form);

        return $form;
    }

    /**
     * Přidá submit button a provede příslušné úpravy kvůli bootstrapu.
     *
     * Pokud už tam byl, tak ho odebere a znovu přidá. To se hodí, když se
     * přidávají v XyzForm.php v metodě  createComponentForm() další políčka.
     *
     * @param Form $form
     */
    protected function addSubmitButton($form, $name = "save", $glyphicon = "ok", $text = "Uložit")
    {
        if (isset($form[$name]))
        {
            $form->removeComponent($form[$name]);
        }

        $form->addSubmit($name, "Uložit");

        $save = $form[$name]->getControlPrototype();
        $save->setName("button");
        $save->create("span class=\"glyphicon glyphicon-{$glyphicon}\"")->add(" {$text}");
    }

    /**
     * Spusti se, kdyz je formular odeslan a je validni.
     *
     * Programator muze metodu prepsat, kdyz chce udelat nejake dalsi serverove
     * validace, pokud je vse OK, zavola tuto nadrazenou metodu.
     *
     * @param \Nette\Application\UI\Form $form
     */
    public function formSubmitted(Form $form, \stdClass $values)
    {
        try
        {
            if (in_array($this->presenter->action, $this->editActions))
            {
                if (NULL === $this->id)
                {
                    $this->id = $this->presenter->request->parameters["id"]; // zjisteni ID z requestu (z duvodu zpetne kompatibility)
                }
            }

            $row = $this->saveForm($form, $values, $this->id);
        }
        catch (\Exception $e)
        {
            \Tracy\Debugger::log($e, \Tracy\Debugger::ERROR);
            $form->addError("Při ukládání formuláře došlo k chybě.");
        }

        if (!$form->hasErrors())
        {
            if ($this->nameOfColumnForAnnouncement !== FALSE)
            {
                // sestaveni hlasky ve stylu: "Záznam 'XYZ' byl uložen."
                $x = "";

                // pokud bylo zadáno, který sloupec/sloupce se mají použít pro hlášku...
                if (!empty($this->nameOfColumnForAnnouncement))
                {
                    // pokud to bylo pole
                    if (is_array($this->nameOfColumnForAnnouncement))
                    {
                        $x = array();

                        // tak se to pole projde a pro existující sloupce se dosadí hodnota, jinak se vezme to, co v poli bylo
                        foreach ($this->nameOfColumnForAnnouncement as $c)
                        {
                            $x[] = ((!empty($row[$c])) ? $row[$c] : $c);
                        }

                        $x = implode("", $x);
                    }
                    else if (!empty($row[$this->nameOfColumnForAnnouncement]))
                    {
                        // nebylo to pole, tak se tam vloží jen hodnota sloupce, pokud existuje
                        $x = $row[$this->nameOfColumnForAnnouncement];
                    }
                }

                $x = ($x !== "" ? "'{$x}'" : "");

                $this->presenter->flashMessage("Záznam {$x} byl uložen.", 'success');
            }

            if (!empty($this->redirectAfterSavingTo))
            {
                if (is_array($this->redirectAfterSavingTo))
                {
                    $this->presenter->redirect($this->redirectAfterSavingTo[0], $this->redirectAfterSavingTo[1]);
                }
                else
                {
                    $this->presenter->redirect($this->redirectAfterSavingTo);
                }
            }
        }
    }

    /**
     * Ukladani formulare.
     * Resi se tady ruzne osetreni ukladanych hodnot predtim, nez se zavola metoda save() z repository.
     *
     * Programator muze tuto metodu prepsat a pridat si dalsi nejake veci do ukladani.
     *
     * @param mixed $formOrValues
     * @param int $id
     * @return type
     */
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        //\Tracy\Debugger::log($values, "debug_saveForm");

        // osetreni ruznych veci pred ukladanim formulare
        foreach($this->repository->columns as $n => &$c)
        {
            // ukladani cisel - kdyz je textbox prazdny, tak aby to nespadlo, da se tam NULL (bez ohledu na to, jestli
            // sloupec umoznuje ulozeni NULL nebo ne - tohle uz si musi osetrit programator)
            if ($this->isThisEmptyNumber($c, $values, $n))
            {
                $values[$n] = NULL;
            }

            // ukladani ajaxovych selectboxu - jelikoz je selectbox plnen ajaxem a zvolena polozka se pri vytvareni
            // formulare nenachazela v items, nenachazi se ani ted ve $values a musi se do $values doplnit z POSTu
            if ($this->isThisAjaxSelectbox($c))
            {
                $values[$n] = $this->httpRequest->getPost($n);

                if ($values[$n] === "")
                {
                    $values[$n] = NULL;
                }
            }

            // upload blobu nebo obrazku:
            // - bud je to ne-multiple uploadbox,
            // - nebo typ image_upload, ktery normalne podporuje jenom jeden soubor (obrazek)
            // - a zaroven ve values mame objekt FileUpload, tzn. ze user uploadnul nejaky soubor
            if ((($c[R::COLUMN_TYPE] == R::TYPE_UPLOAD && !$this->hasOption($c, R::OPTION_MULTIPLE)) || $c[R::COLUMN_TYPE] == R::TYPE_IMAGE_UPLOAD) && isset($values[$n]) && $values[$n] instanceof \Nette\Http\FileUpload)
            {
                if ($values[$n]->isOk())
                {
                    $fp = fopen($values[$n]->getTemporaryFile(), 'rb');
                    $binarydata = fread($fp, $values[$n]->getSize());
                    $values[$n] = $binarydata;
                    fclose($fp);
                }
                else
                {
                    unset($values[$n]);
                }
            }

            // upload vice souboru najednou
            if ($c[R::COLUMN_TYPE] == R::TYPE_UPLOAD && $this->hasOption($c, R::OPTION_MULTIPLE))
            {
                $uploaded = array();
                
                if (isset($values[$n]) && is_array($values[$n]))
                {
                    foreach ($values[$n] as $fileUpload)
                    {
                        if ($fileUpload instanceof \Nette\Http\FileUpload && $fileUpload->isOk())
                        {
                            $fp = fopen($fileUpload->getTemporaryFile(), 'rb');
                            $binarydata = fread($fp, $fileUpload->getSize());
                            $uploaded[] = $binarydata;
                            fclose($fp);
                        }
                    }
                }
                
                $values[$n] = $uploaded;
            }

            // pro image upload se vytvarelo vic komponent - uprava dat
            if ($c[R::COLUMN_TYPE] == R::TYPE_IMAGE_UPLOAD)
            {
                // upload noveho obrazku
                if (isset($values[$n . "_upload"]) && $values[$n . "_upload"] instanceof \Nette\Http\FileUpload)
                {
                    if ($values[$n . "_upload"]->isOk())
                    {
                        $fp = fopen($values[$n . "_upload"]->getTemporaryFile(), 'rb');
                        $binarydata = (fread($fp, $values[$n . "_upload"]->getSize()));
                        $values[$n] = $binarydata;
                        fclose($fp);
                    }
                    else
                    {
                        unset($values[$n]);
                    }

                    unset($values[$n . "_upload"]);
                }

                // mazani obrazku
                if (isset($values[$n . "_delete"]))
                {
                    if ($values[$n . "_delete"])
                    {
                        $values[$n] = NULL; // data ve sloupci se nahradi za NULL
                    }

                    unset($values[$n . "_delete"]);
                }
            }
            
            // sloupec not_deleted - 1/NULL
            if ($c[R::COLUMN_TYPE] == R::TYPE_BOOL && $n === "not_deleted" && isset($values[$n]) && ($values[$n] === FALSE || $values[$n] === "0" || $values[$n] === 0))
            {
                $values[$n] = NULL;
            }

            // nektere typy se standardne neukladaji
            if (in_array($c[R::COLUMN_TYPE], R::$columnTypesNotSaveInForm))
            {
                unset($values[$n]);
            }
        }

        // ulozeni do DB
        //\Tracy\Debugger::log($values, "debug_saveForm");
        return $this->repository->save($values, $id);
    }



    // <editor-fold defaultstate="collapsed" desc=" GET, SET">

    function getId()
    {
        return $this->id;
    }

    function getReturnTo()
    {
        return $this->returnTo;
    }

    function getNameOfColumnForAnnouncement()
    {
        return $this->nameOfColumnForAnnouncement;
    }

    function getRedirectAfterSavingTo()
    {
        return $this->redirectAfterSavingTo;
    }

    function getSelectBoxPrompt()
    {
        return $this->selectBoxPrompt;
    }

    function getEditActions()
    {
        return $this->editActions;
    }

    function getAddActions()
    {
        return $this->addActions;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setReturnTo($returnTo)
    {
        $this->returnTo = $returnTo;
    }

    function setNameOfColumnForAnnouncement($nameOfColumnForAnnouncement)
    {
        $this->nameOfColumnForAnnouncement = $nameOfColumnForAnnouncement;
    }

    function setRedirectAfterSavingTo($redirectAfterSavingTo)
    {
        $this->redirectAfterSavingTo = $redirectAfterSavingTo;
    }

    function setSelectBoxPrompt($selectBoxPrompt)
    {
        $this->selectBoxPrompt = $selectBoxPrompt;
    }

    function setEditActions($editActions)
    {
        $this->editActions = $editActions;
    }

    function setAddActions($addActions)
    {
        $this->addActions = $addActions;
    }

    function getHttpRequest()
    {
        return $this->httpRequest;
    }

    function setHttpRequest(\Nette\Http\Request $httpRequest)
    {
        $this->httpRequest = $httpRequest;
    }

    // </editor-fold>

}
