<?php

namespace RTsoft\Component\GridForm;

use Nette\Application\UI\Form;
use RTsoft\Model\GridFormRepository as R;

/**
 * Trida pro snazsi implementaci komponent vypisujicich data v DataGridu.
 * Obsahuje generator gridu.
 *
 * Pouziti:
 * --------
 * Oddedit tridu.
 * V konstruktoru nastavit $this->repository, ktery splnuje IGridFormRepository.
 * V repository vytvorit pole $columns s definicemi sloupcu.
 * 
 * V latte bude:
 * {control dataGrid}
 * 
 * @property bool $filtering
 * @property bool $sorting
 * @property string $orderByDefault
 * @property bool $pagination
 * @property int  $itemsPerPage
 * @property string $selectboxFilterPromptText
 * @property bool $saveStateInSession
 * @property Datagrid $grid
 * @author vrana
 * @copyright (c) 2016, RTsoft
 */
abstract class GridComponent extends BaseComponent
{   
    /** @var bool Filtrování zapnuto */
    protected $filtering = TRUE;
    
    /** @var bool Zda má ukládat stav filtru v session */
    private $saveStateInSession = FALSE;
    
    /** @var bool Řazení zapnuto */
    protected $sorting = TRUE;

    /** @var string Defaultně řadit podle sloupce */
    protected $orderByDefault = NULL;
    
    /** @var bool Stránkování zapnuto */
    protected $pagination = TRUE;
    
    /** @var int Počet řádek na stránce */
    protected $itemsPerPage = 20;
    
    /** @var bool Inline editace - NEFUNGUJE */
    //protected $inlineEditing = FALSE;
    
    /** @var string Text, ktery se vlozi do prvni moznosti u selectboxu ve filtru */
    protected $selectboxFilterPromptText = "";
    
    /** @var Model\IGridFormRepository Hlavní repozitář, ze kterého čerpá data */
    protected $repository;
    
    /** @var DataGrid Instance Nextras datagridu */
    private $grid;
    
    /** @var ArrayHash Veci, ktere je potreba pridat do template, aby je bylo mozne pouzivat v definicich bunek gridu */
    private $gridTemplate;
    
    /** @var array Možnosti u selectboxu pro typ BOOL */
    private $boolSelectOptions = array(R::BOOL_POSITIVE => "Ano", R::BOOL_NEGATIVE => "Ne");
    
    /** @var array Názvy textboxů pro rozsahový filtr */
    public static $nameSuffixesFromTo = array(R::NAME_SUFFIX_FROM => "od", R::NAME_SUFFIX_TO => "do");
    
    /** @var array Seznam sloupců, které mají mít range filtr */
    public $columnsForRangeFilter = array();
    
    /** @var array Seznam sloupců, které mají mít selectbox */
    public $columnsWithSelectboxFilter = array();
    
    /** @var array Seznam sloupců, které mají mít datepicker */
    public $columnsWithDatepickerFilter = array();

    /** @var array Cesta a název souboru s custom šablonami buňek. */
    public $cellTemplateFileCustom = array();
    

    /**
     * Konstruktor - predani hlavniho repozitare, pres ktery bude brat data.
     * @param \RTsoft\Component\GridForm\IGridFormRepository $repository
     */
    public function __construct(\RTsoft\Model\IGridFormRepository $repository)
    {
        $this->repository = $repository;
    }
    
    /**
     * Vraci pole polozek, ktere je potreba pridat do template.
     * @return ArrayHash
     */
    function getGridTemplate()
    {
        if (empty($this->gridTemplate))
        {
            $this->gridTemplate = new \Nette\Utils\ArrayHash();
        }
        
        return $this->gridTemplate;
    }
    
    /*public function render()
    {
        \Tracy\Debugger::log("render");
        
        // kdyz uzivatel zmackl ve filtru na reset (tlacitko 'X')
        if ($this["dataGrid"]["form"]["filter"]["cancel"]->isSubmittedBy())
        {
            \Tracy\Debugger::log("X");
        }
        
        parent::render();
    }*/
    
    /**
     * Vytvoreni datagridu.
     * @return Datagrid
     */
    protected function createComponentDataGrid()
    { 
        //\Tracy\Debugger::log("createComponentDataGrid");
        //\Tracy\Debugger::log($this->presenter->isAjax() ? "je ajax" : "neni ajax");
        //\Tracy\Debugger::log($_POST, "post");
        
        // pokud programator nenastavil vlastni instanci gridu...
        if (NULL === $this->grid)
        {
            // vytvori se instance zdedene tridy z namespace RTsoft (zdedene od Nextras datagridu)
            $this->grid = new DataGrid();
        }
        
        // preklad
        $this->grid->setTranslator(new \RTsoft\Model\Translator\GridTranslator());
        
        // sloupce
        //$grid->addColumn('id');
        $this->grid->setRowPrimaryKey($this->repository->primaryKeyColumnName);
        
        // prochazi sloupce definovane v $columns v repository a generuje grid podle nastaveni
        foreach($this->repository->columns as $n => &$c)
        {
            // pokud sloupec nema byt v gridu, nebo jeho typ nema byt v gridu defaultne
            if ($this->hasOption($c, R::OPTION_NOT_IN_GRID) || in_array($c[R::COLUMN_TYPE], R::$columnTypesNotInGrid))
            {
                continue;
            }
                
            // pridani sloupce do gridu
            $col = $this->grid->addColumn($n, (isset($c[R::COLUMN_TITLE_GRID]) ? $c[R::COLUMN_TITLE_GRID] : $c[R::COLUMN_TITLE]));
                        
            // razeni
            if ($this->sorting && !$this->hasOption($c, R::OPTION_SORTING_DISABLED))
            {
                $col->enableSort();
            }
            
            // pokud nebyl zadan typ filtru, nastavi se konstanta urcujici vytvoreni filtru podle typu
            if (!isset($c[R::COLUMN_FILTER_TYPE]))
            {
                $c[R::COLUMN_FILTER_TYPE] = R::FILTER_TYPE_ACCORDING_TO_THE_TYPE;
            }
            
            // pokud ma mit sloupec filtr od-do
            /*if ($this->filtering && ($c[R::COLUMN_FILTER_TYPE] == R::FILTER_TYPE_TWO_TEXTBOXES_COMPARING_RANGE 
                                 || ($c[R::COLUMN_FILTER_TYPE] == R::FILTER_TYPE_ACCORDING_TO_THE_TYPE && in_array($c[R::COLUMN_TYPE], R::$columnTypesWithDefaultRangeFilter))))
            {
                foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                {
                    $grid->addColumn($n . "_" . $nameSuffix, $c[R::COLUMN_TITLE] . " - " . $labelSuffix);
                }
            }*/
        }
        
        // filtr
        if ($this->filtering)
        {
            $this->grid->setFilterFormFactory($this->createFilterForm);
        }
        
        // data
        $this->grid->setDatasourceCallback($this->findRows);
        
        // sablony spolecne
        $this->grid->addCellsTemplate(__DIR__ . "/../../../../nextras/datagrid/bootstrap-style/@bootstrap3.datagrid.latte");
        $this->grid->addCellsTemplate(__DIR__ . "/../../../../nextras/datagrid/bootstrap-style/@bootstrap3.extended-pagination.datagrid.latte");
        $this->grid->addCellsTemplate(__DIR__ . "/GridComponent_def.latte");

        // další custom šablona buňek pokud je zadána
        if ($this->cellTemplateFileCustom)
        {
            if (is_array($this->cellTemplateFileCustom))
            {
                foreach ($this->cellTemplateFileCustom as $ctfc)
                {
                    if (file_exists($ctfc))
                    {
                        $this->grid->addCellsTemplate($ctfc);
                    }
                }
            }
            else
            {
                if (file_exists($this->cellTemplateFileCustom))
                {
                    $this->grid->addCellsTemplate($this->cellTemplateFileCustom);
                }
            }
            
        }

        // sablony bunek napr. NecoList_grid.latte
        $cellsTemplateLatteFile = (empty($this->latteFile) ? $this->componentNameWithPath : $this->latteFile) . "_grid.latte";
        
        if (file_exists($cellsTemplateLatteFile))
        {
            $this->grid->addCellsTemplate($cellsTemplateLatteFile);
        }
        
        // pokud je potreba predat neco dalsiho do sablon bunek gridu, preda se to
        if (!empty($this->gridTemplate))
        {
            $this->grid->onRender[] = function($grid)
            {
                $grid->template->setParameters((array)$this->gridTemplate);
            };
        }
        
        // strankovani
        if ($this->pagination)
        {
            @$this->grid->setPagination($this->itemsPerPage, $this->getRowsCount);
        }
        
        // inline editace
        /*if ($this->inlineEditing)
        {
            $this->grid->setEditFormFactory($this->createEditForm);
        }*/
        
        // predani zakladnich parametru do sablon gridu
        $this->grid->onRender[] = function($grid)
        {
            $grid->template->componentName = $this->componentName;
        };
        
        return $this->grid;
    }

    /**
     * Úprava $options.
     * 
     * Programator muze metodu prepsat a zmenit obsah $options predtim, 
     * nez se zavola getRowsCount() a findRows().
     * 
     * @param array $options
     */
    protected function modifyOptions(array &$options)
    {
        //\Tracy\Debugger::log("modifyOptions");
        //\Tracy\Debugger::log($options);
    }
    
    /**
     * Úprava řazení.
     * Nastavení defaultního řazení.
     * 
     * Programator muze metodu prepsat a zmenit podle ceho se radi.
     * Vhodne pro sloupce, ktere nemaji stejne pojmenovany sloupec v DB.
     * 
     * @param string $order
     */
    protected function modifyOrder(&$order)
    {
        // defaultní řazení
        if (!$order && $this->orderByDefault)
        {
            $order = $this->orderByDefault;
        }
    }
    
    /**
     * Uložení hodnot filtru do session.
     */
    private function saveFilterValues(array &$options, $page = 1, $order = "")
    {
        $section = $this->getSessionSectionForThisComponent();
        //$section->setExpiration('2 minutes');
        
        //\Tracy\Debugger::log("saveFilterValues");
        //\Tracy\Debugger::log($options);
        //\Tracy\Debugger::log($page);
        //\Tracy\Debugger::log($order);
        
        $filterOptions = array();
        
        $optionsKeys = array_keys($options);

        // prochazime sloupce
        foreach($this->repository->columns as $n => &$c)
        {
            // pokud sloupec nema byt v gridu, nebo jeho typ nema byt v gridu defaultne, tak se preskoci
            if ($this->hasOption($c, R::OPTION_NOT_IN_GRID) || in_array($c[R::COLUMN_TYPE], R::$columnTypesNotInGrid))
            {
                continue;
            }

            // pokud je nastaven zadny filtr, tak se preskoci
            if ($c[R::COLUMN_FILTER_TYPE] == R::FILTER_TYPE_NONE)
            {
                continue;
            }

            // pokud se dany sloupecek nachazi v $options...
            if (in_array($n, $optionsKeys))
            {
                // vytahnem jeho hodnotu
                $v = $options[$n];
            }
            else
            {
                // pokud ne, dosadime prazdnou hodnotu, pro selectboxy "" kvuli fungovani se setPrompt
                if ($c[R::COLUMN_TYPE] == R::TYPE_BOOL || $c[R::COLUMN_TYPE] == R::TYPE_SELECT)
                {
                    $v = "";
                }
                else
                {
                    // a co se tyce ostatnich hodnot, na ty kaslem
                    continue;
                }
            }

            // pokud je hodnota filtru rovna defaultni hodnote, tak se neuklada
            /*if (isset($c[R::COLUMN_FILTER_DEFAULT]) && $v == $c[R::COLUMN_FILTER_DEFAULT])
            {
                continue;
            }*/

            // zaznamenani hodnoty do pole pro pozdejsi ulozeni
            if ($v instanceof \DateTime)
            {
                $filterOptions[$n] = $v->format("Y-m-d");
            }
            else
            {
                $filterOptions[$n] = $v;
            }
        }
        
        //\Tracy\Debugger::log($filterOptions);

        //$section->remove();

        // ulozeni do session
        $section->filterOptions = $filterOptions;
        $section->page = $page;
        $section->order = $order;
    }
    
    /**
     * Zjisteni poctu zaznamu - standardne se vola metoda z repository.
     */
    public final function getRowsCount(array $options = null, $order = null)
    {
        $this->modifyOptions($options);
        
        return $this->repository->getRowsCount($options, $order);
    }
    
    /**
     * Vraceni dat - standardne se vola metoda z repository.
     * 
     * @param array $options Data z filtru
     * @param string|array $order
     * @param int|\Nette\Utils\Paginator $limit
     * @param int|NULL $offset
     * @return Selection
     */
    public final function findRows(array $options = null, $order = null, $limit = null, $offset = null)
    {
        if ($this->saveStateInSession)
        {
            $page = 1;
            
            if ($limit instanceof \Nette\Utils\Paginator)
            {
                $page = $limit->page;
            }
            else if (is_numeric($limit) && is_numeric($offset))
            {
                $page = ceil($offset / $limit);
            }
            
            $this->saveFilterValues($options, $page, $order);
        }
        
        $this->modifyOptions($options);
        $this->modifyOrder($order);
        
        return $this->repository->findRows($options, $order, $limit, $offset);
    }
    
    
    /**
     * Vytvoreni filtru pro grid.
     * @return \Nette\Forms\Container
     */
    public function createFilterForm()
    {
        //\Tracy\Debugger::log("createFilterForm");
        
        $form = new \Nette\Forms\Container();
        
        // prochazi sloupce z $columns a generuje formular pro filtr
        foreach($this->repository->columns as $n => &$c)
        {
            //\Tracy\Debugger::barDump($c);
            
            // pokud sloupec nema byt v gridu, nebo jeho typ nema byt v gridu defaultne
            if ($this->hasOption($c, R::OPTION_NOT_IN_GRID) || in_array($c[R::COLUMN_TYPE], R::$columnTypesNotInGrid))
            {
                continue;
            }
            
            // pokud je nastaven zadny filtr, filtrovaci policko se nevygeneruje
            if ($c[R::COLUMN_FILTER_TYPE] == R::FILTER_TYPE_NONE)
            {
                continue;
            }
            
            $inputs = array();
            
            // urceni typu filtru
            $rangeFilter = FALSE;
            $typeFilter = $c[R::COLUMN_TYPE];
            
            //dump($n." ".$c[R::COLUMN_FILTER_TYPE]);
            
            if ($c[R::COLUMN_FILTER_TYPE] == R::FILTER_TYPE_ACCORDING_TO_THE_TYPE)
            {
                // default filtr podle typu
                $typeFilter = $c[R::COLUMN_TYPE];
                $rangeFilter = in_array($c[R::COLUMN_TYPE], R::$columnTypesWithDefaultRangeFilter);
                
                if ($rangeFilter)
                {
                    $c[R::COLUMN_FILTER_TYPE] = R::FILTER_TYPE_TWO_TEXTBOXES_COMPARING_RANGE;
                }
            }
            else
            {
                $rangeFilter = ($c[R::COLUMN_FILTER_TYPE] == R::FILTER_TYPE_TWO_TEXTBOXES_COMPARING_RANGE);
            }
            
            if ($rangeFilter)
            {
                $this->columnsForRangeFilter[] = $n;
                
                $form->addContainer($n); // ZÁHADA - tohle tady MUSÍ BÝT, jinak by se v xyz_grid.latte nevykreslil col-filter-xyz
            }
            
            // urceni title
            $title = (isset($c[R::COLUMN_TITLE_GRID]) ? $c[R::COLUMN_TITLE_GRID] : $c[R::COLUMN_TITLE]);
            
            // vygenerovani filtrovaciho policka
            switch ($typeFilter)
            {
                case R::TYPE_INT:
                    
                    if (!$rangeFilter)
                    {
                        $inputs[] = $input = $form->addText($n, $title);
                        $input->addCondition(Form::FILLED)
                              ->addRule(Form::INTEGER, "%label musí být celé číslo.");
                        $input->controlPrototype->title = $title;
                    }
                    else
                    {
                        foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                        {
                            $label = $title . " - " . $labelSuffix;
                            $inputs[] = $input = $form->addText($n . "_" . $nameSuffix, $label);
                            $input->addCondition(Form::FILLED)
                                  ->addRule(Form::INTEGER, "%label musí být celé číslo.");
                            $input->controlPrototype->title = $label;
                        }
                    }
                    break;
                    
                    
                case R::TYPE_FLOAT:
                    
                    if (!$rangeFilter)
                    {
                        $inputs[] = $input = $form->addText($n, $title);
                        $input->addCondition(Form::FILLED)
                              ->addRule(Form::INTEGER, "%label musí být desetinné číslo.");
                        $input->controlPrototype->title = $title;
                    }
                    else
                    {
                        foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                        {
                            $label = $title . " - " . $labelSuffix;
                            $inputs[] = $input = $form->addText($n . "_" . $nameSuffix, $label);
                            $input->addCondition(Form::FILLED)
                                  ->addRule(Form::FLOAT, "%label musí být desetinné číslo.");
                            $input->controlPrototype->title = $label;
                        }
                    }
                    break;
                    
                    
                case R::TYPE_DATE:
                case R::TYPE_DATETIME:
                    
                    if (!$rangeFilter)
                    {
                        $inputs[] = $input = $form->addDate($n, $title, \Vodacek\Forms\Controls\DateInput::TYPE_DATE);
                        $input->addCondition(Form::FILLED)
                              ->addRule('\Vodacek\Forms\Controls\DateInput::validateDateInputValid', "%label musí obsahovat datum ve formátu 'den.měsíc.rok'.");
                        $input->controlPrototype->title = $title;
                    }
                    else
                    {
                        foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                        {
                            $label = $title . " - " . $labelSuffix;
                            $inputs[] = $input = $form->addDate($n . "_" . $nameSuffix, $label, \Vodacek\Forms\Controls\DateInput::TYPE_DATE);
                            $input->addCondition(Form::FILLED)
                                  ->addRule('\Vodacek\Forms\Controls\DateInput::validateDateInputValid', "%label musí obsahovat datum ve formátu 'den.měsíc.rok'.");
                            $input->controlPrototype->title = $label;
                        }
                    }
                    
                    $this->columnsWithDatepickerFilter[] = $n;
                    break;  
                    
                    
                /*case R::TYPE_DATETIME:
                    foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                    {
                        $label = $title . " - " . $labelSuffix;
                        $form->addDate($n . "_" . $nameSuffix, $label, \Vodacek\Forms\Controls\DateInput::TYPE_DATETIME)
                             ->addCondition(Form::FILLED)
                             ->addRule('\Vodacek\Forms\Controls\DateInput::validateDateInputValid', "%label musí obsahovat datum ve formátu 'den.měsíc.rok hodina:minuta:sekunda'.");
                    }
                    break;  */
                
                case R::TYPE_TIME:
                    
                    if (!$rangeFilter)
                    {
                        $inputs[] = $input = $form->addDate($n, $title, \Vodacek\Forms\Controls\DateInput::TYPE_TIME);
                        /*$input->addCondition(Form::FILLED)
                              ->addRule('\Vodacek\Forms\Controls\DateInput::validateDateInputValid', "%label musí obsahovat čas ve formátu 'hodina:minuta:sekunda'."); // nefunguje */
                        $input->controlPrototype->title = $title;
                    }
                    else
                    {
                        foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                        {
                            $label = $title . " - " . $labelSuffix;
                            $inputs[] = $input = $form->addDate($n . "_" . $nameSuffix, $label, \Vodacek\Forms\Controls\DateInput::TYPE_TIME);
                            /*$input->addCondition(Form::FILLED)
                                  ->addRule('\Vodacek\Forms\Controls\DateInput::validateDateInputValid', "%label musí obsahovat čas ve formátu 'hodina:minuta:sekunda'."); // nefunguje */
                            $input->controlPrototype->title = $label;
                        }
                    }
                    
                    $this->columnsWithDatepickerFilter[] = $n;
                    break;
                    
                case R::TYPE_BOOL:
                    
                    if ($rangeFilter)
                    {
                        throw new \Nette\NotImplementedException("GridComponent: Cannot create a range filter for the column '{$n}' of the type BOOL!");
                    }
                    
                    $inputs[] = $input = $form->addSelect($n, $title, $this->boolSelectOptions);
                    $input->setPrompt($this->selectboxFilterPromptText);
                    $input->controlPrototype->title = $title;
                    $input->checkAllowedValues = FALSE;
                    
                    $this->columnsWithSelectboxFilter[] = $n;
                    break;
                
                    
                case R::TYPE_RADIOLIST:
                case R::TYPE_CHECKBOXLIST:
                    
                    if ($rangeFilter)
                    {
                        throw new \Nette\NotImplementedException("GridComponent: Cannot create a range filter for the column '{$n}' of the type CHECKBOXLIST or RADIOLIST!");
                    }
                    
                    $inputs[] = $input = $form->addSelect($n, $title);
                    
                    $input->controlPrototype->title = $title;
                    
                    $this->columnsWithSelectboxFilter[] = $n;
                    break;
                
                case R::TYPE_SELECT:
                    
                    if (!$rangeFilter)
                    {
                        $inputs[] = $input = $form->addSelect($n, $title);
                        
                        $input->setPrompt($this->selectboxFilterPromptText);
                        $input->controlPrototype->title = $title;
                        $input->checkAllowedValues = FALSE;
                    }
                    else
                    {
                        foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                        {
                            $label = $title . " - " . $labelSuffix;
                            $inputs[] = $input = $form->addSelect($n . "_" . $nameSuffix, $label);
                            
                            $input->setPrompt($this->selectboxFilterPromptText);
                            $input->controlPrototype->title = $label;
                            $input->checkAllowedValues = FALSE;
                        }
                    }
                    
                    $this->columnsWithSelectboxFilter[] = $n;
                    break;
                
                case R::TYPE_PASSWORD:
                case R::TYPE_UPLOAD:
                case R::TYPE_IMAGE_UPLOAD:
                    throw new \Nette\NotImplementedException("GridComponent: Cannot create a filter for the column '{$n}' of the type PASSWORD/BLOB/IMAGE_UPLOAD!");
                    break;

                
                default:
                    
                    if (!$rangeFilter)
                    {
                        // bezny textbox
                        $inputs[] = $input = $form->addText($n, $title);
                        $input->controlPrototype->title = $title;
                    }
                    else
                    {
                        // i pro texty lze mit rozsahovy filtr
                        foreach(self::$nameSuffixesFromTo as $nameSuffix => $labelSuffix)
                        {
                            $label = $title . " - " . $labelSuffix;
                            $inputs[] = $input = $form->addText($n . "_" . $nameSuffix, $label);
                            $input->controlPrototype->title = $label;
                        }
                    }
                    break;
            }
            
            // defaultni hodnota ve filtru
            $defaultValue = NULL;
            
            if (isset($inputs))
            {
                // itemy - bud zadane pole nebo zavola metodu z repository
                foreach ($inputs as $input)
                {
                    if ($input instanceof \Nette\Forms\Controls\ChoiceControl || $input instanceof \Nette\Forms\Controls\MultiChoiceControl)
                    {
                        // pokud se jedna o ajaxovy selectbox
                        if ($this->isThisAjaxSelectboxForFilter($c))
                        {
                            // todo
                        }
                        else
                        {
                            // v ostatnich pripadech klasicke plneni items, ...
                            if (!empty($c[R::COLUMN_ITEMS]))
                            {
                                // bud se zadaneho pole
                                $input->setItems($c[R::COLUMN_ITEMS]);
                            }
                            elseif (!empty($c[R::COLUMN_REPOSITORY]))
                            {
                                // nebo zavola fetchItemsForChoiceControl() ze zadaneho repository
                                $input->setItems($c[R::COLUMN_REPOSITORY]->fetchItemsForChoiceControl());
                            }
                        }
                    }
                }

                // default hodnota v poli $columns
                if (isset($c[R::COLUMN_FILTER_DEFAULT]))
                {
                    $defaultValue = $c[R::COLUMN_FILTER_DEFAULT];
                }

                // pokud tedy mame default hodnotu, nastavi se policku (nebo polickum pro rozsahove filtry) ve filtru
                if ($defaultValue)
                {
                    // v pripade, ze bylo zadano pole...
                    if (is_array($defaultValue))
                    {
                        // nastavi se default hodnota polickum ve filtru s odpovidajicim indexem
                        $to = min(count($defaultValue), count($inputs));
                        for ($i = 0; $i < $to; $i++)
                        {
                            $inputs[$i]->setDefaultValue($defaultValue[$i]);
                        }
                    }
                    else
                    {
                        // pokud byla zadana jedna hodnota, nastavi se vsem polickum
                        foreach ($inputs as $input)
                        {
                            $input->setDefaultValue($defaultValue);
                        }
                    }
                }
            
                // tooltip - dava se k filtrovacimu policku a i na nadpis sloupce
                if (isset($c[R::COLUMN_TOOLTIP]))
                {
                    foreach ($inputs as $input)
                    {
                        $input->setAttribute("tooltip", str_replace("%label", $c[R::COLUMN_TITLE], $c[R::COLUMN_TOOLTIP]));
                        $input->setAttribute("title", NULL);
                    }
                }

                // atributy
                if (isset($c[R::COLUMN_FILTER_ATTRIBUTES]) && is_array($c[R::COLUMN_FILTER_ATTRIBUTES]))
                {
                    foreach ($c[R::COLUMN_FILTER_ATTRIBUTES] as $attrKey => $attrVal)
                    {
                        foreach ($inputs as $input)
                        {
                            if($attrKey === R::SEPARATOR)
                            {
                                // je atribut pro nastaveni separatoru v checkboxlistu
                                $input->getSeparatorPrototype()->setName($attrVal);
                                continue;
                            }

                            $input->setAttribute($attrKey, $attrVal);
                        }
                    }
                }
            }
        }
        
        // predani poli do sablony
        $this->grid->onRender[] = function($grid)
        {
            $grid->template->columnsForRangeFilter = $this->columnsForRangeFilter;
            $grid->template->columnsWithDatepickerFilter = $this->columnsWithDatepickerFilter;
            $grid->template->columnsWithSelectboxFilter = $this->columnsWithSelectboxFilter;
        };
        
        return $form;
    }
    
    /**
     * Vytvoreni formu pro inline editaci.
     * 
     * TODO: Problem v Datagrid.php funkce getData()
     *       padá příkaz $this['form']['filter']->getValues(TRUE)
     *       Fatal error: Allowed memory size of NNN bytes exhausted
     * 
     * @param array $row
     * @return \Nette\Forms\Container
     */
    /*public function createEditForm($row)
    {
        $form = new \Nette\Forms\Container();
        
        foreach($this->repository->columns as $n => $c)
        {
            switch ($c[R::COLUMN_TYPE])
            {
                default:
                    $inputs[] = $input = $form->addText($n, $c[R::COLUMN_TITLE]);
            }
        }
        
        if ($row) 
        {
            $form->setDefaults($row);
        }
        
        return $form;
    }*/
    
    // <editor-fold defaultstate="collapsed" desc=" GET, SET">

    function getFiltering()
    {
        return $this->filtering;
    }

    function getSorting()
    {
        return $this->sorting;
    }

    function getOrderByDefault()
    {
        return $this->orderByDefault;
    }

    function getPagination()
    {
        return $this->pagination;
    }

    function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    function setFiltering($filtering)
    {
        $this->filtering = $filtering;
    }

    function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    function setOrderByDefault($orderByDefault)
    {
        $this->orderByDefault = $orderByDefault;
    }

    function setPagination($pagination)
    {
        $this->pagination = $pagination;
    }

    function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    function setSelectboxFilterPromptText($selectboxFilterPromptText)
    {
        $this->selectboxFilterPromptText = $selectboxFilterPromptText;
    }
    
    function getSaveStateInSession()
    {
        return $this->saveStateInSession;
    }

    function setSaveStateInSession($saveStateInSession)
    {
        $this->saveStateInSession = $saveStateInSession;
    }
    
    function getGrid()
    {
        return $this->grid;
    }

    function setGrid(DataGrid $grid)
    {
        $this->grid = $grid;
    }

    // </editor-fold>

}
