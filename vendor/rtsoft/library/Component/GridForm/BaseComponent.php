<?php

namespace RTsoft\Component\GridForm;

use RTsoft\Model\GridFormRepository as R;

/**
 * Zakladni trida pro komponenty GridComponent a FormComponent.
 *
 * @author vrana
 */
class BaseComponent extends \RTsoft\Component\BaseComponent
{   
    /**
     * Overi, zda je u daneho columnu dana option.
     * @param array $c jeden column z pole $columns
     * @param string $option hledana option
     */
    protected final function hasOption(&$c, $option)
    {
        return (isset($c[R::COLUMN_OPTIONS]) && is_array($c[R::COLUMN_OPTIONS]) && in_array($option, $c[R::COLUMN_OPTIONS]));
    }

    protected final function isThisAjaxSelectbox(&$c)
    {
        return (($c[R::COLUMN_TYPE] == R::TYPE_REF || $c[R::COLUMN_TYPE] == R::TYPE_SELECT) && isset($c[R::COLUMN_ATTRIBUTES]) && is_array($c[R::COLUMN_ATTRIBUTES]) && isset($c[R::COLUMN_ATTRIBUTES][R::ATTRIBUTE_SELECTBOX]) && $c[R::COLUMN_ATTRIBUTES][R::ATTRIBUTE_SELECTBOX] == R::ATTRIBUTE_SELECTBOX_AJAX);
    }

    protected final function isThisAjaxSelectboxForFilter(&$c)
    {
        return (($c[R::COLUMN_TYPE] == R::TYPE_REF || $c[R::COLUMN_TYPE] == R::TYPE_SELECT) && isset($c[R::COLUMN_FILTER_ATTRIBUTES]) && is_array($c[R::COLUMN_FILTER_ATTRIBUTES]) && isset($c[R::COLUMN_FILTER_ATTRIBUTES][R::ATTRIBUTE_SELECTBOX]) && $c[R::COLUMN_FILTER_ATTRIBUTES][R::ATTRIBUTE_SELECTBOX] == R::ATTRIBUTE_SELECTBOX_AJAX);
    }

    protected final function isThisEmptyNumber(&$c, &$values, &$n)
    {
        return (($c[R::COLUMN_TYPE] == R::TYPE_INT || $c[R::COLUMN_TYPE] == R::TYPE_FLOAT) && isset($values[$n]) && $values[$n] === "");
    }
}
