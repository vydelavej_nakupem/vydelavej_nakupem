<?php

namespace RTsoft\Component\GridForm;

/**
 * Tohle je zdedena komponenta Nextras Datagrid.
 * Nutne udelat nejake zmeny.
 */
class DataGrid extends \Nextras\Datagrid\Datagrid
{
    public function render()
    {
        // osetreni datepickeru ve filtru, aby to nepadalo
        if (is_array($this->filter))
        {
            foreach($this->filter as &$f)
            {
                if (is_array($f) && isset($f["date"]))
                {
                    $f = $f["date"];
                }
            }
        }
        
        // registrace filtru
        //\RTsoft\Utils\Filters::registerAll($this->template);
        
        parent::render();
    }
    
    public function createComponentForm()
    {
        $form = parent::createComponentForm();
        
        // doplnění tooltipu k tlačítkům "F" a "X"
        if (isset($form["filter"]))
        {
            if (isset($form["filter"]["filter"]))
            {
                $form["filter"]["filter"]->setAttribute("title", "Filtrovat");
            }
            if (isset($form["filter"]["cancel"]))
            {
                $form["filter"]["cancel"]->setAttribute("title", "Zrušit filtr");
            }
        }
        
        return $form;
    }

}
