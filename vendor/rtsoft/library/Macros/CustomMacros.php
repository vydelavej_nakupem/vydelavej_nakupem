<?php

namespace RTsoft\Macros;

use \Latte\MacroNode;
use \Latte\PhpWriter;
use \Latte\CompileException;

/**
 * Vlastní makra
 * 
 * instalace přes konfig
 * nette:
 *    latte:
 *       macros:
 *          - RTsoft\Macros\CustomMacros::install
 *
 * @version 0.1
 * @author Tomas Huda <tomas.huda@rtsoft.cz>
 */
class CustomMacros extends \Latte\Macros\MacroSet
{
    public static function install(\Latte\Compiler $compiler)
    {
        $set = new static($compiler);
        $set->addMacro('linkJs', array($set, 'macroLinkJs'));
        $set->addMacro('linkCss', array($set, 'macroLinkCss'));
        
        return $set;
    }
    
    public function macroLinkJs(MacroNode $node, PhpWriter $writer)
    {
        if ($node->args == "")
        {
            throw new CompileException('Chybný počet parametrů');
        }
        
        $link = 'echo \'<script src="\'.$basePath.\'/\'.stripslashes(%node.word).\'?v=\'.sha1(filemtime(__DIR__ ."/../../../". %node.word)).\'"></script>\'';
        
        return $writer->write($link);
    }
    
    public function macroLinkCss(MacroNode $node, PhpWriter $writer)
    {
        $argsArray = explode(" ", $node->args);

        if (count($argsArray) !== 1 && count($argsArray) !== 2)
        {
            throw new CompileException('Chybný počet parametrů');
        }
        
        $link = 'echo \'<link rel="stylesheet" href="\'.$basePath.\'/\'.stripslashes(%node.word).\'?v=\'.sha1(filemtime(__DIR__ ."/../../../".%node.word)).\'" media="'.((count($argsArray) == 2) ? '\'.stripslashes(%node.args).\'' : 'screen').'">\'';
        return $writer->write($link);
    }
}
