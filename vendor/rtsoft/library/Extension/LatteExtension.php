<?php

namespace RTsoft\Extension;

use IntlDateFormatter as Date;

final class LatteExtension extends \Nette\DI\CompilerExtension
{
    private $filters = array(
        "currency",
        "dateName",
        "dateLocale",
    );

    public function beforeCompile()
    {
        if (!extension_loaded('intl'))
        {
            throw new \Exception("Chybí PHP knihova intl.");
        }

        $latte = $this->getContainerBuilder()->getDefinition('nette.latteFactory');

        foreach($this->filters as $f)
        {
            $latte->addSetup("addFilter", array($f, __CLASS__ . "::" . $f));
        }
    }

    /**
     * Vypíše měnu formátovaně.
     *
     * @param float $value částka
     * @return string
     */
    public static function currency($value, $decimals = 0, $unit = "\xc2\xa0Kč")
    {
        return str_replace(" ", "\xc2\xa0", number_format($value, $decimals, ",", " ")) . $unit;
    }

    /**
     * @param  mixed   $value
     * @param  string  $date  Type of date formatter
     * @param  string  $time  Type of time formatter
     * @param  string  $locale
     * @return string
     */
    public static function dateLocale($value, $format = 'EEEE d. MMMM Y', $locale = 'cs_CZ')
    {
        if (!$value instanceof \Nette\Utils\DateTime)
        {
            $value = new \Nette\Utils\DateTime($value);
        }

        $idf = new Date($locale, Date::FULL, Date::FULL, 'Europe/Prague', Date::GREGORIAN, $format);

        return $idf->format($value);
    }
    
    /**
     * Vrací datum slovně pokud je +1 až -1 den nebo dle zadaného formátu
     * 
     * @param \DateTime $value
     * @param string $format
     * @return string
     */
    public static function dateName($value, $format)
    {
        $today = new \DateTime(); 
        $today->setTime(0, 0, 0);

        $value->setTime(0, 0, 0);

        $diff = $today->diff($value);
        $diffDays = (integer) $diff->format("%R%a");

        switch ($diffDays)
        {
            case 0:
                return "Dnes";
            case -1:
                return "Včera";
            case +1:
                return "Zítra";
            default:
                return $value->format($format);
        }
    }

}
