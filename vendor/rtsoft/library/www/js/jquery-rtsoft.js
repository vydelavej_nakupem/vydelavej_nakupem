// ruzne uzitecne doplnky do jQuery

/**
 * Zabraneni vicenasobnemu odesilani formulare.
 */
jQuery.fn.preventDoubleSubmission = function() 
{
    var last_clicked, time_since_clicked;

    jQuery(this).bind('submit', function(event) 
    {
        if(last_clicked) 
        {
            time_since_clicked = jQuery.now() - last_clicked;
        }

        last_clicked = jQuery.now();

        if(time_since_clicked < 2000) 
        {
            // Blocking form submit because it was too soon after the last submit.
            event.preventDefault();
        }

        return true;
    });
    return this;
};

/**
 * Pomucka na zjisteni, jestli element ma atribut.
 */
$.fn.hasAttr = function(name) 
{  
   return this.attr(name) !== undefined;
};

/**
 * Workaround pro likvidaci nechtenych title u selectpickeru obsahujici 
 * vybrane hodnoty.
 * https://github.com/silviomoreto/bootstrap-select/issues/498
 */
$.fn.selectpickerRemoveTitle = function() 
{
    $(this).next().children().removeAttr('title');
    return this;
};

/**
 * Sbalitelné elementy.
 * Vyžaduje Cookies-js.
 */
$.fn.expandator = function()
{   
    var that = this;
    
    that.each(function()
    {
        var children = $(this).children();
        
        // klic - pouzije se bud id, nebo vnitrni html prvniho ditete
        var k = $(this).attr("id");
        if (!k)
        {
            k = children[0].innerHTML;
        }
        
        //console.log(k);

        // nastaveni stavu rozbaleni/zabaleni po nacteni stranky
        $(this).ready(function ()
        {
            if (k in Cookies.get())
            {
                for (var i = 1; i < children.length; i++)
                {
                    $(children[i]).toggle()
                        .parent().addClass('collapsed');
                }
            }
        });

        // obsluha klikani
        $(children[0]).click(function ()
        {
            for (var i = 1; i < children.length; i++)
            {
                if($(children[i]).is(':visible')) {

                    $(children[i]).parent().addClass('collapsed');
                }
                else {

                    $(children[i]).parent().removeClass('collapsed');
                }

                $(children[i]).toggle();
            }
            if (k in Cookies.get())
            {
                Cookies.remove(k, { path: '' });
            }
            else
            {
                Cookies.set(k, '1', { path: '' });
            }
        });
        
    });
    
    return this;
};

/**
 * Konverze radiolistu vygenerovaneho z nette na buttonsety.
 *
 * Musi se to pustit na div, ve kterem jsou:
 * <label>
 *      <input ...>
 * </label>
 * Pokud tam nette vygeneruje i <div class="radio">, tak ty divy se zlikviuje.
 *
 * Labely a inputy se presunou:
 * <input ...>
 * <label ...>
 * <input ...>
 * <label ...>
 * A pak se pusti konverze na buttonsety.
 *
 * Priklad inicializace:
 * $("input[type='radio'][buttonset]").closest("div.radio").parent().convertRadioListToButtonSet();
 */
$.fn.convertRadioListToButtonSet = function ()
{
    var that = this;

    that.each(function ()
    {
        var buttonset = $(this);

        // napred se musi zmenit <label><input type="radio"></label><br /> na <label></label><input type="radio">
        $("label", buttonset).each(function ()
        {
            // najde se input a label
            var input = $("input", this);
            var label = $(this);

            // normalne radiobuttony vygenerovane z nette zadne id nemaji, musi se nejake sestavit
            var id = input.attr("name") + "_" + input.attr("value");
            input.attr("id", id);
            label.attr("for", id);

            // input i label se hodi na konec divu, ze ktereho se ma stat buttonset
            buttonset.append(input);
            buttonset.append(label);
        });

        // vyhodi nadbytecne prazdne divy s class radio, ktere tam udelalo nette
        $("div.radio", buttonset).remove();
        
        // konverze na buttonset
        buttonset.buttonset();
    });
};
