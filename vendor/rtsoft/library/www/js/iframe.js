var iframe = true;

$(document).ready(function()
{
    var isGrouped = ($("form div.group-default")).length == 0 ? false : true;
    
    // blok s tlacitkem Ulozit premeni na zapati
    var btnSave = $("button[name='save']");

    // Jenze v iframe muze byt pouze grid => tlacitko Ulozit chybi => patch
    if (btnSave.length)
    {
        var divFooter = btnSave.closest("div.form-group");
        divFooter.attr("class", "fancy-footer");
        if (isGrouped)
        {
            $("form").prepend(divFooter);
        }
    }

    // prehazi vsechny formularove radky do contentu
    var fancyContent = $("div.fancy-content");
            
    if (fancyContent.length == 0)
    {
        fancyContent = $("<div class='fancy-content' />");
        
        // pokud formulář má skupiny
        if (isGrouped)
        {
            // elemenety v defaultní skupině
            fancyContent.append($("form div.group-default div.form-group"));
            // fieldsety
            fancyContent.append($("form .group-nondefault"));
        }
        // pokud formulář nemá skupiny
        else
        {
            fancyContent.append($("form div.form-group"));
        }
        $("div.container > form").prepend(fancyContent).removeClass("form-normal");
    }
    
    // errory da na konec fancy contentu
    $("ul.error").prependTo(fancyContent);
    
    // Vyrobi cancel button x tlacitko Ulozit nemusi existovat => patch
    if (btnSave.length)
    {    
        var btnCancel = btnSave.clone();
        btnCancel.removeClass("btn-primary").addClass("btn-warning");
        btnCancel.attr("type", "button").attr("title", "Zavřít bez uložení");
        btnCancel.attr("name", "cancel");
        if (btnCancel.hasAttr("id"))
        {
            btnCancel.attr("id", btnCancel.attr("id").replace("save", "cancel"));
        }
        btnCancel.val("Zrušit");
        btnCancel.html(btnCancel.html().replace("Uložit", "Storno"));
        $("span", btnCancel).removeClass("glyphicon-ok").addClass("glyphicon-off");
        btnCancel.click(function()
        {
            parent.$.fancybox.close();
            return false;
        });
        btnCancel.insertBefore(btnSave);
    }
});
