/**
 * Obnoví obsah datagridu.
 *
 * Pokud byl zadan nazev gridu, refreshne se pouze ten jeden grid.
 *
 * Pokud nebyl zadan nazev gridu a
 * a) nebyl zaznamenan element, na ktery uzivatel naposled kliknul, najde uplne vsechny
 *    gridy na strance a zavola signal, ktery zajisti refreshnuti,
 * b) byl zaznamenan element, na ktery uzivatel naposled kliknul, najde ten jeden grid, ve kterem je ten element a
 *    ten grid refreshne.
 */
function refreshGrid(name)
{
    var divGrid;
    var selector;

    if (typeof name === "undefined" || name === null || name === "")
    {
        if (lastClickedElementInGrid)
        {
            divGrid = lastClickedElementInGrid.closest("div.grid");
        }

        if (!divGrid || !divGrid.length)
        {
            selector = "div.grid[data-grid-name]"; // vsechny gridy
        }
    }
    else
    {
        selector = "div.grid[data-grid-name='" + name + "']";
    }

    if (selector)
    {
        divGrid = $(selector);

        //console.log("refreshGrid selector"); console.log(selector);
    }

    //console.log("refreshGrid divGrid"); console.log(divGrid);

    divGrid.each(function()
    {
        // priprava - nalezeni potrebnych veci v gridu
        var urlSignalGridSort = $("a.link-sort", this).attr("href");
        var urlSignalGridPaginate = $("a.link-paginate", this).attr("href");
        var divWithPagination = $("ul.pagination", this);

        // vybrani signalu pro refresh gridu
        // - kdyz je v UL se strankovanim vic nez 3 LI (tzn. vic nez 'Predchozi', '1', 'Dalsi'), tak existuje vice
        //   stranek nez jen 1 a musi se volat signal se strankovanim, protoze pokud by se zavolal signal s razenim,
        //   uzivatel by se vratil na prvni stranku a to nechceme
        // - pokud ma grid jen jednu stranku, musi se zavolat grid s razenim, protoze ten se strankovanim nefunguje
        var urlRefresh = urlSignalGridSort;
        if ($("li", divWithPagination).length > 3)
        {
            urlRefresh = urlSignalGridPaginate;
        }

        //console.log("refreshGrid urlRefresh"); console.log(urlRefresh);

        // refresh
        $.nette.ajax(
        {
            url: urlRefresh,
            data: { },
            async: (divGrid.length == 1) // kdyz se najednou refreshuje vic gridu, nefunguje najednou asynchronni refreshnuti, proto se musi delat jedno za druhym synchronne
        });
    });
}

var lastClickedElementInGrid = null;

/**
 * Pamatovani, na co uzivatel v gridu kliknul naposled.
 * Volani funkce je pro kazdy grid v GridComponent_def.latte.
 */
function rememberLastClickedElementInGrid(grid)
{
    $("table tbody a", grid).click(function()
    {
        lastClickedElementInGrid = $(this);

        //console.log("rememberLastClickedElementInGrid"); console.log(lastClickedElementInGrid);

        return (lastClickedElementInGrid.attr("href") !== "#");
    });
}

/**
 * Do zadaneho gridu prida text, ze nebyl nalezen zadny zaznam, pokud tam zadny radek neni.
 * Volani funkce je pro kazdy grid v GridComponent_def.latte.
 */
function addTextToGridWithoutRows(grid)
{
    var columnsCount = $("form > table > thead tr.grid-columns > th", grid).length;

    if (columnsCount)
    {
        $("form > table > tbody:not(:has(*))", grid).html("<tr><td colspan=\"" + columnsCount.toString() + "\">Nebyl nalezen žádný záznam.</td></tr>")
    }
}

/**
 * Pokud některé políčko má atribut tooltip="...", přesune ten text do title u
 * tagu, který představuje buňku filtru a také na buňku s nadpisem sloupce.
 * Volání funkce je pro každý grid v GridComponent_def.latte.
 */
function setTooltipForFilterItems(grid)
{
    // projedou se všechna políčka ve filtru
    $("tr.grid-filters input[tooltip], tr.grid-filters select[tooltip]", grid).each(function()
    {
        // title se dá na buňku s filtrovacím políčkem
        th = $(this).closest("th");
        th.attr("title", $(this).attr("tooltip"));
        
        // a také na buňku s nadpisem sloupce
        th.parent().prev().children("." + th.attr("class")).attr("title", $(this).attr("tooltip"));
    });
}


/**
 * Prehazeni filtru z theadu pryc nad tabulku.
 */
function moveFilterAboveGrid(grid)
{
    if ($("table thead tr.grid-filters th", grid).length)
    {
       var tableFilters = $("<table class='table-filters'></table>");

        $("table thead tr.grid-filters th", grid).each(function()
        {
            var trToFilter = $("<tr />");

            var inputFilter = $("input,select", $(this));
            var labelFilter = (inputFilter.length ? inputFilter.attr("title") : "");

            trToFilter.append("<th>" + (labelFilter != undefined ? labelFilter : "")  + "</th>");
            trToFilter.append($(this));

            tableFilters.append(trToFilter);
        });

        $("table.list", grid).before(tableFilters);
        $("table thead tr.grid-filters", grid).remove();

        $("input[type='submit']", tableFilters).addClass("button");
    }
}

/**
 * Vylepseni filtru o tlacitko s moznosti zadat cislo stranky do textboxu.
 * Volani funkce je pro kazdy grid v GridComponent_def.latte.
 */
function improveGridPagination(grid)
{
    var pagination = $("ul.pagination", grid);
    var btn = $('<ul class="pagination"><li><a href="#" class="btn btn-default page-entering" onclick="askForGridPageNumber(this); return false;"><span class="glyphicon glyphicon-pencil"></span> </a></li></ul>');
    pagination.before(btn);
}

/**
 * Nahradi tlacitko s tuzkou textboxem, napise do nej aktualni cislo stranky a nastavi udalosti na textbox.
 */
function askForGridPageNumber(a)
{
    // textbox ktery docasne nahradi tlacitko s tuzkou
    var tbx = $('<input type="text" class="form-control" onfocus="this.select();" onmouseup="return false;" />');
    
    // zjisti aktualni cislo stranky a da ho do textboxu
    var actualPage = $("li.active a", $(a).closest("tfoot")).html();
    tbx.val(actualPage);

    // obsluha psani
    tbx.keypress(function(event)
    {
        //alert(event.keyCode);
        
        if (event.keyCode == 27) // escape
        {
            hideTextboxWithGridPageNumber(this);
            event.preventDefault();
        }
        
        if (event.which == 13) // enter
        {
            var linkPaginate = $("a.link-paginate-123", $(a).closest("div.grid"));
            linkPaginate.attr("href", linkPaginate.attr("href").replace("=123", "=" + tbx.val()));
            //console.log(linkPaginate);
            linkPaginate.get(0).click();
            event.preventDefault();
        }
    });
    
    tbx.blur(function()
    {
        hideTextboxWithGridPageNumber(this);
    });
    
    // schova tuzku a vlozi textbox a da se na nej focus
    $(a).hide().after(tbx);
    tbx.get(0).focus();
}

/**
 * Schova textbox s cislem stranky a vrati tam tlacitko s tuzkou.
 */
function hideTextboxWithGridPageNumber(tbx)
{
    $(tbx).prev().show();
    tbx.remove();
}