// RTsoft konvence
var FALSE = false;
var TRUE = true;
var NULL = null;

// priznak defaultne nejsme v iframu
var iframe = false;

$(document).ready(function() 
{
    // aby obsah stranky byl vzdycky pod zahlavim, ktere muze byt ruzne vysoke
    if ($("nav").hasClass("navbar-fixed-top"))
    {
        $(window).resize(changeBodyPadding);
        changeBodyPadding();
    }
    
    // kdyz vyprsi sesna, login screen by se objevil ve fancyboxu, tohle zajisti presmerovani
    if (window != window.top && window.location.pathname.indexOf("sign/in") > -1)
    {
        self.parent.location = window.location;
    }
    
    // uprava checkboxu v nette formularich
    if (typeof adjustCheckboxes !== "undefined") adjustCheckboxes();
    
    // tooltipy
    if (typeof setTooltipForFormItems !== "undefined") setTooltipForFormItems();
    
    // pridani datepickeru
    initDateInput();
    
    // predelani selectboxu
    initSelectBoxes();
    
    // inicializace ajaxovych naseptavacu (pouze pokud existuje promenna s url na akci, kterou muzou naseptavace ziskavat data)
    if (typeof urlFetchItemsForWhisperer !== "undefined")
    {
        setTimeout(initAjaxSelectboxes, 500);
    }

    // init buttonsetu
    initButtonsets();
    
    // ajax akce u vseho, co ma class ajax
    $.nette.init();
    
    // otvirani ve fancyboxech
    openingInFancyboxes();
});

/**
 * Upravuje padding-top obsahu stranky v zavislosti na vysce zahlavi (nav).
 */
function changeBodyPadding()
{
    var headerHeight = $("nav").height(); 
    
    if (headerHeight)
    {
        $("body").css("padding-top", (headerHeight + 1).toString() + "px");
    }
    
    var footerHeight = $("footer").height();
    
    if (footerHeight)
    {
        $("body").css("padding-bottom", footerHeight.toString() + "px");
    }
}

/**
 * Univerzální funkce na vyprázdnění položek filtru.
 * Textboxy - vyprázdní
 * Checkboxy - nastaví nezaškrtnuto
 * Selectboxy - vybere první položku (obvykle to je ta prompt položka)
 * @param filterArea Kde mazat.
 */
function clearFilter(filterArea)
{
    $('input[type=text]', filterArea).val(''); 
    $('input[type=checkbox]', filterArea).prop('checked', false); 
    $('select', filterArea).val('');
}

/**
 * Inicializace buttonsetů.
 * Překonvertuje ty radiobuttony, které mají atribut 'buttonset'.
 */
function initButtonsets()
{
    $("form input[type='radio'][buttonset]").closest("div.radio").parent().convertRadioListToButtonSet();
}

/**
 * Inicializuje selectboxy.
 *
 * Bez zadání selektoru mění defaultně všechny selcetboxy, které nemají:
 * - class normal-selectbox
 * - atribut selectbox='normal'
 *
 * Má v sobě objekt s defaultními parametry, které se předávají tomu pluginu bootstrap-selectpicker.
 * Ty jsou v objektu selectPickerParams, který vznikne pomocí $.extend, jenž merguje ten objekt uvnitř s
 * objektem selectPickerOptions, pokud existuje.
 * Programátor v projektu může nastavit do 'selectPickerOptions' ty options, které chce mít jinak.
 */
function initSelectBoxes(selector)
{
    var selectorDefault = 'select:not(.normal-selectbox):not([selectbox=normal])';
    
    if (typeof selector == "undefined")
    {
        selector = selectorDefault;
    }
    
    $selectboxes = $(selector).filter(selectorDefault + ":visible");
    
    $selectboxes.attr("title", " ");

    var selectPickerParams = $.extend(
    {
        liveSearch: TRUE,
        liveSearchNormalize: true,
        selectedTextFormat: 'values',
        size: 10,
    }, (typeof selectPickerOptions !== "undefined" ? selectPickerOptions : {}));

    $sel = $selectboxes.selectpicker(selectPickerParams);
    
    // likvidace nechtenych title obsahujici vybrane hodnoty
    // workaround https://github.com/silviomoreto/bootstrap-select/issues/498
    $sel.selectpickerRemoveTitle();
    $sel.change($.fn.selectpickerRemoveTitle);
}

/**
 * Inicializuje ajaxové našeptávače.
 *
 * Má v sobě objekt s defaultními parametry, které se předávají tomu pluginu ajaxSelectPicker.
 * Ty jsou v objektu ajaxSelectPickerParams, který vznikne pomocí $.extend, jenž merguje ten objekt uvnitř s
 * objektem ajaxSelectPickerOptions, pokud existuje.
 * Programátor v projektu může nastavit do 'ajaxSelectPickerOptions' ty options, které chce mít jinak.
 *
 * Pluginu ajaxSelectPicker bohužel chybí možnost nastavit, od kolika znaků má vyhledávat, takže to je implementováno
 * přímo zde ve funkci beforeSend. Defaultně se vyhledává už od prvního písmena, ale programátor to může změnit:
 * var ajaxSelectPickerOptions = { minQueryLength: 0 }; // bez omezení
 * var ajaxSelectPickerOptions = { minQueryLength: 5 }; // vyhledávat až od 5 znaků
 */
function initAjaxSelectboxes()
{
    // defaultní nastavení ajaxového našeptávače
    var ajaxSelectPickerParams = $.extend(
    {
       ajax:
       {
           url: urlFetchItemsForWhisperer,
           beforeSend: function (xhr, opts)
           {
               var minQueryLength = ((typeof ajaxSelectPickerOptions !== "undefined" && typeof ajaxSelectPickerOptions.minQueryLength !== "undefined") ? ajaxSelectPickerOptions.minQueryLength : 0);

               if (minQueryLength && this.options.data.q.length < minQueryLength)
               {
                   xhr.abort(); // zrušení následného ajax požadavku
                   $("div.status", $("select[name='" + this.options.data.name + "']").parent()).hide(); // schovani divu s textem "Načítám"
               }
           },
       },
       langCode: 'cs-CZ',
       preserveSelected: false,

    }, (typeof ajaxSelectPickerOptions !== "undefined" ? ajaxSelectPickerOptions : {}));

    // inicializace naseptavace
    $("select[selectbox='ajax']").each(function()
    {
        var name = $(this).attr("name");

        ajaxSelectPickerParams.ajax.data = function(a)
        {
            var params =  { q: '{'+'{'+'{'+'q}}}', name: name };
            return params;
        };
        
        $(this).ajaxSelectPicker(ajaxSelectPickerParams);
    });
}

/**
 * Inicializuje date pickery (Vodacek).
 */
function initDateInput(what)
{
    if (typeof what == "undefined")
    {
        what = 'input[data-dateinput-type]';
    }
    
    $(what).addClass("form-control text").dateinput(
    {
        datetime: 
        {
            dateFormat: 'd.m.yy',
            timeFormat: 'H:mm', // 'H:mm:ss'
            options: 
            { // nastavení datepickeru pro konkrétní typ
                changeYear: true
            }
        },
        'datetime-local': 
        {
            dateFormat: 'd.m.yy',
            timeFormat: 'H:mm' // 'H:mm:ss'
        },
        date: 
        {
            dateFormat: 'd.m.yy'
        },
        month: 
        {
            dateFormat: 'MM yy'
        },
        week: 
        {
            dateFormat: "w. 'týden' yy"
        },
        time: 
        {
            timeFormat: 'H:mm' // 'H:mm:ss'
        },
        options: 
        {   // globální nastavení datepickeru
            closeText: "Zavřít",
            selectOtherMonths: true,
            showSecond: false,
            showMillisec: false,
            showMicrosec: false,
            showTimezone: false
        }
    });
    
    if(navigator.appVersion.indexOf("MSIE 7.0") != -1)
    { 
        $("input[type='text'][name='filter_from'], input[type='text'][name='filter_to']").css("display", "none");
    }
}

var lastObjectUsedForOpeningFancybox = NULL;

/**
 * Otevirani stranek ve fancyboxech.
 * Pokud ma nejaky odkaz nebo button tridu 'open-in-fancybox', zajisti otevreni ciloveho odkazu ve fancyboxu.
 * Ta stranka, ktera se takto otevre, musi byt na zobrazeni ve fancyboxu resp. iframe uzpusobena (napriklad by
 * nemela zobrazovat zahlavi a zapati).
 */
function openingInFancyboxes(context, fancyBoxWidth)
{
    // otvírání do fancyboxů
    if ($.fancybox)
    {
        context = (typeof context !== "undefined" ? context : null);
        fancyBoxWidth = (typeof fancyBoxWidth !== "undefined" ? fancyBoxWidth : 800);

        var fb = $("a.open-in-fancybox, button.open-in-fancybox", context);

        // otevreni fancyboxu
        fb.fancybox(
        {
            type: "iframe",
            //width: '800',
            width: fancyBoxWidth,
            minHeight: 400,
            padding: 0,
            closeBtn: false,
            helpers:
            {
                title: null, // aby nebyl pod fancyboxem titulek s textem, ktery byl v title
                overlay:
                {
                    closeClick: false // aby nesel fancybox zavrit kliknutim mimo fancybox
                }
            },
            /*afterShow: function()
            {
                var height = $('.fancybox-inner').height() + 45; // pricteni vysky zapati
                var iframe = $('.fancybox-inner iframe').contents();
                iframe.find('.fancy-content').height(height - 150);
            }*/
            beforeLoad: function()
            {
                // do globalni promenne se zaznamena posledni objekt, pres ktery se kliklo, aby se otevrel fancybox
                lastObjectUsedForOpeningFancybox = $(this.element);

                //console.log("lastObjectUsedForOpeningFancybox");console.log(this.element);
            }
        });
    }
}


var sdiak="áäčďéěíĺľňóôőöŕšťúůűüýřžÁÄČĎÉĚÍĹĽŇÓÔŐÖŔŠŤÚŮŰÜÝŘŽ";
var bdiak = "aacdeeillnoooorstuuuuyrzAACDEEILLNOOOORSTUUUUYRZ";

/**
 * Odstraneni diakritiky v retezci.
 * @author http://www.poeta.cz/ms/priklad57.php
 * @param string txt
 * @returns string
 */
function bezdiak(txt)
{
    tx = "";
    
    for (p = 0; p < txt.length; p++)
    {
        if (sdiak.indexOf(txt.charAt(p)) != -1)
        {
            tx += bdiak.charAt(sdiak.indexOf(txt.charAt(p)));
        }
        else
        {
            tx += txt.charAt(p);
        }
    }
    
    return tx;
}
