/**
 * Workaround pro checkboxy - Nette totiz pri pouziti $form->addCheckbox()
 * generuje label vpravo od checkboxu, bunka <th> pak je vlevo prazdna a
 * formular vypada divne, tenhle kod prehodi label do leve bunky.
 * Uprava pro bootstrap.
 */
function adjustCheckboxes()
{
    $("form input[type='checkbox']").each(function() 
    {
        var checkbox = $(this);

        // checkboxy, ktere jsou v checkboxlistech, se neupravuji, nebot ty maji sve labely u sebe spravne
        if (checkbox.attr("name") && checkbox.attr("name").indexOf('[') != -1 || (checkbox.attr("class") && checkbox.attr("class").indexOf('no_change') != -1))
        {
            return true; // continue
        }

        var label = checkbox.parent();
        var td = label.parent().parent();
        var th = td.prev();

        if (th.length && th.get(0).tagName == "DIV")
        {
              checkbox.appendTo(td);   // checkbox presune z labelu primo do bunky
              label.appendTo(th);      // samotny label da do bunky vlevo
              checkbox.prev().remove(); // smaze zbytecny div class checkbox
        }
    });
}

/**
 * Pokud některé políčko má atribut tooltip="...", přesune ten text do title u
 * tagu, který představuje celý řádek formuláře, což u bootstrap formů bývá
 * div.form-group, čímž udělá bublinovou nápovědu pro celou položku formuláře.
 */
function setTooltipForFormItems(rowSelector)
{
    rowSelector = typeof rowSelector !== "undefined" ? rowSelector : "div.form-group";
    
    $("form[role='form'] input[tooltip], form[role='form'] select[tooltip], form[role='form'] textarea[tooltip]").each(function()
    {
        $(this).closest(rowSelector).attr("title", $(this).attr("tooltip"));
    });
}

/**
 * Uprava tri radku s predcislim, cislem uctu a kodem banky:
 * - prehazi je na jeden radek
 * - radek pro predcisli a banku zrusi
 * - da mezi textboxy oddelovace
 * - pokud textbox obsahuje nulu, smaze se jeho obsah
 * 
 * TODO: Hnus, udelat vlastni formularovy prvek misto teto JS hruzy!
 */
function adjustAccountNumber(n)
{
   n = typeof n !== 'undefined' ? n : "";
    
   var account_prefix = $("input[name='account_prefix" + n + "']");
   var account_number = $("input[name='account_number" + n + "']");
   var account_bank = $("input[name='account_bank" + n + "'], select[name='account_bank" + n + "']");
   
   var tr_account_prefix = account_prefix.parent();
   var td_account_number = account_number.parent();
   var tr_account_bank = account_bank.parent().parent();
   
   account_prefix.prependTo(td_account_number);
   account_bank.appendTo(td_account_number);
   
   account_prefix.addClass("account_prefix").attr("maxlength", 6).after(" - ");
   account_number.addClass("account_number").attr("maxlength", 10).after(" / ");
   account_bank.addClass("account_bank").attr("maxlength", 4);
   
   tr_account_prefix.parent().remove();
   tr_account_bank.remove();
   
   if (account_prefix.val() == "0")
   {
       account_prefix.val("");
   }
   if (account_number.val() == "0")
   {
       account_number.val("");
   }
   if (account_bank.val() == "0")
   {
       account_bank.val("");
   }
}

/**
 * Přesun předvolby před telefon.
 * 
 * TODO: Hnus, udelat vlastni formularovy prvek misto teto JS hruzy!
 */
function movePhonePre()
{ 
    var phonePre = $("select[name='phone_pre']");
    var trPhonePre = phonePre.closest("div.form-group");
    var phone = $("input[name='phone']");
    phonePre.insertBefore(phone);
    phonePre.css("width", "25%").css("float", "left");
    phonePre.after("<span style='width: 3%; display: inline-block; text-align: center;'>/</span>");
    phone.css("width", "72%").css("display", "inline-block");
    phone.closest("div").css("white-space", "nowrap");
    trPhonePre.remove();
}