<?php

namespace App\Model;

use RTsoft\Model\GridFormRepository as R;

class BaseRepository extends R
{
    const DB_COL_URL = 'url';
    
    /**
     * Převede pole či objekt na pole se sloupci, které se dají použít v DB
     * @param mixed $data
     * @return array
     */
    public function convertToDBArray($data)
    {
        $result = [];
        
        foreach ($data as $key => $value)
        {
            if ($value instanceof \SimpleXMLElement)
            {
                $value = (string) $data->$key;
            }
            
            $fixedKey = str_replace('-', '_', $key);
            $result[$fixedKey] = $value;
        }
        
        
        return $result;
    }
    
    public function saveItems($items)
    {
        foreach ($items as $item)
        {
            try
            {
                $this->saveFiltered($item);
            } catch (\Nette\Database\UniqueConstraintViolationException  $ex) {
                throw $ex;
            }
        }
    }
    
    /**
     * Vrací URL podle ID
     * @param int $id
     * @return string
     */
    public function getUrlById($id)
    {
        $row = $this->findRow($id);
        
        if (!$row)
        {
            return '';
        }
        
        return $row->url;
    }
    
    /**
     * Vrací ID podle URL
     * @param string $url
     * @return int|NULL
     */
    public function getIdByUrl($url)
    {
        $by = [
          self::DB_COL_URL => $url
        ];
        
        $row = $this->findBy($by)->fetch();
        
        if (!$row)
        {
            return NULL;
        }
        
        return $row->id;
    }
    
}
