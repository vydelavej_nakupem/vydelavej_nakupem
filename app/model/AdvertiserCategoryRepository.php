<?php

namespace App\Model;

class AdvertiserCategoryRepository extends BaseRepository
{
    const TABLE_NAME = 'advertiser_category';
    
    protected $tableName = self::TABLE_NAME;
    
    const DB_COL_ADVERTISER_ID  = 'advertiser_id';
    const DB_COL_CATEGORY_ID    = 'category_id';
    
    const STORE_IN_MENU_LIMIT = 6;
    
    public function saveAdvertiserCategories($storeId, $categories)
    {
        $this->findBy(array(self::DB_COL_ADVERTISER_ID => $storeId))->delete();
        $data = array();
        
        foreach ($categories as $category)
        {
            $pair = array(
                
                self::DB_COL_ADVERTISER_ID  => $storeId,
                self::DB_COL_CATEGORY_ID    => $category
            );
            
            $data[] = $pair;
        }
        
        if ($data)
        {
            $this->context->queryArgs("INSERT INTO {$this->tableName} ", array($data));
        }
    }
    
    public function getAdvertiserCategories($storeId)
    {
        $rows = $this->findBy(array(self::DB_COL_ADVERTISER_ID => $storeId))->
                fetchPairs(self::DB_COL_CATEGORY_ID, self::DB_COL_CATEGORY_ID);
        
        return array_keys($rows);
    }
    
    public function getTopAdvertisersForCategories($limit = self::STORE_IN_MENU_LIMIT)
    {
        $query = 'SELECT a.id, a.advertiser_name, a.advertiser_alias, a.logo, c.title, c.id AS categoryId FROM `advertiser` a INNER JOIN advertiser_category ac ON ac.advertiser_id = a.id INNER JOIN category c ON c.id = ac.category_id WHERE c.parent_id IS NULL AND c.active = 1 AND a.show_in_menu = 1';
        
        $items =  $this->context->query($query)->fetchAll();
        
        $res = [];
        
        foreach ($items as $item)
        {
            if (!isset($res[$item->categoryId]) || count($res[$item->categoryId]) <= $limit)
            {
                $res[$item->categoryId][] = $item;
            }
        }
        
        return $res;
    }
}