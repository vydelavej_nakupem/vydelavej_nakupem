<?php

namespace App\Model;

use RTsoft\Model\GridFormRepository as R;
use App\Enum\ECommissionStatus;
use App\Enum\EUserAccountType;

class UserAccountRepository extends R
{
    const TABLE_NAME = "user_account";
    
    const DB_COL_COMMISSION_ID      = 'commission_log_id';
    const DB_COL_COMMISSION_VALUE   = 'commission_value';
    const DB_COL_CREATED            = 'created';
    const DB_COL_USER_ID            = 'user_id';
    const DB_COL_UPDATED            = 'updated';
    const DB_COL_CONFIRMED          = 'confirmed';
    const DB_COL_TYPE               = 'type';
    
    public $columns = array(

        self::DB_COL_USER_ID          =>
        
        array(self::COLUMN_TITLE      => "Uživatel"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ),
        
        self::DB_COL_COMMISSION_VALUE   =>
        
        array(self::COLUMN_TITLE      => "Hodnota"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ),
        
        self::DB_COL_CREATED            =>
        
        array(self::COLUMN_TITLE      => "Vytvořeno"
             ,self::COLUMN_TYPE       => self::TYPE_DATETIME
             ),
        
        self::DB_COL_UPDATED            =>
            
        array(self::COLUMN_TITLE      => "Upraveno"
             ,self::COLUMN_TYPE       => self::TYPE_DATETIME
             ),
        
        self::DB_COL_CONFIRMED          =>
            
        array(self::COLUMN_TITLE      => "Potvrzeno"
             ,self::COLUMN_TYPE       => self::TYPE_BOOL
             ),
        
        self::DB_COL_TYPE               =>
            
        array(self::COLUMN_TITLE      => "Typ"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
    );
        
    protected $tableName = self::TABLE_NAME;
    
    public function getCurrentState($userId)
    {
        return $this->findAll()->
                where("user_id = ?", $userId)->
                sum('commission_value');
    }
    
    public function getLastReward($userId, $minus = false)
    {
        return $this->getLastRewards($userId, $minus)->limit(1);
    }
    
    public function getLastRewards($userId, $minus = false)
    {
        return $this->findAll()->
                where("user_id = ?", $userId)->
                where(self::DB_COL_COMMISSION_VALUE . ($minus ? "<" : ">") . " ?", 0)->
                order(self::DB_COL_CREATED." DESC");
    }
    
    public function getLastPositiveReward($userId)
    {
        return $this->getLastReward($userId)->fetch();
    }
    
    public function getPayoutReward($userId)
    {
        return $this->getLastReward($userId, true)->where(self::DB_COL_COMMISSION_ID, NULL)->fetch();
    }
    
    public function getForpaycheckReward($userId)
    {
        $commissionLogTable = CommissionLogRepository::TABLE_NAME; 
       
        return $this->findAll()->
                where("user_id = ?", $userId)->
                where("$commissionLogTable.action_status = ? OR commission_log_id IS NULL", ECommissionStatus::STATUS_CLOSED)->
                sum("commission_value");
    }
    
    public function createPayOutRequest($userId, $payLimit)
    {
        $forPaycheck = $this->getForpaycheckReward($userId);
        
        if($forPaycheck >= $payLimit)
        {
            $sqlNow = new \Nette\Database\SqlLiteral('NOW()');
            
            $this->insert([
                self::DB_COL_USER_ID            => $userId,
                self::DB_COL_COMMISSION_VALUE   => -1 * $forPaycheck,
                self::DB_COL_CREATED            => $sqlNow,
                self::DB_COL_UPDATED            => $sqlNow,
                self::DB_COL_CONFIRMED          => 0,
                self::DB_COL_TYPE               => EUserAccountType::PAYOUT,
            ]);
            
            return true;
        }
        return false;
    }
    
    public function getSavedMoney()
    {
        return $this->findAll()->sum(self::DB_COL_COMMISSION_VALUE);
    }
    
        
    /**
     * Vrací provize, které jsou větší než x pro výpis na frontendu
     * 
     * 
     * @return \Nette\Database\Table\Selection
     */
    public function getCommissionsForListFrontend()
    {
        $by = [
            self::DB_COL_COMMISSION_VALUE .' > ?' => 0,
            self::DB_COL_TYPE .' = ?' => EUserAccountType::PAYMENT,
        ];
        
        return $this->findBy($by)->order('id DESC')->limit(10);
    }
}
