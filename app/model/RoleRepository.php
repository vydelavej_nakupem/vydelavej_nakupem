<?php

namespace App\Model;

use Nette\Application\UI\Form;
use RTsoft\Model\GridFormRepository as R;

class RoleRepository extends R
{
    /** Výchozí role pro nového uživatele. */
    public static $defaultRolesForNewUser = array(2);
    
    /** Nazev tabulky */
    protected $tableName = 'role';
    
    public $columns = array(

        "name" =>
        
        array(self::COLUMN_TITLE      => "Systémový název"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_FORM, self::OPTION_NOT_IN_GRID)
             ),
      
        "title" =>
        
        array(self::COLUMN_TITLE      => "Název role"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array()
             ),
      
        "users" =>
        
        array(self::COLUMN_TITLE       => "Uživatelé v roli"
             ,self::COLUMN_TYPE        => self::TYPE_TEXT
             ,self::COLUMN_OPTIONS     => array(self::OPTION_NOT_IN_FORM, self::OPTION_SORTING_DISABLED)
             ,self::COLUMN_FILTER_TYPE => self::FILTER_TYPE_NONE
             ),
      
        "active" =>
        
        array(self::COLUMN_TITLE      => "Aktivní"
             ,self::COLUMN_TYPE       => self::TYPE_BOOL
             ,self::COLUMN_DEFAULT    => TRUE
             ,self::COLUMN_FILTER_DEFAULT => self::BOOL_POSITIVE
             )
    );
    
    public function findByNameOrTitle($name, $title)
    {
        $a = array();
        
        if (!empty($name) && !empty($title))
        {
            $a["name = ? OR title = ?"] = array($name, $title);
        }
        else if (!empty($name))
        {
            $a["name"] = $name;
        }
        else if (!empty($title))
        {
            $a["title"] = $title;
        }
        
        return $this->findBy($a)->fetch();
    }
    
    /**
     * Vraci polozky pro selectbox.
     * @param array $options
     * @return array
     */
    public function fetchItemsForSelectbox()
    {
        $rows = $this->findAll();
        
        $rows->order("role.active DESC, title ASC");
        
        $data = array();
        
        foreach($rows as $r)
        {
            $data[$r["id"]] = $r["title"] . (!$r->active ? " " . self::$textAddonForInactiveOption : "");
        }
        
        return $data;
    }
}
