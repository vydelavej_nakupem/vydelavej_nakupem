<?php

namespace App\Model;

class UserRoleRepository extends \RTsoft\Model\Repository
{
    protected $tableName = 'user_role';
    
    public function findRolesForUser($userId, $getName = TRUE, $onlyActive = TRUE)
    {
        $a = array("user_id" => $userId);
        
        if ($onlyActive)
        {
            $a["role.active"] = TRUE;
        }
        
        $rows = $this->findBy($a);
        
        $roles = array();
        
        foreach($rows as $r)
        {
            $roles[] = ($getName ? $r->role->name : $r->role_id);
        }
        
        return $roles;
    }
    
    public function findUsersForRole($roleId, $getName = TRUE, $onlyActive = TRUE)
    {
        $a = array("role_id" => $roleId);
        
        if ($onlyActive)
        {
            $a["user.active"] = TRUE;
        }
        
        $rows = $this->findBy($a);
        
        $users = array();
        
        foreach($rows as $r)
        {
            $users[] = ($getName ? $r->user->login : $r->user_id);
        }
        
        return $users;
    }
    
    public function saveRolesForUser($roles, $userId)
    {
        $this->findBy(array("user_id" => $userId))->delete();
        
        if (is_array($roles))
        {
            foreach($roles as $roleId)
            {
                $this->insert(array("user_id" => $userId, "role_id" => $roleId));
            }
        }
    }
    
    public function saveUsersForRole($users, $roleId)
    {
        $this->findBy(array("role_id" => $roleId))->delete();
        
        if (is_array($users))
        {
            foreach($users as $userId)
            {
                $this->insert(array("user_id" => $userId, "role_id" => $roleId));
            }
        }
    }
}

