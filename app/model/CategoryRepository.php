<?php

namespace App\Model;

use Nette\Application\UI\Form;

class CategoryRepository extends BaseRepository
{
    protected $tableName = 'category';
     
    const DB_COL_PRIMARY        = 'id';
    const DB_COL_TITLE          = 'title';
    const DB_COL_URL            = 'url';
    const DB_COL_DESC           = 'desc';
    const DB_COL_ACTIVE         = 'active';
    const DB_COL_PARENT_ID      = 'parent_id';

    public $columns = array(
        
        self::DB_COL_TITLE   =>
        
        array(self::COLUMN_TITLE      => "Název kategorie"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ),
        
        self::DB_COL_URL   =>
        
        array(self::COLUMN_TITLE      => "URL"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ),
        
        self::DB_COL_DESC   =>
        
        array(self::COLUMN_TITLE      => "Popis kategorie"
             ,self::COLUMN_TYPE       => self::TYPE_TEXTAREA
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID, self::OPTION_CKEDITOR)
             ),
        
        self::DB_COL_PARENT_ID =>
        
        array(self::COLUMN_TITLE      => "Nadřazená kategorie"
             ,self::COLUMN_TYPE       => self::TYPE_REF
             ),
        
        self::DB_COL_ACTIVE   =>
        
        array(self::COLUMN_TITLE      => "Aktivní"
             ,self::COLUMN_TYPE       => self::TYPE_BOOL
             ),
    );
    
    public function getCategoriesForSelectBox($currentId = null)
    {
        $rows = $this->findAll();
        
        if($currentId)
        {
            $rows->where("id != ?", $currentId);
        }
        $rows->where(self::DB_COL_ACTIVE . " = ?", 1);
        
        return $rows->fetchPairs(self::DB_COL_PRIMARY, self::DB_COL_TITLE);
    }
    
    public function filterRows($rows, array $options = null)
    {
        if(!empty($options[self::DB_COL_PARENT_ID]))
        {
            $parameter = $options[self::DB_COL_PARENT_ID];
            
            $rows->where(self::DB_COL_PARENT_ID . " IN (SELECT id FROM {$this->tableName} WHERE " . self::DB_COL_TITLE . " LIKE ?)", "%{$parameter}%");
            
            unset($options[self::DB_COL_PARENT_ID]);
        }
        
        parent::filterRows($rows, $options);
    }

    public function getAllActiveCategories()
    {
        $by = [
          self::DB_COL_ACTIVE => 1  
        ];
        
        return $this->findBy($by);
    }
    
    public function getMenuArray()
    {
        $categories = $this->getAllActiveCategories()->fetchAll();
        
        $menuArray = [];
        
        foreach ($categories as $category)
        {
            $menuArray[$category->{self::DB_COL_PARENT_ID}][] = $category;
        }
        
        return $menuArray;
    }


    public function getActiveCategory($id)
    {
        $by = [
          self::DB_COL_PRIMARY => $id  
        ];
        
        return $this->findBy($by)->fetch();
    }
    
    public function getAllActiveMainCategories()
    {
        return $this->getAllActiveCategories()->where(self::DB_COL_PARENT_ID, NULL);
    }
}
