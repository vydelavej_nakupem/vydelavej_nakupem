<?php

namespace App\Model;

use Nette\Application\UI\Form;
use App\Model\AdvertiserCategoryRepository AS ACR;
use App\Enum\EAdvertAccountStatus;

class AdvertisersRepository extends BaseRepository
{

    protected $tableName = "advertiser";

    const DB_COL_ID = 'id';
    const DB_COL_ADVERTISER_ID = 'advertiser_id';
    const DB_COL_PARENT_ID = 'parent_id';
    const DB_COL_ACCOUNT_STATUS = 'account_status';
    const DB_COL_LANGUAGE = 'language';
    const DB_COL_ADVERTISER_NAME = 'advertiser_name';
    const DB_COL_ADVERTISER_ALIAS = 'advertiser_alias';
    const DB_COL_PROGRAM_URL = 'program_url';
    const DB_COL_RELATIONSHIP_STATUS = 'relationship_status';
    const DB_COL_MOBILE_SUPPORTED = 'mobile_supported';
    const DB_COL_MOBILE_TRACKING_CERTIFIED = 'mobile_tracking_certified';
    const DB_COL_COMMISSION_LINK = 'commission_link';
    const DB_COL_LOGO = 'logo';
    const DB_COL_DESC_SHORT = 'desc_short';
    const DB_COL_DESC_FULL = 'desc_full';
    const DB_COL_DESC_EXTRA = 'desc_extra';
    const DB_COL_CASHBACK_VALUE = 'cashback_value';
    const DB_COL_CASHBACK_CUSTOMER = 'cashback_customer';
    const DB_COL_CASHBACK_CONST = 'cashback_const';
    const DB_COL_CASHBACK_PERCENT = 'cashback_percent';
    const DB_COL_URL = 'url';
    const DB_COL_ACTIVE = 'active';
    const DB_COL_BROWSER_PANEL = 'browser_panel';
    const DB_COL_CATEGORY_ID = 'category_id';
    const DB_COL_IS_FAVOURITE = 'is_favourite';
    const DB_COL_SHOW_IN_MENU = 'show_in_menu';
    const DB_COL_SOURCE = 'source';
    const DB_COL_FIRST_PUHLISHED = 'first_published';
    const DB_COL_SITE_DOMAIN = 'site_domain';

    public $columns = array(
        self::DB_COL_ADVERTISER_ID =>
        array(self::COLUMN_TITLE => "ID v CJ"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_ADVERTISER_NAME =>
        array(self::COLUMN_TITLE => "Jméno"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_PARENT_ID =>
        array(self::COLUMN_TITLE => "Nadřazený obchod"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
        ),
        
        self::DB_COL_ADVERTISER_ALIAS =>
        array(self::COLUMN_TITLE => "Alias"
            , self::COLUMN_TYPE => self::TYPE_TEXT
        ),
        
        self::DB_COL_COMMISSION_LINK =>
        array(self::COLUMN_TITLE => "Affil Link"
            , self::COLUMN_TITLE_GRID => 'Affil Link vyplněn'
            , self::COLUMN_TYPE => self::TYPE_TEXT
        ),
        
        self::DB_COL_CATEGORY_ID =>
        array(self::COLUMN_TITLE => "Kategorie"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_MULTIPLE)
        ),
        self::DB_COL_PROGRAM_URL =>
        array(self::COLUMN_TITLE => "URL"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_VALIDATORS => array()
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_URL =>
        array(self::COLUMN_TITLE => "URL na našich stránkách"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_VALIDATORS => array()
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
        ),
        self::DB_COL_ACCOUNT_STATUS =>
        array(self::COLUMN_TITLE => "Stav"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_ACCOUNT_STATUS =>
        array(self::COLUMN_TITLE => "Stav u dodavatele"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_LANGUAGE =>
        array(self::COLUMN_TITLE => "Jazyk"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_RELATIONSHIP_STATUS =>
        array(self::COLUMN_TITLE => "Stav připojení"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_CASHBACK_VALUE =>
        array(self::COLUMN_TITLE => "Velikost odměny pro nás"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
        ),
        self::DB_COL_CASHBACK_CUSTOMER =>
        array(self::COLUMN_TITLE => "Velikost odměny pro zákazníka"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
        ),
        self::DB_COL_SITE_DOMAIN =>
        array(self::COLUMN_TITLE => "Doména obchodu"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
        ),
        self::DB_COL_FIRST_PUHLISHED =>
        array(self::COLUMN_TITLE => "První publikace"
            , self::COLUMN_TYPE => self::TYPE_DATE
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_IS_FAVOURITE =>
        array(self::COLUMN_TITLE => "Oblíbený"
            , self::COLUMN_TYPE => self::TYPE_BOOL
        ),
        self::DB_COL_SHOW_IN_MENU =>
        array(self::COLUMN_TITLE => "Zobrazovat ikonku v menu"
            , self::COLUMN_TYPE => self::TYPE_BOOL
        ),
        self::DB_COL_CASHBACK_CONST =>
        array(self::COLUMN_TITLE => "Je odměna pevná"
            , self::COLUMN_TITLE_GRID => "Pevná"
            , self::COLUMN_TYPE => self::TYPE_BOOL
        ),
        self::DB_COL_CASHBACK_PERCENT =>
        array(self::COLUMN_TITLE => "Je odměna v procentech"
            , self::COLUMN_TITLE_GRID => "V %"
            , self::COLUMN_TYPE => self::TYPE_BOOL
        ),
        self::DB_COL_MOBILE_SUPPORTED =>
        array(self::COLUMN_TITLE => "Podporuje mobil"
            , self::COLUMN_TYPE => self::TYPE_BOOL
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_MOBILE_TRACKING_CERTIFIED =>
        array(self::COLUMN_TITLE => "Mobilní trackování"
            , self::COLUMN_TYPE => self::TYPE_BOOL
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
        ),
        self::DB_COL_ACTIVE =>
        array(self::COLUMN_TITLE => "Aktivní"
            , self::COLUMN_TYPE => self::TYPE_BOOL
        ),
        self::DB_COL_BROWSER_PANEL =>
        array(self::COLUMN_TITLE => "Lištička"
            , self::COLUMN_TYPE => self::TYPE_BOOL
        ),
        self::DB_COL_DESC_SHORT =>
        array(self::COLUMN_TITLE => "Popis - krátký"
            , self::COLUMN_TYPE => self::TYPE_TEXTAREA
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_CKEDITOR)
        ),
        self::DB_COL_DESC_FULL =>
        array(self::COLUMN_TITLE => "Popis - úplný"
            , self::COLUMN_TYPE => self::TYPE_TEXTAREA
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_CKEDITOR)
        ),
        self::DB_COL_DESC_EXTRA =>
        array(self::COLUMN_TITLE => "Popis - doplňující"
            , self::COLUMN_TYPE => self::TYPE_TEXTAREA
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_CKEDITOR)
        ),
        self::DB_COL_LOGO =>
        array(self::COLUMN_TITLE => "Logo"
            , self::COLUMN_TYPE => self::TYPE_IMAGE_UPLOAD
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
        ),
    );

    public function __construct(\Nette\Database\Context $context)
    {
        parent::__construct($context);
    }

    public function saveItemsFromCJ($items, $advertisersIdsFromDb = array())
    {

        foreach ($items as $itemRaw)
        {
            $item = $this->convertToDBArray($itemRaw);
            $item['source'] = \App\Enum\EAdvertiserSource::CJ;
            
            $by = [
                self::DB_COL_ADVERTISER_ID => $item[self::DB_COL_ADVERTISER_ID]
            ];
            
            if (isset($advertisersIdsFromDb[$item[self::DB_COL_ADVERTISER_ID]]))
            {
                unset($advertisersIdsFromDb[$item[self::DB_COL_ADVERTISER_ID]]);
            }

            $row = $this->findBy($by)->fetch();

            if ($row)
            {
                $filteredItem = $this->getFilteredData($item);
                $row->update($filteredItem);
            } else
            {
                $this->saveFiltered($item);
            }
        }
        
        // IDcka, která zbydou po synchronizaci, která vypadla z API
        if ($advertisersIdsFromDb)
        {
            $this->deactivateCjAdvertisers($advertisersIdsFromDb);
        }
    }
    
    public function getLinkWithoutHttp($link)
    {
        $linkNoHttp =  str_replace("http:", '', $link);
        $cleanLink =  str_replace("https:", '', $linkNoHttp);
        
        return $cleanLink;
    }
    
    public function deactivateCjAdvertisers($advertisersIdsFromDb)
    {
        $by = [
            self::DB_COL_ADVERTISER_ID => $advertisersIdsFromDb
        ];
        
        $data = [
            self::DB_COL_ACCOUNT_STATUS => EAdvertAccountStatus::DEACTIVE
        ];
        
        $this->findBy($by)->update($data);
    }

    /**
     * Vrací všechny aktivní inzerenty
     * @return \Nette\Database\Table\Selection
     */
    public function getAllActiveAdvertisers()
    {
        $by = [
            'advertiser.' . self::DB_COL_ACTIVE => 1,
            'advertiser.' . self::DB_COL_COMMISSION_LINK .' <> ?' => '',
            '(advertiser.' . self::DB_COL_PARENT_ID . ' IS NULL ) OR (parent.' . self::DB_COL_ACTIVE . ' = ? )' => 1
        ];

        return $this->findBy($by);
    }

    /**
     * Vrací všechny aktivní inzerenty na zaklade hleadni
     * @return \Nette\Database\Table\Selection
     */
    public function searchAllActiveAdvertisers($keyword)
    {

        $searcgParameter = "%$keyword%";

        return $this->getAllActiveAdvertisers()->
                        where('advertiser.'.self::DB_COL_ADVERTISER_NAME . " LIKE ? OR advertiser." . self::DB_COL_ADVERTISER_ALIAS . " LIKE ?", array($searcgParameter, $searcgParameter));
    }

    /**
     * Vraci data ve formatu pro naseptavac.
     * @param type $keyword
     */
    public function getSearchWhisperResults($keyword)
    {
        $searchData = $this->searchAllActiveAdvertisers($keyword);
        $results = array();

        foreach ($searchData as $row)
        {
            $results[] = array(
                "value" => ($row->advertiser_alias) ? $row->advertiser_alias : $row->advertiser_name,
                "data" => $row->id,
                'logo' => $row->logo
            );
        }
        return $results;
    }

    /**
     * Uloží k obchodu URL, přes kterou se může nakupovat
     * @param int $advertiseId
     * @param \SimpleXMLElement $url
     */
    public function saveLink($advertiseId, \SimpleXMLElement $link)
    {
        $row = $this->findRow($advertiseId);

        $data = $this->convertToDBArray($link);
        
        $saleCommission = str_replace('%', '', $data['sale_commission']);

        if (!isset($data['clickUrl']))
        {
            return;
        }
        
        if ($row)
        {
            $data = [
                self::DB_COL_COMMISSION_LINK => $data['clickUrl'],
                self::DB_COL_CASHBACK_VALUE => $saleCommission
            ];

            $row->update($data);
        }
    }

    public function getActiveStore($id)
    {
        return $this->getAllActiveAdvertisers()->where("advertiser.id = ?", $id)->fetch();
    }

    /**
     * TODO nějakou metriku pro oblíbenost
     * @return \Nette\Database\Table\Selection
     */
    public function getFavouriteAdvertisers()
    {
        return $this->getAllActiveAdvertisers()->where('advertiser.' . self::DB_COL_IS_FAVOURITE, 1);
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    public function getLatestAdvertisers()
    {
        return $this->getAllActiveAdvertisers()
                ->order('advertiser.' . self::DB_COL_FIRST_PUHLISHED . ' DESC, ' . self::DB_COL_ADVERTISER_NAME);
    }

    public function getAllActiveAdvertisersInCategory($categoryId)
    {
        return $this->getAllActiveAdvertisers()
                        ->where(':' . ACR::TABLE_NAME . '.' . ACR::DB_COL_CATEGORY_ID . ' = ?', $categoryId);
    }

    public function getItemsForSelectBox()
    {
        return $this->findAll()->order(self::DB_COL_ADVERTISER_NAME . ' ASC')->fetchPairs('id', self::DB_COL_ADVERTISER_NAME);
    }

    public function getAdvertiserByCjId($cjAdvertiserId)
    {
        $by = [
            self::DB_COL_ADVERTISER_ID => $cjAdvertiserId
        ];

        return $this->findBy($by)->fetch();
    }

    /**
     * Vrací záznam podle URL advertisera (jeho url ne url na našich stránkách)
     */
    public function getItemByAdvertiserUrl($url)
    {
        $by = [
            self::DB_COL_PROGRAM_URL . ' LIKE ?' => '%' . $url . '%'
        ];

        return $this->findBy($by)->fetch();
    }
    
    /**
     * Vrací všechny záznamy.
     */
    public function getAdvertisersForExplorerApi()
    {
        return $this->getAllActiveAdvertisers()->where($this->tableName . '.' . self::DB_COL_BROWSER_PANEL, 1);
    }

    /**
     * Vrací všechny obchody ze CJ API
     * zatím pouze podle toho, že 
     */
    public function getAllCjAdvertisers()
    {
        $by = [
            self::DB_COL_SOURCE => \App\Enum\EAdvertiserSource::CJ
        ];

        return $this->findBy($by);
    }
    
}
