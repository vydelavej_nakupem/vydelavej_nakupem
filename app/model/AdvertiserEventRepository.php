<?php

namespace App\Model;

use Nette\Application\UI\Form;

class AdvertiserEventRepository extends BaseRepository
{

    protected $tableName = 'advertiser_event';

    const DB_COL_PRIMARY = 'id';
    const DB_COL_ADVERTISER_ID = 'advertiser_id';
    const DB_COL_URL = 'url';
    const DB_COL_TITLE = 'title';
    const DB_COL_DESCRIPTION = 'description';
    const DB_COL_CODE = 'code';
    const DB_COL_PUBLISHED_FROM = 'published_from';
    const DB_COL_PUBLISHED_TO = 'published_to';
    const DB_COL_ACTIVE = 'active';

    public $columns = array(
        self::DB_COL_ADVERTISER_ID =>
        array(self::COLUMN_TITLE => "Obchod"
            , self::COLUMN_TYPE => self::TYPE_SELECT
            , self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
        ),
        self::DB_COL_TITLE =>
        array(self::COLUMN_TITLE => "Název"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
        ),
        self::DB_COL_URL =>
        array(self::COLUMN_TITLE => "URL"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
        ),
        self::DB_COL_DESCRIPTION =>
        array(self::COLUMN_TITLE => "Popis"
            , self::COLUMN_TYPE => self::TYPE_TEXTAREA
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_CKEDITOR)
        ),
        self::DB_COL_CODE =>
        array(self::COLUMN_TITLE => "Kód"
            , self::COLUMN_TYPE => self::TYPE_TEXT
            , self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
        ),
        self::DB_COL_PUBLISHED_FROM =>
        array(self::COLUMN_TITLE => "Publikovat od"
            , self::COLUMN_TYPE => self::TYPE_DATE
        ),
        self::DB_COL_PUBLISHED_TO =>
        array(self::COLUMN_TITLE => "Publikovat do"
            , self::COLUMN_TYPE => self::TYPE_DATE
        ),
        self::DB_COL_ACTIVE =>
        array(self::COLUMN_TITLE => "Aktivní"
            , self::COLUMN_TYPE => self::TYPE_BOOL
        ),
    );

    public function getActiveRecord($id)
    {
        $by = [
            self::DB_COL_PRIMARY => $id,
            self::DB_COL_ACTIVE => 1,
        ];

        return $this->findBy($by)->fetch();
    }
    
    public function getActiveItems()
    {
        $by = [
            self::DB_COL_ACTIVE => 1,
            self::DB_COL_PUBLISHED_FROM . " ? OR ".self::DB_COL_PUBLISHED_FROM . ' < ?' => [NULL, new \Nette\Database\SqlLiteral('NOW()')],
            self::DB_COL_PUBLISHED_TO . " ? OR ".self::DB_COL_PUBLISHED_TO . ' > ?' => [NULL, new \Nette\Database\SqlLiteral('NOW()')]
        ];
        
        return $this->findBy($by);
    }
    
    public function getActiveStoreItems($storeId)
    {
        return $this->getActiveItems()->where(self::DB_COL_ADVERTISER_ID, $storeId);
    }
}
