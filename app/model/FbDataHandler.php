<?php

namespace App\Model;

/**
 * @author Samuel
 */
class FbDataHandler extends \Nette\Object implements \Facebook\PersistentData\PersistentDataInterface
{
    private $session;
    
    private $sessionFb;
    
    const SESSION_NAME = 'FBRLH';
    const SESSION_PREFIX = 'FBRLH_';
    
    
    public function __construct(\Nette\Http\Session $session)
    {
        $this->session = $session;
        $this->sessionFb = $session->getSection(self::SESSION_NAME);
    }

    /**
     * @inheritdoc
     */
    public function get($key)
    {
        return $this->sessionFb->{self::SESSION_PREFIX.$key};
    }

    /**
     * @inheritdoc
     */
    public function set($key, $value)
    {
        $this->sessionFb->{self::SESSION_PREFIX.$key} = $value;
    }
}