<?php

namespace App\Model;

use Nette\Application\UI\Form;
use RTsoft\Model\GridFormRepository as R;

class CompanyRepository extends R
{
    public $columns = array(
        
        "name" =>
        
        array(self::COLUMN_TITLE      => "Název společnosti"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array()
             ),
        
        "addr_label" =>
        
        array(self::COLUMN_TITLE      => "Adresa"
             ,self::COLUMN_TYPE       => self::TYPE_LABEL
             ),
        
        "address" =>
        
        array(self::COLUMN_TITLE      => "Adresa"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_FORM)
             ,self::COLUMN_FILTER_BY  => "CONCAT(street, land_registry_number, house_number, city, postcode)"
             ),
        
        "street" =>
        
        array(self::COLUMN_TITLE      => "Ulice"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "land_registry_number" =>
        
        array(self::COLUMN_TITLE      => "Číslo popisné"
             ,self::COLUMN_TITLE_GRID => "Č.p."
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array()
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "house_number" =>
        
        array(self::COLUMN_TITLE      => "Číslo orientační"
             ,self::COLUMN_TITLE_GRID => "Č.o."
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array( )
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "city" =>
        
        array(self::COLUMN_TITLE      => "Město"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "postcode" =>
        
        array(self::COLUMN_TITLE      => "PSČ"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "state_id" =>
        
        array(self::COLUMN_TITLE      => "Stát"
             ,self::COLUMN_TYPE       => self::TYPE_SELECT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "ic" =>
        
        array(self::COLUMN_TITLE      => "IČ"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array()
             ),
        
        "dic" =>
        
        array(self::COLUMN_TITLE      => "DIČ"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array()
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "phone" =>
        
        array(self::COLUMN_TITLE      => "Telefon"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array()
             ,self::COLUMN_OPTIONS    => array()
             ),
        
        "email" =>
        
        array(self::COLUMN_TITLE      => "Email"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::EMAIL => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array()
             ),
        
        "account_prefix" =>
        
        array(self::COLUMN_TITLE => "Předčíslí"
             ,self::COLUMN_TYPE  => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::INTEGER => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS => array(R::OPTION_NOT_IN_GRID)
             ),
        
        "account_number" =>
        
        array(self::COLUMN_TITLE => "Číslo účtu"
             ,self::COLUMN_TYPE  => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::INTEGER => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS => array(R::OPTION_NOT_IN_GRID)
             ),
        
        "account_bank" =>
        
        array(self::COLUMN_TITLE => "Kód banky"
             ,self::COLUMN_TYPE  => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::INTEGER => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS => array(R::OPTION_NOT_IN_GRID)
             ),
        
        "note" =>
        
        array(self::COLUMN_TITLE      => "Poznámka"
             ,self::COLUMN_TYPE       => self::TYPE_TEXTAREA
             ,self::COLUMN_VALIDATORS => array()
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),

        "value" =>

        array(self::COLUMN_TITLE      => "Hodnota"
             ,self::COLUMN_TYPE       => self::TYPE_FLOAT
             ,self::COLUMN_VALIDATORS => array()
             ,self::COLUMN_OPTIONS    => array()
             ,self::COLUMN_ATTRIBUTES => array("placeholder" => "0.00")
             ),
        
        "created" =>
        
        array(self::COLUMN_TITLE      => "Přidáno"
             ,self::COLUMN_TYPE       => self::TYPE_DATETIME
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_FORM)
             ),
        
        "not_deleted" =>
        
        array(self::COLUMN_TITLE      => "Aktivní"
             ,self::COLUMN_TYPE       => self::TYPE_BOOL
             ,self::COLUMN_DEFAULT    => TRUE
             ,self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
             ,self::COLUMN_FILTER_DEFAULT => TRUE
             )
        
    );
    
    public function __construct(\Nette\Database\Context $context)
    {
        parent::__construct($context);
    }

    public function filterRows($rows, array $options)
    {
        if (!empty($options["address"]))
        {
            $params = array_fill(0, 5, "%{$options["address"]}%");
            $rows->where("street LIKE ? OR land_registry_number LIKE ? OR house_number LIKE ? OR city LIKE ? OR postcode LIKE ?", $params);
            unset($options["address"]);
        }
        
        parent::filterRows($rows, $options);
    }
    
    public function findByName($name)
    {
        $rows = $this->findBy(array("name" => $name, "not_deleted" => "1"));
        $row = $rows->fetch();
        return $row;
    }
    
    /**
     * Vraci polozky pro selectbox.
     * @return array
     */
    public function fetchItemsForSelectbox($options = array())
    {
        $rows = $this->findAll();
        
        $this->filterRows($rows, $options);
        
        $rows->order("name");
        
        return $rows->fetchPairs("id", "name");
    }
    
    public function enterCompany($name, $loggedUserId)
    {
        $row = $this->findByName($name);
        
        if (!$row)
        {
            $a = array(
                "name" => $name,
                "user_created_id" => $loggedUserId
            );

            $row = $this->insert($a);
        }
        
        return $row;
    }
}
