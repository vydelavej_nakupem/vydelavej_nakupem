<?php

namespace App\Model;

class LoginLogRepository extends \RTsoft\Model\Repository
{
    protected $tableName = 'login_log';
    
    public function saveNewLogin($login, $userId)
    {
        $data = array(
            'user_id' => $userId,
            'login' => $login,
            'success' => !empty($userId),
            'ip' => $_SERVER["REMOTE_ADDR"],
            'user_agent' => $_SERVER["HTTP_USER_AGENT"],
        );
        
        //\Tracy\Debugger::barDump($data);
        
        $this->save($data);
    }
}
