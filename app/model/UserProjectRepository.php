<?php

namespace App\Model;

use Nette\Application\UI\Form;

class UserProjectRepository extends BaseRepository
{
    const TABLE_NAME = "user_project";
    
    const DB_COL_USER_ID = 'user_id';
    const DB_COL_TITLE = 'title';
    const DB_COL_DESCRIPTION = 'description';
    const DB_COL_URL = 'url';
    const DB_COL_IMG = 'img';
    const DB_COL_ACTIVE = 'active';

    protected $tableName = self::TABLE_NAME;
    
    public $columns = array(

        self::DB_COL_USER_ID =>
        
        array(self::COLUMN_TITLE      => "Vlastník"
             ,self::COLUMN_TYPE       => self::TYPE_SELECT
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_FORM)
             ),
        
        self::DB_COL_TITLE =>
        
        array(self::COLUMN_TITLE      => "Název"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ),
        
        self::DB_COL_URL =>
        
        array(self::COLUMN_TITLE      => "Url"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ),
        
        self::DB_COL_DESCRIPTION =>
        
        array(self::COLUMN_TITLE      => "Popis"
             ,self::COLUMN_TYPE       => self::TYPE_TEXTAREA
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        self::DB_COL_IMG =>
        
        array(self::COLUMN_TITLE      => "Obrázek"
             ,self::COLUMN_TYPE => self::TYPE_IMAGE_UPLOAD
             ,self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID)
             ),
        
        self::DB_COL_ACTIVE =>
        
        array(self::COLUMN_TITLE      => "Aktivní"
             ,self::COLUMN_TYPE       => self::TYPE_BOOL
             ,self::COLUMN_DEFAULT    => TRUE
             ,self::COLUMN_FILTER_DEFAULT => self::BOOL_POSITIVE
             ),
    );
    
    private $userRoleRepository;
    
    public function __construct(
            \Nette\Database\Context $context,
            \App\Model\UserRoleRepository $userRoleRepository)
    {
        parent::__construct($context);
        $this->userRoleRepository = $userRoleRepository;
    }
    
    public function getActiveProject($id)
    {
        return $this->getActiveProjects()->where('id', $id)->fetch();
    }
    
    public function getActiveProjects()
    {
        $by = [
            self::DB_COL_ACTIVE => 1
        ];
        
        return $this->findBy($by);
    }
}
