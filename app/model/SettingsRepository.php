<?php

namespace App\Model;

use RTsoft\Model\Repository;

class SettingsRepository extends Repository
{      
    public function fetchAllAsPairs()
    {
        $rows = $this->findAll();
        
        $rows->select("key");
        $rows->select("COALESCE(value, blob) AS `value`");
                
        return $rows->fetchPairs("key", "value");
    }
    
    public function getByKey($key)
    {
        $row = $this->findBy(array('key' => $key))->fetch();
        
        if ($row) 
        {
            if (!empty($row->value))
            {
                return $row->value;
            }
            if (!empty($row->blob))
            {
                return $row->blob;
            }
        }
        
        return FALSE;
    }
    
    public function fetchAllWithKeyBeginningWith($with)
    {
        $rows = $this->findAll();
        
        $rows->where("key LIKE ?", "{$with}%");
        
        return $rows;
    }
    
    public function saveArray($array)
    {
        foreach($array as $k => $v)
        {
            $row = $this->findBy(array("key" => $k))->fetch();
            
            if ($v instanceof \Nette\Http\FileUpload)
            {
                /** @var $v \Nette\Http\FileUpload */
                if ($v->isOk())
                {
                    $fp = fopen($v->getTemporaryFile(), 'rb');
                    $binarydata = (fread($fp, $v->getSize()));
                    $blob = $binarydata;
                    fclose($fp);
                    
                    $row->update(array("blob" => $blob));
                }
            }
            else if (strpos($k, "image_") === 0 && strpos($k, "_delete") !== FALSE)
            {
                if ($v)
                {
                    $row = $this->findBy(array("key" => str_replace("_delete", "", $k)))->fetch();
                    $row->update(array("blob" => NULL));
                }
            }
            else
            {
                $row->update(array("value" => $v));
            }
        }
    }
}