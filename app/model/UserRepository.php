<?php

namespace App\Model;

use Nette\Application\UI\Form;
use RTsoft\Model\GridFormRepository as R;

class UserRepository extends R
{
    const MIN_PSWD_LEN = 5;
    const SID_CHECK_ATTEMPTS = 3;
    const SALT_FOR_USER_VERIFICATION = "khjdfakljdfh78f6asd78f67";
    const BANK_WIRE_PATTERN = '(([0-9]{0,6})-)?([0-9]{1,10}\/[0-9]{4})';
    
    public $columns = array(

        "name" =>
        
        array(self::COLUMN_TITLE      => "Jméno"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_FORM)
             ),
        
        "firstname" =>
        
        array(self::COLUMN_TITLE      => "Jméno"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "lastname" =>
        
        array(self::COLUMN_TITLE      => "Příjmení"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "email" =>
        
        array(self::COLUMN_TITLE      => "Email"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE, Form::EMAIL => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array()
             ),
      
        "password" =>
        
        array(self::COLUMN_TITLE      => "Heslo"
             ,self::COLUMN_TYPE       => self::TYPE_PASSWORD
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
      
        "password2" =>
        
        array(self::COLUMN_TITLE      => "Heslo znovu"
             ,self::COLUMN_TYPE       => self::TYPE_PASSWORD
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_GRID)
             ),
        
        "roles" =>
        
        array(self::COLUMN_TITLE       => "Role"
             ,self::COLUMN_TYPE        => self::TYPE_TEXT
             ,self::COLUMN_OPTIONS     => array(self::OPTION_NOT_IN_FORM, self::OPTION_SORTING_DISABLED)
             ,self::COLUMN_FILTER_TYPE => self::FILTER_TYPE_NONE
             ),
        
        "active" =>
        
        array(self::COLUMN_TITLE      => "Aktivní"
             ,self::COLUMN_TYPE       => self::TYPE_BOOL
             ,self::COLUMN_DEFAULT    => TRUE
             ,self::COLUMN_FILTER_DEFAULT => self::BOOL_POSITIVE
             ),
        
        "bank_wire" =>
        
        array(self::COLUMN_TITLE      => "č.ú."
             ,self::COLUMN_OPTIONS     => array(self::OPTION_NOT_IN_GRID)
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ),
    );
    
    private $userRoleRepository;
    
    public function __construct(
            \Nette\Database\Context $context,
            \App\Model\UserRoleRepository $userRoleRepository)
    {
        parent::__construct($context);
        $this->userRoleRepository = $userRoleRepository;
    }


    public function findByEmail($email)
    {
        return $this->findBy(array('email' => $email))->fetch();
    }
    
    /**
     * Nalezeni uzivatele dle fb Id
     * @param type $userFbId
     * @param type $userId
     * @return type
     */
    public function findByFbId($userFbId, $userId = null)
    {
        if(!$userFbId)
        {
            return null;
        }
        
        return $this->findBy(array('fb_id' => $userFbId))->fetch();
    }

    public function findUsers(array $options = null)
    {
        $rows = $this->findAll()->order('email');

        $this->filterRows($rows, $options);

        return $rows;
    }
    
    public function filterRows($rows, array $options)
    {
        // pro selectbox
        if (!empty($options["user_id"]) && !empty($options["role_id"]))
        {
            $rows->where(":user_role.role.name = ? OR id = ?", array($options["role_id"], $options["user_id"]));
        }
        else if (!empty($options["user_id"])) 
        {
            $rows->where("id = ? OR active = 1", $options["user_id"]);
        }
        else if (!empty($options["role_id"]))
        {
            $rows->where(":user_role.role.name", $options["role_id"]);
        }
        
        // jmeno prijmeni
        if (!empty($options["name"]))
        { 
            $params = array_fill(0, 2, "%{$options["name"]}%");
            $rows->where("user.firstname LIKE ? OR user.lastname LIKE ?", $params);
            unset($options["name"]);
        }
        
        parent::filterRows($rows, $options);
    }

    /**
     * Vraci polozky pro selectbox.
     * @param array $options
     * @return array
     */
    public function fetchItemsForSelectbox($options = array())
    {
        $rows = $this->findAll();
        
        $rows->select("`user`.`id` AS `value`");
        $rows->select("CONCAT_WS(' ', user.lastname, user.firstname, CASE WHEN `user`.active THEN NULL ELSE ? END) AS `text`", R::$textAddonForInactiveOption);
        
        if (!empty($options))
        {
            $this->filterRows($rows, $options);
        }
        
        $rows->order("`user`.active DESC, text ASC");
        
        $data = array();
        
        foreach($rows as $r)
        {
            $data[$r["value"]] = $r["text"];
        }
        
        return $data;
    }

    /**
     * Vraci overovaci retezec pro uzivatele.
     * Pouziva se treba v zapomenutem hesle.
     * 
     * @param \Nette\Database\Table\ActiveRow $row
     * @return string
     */
    public function getVerificationStringForUser(\Nette\Database\Table\ActiveRow $row, $addon = "")
    {
        return \Nette\Security\Passwords::hash($row->id . $row->email . $addon, array("salt" => self::SALT_FOR_USER_VERIFICATION));
    }
    
    /**
     * Vygenerovani SID pro uzivatele
     * @param \Nette\Database\Table\ActiveRow $user
     */
    public function generateSid(\Nette\Database\Table\ActiveRow $user)
    {
        if($user)
        {
            $sid = $this->checkSid($user);
            
            if(!$sid)
            {
                return false;
            }
            $user->update(array('sid' => $sid));
        }
        return true;
    }
    
    /**
     * Kontrola SID pro uzivatele
     * @param \Nette\Database\Table\ActiveRow $user
     * @return string
     */
    private function checkSid(\Nette\Database\Table\ActiveRow $user)
    {
        $sid = null;
        
        for($i = 0; $i < self::SID_CHECK_ATTEMPTS; $i++)
        {
            $sid = sha1($user->id . time() . $i);
            if($this->findBy(array('sid' => $sid))->count() === 0)
            {
                break;
            }
        }
        return $sid;
    }

    /**
     * @param  int $id
     * @param  string $password
     */
    public function setPassword($id, $password)
    {
        $row = $this->findRow($id);
        
        $row->update(array(
            'password' => \Nette\Security\Passwords::hash($password)
        ));
    }
    
    /**
     * Data pro CSV export.
     */
    public function fetchForExport()
    {
        $rows = $this->findAll();
        
        //$rows->where(":user_role.role.name", \App\Enum\ERole::USER);
        
        $rows->select("`user`.`id`, `firstname` AS ?, `lastname` AS ?, `email`, DATE_FORMAT(`created`, ?) AS ?", "Přihlašovací jméno", "Jméno", "Příjmení", "%e.%c.%Y %k:%i:%s", "Vytvořen");
        $rows->where("`user`.`active`");
        
        return $rows;
    }
    
    /**
     * Aktivace uživatele.
     * @param \Nette\Database\Table\ActiveRow $row
     */
    public function activate($row)
    {
        $row->update(array("active" => TRUE));
    }
    
    /**
     * Vraci sid k danemu uzivateli
     * @param int $userId
     * @return string
     */
    public function getUserSid($userId)
    {
        $user = $this->findRow($userId);
        
        if($user)
        {
            return $user->sid;
        }
        return null;
    }
    
    /**
     * Overeni platneho sid resp. reg. hashe. 
     * @param type $sid
     * @return type
     */
    public function checkUserSid($sid)
    {
        if(!$sid)
        {
            return null;
        }
        
        $user = $this->findBy(array(
            'sid'       => $sid,
            'active'    => 1))->fetch();
                
        return $user ? $user : null;
    }
    
    
    /**
     * Vrati seznam vlastnikovych uzivatelu
     * @param type $userId
     */
    public function getOwnerUsers($userId)
    {
        return $this->findBy(array('parent_id' => $userId));
    }
    
    /**
     * Vrati seznam ID vlastnikovych uzivatelu
     * @param type $userId
     * @param type $sid
     */
    public function getOwnerUsersId($userId, $sid = false)
    {
        $users = $this->getOwnerUsers($userId);
        
        if($users)
        {
            $rows = $users->fetchPairs('id', 'sid');
            return $sid ? array_values($rows) : array_keys($rows);
        }
        return array();
    }
    
    /**
     * Vytvori uzivatelsky ucet uzivatele.
     * @param type $data
     */
    public function registerFbUser($data, $parentId = null)
    {
        $userFbId = null;
        $email = null;
        
        if(!empty($data["id"]))
        {
            $userFbId = $data["id"];
        }
        if(!empty($data["email"]))
        {
            $email = $data["email"];
        }
        $user = $this->findByEmail($email);
        
        if($user) // sparovani se soucasnym uctem dle emailu
        {
            return $this->save(array("fb_id" => $userFbId), $user->id);
        }
        
        $user = $this->findByFbId($userFbId);
        
        if($user)
        {
            return $user;
        }
        $data = array(
            
            "fb_id"     => $userFbId,
            "email"     => $email,
            "active"    => true,
        );
        
        if($parentId)
        {
            $data["parent_id"] = $parentId;
        }
        $user = $this->save($data);
        
        $this->userRoleRepository->save(array(
            
            "user_id" => $user->id,
            "role_id" => \App\Enum\ERole::$id[\App\Enum\ERole::USER]
            
        ));
        
        return $this->generateSid($user) ? $user : null;
    }
}
