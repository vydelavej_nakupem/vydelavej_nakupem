<?php

namespace App\Model;

class RegisterLogRepository extends \RTsoft\Model\Repository
{
    protected $tableName = 'register_log';
    
    public function saveLog($userId, $mesage)
    {
        $data = array(
            'user_id' => $userId,
            'message' => $mesage,
        );
        $this->save($data);
    }
}
