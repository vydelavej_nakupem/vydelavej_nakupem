<?php

namespace CjApi;

use App\Enum\ELinkType;

/**
 * Třída pro práci s CJ API
 *
 * @author Tomas Huda
 */
class CjApi extends \Nette\Object
{

    const DATE_FORMAT = 'Y-m-d';
    
    const COMMISION_URL = 'https://commission-detail.api.cj.com/v3/commissions';
    const ADVERTISERS_URL = 'https://advertiser-lookup.api.cj.com/v2/advertiser-lookup';
    const LINK_SEARCH_URL = 'https://link-search.api.cj.com/v2/link-search';
    
    private $developerKey;
    private $websiteId;

    public function __construct($developerKey, $websiteId)
    {
        $this->developerKey = $developerKey;
        $this->websiteId = $websiteId;
    }

    public function getLinkForAdvertiser($cid)
    {
        $url = self::LINK_SEARCH_URL."?website-id=".$this->websiteId.'&link-type=Text%20Link&advertiser-ids='.$cid;
        $result = $this->getApiResult($url);

        $simpleXMLElement = simplexml_load_string($result);

        if(isset($simpleXMLElement->links->link))
        {
            return $simpleXMLElement->links->link[0];
        }

        return NULL;
    }
    
    /**
     * 
     * @param \DateTime $dateFromObject
     * @param \DateTime $dateToObject
     * @return boolean
     */
    public function getCommissions(\DateTime $dateFromObject = NULL, \DateTime $dateToObject = NULL)
    {
        if (!$dateFromObject)
        {
            $dateFromObject = new \DateTime();
        }
        
        if (!$dateToObject)
        {
            $dateToObject = new \DateTime();
        }

        $dateFrom = $dateFromObject->format(self::DATE_FORMAT);
        $dateTo = $dateToObject->format(self::DATE_FORMAT);
        
        $url = self::COMMISION_URL."?date-type=posting&start-date=".$dateFrom."&end-date=".$dateTo;

        $result = $this->getApiResult($url);
        
        $simpleXMLElement = simplexml_load_string($result);
        
        if(isset($simpleXMLElement->commissions->commission))
        {
            return $simpleXMLElement->commissions->commission;
        }
        
        return FALSE;
    }
    
    private function callAdvertisers($baseUrl)
    {
        $advertisers = [];
        
        $needMoreResults = TRUE;
        $page = 1;
        
        while ($needMoreResults == TRUE)
        {
            $url = $baseUrl."&page-number=".$page;
            $result = $this->getApiResult($url);
            $simpleXMLElement = simplexml_load_string($result);

            if(isset($simpleXMLElement->advertisers->advertiser))
            {
                $recordsTotal = count($advertisers);
                
                $totalRecordsInSystem = $simpleXMLElement->advertisers['total-matched'];
                $recordsReturned = $simpleXMLElement->advertisers['records-returned'];
                
                if ($recordsReturned + $recordsTotal >= $totalRecordsInSystem)
                {
                    $needMoreResults = FALSE;
                }
                
                foreach ($simpleXMLElement->advertisers->advertiser as $advertiser)
                {
                    $advertisers[] = $advertiser;
                }
            }
            
            $page++;
        }

        return $advertisers;
    }
    
    public function checkAdvertisers($advertiserIdsArray)
    {
        $advertiserIdsString = implode(',', $advertiserIdsArray);
        $baseUrl = self::ADVERTISERS_URL.'?advertiser-ids='.$advertiserIdsString;
        
        return $this->callAdvertisers($baseUrl);
    }
    
    public function getAdvertisers()
    {
        $baseUrl = self::ADVERTISERS_URL.'?advertiser-ids=joined';
        
        return $this->callAdvertisers($baseUrl);
    }

    private function getApiResult($url)
    {
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/xml", "Authorization:" . $this->developerKey . ""));
        
        return curl_exec($ch);
    }

}
