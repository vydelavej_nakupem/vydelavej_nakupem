<?php

namespace App\Model;

use Nette\Application\UI\Form;
use RTsoft\Model\GridFormRepository as R;

class StateRepository extends R
{
    const DEFAULT_STATE = 39; // CZ
    const DEFAULT_PHONE_PRE = "+420";
     
    protected $tableName = 'state';

    protected $columnNameForText = "name";
    
    public $columns = array(

        "shortcut" =>
        
        array(self::COLUMN_TITLE      => "Zkratka"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array()
             ),
        
        "name" =>
        
        array(self::COLUMN_TITLE      => "Název"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ),
        
        "phone" =>
        
        array(self::COLUMN_TITLE      => "Tel. předvolba"
             ,self::COLUMN_TYPE       => self::TYPE_TEXT
             ,self::COLUMN_OPTIONS    => array()
             ),
        
        "vat" =>
        
        array(self::COLUMN_TITLE      => "DPH"
             ,self::COLUMN_TYPE       => self::TYPE_FLOAT
             ,self::COLUMN_OPTIONS    => array()
             ),
    );
    
    
    /**
     * Vraci staty pro selectbox.
     * @param array $options
     * @return array
     */
    public function fetchStatesForSelectbox()
    {
        $rows = $this->findAll();
        
        $rows->order("id = 39 DESC, id = 192 DESC, state.name ASC");
        
        $data = $rows->fetchPairs("id", "name");
        
        return $data;
    }
    
    /**
     * Vraci telefonni predvolby pro selectbox.
     * @param array $options
     * @return array
     */
    public function fetchPhonesForSelectbox()
    {
        $rows = $this->findAll();
        
        $rows->select("CONCAT('+', phone) AS phone");
        
        $rows->order("state.phone ASC");
        
        $data = $rows->fetchPairs("phone", "phone");
        
        return $data;
    }

    /**
     * Vrati polozky pro naseptavac.
     * @param string $q Hledany vyraz.
     * @return array Pole pro naseptavac.
     */
    public function fetchItemsForWhisperer($q, $name, $options = NULL)
    {
        $rows = $this->findAll();

        $rows->order("id = 39 DESC, id = 192 DESC, state.name ASC");

        $rows->where("name LIKE ?", "%{$q}%");

        $rows->limit(20);

        $data = array();

        foreach ($rows as $r)
        {
            $data[] = array(
                "text" => $r->name,
                "value" => $r->id,
            );
        }

        return $data;
    }
    
    /**
     * Vrátí DPH pro daný stát.
     * Pokud stát nemá zadáno DPH, tak vrátí DPH pro ČR.
     * @param int $stateId
     * @return float|NULL
     */
    public function getVatForState($stateId)
    {
        $vat = NULL;
        $row = NULL;
        
        // byl-li zadan stat (u uzivatele, ktery objednal, je stat vyplnen)
        if (!empty($stateId))
        {
            $row = $this->findRow($stateId);
        }
        
        // pokud se nasel stat, vezme se z nej DPH
        if (!empty($row))
        {
            $vat = $row->vat;
        }
        
        // pokud nemame DPH - u statu nebylo vyplneno, nebo se nenasel stat
        if (empty($vat) && $stateId != self::DEFAULT_STATE)
        {
            // vrati se DPH pro ČR
            return $this->getVatForState(self::DEFAULT_STATE);
        }
            
        return $vat;
    }
}
