<?php

namespace App\Model;

use Nette\Application\UI\Form;
use RTsoft\Model\GridFormRepository as R;

class CommissionLogRepository extends BaseRepository
{
    const TABLE_NAME = "commission_log";
    
    protected $tableName = self::TABLE_NAME;
    
    const DB_COL_COMMISSION_ID = 'commission_id';
    const DB_COL_CREATE_AT = 'created';
    const DB_COL_COMMISSION_AMOUNT = 'commission_amount';
    
    public $columns = array(
        
        self::DB_COL_COMMISSION_AMOUNT =>

        array(self::COLUMN_TITLE      => "Hodnota"
             ,self::COLUMN_TYPE       => self::TYPE_FLOAT
             ,self::COLUMN_VALIDATORS => array()
             ,self::COLUMN_OPTIONS    => array()
             ,self::COLUMN_ATTRIBUTES => array("placeholder" => "0.00")
             ),
        
        self::DB_COL_CREATE_AT =>
        
        array(self::COLUMN_TITLE      => "Přidáno"
             ,self::COLUMN_TYPE       => self::TYPE_DATETIME
             ,self::COLUMN_VALIDATORS => array(Form::REQUIRED => self::DEFAULT_MESSAGE)
             ,self::COLUMN_OPTIONS    => array(self::OPTION_NOT_IN_FORM)
             ),
        
        "not_deleted" =>
        
        array(self::COLUMN_TITLE      => "Aktivní"
             ,self::COLUMN_TYPE       => self::TYPE_BOOL
             ,self::COLUMN_DEFAULT    => TRUE
             ,self::COLUMN_OPTIONS => array(self::OPTION_NOT_IN_GRID, self::OPTION_NOT_IN_FORM)
             ,self::COLUMN_FILTER_DEFAULT => TRUE
             )
        
    );
    
    private $userRepository;
    private $advertiserRepository;
    
    public function __construct(
        \Nette\Database\Context $context,
        \App\Model\UserRepository $userRepository,
        \App\Model\AdvertisersRepository $advertiserRepository
    )
    {
        parent::__construct($context);
        
        $this->userRepository = $userRepository;
        $this->advertiserRepository = $advertiserRepository;
    }
    
    public function getItemByCommissionId($commissionId)
    {
        $by = [
            self::DB_COL_COMMISSION_ID => $commissionId
        ];
        
        return $this->findBy($by)->fetch();
    }

    public function saveItems($commissions)
    {
        foreach ($commissions as $key => $commission)
        {
            $data = $this->convertToDBArray($commission);
            
            $data['advertiser_id'] = $this->advertiserRepository->getAdvertiserByCjId($data['cid']);
            
            $commissionRow = $this->getItemByCommissionId($data['commission_id']);
            
            $id = NULL;
            if ($commissionRow)
            {
                $id = $commissionRow->id;
            }
            
            try
            {
                $this->saveFiltered($data, $id);
            } catch (\Nette\Database\UniqueConstraintViolationException  $ex) {
                
            }
        }
    }

    /**
     * Vse se ted hrne pres tuhle metodu, protoze od jiste verze luuuuvry jsou
     * nektery metody final 
     * @param type $rows
     * @param array $options
     */
    public function filterRows($rows, array $options = null)
    {
        parent::filterRows($rows, $options);
        
        $com_coef = ECommissionType::LEVEL_DEFAULT_VALUE;
        $select = null;
        
        if(!empty($options['c_level']) && in_array($options['c_level'], ECommissionType::$levels))
        {   
            $com_coef = $options['c_level'];
        }
        
        if(!empty($options['summary']))
        {   
            $rows->limit(1);
            $select = 
                    "id, " . 
                    "SUM(`commission-amount` * $com_coef) AS commission_amount";
        }
        else
        {
            $select = 
                    "id, " .
                    "`action-status` AS action_status, " .
                    "`action-type` AS action_type, " .
                    "`commission-id` AS commission_id, " .
                    "`event-date` AS event_date, " . 
                    "(`commission-amount` * $com_coef) AS commission_amount";
        }
        
        
        if(!empty($options['user_id']))
        {
            $userId = $options['user_id'];
            $checkSid = $this->userRepository->getUserSid($userId);
            
            if($checkSid)
            {
                $rows->where("sid = ?", $checkSid);
            }
        }
        if($select)
        {
            $rows->select($select);
        }
    }

    /**
     * Zaznam o provizich pro daneho uzivatele
     * @param array $users
     * @param \Nette\Utils\DateTime $from
     * @param \Nette\Utils\DateTime $to
     * @return type
     */
    public function getUsersLog($users, \Nette\Utils\DateTime $from = null, \Nette\Utils\DateTime $to = null)
    {
        $rows = $this->findBy(array("user_id" => $users));
        
        if($from)
        {
            $rows->where("event-date >= ?", $from);
        }
        if($to)
        {
            $rows->where("event-date <= ?", $to);
        }
        return $rows;
    }
    
    /**
     * Vrati celkovou provizi uzivatele.
     * @param int $ownerId
     * @param array $users
     * @param \Nette\Utils\DateTime $from
     * @param \Nette\Utils\DateTime $to
     * @return type
     */
    public function getUserSummary($ownerId, $users = array(), \Nette\Utils\DateTime $from = null, \Nette\Utils\DateTime $to = null)
    {
        $mCoef = ECommissionType::LEVEL0_VALUE;
        $sCoef = ECommissionType::LEVEL1_VALUE;
        
        if($ownerId)
        {
            $users[] = $ownerId;
        }
        else
        {
            $ownerId = 0;
        }
        
        $rows = $this->getUsersLog($users, $from, $to);
        
        $select = "COUNT(id) AS count, "
                . "SUM(`commission-amount` * IF( user_id = $ownerId, $mCoef, $sCoef)) AS amount, "
                . "MIN(`event-date`) AS from, "
                . "MAX(`event-date`) AS to";
        
        $rows->select($select);
        return $rows->fetch();
    }
}