<?php

namespace App\Model;

use Nette\Http\Session;
use Nette\Database\Table\ActiveRow;

class UserProjectSession extends \Nette\Object
{
    private $section;
    
    public function __construct(Session $session)
    {
        $this->section = $session->getSection('support_project');
    }
    
    public function setProject(ActiveRow $row)
    {
        if (!isset($row->user_id) || !isset($row->user->sid))
        {
            throw new \Exception('Chybný parametr.');
        }
        
        $this->section->projectId = $row->id;
        $this->section->sid = $row->user->sid;
        $this->section->title = $row->title;
    }
    
    public function deleteProject()
    {
        $this->section->projectId = NULL;
        $this->section->sid = NULL;
        $this->section->title = NULL;
    }
    
    public function getProject()
    {
        $object = new \stdClass();
        $object->projectId =  $this->section->projectId;
        $object->sid =  $this->section->sid;
        $object->title =  $this->section->title;
        
        if (!$this->section->sid || !$this->section->projectId)
        {
            return NULL;
        }
        
        return $object;
    }
}
