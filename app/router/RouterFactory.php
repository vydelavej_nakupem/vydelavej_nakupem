<?php

namespace App;

use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;
use App\Model\CategoryRepository;
use App\Model\AdvertisersRepository;
use App\Model\UserProjectRepository;

/**
 * Router factory.
 */
class RouterFactory
{
    /** @var \Nette\DI\Container */
    private $container;
    
    /** @var \Nette\Http\Request */
    private $httpRequest;
    
    /** @var AdvertisersRepository */
    private $advertisersRepository;
    
    /** @var UserProjectRepository */
    private $userProjectRepository;
    
    /** @var CategoryRepository */
    private $categoryRepository;

    private $paletteFolder;
    
    public function __construct(
            $paletteFolder,
            \Nette\DI\Container $container,
            \Nette\Http\Request $httpRequest,
            AdvertisersRepository $advertisersRepository,
            CategoryRepository $categoryRepository,
            UserProjectRepository $userProjectRepository
    )
    {
        $this->container = $container;
        $this->httpRequest = $httpRequest;
        
        $this->paletteFolder = $paletteFolder;
        
        $this->advertisersRepository = $advertisersRepository;
        $this->userProjectRepository = $userProjectRepository;
        $this->categoryRepository = $categoryRepository;
    }
    
    /**
     * @return \Nette\Application\IRouter
     */
    public function createRouter()
    {
        // timto se umozni http i https
        if ($this->httpRequest->isSecured()) 
        {
            Route::$defaultFlags = Route::SECURED;
        }

        $router = new RouteList();
        
        $router[] = new Route($this->paletteFolder.'<path .+>', 'Palette:Palette:image', Route::ONE_WAY);
        $router[] = new Route('/'.$this->paletteFolder.'<path .+>',  'Palette:Palette:image', Route::ONE_WAY);

        // pouze pri spousteni jako CLI - napriklad CRON
        if ($this->container->parameters['consoleMode'])
        {
            $router[] = new \Nette\Application\Routers\CliRouter(array('action' => "Cron:test"));
        }

        $router[] = new Route('cron/<action>', 'Cron:default');
        $router[] = new Route('explorer-api/<action>', 'ExplorerApi:default');
        
        $adminRouter = new RouteList('Admin');
        $adminRouter[] = new Route('admin', 'Default:default');
        $adminRouter[] = new Route('admin/<presenter>/<action>[/<id>]', '<presenter>:<action>:<id>');
        
        $router[] = $adminRouter;
        
        $frontRouter = new RouteList('Front');
        
        $frontRouter[] = new Route('caste-dotazy', 'Faq:default');
        $frontRouter[] = new Route('o-nas', 'AboutUs:default');
        $frontRouter[] = new Route('kontakt', 'Contact:default');
        $frontRouter[] = new Route('hledani', 'Search:default');
        $frontRouter[] = new Route('prihlaseni', 'Sign:in');
        $frontRouter[] = new Route('registrace', 'Sign:register');
        $frontRouter[] = new Route('zapomenuté-heslo', 'Sign:forgottenPassword');
        $frontRouter[] = new Route('registrace', 'Sign:register');
        $frontRouter[] = new Route('profil/nastaveni', 'Profile:settings');
        $frontRouter[] = new Route('profil/moje-konto', 'Profile:account');
        $frontRouter[] = new Route('obchodni-podminky', 'Toc:default');
        $frontRouter[] = new Route('listicka-do-prohlizece', 'BrowserExtension:default');
        $frontRouter[] = new Route('jak-to-funguje', 'Default:howItWorks');
        
        $frontRouter[] = new Route('obchody/<id>',  array(
                
            'presenter' => 'Store',
            'action'    => 'detail',
            'id' => array(
                Route::FILTER_OUT => function ($id)
                {
                    return $this->advertisersRepository->getUrlById($id);                    
                },
                Route::FILTER_IN => function ($url)
                {
                    return $this->advertisersRepository->getIdByUrl($url);               
                }
            ),
        ));
        
        $frontRouter[] = new Route('podporovane-projekty/<id>',  array(
                
            'presenter' => 'UserProject',
            'action'    => 'detail',
            'id' => array(
                Route::FILTER_OUT => function ($id)
                {
                    return $this->userProjectRepository->getUrlById($id);                    
                },
                Route::FILTER_IN => function ($url)
                {
                    return $this->userProjectRepository->getIdByUrl($url);               
                }
            ),
        ));
        
        $frontRouter[] = new Route('kategorie/<id>',  array(
                
            'presenter' => 'Category',
            'action'    => 'detail',
            'id' => array(
                Route::FILTER_OUT => function ($id)
                {
                    return $this->categoryRepository->getUrlById($id);                    
                },
                Route::FILTER_IN => function ($url)
                {
                    return $this->categoryRepository->getIdByUrl($url);               
                }
            ),
        ));
            
            
        $frontRouter[] = new Route('obchody', 'Store:default');
        $frontRouter[] = new Route('podporovane-projekty', 'UserProject:default');
            
        $frontRouter[] = new Route('<presenter>[/<action>][/<id>]', 'Default:default');
        
        
        //pokud vše funguje, odstranit
        //$frontRouter[] = new Route('<presenter>/<action>[/<id>]', '<presenter>:<action>:<id>');
        
        $router[] = $frontRouter;
        

        return $router;
    }

}
