<?php

namespace VydelavejNakupem\Extension;

use Swap\Builder;
use Nette\Caching\Cache;

final class LatteExtension extends \Nette\DI\CompilerExtension
{
    private $filters = array(
        "storeName",
        "getInCurrency",
    );

    public function beforeCompile()
    {
        $latte = $this->getContainerBuilder()->getDefinition('nette.latteFactory');

        foreach($this->filters as $f)
        {
            $latte->addSetup("addFilter", array($f, __CLASS__ . "::" . $f));
        }
    }

    public static function storeName($value)
    {
        if ($value->advertiser_alias)
        {
            return $value->advertiser_alias;
        }
        
        return $value->advertiser_name;
    }

    public static function getInCurrency($value, $from='EUR', $to = 'CZK')
    {
        $rateString = $from.'/'.$to;
        
        $cnbCurrencyRateCache = new Cache(new \Nette\Caching\Storages\FileStorage(__DIR__ . '/../../temp') ,'cnb_currency_rate');
        
        $rate = $cnbCurrencyRateCache->load($rateString);

        if (!$rate)
        {
            // Build Swap with Fixer.io
            $swap = (new Builder())
                ->add('central_bank_of_czech_republic')
                ->build();

            // Get the latest EUR/USD rate
            $rateObject = $swap->latest($from.'/'.$to);

            $rate = $rateObject->getValue();
            
            $cnbCurrencyRateCache->save($rateString, $rate, [
                Cache::EXPIRE => '120 minutes', // akceptuje i sekundy nebo timestamp
            ]);
        }

        
        return $value * $rate;
    }
}
