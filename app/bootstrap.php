<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

//$configurator->setDebugMode(TRUE);  // debug mode MUST NOT be enabled on production server
if (isset($_SERVER["REMOTE_ADDR"]))
{
    if ($_SERVER["REMOTE_ADDR"] == "93.99.39.209") $configurator->setDebugMode(true);
}

$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
        ->addDirectory(__DIR__ . '/../vendor/rtsoft')
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
//$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

Vodacek\Forms\Controls\DateInput::register();

return $container;
