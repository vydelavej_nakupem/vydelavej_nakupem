<?php

namespace App\Enum;

use MabeEnum\Enum;

class EAdvertLanguage extends Enum
{   
    const CODE_EN   = "en";
    const CODE_CS   = "cs";
    
    public static $types = array(
        self::CODE_EN    => self::CODE_EN,
        self::CODE_CS    => self::CODE_CS,
    );
}
