<?php

namespace App\Enum;

use MabeEnum\Enum;

class EPersonType extends Enum
{
    const NATURAL = "natural";
    const LEGAL   = "legal";

    public static $items = array(self::NATURAL => "Fyzická", self::LEGAL => "Právnická");
}
