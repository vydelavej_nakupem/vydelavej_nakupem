<?php

namespace App\Enum;

use MabeEnum\Enum;

class EAdvertRelaionStatus extends Enum
{   
    const JOINED    = "joined";
    const NOTJOINED = "notjoined";
    
    public static $types = array(
        self::JOINED    => "Joined",
        self::NOTJOINED => "Notjoined",
    );
}
