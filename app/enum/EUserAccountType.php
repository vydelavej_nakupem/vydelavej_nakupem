<?php

namespace App\Enum;

use MabeEnum\Enum;

class EUserAccountType extends Enum
{
    const PAYMENT            = 'payment';
    const STORNO             = 'storno';
    const PAYOUT             = 'payout';
    const BONUS              = 'bonus';
    const REFERRAL_REWARD    = 'referral_reward';
    const REFERRAL_STORNO    = 'referral_storno';
    
    public static $types = [
        self::PAYMENT => 'Odměna',
        self::STORNO => 'Storno',
        self::PAYOUT => 'Výplata',
        self::BONUS => 'Bonus',
        self::REFERRAL_REWARD => 'Odměna za přítele',
        self::REFERRAL_STORNO => 'Storno přítele'
    ];
}
