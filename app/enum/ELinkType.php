<?php

namespace App\Enum;

use MabeEnum\Enum;

class ELinkType extends Enum
{
    const BANNER = 'Banner';
    const ADVANCED_LINK = 'Advanced Link';
    const TEXT_LINK = 'Text Link';
    const CONTENT_LINK = 'Content Link';
    const SMARTLINK = 'SmartLink';
    const PRODUCT_CATALOG = 'Product Catalog';
    const KEYWORD_LINK = 'Keyword Link';
}
