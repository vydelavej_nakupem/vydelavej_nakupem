<?php

namespace App\Enum;

use MabeEnum\Enum;

class EAdvertiserDomain extends Enum
{   
    const CZ   = "cz";
    const SK   = "sk";
    
    public static $domains = array(
        self::CZ => self::CZ,
        self::SK => self::SK
    );
}
