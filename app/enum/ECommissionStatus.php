<?php

namespace App\Enum;

use MabeEnum\Enum;

class ECommissionStatus extends Enum
{
    const STATUS_NEW        = "new";
    const STATUS_LOCKED     = "locked";
    const STATUS_EXTENDED   = "extended";
    const STATUS_CLOSED     = "closed";
}
