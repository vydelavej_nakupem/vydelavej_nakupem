<?php

namespace App\Enum;

use MabeEnum\Enum;

class EAdvertiserSource extends Enum
{   
    const CJ   = "cj";
    const VYDELAVEJNAKUPEM   = "vydelavejnakupem";
    
    public static $types = array(
        self::CJ,
        self::VYDELAVEJNAKUPEM
    );
}
