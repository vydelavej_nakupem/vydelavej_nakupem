<?php

namespace App\Enum;

use MabeEnum\Enum;

class ECommissionType extends Enum
{
    const LEVEL0 = "l0";
    const LEVEL1 = "l1";
    
    const LEVEL0_VALUE = 0.4;
    const LEVEL1_VALUE = 0.05;
    
    const LEVEL_DEFAULT_VALUE = self::LEVEL0_VALUE;
    
    public static $levels = array
    (
        self::LEVEL0 => self::LEVEL0_VALUE,
        self::LEVEL1 => self::LEVEL1_VALUE
    );
}
