<?php

namespace App\Enum;

use MabeEnum\Enum;

class EAdvertAccountStatus extends Enum
{   
    const ACTIVE    = "Active";
    const DEACTIVE  = "Deactive";
    
    public static $types = array(
        self::ACTIVE    => 'Aktivní',
        self::DEACTIVE  => 'Neaktivní',
    );
}
