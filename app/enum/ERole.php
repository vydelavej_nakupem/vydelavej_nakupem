<?php

namespace App\Enum;

use MabeEnum\Enum;

class ERole extends Enum
{
    const ADMIN = "admin";
    const USER = "user";
    
    /**
     * IDčka.
     */
    public static $id = array(self::ADMIN => 1, self::USER => 2);
}
