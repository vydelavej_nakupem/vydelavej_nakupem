<?php

namespace App\Enum;

use MabeEnum\Enum;

class ESettings extends Enum
{
    // konstanty musi souhlasit s obsahem tabulky settings
    
    const AAA = "aaa";
    const BBB = "bbb";
    const TEXT_HELP = "text_help";
    
    public static $settings = array(
        self::AAA => "Nastavení A",
        self::BBB => "Nastavení B",
        self::TEXT_HELP => "Nápověda",
    );
}
