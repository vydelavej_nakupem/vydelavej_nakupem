<?php

namespace App\Presenter;

use CjApi\CjApi;
use Nette\Application\Responses\TextResponse;
use App\Model\CommissionLogRepository;
use App\Model\AdvertisersRepository;
use App\Model\CategoryRepository;

class CronPresenter extends \Nette\Application\UI\Presenter
{
    /** @var CjApi */
    private $cjApi;
    
    /** @var CommissionLogRepository */
    private $commissionLogRepository;
    
    /** @var AdvertisersRepository */
    private $advertisersRepository;
    
    /** @var CategoryRepository */
    private $categoryRepository;
    
    function __construct(
            CjApi $cjApi,
            CommissionLogRepository $commissionLogRepository,
            AdvertisersRepository $advertisersRepository,
            CategoryRepository $categoryRepository
    )
    {
        parent::__construct();
        
        $this->cjApi = $cjApi;
        $this->commissionLogRepository = $commissionLogRepository;
        $this->advertisersRepository = $advertisersRepository;
        $this->categoryRepository = $categoryRepository;
    }

    protected function startup()
    {
        parent::startup();
        
        if (!$this->context->parameters['consoleMode'])
        {
            //nechat zakomentované, zatím nemáme možnost to spouštět přes CLI
            //TODO nahradit ošetřením IP adresy
            //throw new Nette\Security\AuthenticationException;
        }
    }
    
    /**
     * url: get-cj-commisions
     */
    public function actionGetCjCommissions()
    {
        $date = new \DateTime();
        $dateTo = clone $date;
        
        $dateFrom = $date->modify('-1 month');
        
        $commissions = $this->cjApi->getCommissions($dateFrom, $dateTo);
        
        if ($commissions)
        {
            $this->commissionLogRepository->saveItems($commissions);
        }
        
        \Tracy\Debugger::log(__METHOD__ . ' proběhla','cron');
        
        $this->terminate();
    }
    
    /**
     * url: get-cj-advertisers
     */
    public function actionGetCjAdvertisers()
    {
        $advertisers = $this->cjApi->getAdvertisers();
        
        if ($advertisers)
        {
            $this->advertisersRepository->saveItemsFromCJ($advertisers);
        }
        
        \Tracy\Debugger::log(__METHOD__ . ' proběhla','cron');
        
        $this->terminate();
    }
    
    /**
     * url: get-cj-advertisers
     */
    public function actionCheckCjAdvertisers()
    {
        $advertisersIds = $this->advertisersRepository->getAllCjAdvertisers()->fetchPairs('advertiser_id', 'advertiser_id');
        $advertisers = $this->cjApi->checkAdvertisers($advertisersIds);
        
        if ($advertisers)
        {
            $this->advertisersRepository->saveItemsFromCJ($advertisers, $advertisersIds);
        }
        
        \Tracy\Debugger::log(__METHOD__ . ' proběhla','cron');
        
        $this->terminate();
    }
    
    /**
     * url: get-cj-links
     */
    public function actionGetCjLinks()
    {
        die;
        set_time_limit(3600);
        $advertisers = $this->advertisersRepository->getAllCjAdvertisers();
        
        foreach ($advertisers as $advertiser)
        {
            $link = $this->cjApi->getLinkForAdvertiser($advertiser->advertiser_id);
            
            if ($link)
            {
                $this->advertisersRepository->saveLink($advertiser->id, $link);
            }
        }
        
        \Tracy\Debugger::log(__METHOD__ . ' proběhla','cron');
        
        $this->terminate();
    }
    
    public function actionGenerateSitemap()
    {
        $stores = $this->advertisersRepository->getAllActiveAdvertisers();
        $categories = $this->categoryRepository->getAllActiveCategories();
        $parameters = \Nette\Utils\ArrayHash::from($this->context->parameters);
        $extraUrls = [];
        
        if($parameters->sitemap)
        {
            $extraUrls = $parameters->sitemap->extraUrls;
        }
        
        $this->template->stores = $stores;
        $this->template->extraUrls = $extraUrls;
        $this->template->categories = $categories;
  
        $this->template->setFile(__DIR__ . "/../templates/{$this->name}/$this->action.latte");
        $xml = $this->template->__toString();
        $path = __DIR__ . "/../../www/sitemap.xml";

        \Nette\Utils\FileSystem::write($path, $xml);
        
        if ($this->context->parameters['consoleMode'])
        {
            $this->sendResponse(new TextResponse("sitemap.xml done"));
        }
        
        \Tracy\Debugger::log(__METHOD__ . ' proběhla','cron');
    }
}
