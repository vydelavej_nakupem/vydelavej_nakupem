<?php

namespace App\Presenter;

use App\FrontModule\Presenter\SignPresenter as SP;

/**
 * Sem se budou davat metody, ktere jsou potreba v BasePresenter a zaroven i v BaseDashboardPresenter.
 */
trait BaseTraitPresenter
{
    public function clearRegisterHash()
    {
        $this->getHttpResponse()->setCookie(SP::COOKIE_REGISTER_HASH, null, new \Nette\Utils\DateTime());
    }
}
