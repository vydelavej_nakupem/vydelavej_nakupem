<?php

namespace App\Presenter;

use App\FrontModule\Presenter\SignPresenter as SP;

abstract class BasePresenter extends \RTsoft\Presenter\BasePresenter
{
    public function clearRegisterHash()
    {
        $cookieRegisterHash = \App\FrontModule\Presenter\SignPresenter::COOKIE_REGISTER_HASH;
        $this->getHttpResponse()->setCookie($cookieRegisterHash, null, new \Nette\Utils\DateTime());
    }
    
    
    /**
     * Vrati dany prarametr z konfigu
     * @param array $parameterNames - jednotlive urovne parametru
     * @return type
     */
    public function getContextParameter($parameterNames)
    {
        $parameters = $this->context->parameters;
        $parameter = null;
        
        if(!is_array($parameterNames))
        {
            $parameterNames = array($parameterNames);
        }
        
        foreach ($parameterNames as $parameterName)
        {
            if(!empty($parameters[$parameterName]))
            {
                $parameter = $parameters = $parameters[$parameterName];
            }
        }
        return $parameter;
    }
    
    public function getCurrentUrl()
    {
        $url = $this->getHttpRequest()->getUrl();
        return $url;
    }
}
