<?php

namespace App\Presenter;

use App\Model\AdvertisersRepository;

class ExplorerApiPresenter extends \Nette\Application\UI\Presenter
{
    
    private $advertisersRepository;
    
    public function __construct(
            AdvertisersRepository $advertisersRepository
    )
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
    }

    protected function startup()
    {
        parent::startup();
        
        if (!$this->context->parameters['consoleMode'])
        {
            //throw new Nette\Security\AuthenticationException;
        }
    }
    
    public function actionCheckUrl($url)
    {
        $advertiserRow = $this->advertisersRepository->getItemByAdvertiserUrl($url);

        $return = [];
        
        if ($advertiserRow)
        {
            $return['supported'] = TRUE;
            $return['options'] = [
                'discount' => $advertiserRow->cashback_customer,
                'cashback_percent' => $advertiserRow->cashback_percent,
                'backurl' => $this->link('//Front:Store:detail', $advertiserRow->id)
            ];
        }
        else
        {
            $return['supported'] = FALSE;
            $return['options'] = [];
        }
        
        $this->sendResponse(new \Nette\Application\Responses\JsonResponse($return));
        $this->terminate();
   }
    
    public function renderGetTemplate($url)
    {
        $response = $this->getHttpResponse();
        $response->addHeader('Access-Control-Allow-Origin', '*');
        $response->addHeader('Access-Control-Allow-Methods', 'GET');
        $response->addHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        
        $html = $this->_loadTemplateByUrl($url);
        if(!$html) {
            $return = array();
        } else {
            $return = array("template" => $html);
        }
        
        $this->sendResponse(new \Nette\Application\Responses\JsonResponse($return));
        $this->terminate();
    }
    
    public function renderGetSupportedShop()
    {
        $response = $this->getHttpResponse();
        $response->addHeader('Access-Control-Allow-Origin', '*');
        $response->addHeader('Access-Control-Allow-Methods', 'GET');
        $response->addHeader('Access-Control-Allow-Headers', 'X-Requested-With');
        
        $advertiserRows = $this->advertisersRepository->getAdvertisersForExplorerApi();
        
        $return = [];
        
        foreach ($advertiserRows as $advertiserRow) {
            $item = [
                'url' => $advertiserRow->program_url,
                'nice_url' => $advertiserRow->advertiser_alias ?: $advertiserRow->advertiser_name,
                'discount' => $advertiserRow->cashback_customer,
                'cashback_percent' => $advertiserRow->cashback_percent,
                'backurl' => $this->link('//Front:Store:detail', $advertiserRow->id)
            ];
            
            $return[] = $item;
        }
        
        $this->sendResponse(new \Nette\Application\Responses\JsonResponse($return));
        $this->terminate();
    }
    
    private function _loadTemplateByUrl($url) {
        if(!$url) {
            return false;
        }
        
        $urlMeta = parse_url($url);
        if(!$urlMeta || !isset($urlMeta['host'])) {
            return false;
        }

        $urlHost     = $urlMeta['host'];
        $link        = $this->link('//ExplorerApi:checkUrl', $urlHost);
        $analysisRaw = file_get_contents($link);
        $analysis    = json_decode($analysisRaw, true);
        $options     = $analysis['options'];

        if(!$analysis['supported']) {
            return false;
        }

        $tmp = $this->createTemplate();
        $tmp->setFile(__DIR__.'/../templates/ExplorerApi/barPlugin.latte');
        $tmp->text = 'Nezapoměňte, za nákup na '.$urlHost.' můžeš získat zpět až '.$options['discount'].'%.';
        $tmp->backurl = $options['backurl'];
        $html = $tmp->__toString();
        return $html;
    }
}
