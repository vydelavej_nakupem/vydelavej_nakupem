<?php

namespace App\Tools;

class Mailer extends \RTsoft\Utils\Mailer
{
    /**
     * Poslání emailu se zapomenutým heslem.
     * @param string $email Email příjemce.
     * @param string $url Link s odkazem na formulář na resetování hesla.
     */
    public function sendForgottenPassword($email, $url)
    {
        $this->params["url"] = $url;
        $this->setRecipients($email);
        $this->setSubject("Zapomenuté heslo");
        $this->setTemplate(__DIR__ . "/../templates/Email/forgotten-password.latte");
        $this->send();
    }
    
    /**
     * Poslání emailu po registraci.
     * @param string $email Email příjemce.
     * @param string $url Link s odkazem na potvrzení registrace.
     */
    public function sendRegistrationEmail($email, $url)
    {
        $this->params["url"] = $url;
        $this->setRecipients($email);
        $this->setSubject("Potvrzení registrace");
        $this->setTemplate(__DIR__ . "/../templates/Email/registration-email.latte");
        $this->send();
    }
    
    /**
     * Odeslani testovaciho mailu - k vyzkouseni teto tridy.
     */
    public function sendTest()
    {
        // dalsi extra parametry, ktere se nacpou do latte
        $this->params["aaa"] = "bbb";
        
        // nastavi se prijemce, zaroven se do latte daji parametry email a hash
        // nutne udelat driv, nez setTemplate()
        $this->setRecipients("vrana@rtsoft.cz");
        
        // nastavi se predmet
        $this->setSubject("Test");
        
        // zada se nazev sablony v adresari app/templates/Mailer
        // tim se zaroven vygeneruje html obsah mailu
        $this->setTemplate(__DIR__ . "/../templates/Email/test.latte");
        
        // a mail uz se jenom odesle
        $this->send();
    }
}
