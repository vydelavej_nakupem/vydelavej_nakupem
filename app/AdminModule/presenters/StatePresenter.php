<?php

namespace App\AdminModule\Presenter;

use App\Model;

/**
 * @persistent(list)
 */
class StatePresenter extends BaseDashboardPresenter
{
    /** @var Model\CourseRepository */
    //protected $courseRepository;
    
    function __construct(Model\StateRepository $stateRepository)
    {
        $this->repository = $stateRepository;
    }

    protected function startup()
    {
        parent::startup();
        
        /*if (!$this->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->flashMessage("Přístup zamítnut.", "danger");
            $this->redirect("Default:default");
        }*/
        
        $this->itemName = "Stát";
        $this->verbSuffix = "";
    }
}
