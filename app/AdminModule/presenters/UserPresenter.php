<?php

namespace App\AdminModule\Presenter;

use Nette\Application\UI\Form;
use App\Model;

/**
 * @persistent(list)
 */
class UserPresenter extends BaseDashboardPresenter
{
    /** @var Model\UserRepository */
    protected $userRepository;
    
    function __construct(Model\UserRepository $userRepository)
    {
        $this->userRepository = $this->repository = $userRepository;
        
        parent::__construct();
    }

    protected function startup()
    {
        parent::startup();
        
        $this->itemName = "Uživatel";
        
        /*if (!$this->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->flashMessage("Přístup zamítnut.", "danger");
            $this->redirect("Default:default");
        */
    }
    
    public function actionExportCsv()
    {
        $rows = $this->userRepository->fetchForExport();
        
        $response = new \OHWeb\Application\Responses\CsvResponse($rows, $this->context->parameters["appTitle"] . " - uživatelé.csv", TRUE);
        
        $response
            ->setGlue(\OHWeb\Application\Responses\CsvResponse::SEMICOLON)
            ->setOutputCharset('cp1250')
            ->setContentType('application/csv')
            ->setDataFormatter('trim');
        
        $this->sendResponse($response);
    }
    
    public function actionExportXls()
    {
        $rows = $this->userRepository->fetchForExport();
        $response = new \RTsoft\Response\SpreadSheetResponse($rows, $this->context->parameters["appTitle"] . " - uživatelé.xlsx", \RTsoft\Response\SpreadSheetResponse::FORMAT_XLSX);
        $this->sendResponse($response);
    }
    
    public function createComponentUserForm()
    {
        $service = $this->context->createService('UserForm');
        
        $service->setRedirectAfterSavingTo('User:');
        
        return $service;
    }
    
    public function createComponentPayoutBackendList()
    {
        return $this->context->getService('PayoutBackendList');
    }
}
