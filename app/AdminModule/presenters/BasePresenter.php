<?php

namespace App\AdminModule\Presenter;

abstract class BasePresenter extends \App\Presenter\BasePresenter
{
    use BaseTraitPresenter;
}
