<?php

namespace App\AdminModule\Presenter;

use App\Model;

/**
 * @persistent(list)
 */
class HelpPresenter extends BasePresenter
{
    /** @var Model\SettingsRepository */
    private $settingsRepository;
    
    function __construct(Model\SettingsRepository $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    public function actionDefault()
    {
        $help = $this->settingsRepository->getByKey(\App\Enum\ESettings::TEXT_HELP());
        $this->template->help = $help;
    }
}


