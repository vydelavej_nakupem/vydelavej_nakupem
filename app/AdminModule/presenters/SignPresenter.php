<?php

namespace App\AdminModule\Presenter;

use Nette\Application\UI;
use Nette\Application\UI\Form;

/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{
    /** \App\Tools\Mailer $mailer */
    private $mailer;
    
    /** @var \App\Model\UserRepository */
    protected $userRepository;
    
    private $secureLoginRequired;
    
    function __construct(\App\Tools\Mailer $mailer, \App\Model\UserRepository $userRepository)
    {
        $this->mailer = $mailer;
        $this->userRepository = $userRepository;
    }
    
    public function actionIn($returnKey = NULL)
    {
        if ($this->user->loggedIn)
        {
            $this->redirect('Default:');
        }
    
        $this->secureLoginRequired = $this->secureLoginRequired();
        
        $this->template->secureLoginEnabled = ($this->secureLoginRequired !== NULL);
        
        if (!$this->secureLoginRequired)
        {
            $this->template->noCodeRequired = TRUE;
        }
    }
    
    public function actionRegister()
    {
        
    }
    
    protected function createComponentRegisterForm()
    {
        $form = $this->context->createService("UserBackendForm");
        $form->register = TRUE;
        
        $form->onRegistrationCompleted[] = $this->registrationCompleted;
        
        return $form;
    }        
    
    public function registrationCompleted($component, $row)
    {
        $this->flashMessage("Děkujeme za registraci, všechny důležité informace jsme Vám poslali na e-mail. Před prvním přihlášením je registraci potřeba potvrdit kliknutím na aktivační odkaz v emailu.", "success permanent");
            
        // overovaci email
        $time = time();
        $url = $this->link("//Sign:verify", array("id" => $row->id, "email" => $row->email, "t" => $time, "c" => $this->userRepository->getVerificationStringForUser($row, $time)));

        $this->mailer->sendRegistrationEmail($row->email, $url);
    }
    
    public function actionVerify($id, $email, $c, $t)
    {
        try
        {
            if (!is_numeric($id) || !is_numeric($t) || $t < time() - 3600)
            {
                throw new \Nette\Application\BadRequestException();
            }

            $row = $this->userRepository->findRow($id);

            if (!$row || $email != $row->email || $c != $this->userRepository->getVerificationStringForUser($row, $t))
            {
                throw new \Nette\Application\BadRequestException();
            }
            
            if ($row->active)
            {
                $this->flashMessage("Vaše registrace již byla potvrzena, můžete se přihlásit.");
            }
            else
            {
                $this->userRepository->activate($row);
                $this->flashMessage("Vaše registrace byla potvrzena, můžete se přihlásit.");
            }
        }
        catch (\Nette\Application\BadRequestException $e)
        {
            $this->flashMessage("Platnost odkazu vypršela.", \Tracy\Debugger::ERROR);
        }
        
        $this->redirect("Sign:in");
    }
    
    public function actionResetPassword($id, $email, $c, $t)
    {
        try
        {
            if (!is_numeric($id) || !is_numeric($t) || $t < time() - 300)
            {
                throw new \Nette\Application\BadRequestException();
            }

            $row = $this->userRepository->findRow($id);

            if (!$row || $email != $row->email || $c != $this->userRepository->getVerificationStringForUser($row, $t))
            {
                throw new \Nette\Application\BadRequestException();
            }
        }
        catch (\Nette\Application\BadRequestException $e)
        {
            $this->flashMessage("Platnost odkazu vypršela.", \Tracy\Debugger::ERROR);
            $this->redirect("Sign:in");
        }
        
        $this["resetPasswordForm"]->setDefaults(array("id" => $id, "email" => $email, "c" => $c, "t" => $t));
    }
    
    /**
     * Sign-in form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentSignInForm()
    {
        $form = new UI\Form;
        $form->addText('username', 'Login:')
             ->setRequired('Zadejte své přihlašovací jméno.');

        $form->addPassword('password', 'Heslo:')
             ->setRequired('Zadejte své heslo.');

        $form->addCheckbox('remember', 'Pamatovat přihlášení');
        
        if ($this->secureLoginRequired)
        {
            $length = $this->context->parameters["secureLogin"]["codeLength"];
            $form->addText("code", "Kód:")
                 ->setAttribute("maxlength", $length)
                 ->setRequired("Zadejte bezpečnostní kód")
                 ->addCondition(UI\Form::FILLED)
                 ->addRule(UI\Form::LENGTH, "Kód musí být {$length} znaků dlouhý.", $length);
            
            $form->addCheckbox("code_ignore", "Dočasně nevyžadovat zadání bezpečnostního kódu")
                 ->setDefaultValue(TRUE);
        }
        
        //$form->addReCaptcha('captcha', NULL, 'Prokažte prosím svou nerobotičnost.');

        $form->addSubmit('send', 'Přihlásit');

        // call method signInFormSubmitted() on success
        $form->onSuccess[] = $this->signInFormSubmitted;
        
        return $form;
    }

    public function signInFormSubmitted($form, $values)
    {
        try 
        {
            // bezpecnostni kod
            if ((!isset($values->code_ignore) || !$values->code_ignore) && $this->secureLoginRequired())
            {
                $v = $this->verifyCode($values->code);
                
                if ($v !== TRUE)
                {
                    $form->addError($v);
                    return;
                }
            }
            
            if ($values->remember)
            {
                $this->user->setExpiration('+ 14 days', FALSE);
            }
            else 
            {
                $this->user->setExpiration('+ 3 days', TRUE);
            } 
            
            // normalni prihlaseni
            $this->user->login($values->username, $values->password);
        } 
        catch (\Nette\Security\AuthenticationException $e) 
        {
            $form->addError($e->getMessage());
            return;
        }

        // vraceni se tam, odkud prisel
        $key = $this->getParameter("returnKey");
        if ($key)
        {
            $this->restoreRequest($key);
        }   
        else
        {
            $this->redirect('Default:');
        }
    }
    
    protected function createComponentForgottenPasswordForm()
    {
        $form = new Form;
        
        $form->addText('username_or_email', '')
             ->setRequired('Zadejte své přihlašovací jméno nebo email.');

        $form->addReCaptcha('captcha', NULL, 'Prokažte prosím svou nerobotičnost.');
        
        $form->addSubmit('send', 'Odeslat');
        
        $form->onSuccess[] = $this->forgottenPasswordFormSubmitted;
        
        return $form;
    }
    
    public function forgottenPasswordFormSubmitted($form, $values)
    {
        $xUrl = $form->getHttpData($form::DATA_TEXT, 'x_url');
        
        if (empty($xUrl) || $xUrl != "nospam")
        {
            throw new \Nette\Application\ForbiddenRequestException();
        }
        
        $v = $values["username_or_email"];
        
        if (!empty($v))
        {
            $row = $this->userRepository->findByLogin($v);
            
            if (!$row)
            {
                $row = $this->userRepository->findByEmail($v);
            }
            
            if ($row && !empty($row->email) && $row->active)
            {
                $time = time();
                $url = $this->link("//Sign:resetPassword", array("id" => $row->id, "email" => $row->email, "t" => $time, "c" => $this->userRepository->getVerificationStringForUser($row, $time)));
                
                $this->mailer->sendForgottenPassword($row->email, $url);
            }
            
            // pro zmateni nepritele se v kazdem pripade zobrazi hlaska, ze email byl odeslan
            $this->flashMessage("Na emailovou adresu k zadanému účtu bude odeslán email s instrukcemi pro resetování hesla.");
        }
        
        $this->redirect("Sign:in");
    }
    
    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentResetPasswordForm()
    {
        $form = new Form();
        
        $form->addPassword('newPassword', 'Nové heslo:', 30)
                ->addRule(Form::MIN_LENGTH, 'Nové heslo musí mít alespoň %d znaků.',  \App\Model\UserRepository::MIN_PSWD_LEN);

        $form->addPassword('confirmPassword', 'Heslo znovu:', 30)
                ->addRule(Form::FILLED, 'Nové heslo je nutné zadat ještě jednou pro potvrzení.')
                ->addRule(Form::EQUAL, 'Zadná hesla se musí shodovat.', $form['newPassword']);
        
        $form->addHidden("id");
        $form->addHidden("email");
        $form->addHidden("t");
        $form->addHidden("c");

        $form->addSubmit('set', 'Změnit heslo');
        $form->onSuccess[] = $this->resetPasswordFormSubmitted;

        return $form;
    }

    public function resetPasswordFormSubmitted(Form $form, \stdClass $values)
    {
        try
        {
            $xUrl = $form->getHttpData($form::DATA_TEXT, 'x_url');

            if (empty($xUrl) || $xUrl != "nospam")
            {
                throw new \Nette\Application\ForbiddenRequestException();
            }
        
            $row = $this->userRepository->findByEmail($values["email"]);
            
            if (!$row || $row->id != $values["id"] || $values["c"] != $this->userRepository->getVerificationStringForUser($row, $values["t"]))
            {
                throw new \Nette\Application\BadRequestException();
            }
            
            // zmena hesla
            $this->userRepository->setPassword($row->id, $values["newPassword"]);

            // hotovo
            $this->flashMessage('Heslo bylo změněno, nyní se můžete přihlásit.', 'success');
            $this->redirect('Sign:in');
        }
        catch (\Nette\Application\BadRequestException $e)
        {
            \Tracy\Debugger::log($e, \Tracy\Debugger::ERROR);
            $form->addError('Neplatná akce.');
        }
    }
    
    /**
     * Overeni bezpecnostniho kodu.
     * @param string $code
     * @return string|boolean
     */
    private function verifyCode($code)
    {
        $code = urlencode($code);
        $projectId = $this->context->parameters["secureLogin"]["projectId"];
        $t = time();
        $hash = md5("guasg-53qtgsas_{$code}_gd56sf5s_{$projectId}_tgwa84fgasd-df-as_{$t}_hhh");

        $url = $this->context->parameters["secureLogin"]["verificationUrl"] . "?code={$code}&projectId={$projectId}&h={$hash}&t={$t}";

        @$response = file_get_contents($url);

        if ($response == "BAD_ACCESS")
        {
            \Tracy\Debugger::log("secureLogin {$response} URL: {$url}", \Tracy\Debugger::ERROR);
            return "Ověření se nezdařilo, nezdařil se přístup k ověřovací službě.";
        }
        elseif ($response == "ERROR")
        {
            \Tracy\Debugger::log("secureLogin {$response} URL: {$url}", \Tracy\Debugger::ERROR);
            return "Ověření se nezdařilo, došlo k vnitřní chybě ověřovací služby.";
        }
        elseif ($response == "ACCESS_DENIED")
        {
            \Tracy\Debugger::log("secureLogin {$response} URL: {$url}", \Tracy\Debugger::DEBUG);
            return "Ověření se nezdařilo, byl zadán neplatný kód.";
        }
        elseif ($response != "ACCESS_GRANTED")
        {
            \Tracy\Debugger::log("secureLogin {$response} URL: {$url}", \Tracy\Debugger::ERROR);
            return "Ověření se nezdařilo, došlo k neznámé chybě.";
        }
        
        return TRUE;
    }

    public function actionOut()
    {
        $this->user->logout();
        $this->flashMessage('Odhlášení proběhlo úspěšně.');
        $this->redirect('in');
    }
}