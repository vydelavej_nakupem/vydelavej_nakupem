<?php

namespace App\AdminModule\Presenter;

use Nette;
use App\Model;

class DefaultPresenter extends BasePresenter
{
    protected function startup()
    {
        parent::startup();

        if (!$this->getUser()->isLoggedIn())
        {
            $this->redirect('Sign:in');
        }
    }
    
    public function actionDefault()
    {
        
    }
    
    protected function createComponentUserStatList()
    {
        /** @var $c \App\Component\UserStatList */
        $c = $this->context->createService("UserStatList");
        return $c;
    }
}
