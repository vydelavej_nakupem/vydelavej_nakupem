<?php

namespace App\AdminModule\Presenter;

use \App\Model;
use \Nette\Application\UI\Form;

class SettingsPresenter extends BasePresenter
{
    /** @var Model\SettingsRepository */
    protected $settingsRepository;
    
    function __construct(Model\SettingsRepository $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }
    
    protected function startup()
    {
        parent::startup();
        
        /*if (!$this->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->flashMessage("Přístup zamítnut.", "danger");
            $this->redirect("Default:default");
        }*/
    }
    
    public function actionDefault()
    {
        if (!$this["formSettings"]->isSubmitted())
        {
            $defaults = $this->settingsRepository->findAll()->fetchPairs("key", "value");
            
            $this["formSettings"]->setDefaults($defaults);
        }
    }
    
    public function createComponentFormSettings()
    {
        $form = new \RTsoft\Form\BootstrapForm();
        $form->setMethod(Form::POST);
        
        foreach (\App\Enum\ESettings::$settings as $k => $v)
        {
            // polozky zacinajici na text budou textarea, ostatni textbox
            if (strpos($k, "text_") === 0)
            {
                $input = $form->addTextArea($k, $v);
                
                // ckeditor
                $input->getControlPrototype()->class('ckeditor');
                $form->getElementPrototype()->onsubmit('CKEDITOR.instances["' . $input->getHtmlId() . '"].updateElement();');
            }
            else
            {
                $input = $form->addText($k, $v);
            }

            // vsechny povinne
            $input->addRule(Form::FILLED, "Položka '%label' musí být zadána.");
        }
        
        // ulozit
        $form->addSubmit("save", "Uložit");
        
        $form->onSuccess[] = $this->saveFormSettings;
        
        return $form;
    }
    
    public function saveFormSettings(Form $form, \stdClass $values)
    {
        $this->settingsRepository->saveArray($values);
        $this->flashMessage("Nastavení bylo uloženo.", "info");
        $this->redirect("this");
    }
}
