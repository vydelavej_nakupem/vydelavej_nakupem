<?php

namespace App\AdminModule\Presenter;

use App\Model\UserProjectRepository;

/**
 * @persistent(list)
 */
class UserProjectPresenter extends BaseDashboardPresenter
{
    /** @var UserProjectRepository */
    protected $userProjectRepository;
    
    function __construct(UserProjectRepository $userProjectRepository)
    {
        $this->userProjectRepository = $this->repository = $userProjectRepository;
        
        parent::__construct();
    }

    protected function startup()
    {
        parent::startup();
    }
    
    public function createComponentUserForm()
    {
        $service = $this->context->createService('UserProjectForm');
        
        $service->setRedirectAfterSavingTo('UserProject:');
        
        return $service;
    }
}
