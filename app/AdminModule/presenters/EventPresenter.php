<?php

namespace App\AdminModule\Presenter;

use App\Model\AdvertisersRepository;
use App\Model\AdvertiserEventRepository;

/**
 * @persistent(list)
 */
class EventPresenter extends BaseDashboardPresenter
{
    private $advertisersRepository;
    private $advertiserEventRepository;
    
    public function __construct(
            AdvertisersRepository $advertisersRepository,
            AdvertiserEventRepository $advertiserEventRepository)
    {
        parent::__construct();
        
        $this->repository = $this->advertisersRepository = $advertisersRepository;
        $this->advertiserEventRepository = $advertiserEventRepository;
    }
    

}
