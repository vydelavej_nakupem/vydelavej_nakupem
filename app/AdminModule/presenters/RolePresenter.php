<?php

namespace App\AdminModule\Presenter;

use App\Model;

/**
 * @persistent(list)
 */
class RolePresenter extends BaseDashboardPresenter
{
    /** @var Model\RoleRepository */
    //protected $roleRepository;
    
    function __construct(Model\RoleRepository $roleRepository)
    {
        $this->repository = $roleRepository;
        
        parent::__construct();
    }

    protected function startup()
    {
        parent::startup();
        
        $this->itemName = "Role";
        $this->verbSuffix = "a";
        
        /*if (!$this->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->flashMessage("Přístup zamítnut.", "danger");
            $this->redirect("Default:default");
        }*/
    }
}
