<?php

namespace App\AdminModule\Presenter;

abstract class BaseDashboardPresenter extends \RTsoft\Presenter\BaseDashboardPresenter
{
    use BaseTraitPresenter;
    
    protected function startup()
    {
        $this->saveAndRestoreStateFromSession = TRUE;
        
        parent::startup();
    }

    /** @var \App\Model\StateRepository */
    private $stateRepository;

    public function injectStateRepository(\App\Model\StateRepository $stateRepository)
    {
        $this->stateRepository = $stateRepository;
    }

    public function actionFetchItemsForWhisperer($q, $name)
    {
        //\Tracy\Debugger::log($q, "fi");\Tracy\Debugger::log($name, "fi");

        $repository = NULL;

        switch ($name)
        {
            case "state_id":
                $repository = $this->stateRepository;
                break;
        }

        if ($repository)
        {
            $data = $repository->fetchItemsForWhisperer($q);
        }
        else
        {
            $data = array();
        }

        $this->sendResponse(new \Nette\Application\Responses\JsonResponse($data));
    }
}
