<?php

namespace App\AdminModule\Presenter;

/**
 * Sem se budou davat metody, ktere jsou potreba v BasePresenter a zaroven i v BaseDashboardPresenter.
 */
trait BaseTraitPresenter
{
    use \App\Presenter\BaseTraitPresenter;
}
