<?php

namespace App\AdminModule\Presenter;

use Nette\Application\UI\Form;
use App\Model;

/**
 * @persistent(list)
 */
class ProfilePresenter extends BasePresenter
{
    protected $presenterNamesWithPublicAccess = ['Sign', 'Error', 'Admin:Sign', 'Admin:Error'];
    
    /** @var \App\Tools\Authenticator */
    private $authenticator;
    
    /** @var Model\StateRepository */
    protected $stateRepository;
    
    /** @var Model\UserRepository */
    protected $userRepository;
    
    function __construct(\App\Tools\Authenticator $authenticator, Model\StateRepository $stateRepository, Model\UserRepository $userRepository)
    {
        $this->authenticator = $authenticator;
        $this->stateRepository = $stateRepository;
        $this->userRepository = $userRepository;
    }

    protected function startup()
    {
        parent::startup();
        
        /*if (!$this->user->isInRole(\App\Enum\ERole::USER))
        {
            $this->flashMessage("Přístup zamítnut.", "danger");
            $this->redirect("Default:default");
        }*/
    }
    
    public function actionSettings()
    {
        
    }

    protected function createComponentUserForm()
    {
        $form = $this->context->createService("UserBackendForm");
        $form->profile = TRUE;
        
        return $form;
    }  
    
    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentPasswordForm()
    {
        $form = new \RTsoft\Form\BootstrapForm();
        
        $form->addPassword('newPassword', 'Nové heslo:', 30)
                ->addRule(Form::MIN_LENGTH, 'Nové heslo musí mít alespoň %d znaků.',  Model\UserRepository::MIN_PSWD_LEN);

        $form->addPassword('confirmPassword', 'Potvrzení hesla:', 30)
                ->addRule(Form::FILLED, 'Nové heslo je nutné zadat ještě jednou pro potvrzení.')
                ->addRule(Form::EQUAL, 'Zadná hesla se musí shodovat.', $form['newPassword']);

        $form->addSubmit('set', 'Změnit heslo');
        $form->onSuccess[] = $this->passwordFormSubmitted;

        return $form;
    }

    public function passwordFormSubmitted(Form $form, \stdClass $values)
    {
        try
        {

            $this->authenticator->setPassword($this->user->id, $values->newPassword);
            $this->user->logout();

            // hotovo
            $this->flashMessage('Heslo bylo změněno.', 'success');
            $this->redirect('Sign:in');
        }
        catch (\Nette\Security\AuthenticationException $e)
        {
            $form->addError('Změna hesla se nepodařila.');
        }
    }
}
