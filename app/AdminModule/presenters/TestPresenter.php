<?php

namespace App\AdminModule\Presenter;

/**
 * Presenter pro testování různých kravin.
 */
class TestPresenter extends BasePresenter
{
    /** \App\Tools\Mailer $mailer */
    private $mailer;
    
    function __construct(\App\Tools\Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    
    public function actionSendTestEmail()
    {
        $this->mailer->sendTest();
        $this->terminate();
    }
}
