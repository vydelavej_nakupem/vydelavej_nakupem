<?php

namespace App\AdminModule\Presenter;

use App\Model\AdvertisersRepository;

/**
 * @persistent(list)
 */
class StorePresenter extends BaseDashboardPresenter
{
    protected $presenterNamesWithPublicAccess = ['Front:Store', 'Front:Sign', 'Error'];
    
    private $advertisersRepository;
    
    
    public function __construct(AdvertisersRepository $advertisersRepository)
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
    }
    
    protected function switchRowWithId($id, $repository = NULL)
    {
        $row = $this->advertisersRepository->findRow($id);
        
        if($row)
        {
            $row->update(array(AdvertisersRepository::DB_COL_ACTIVE => !$row->active));
            $op = $row->active ? "aktivován" : "deaktivován";
            
            $this->flashMessage("Obchod $op.", 'success');

        }
    }
    
    public function actionEdit($id, $returnTo = NULL)
    {
        $advertiser = $this->advertisersRepository->findRow($id);
        $this->template->advertiser = $advertiser;
        
        parent::actionEdit($id, $returnTo);
    }
    
    public function createComponentList()
    {
        return $this->context->createService('StoreBackendList');
    }
    
    public function createComponentForm()
    {
        return $this->context->createService('StoreBackendForm');
    }
}
