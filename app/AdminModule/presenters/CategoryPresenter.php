<?php

namespace App\AdminModule\Presenter;

use App\Model\CategoryRepository;
use App\Model\AdvertisersRepository;
use App\Model\AdvertiserCategoryRepository;

/**
 * @persistent(list)
 */
class CategoryPresenter extends BaseDashboardPresenter
{
    private $categoryRepository;
    private $advertisersRepository;
    private $advertiserCategoryRepository;
    
    
    public function __construct(
            CategoryRepository $categoryRepository,
            AdvertisersRepository $advertisersRepository,
            AdvertiserCategoryRepository $advertiserCategoryRepository)
    {
        parent::__construct();
        
        $this->categoryRepository = $categoryRepository;
        $this->advertisersRepository = $advertisersRepository;
        $this->advertiserCategoryRepository = $advertiserCategoryRepository;
    }
    
    protected function switchRowWithId($id, $repository = NULL)
    {
        $row = $this->categoryRepository->findBy(array(
            
            CategoryRepository::DB_COL_PARENT_ID => $id
                
        ))->fetch();
        
        if($row)
        {
            $this->flashMessage("Kategorie je rodičovská.", 'warning');
            $row = null;
        }
        else
        {
            $row = $this->advertiserCategoryRepository->findBy(array(

                AdvertiserCategoryRepository::DB_COL_CATEGORY_ID => $id

            ))->fetch();
        }
        
        if($row)
        {
            $this->flashMessage("Kategorie je přirazená k obchodu.", 'warning');
            $row = null;
        }
        else
        {
            $row = $this->categoryRepository->findRow($id);
        }
        
        if($row)
        {
            $row->update(array(CategoryRepository::DB_COL_ACTIVE => !$row->active));
            $op = $row->active ? "aktivována" : "deaktivována";
         
            $this->flashMessage("Kategorie $op.", 'success');
        }
    }
    
    public function actionEdit($id, $returnTo = NULL)
    {
        $category = $this->categoryRepository->findRow($id);
        $this->template->category = $category;
        
        parent::actionEdit($id, $returnTo);
    }
    
    
    public function createComponentList()
    {
        return $this->context->createService('AdvertiserCategoryList');
    }
    
    public function createComponentForm()
    {
        return $this->context->createService('AdvertiserCategoryForm');
    }
}
