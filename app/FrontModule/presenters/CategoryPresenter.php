<?php

namespace App\FrontModule\Presenter;

use App\Model\CategoryRepository;

/**
 * @persistent(list)
 */
class CategoryPresenter extends BasePresenter
{
    private $id = NULL;
    
    private $categoryRepository;
    
    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct();
        
        $this->categoryRepository = $categoryRepository;
    }
    public function actionDetail($id)
    {
        $this->id = $id;
        $category = $this->categoryRepository->getActiveCategory($id);
        
        if (!$category)
        {
            throw new \Nette\Application\BadRequestException;
        }
        
        $this->template->category = $category;
    }
    
    public function createComponentStoreCategoryList()
    {
        $service = $this->context->createService('StoreCategoryList');
        $service->setCategoryId($this->id);
        
        return $service;
    }
}