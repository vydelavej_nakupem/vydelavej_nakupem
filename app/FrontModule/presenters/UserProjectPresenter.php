<?php

namespace App\FrontModule\Presenter;

use App\Model\UserProjectRepository;
use App\Model\UserProjectSession;

/**
 * @persistent(list)
 */
class UserProjectPresenter extends BasePresenter
{
    private $userProjectRepository;
    private $userProjectSession;
    
    private $imageFolder;
    
    public function __construct(
        \Nette\DI\Container $container,
        UserProjectRepository $userProjectRepository,
        UserProjectSession $userProjectSession
    )
    {
        parent::__construct();
        
        $this->imageFolder = $container->getParameters()['images']['userProject'];
        $this->userProjectRepository = $userProjectRepository;
        $this->userProjectSession = $userProjectSession;
    }
    
    public function actionDetail($id)
    {
        $project = $this->userProjectRepository->getActiveProject($id);
        
        if(!$project)
        {
            throw new \Nette\Application\BadRequestException;
        }
        $this->template->project = $project;
    }
    
    public function actionDefault()
    {
        $projects = $this->userProjectRepository->getActiveProjects();
        $this->template->imgUrl = $this->imageFolder;
        $this->template->projects = $projects;
    }
    
    public function handleSupport($id)
    {
        
        $userProject = $this->userProjectRepository->getActiveProject($id);
        
        if (!$userProject)
        {
            $this->flashMessage('Při výběru projektu došlo k chybě.', 'error');
        }
        else
        {
            $this->userProjectSession->setProject($userProject);
        }
        
        $this->redirect('this');
    }
}
