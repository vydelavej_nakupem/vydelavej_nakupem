<?php

namespace App\FrontModule\Presenter;

use \App\Model\AdvertisersRepository;

/**
 * @persistent(list)
 */
class SearchPresenter extends BasePresenter
{
    private $advertisersRepository;
    
    
    public function __construct(AdvertisersRepository $advertisersRepository)
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
    }

    public function actionDefault($q = null)
    {
    }
    
    public function createComponentStoreSearchList()
    {
        return $this->context->createService('StoreSearchList');
    }
}
