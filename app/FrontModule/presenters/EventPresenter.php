<?php

namespace App\FrontModule\Presenter;

use App\Model\AdvertiserEventRepository;

/**
 * @persistent(list)
 */
class EventPresenter extends BasePresenter
{
    private $id = NULL;
    
    private $advertiserEventRepository;
    
    public function __construct(AdvertiserEventRepository $advertiserEventRepository)
    {
        parent::__construct();
        
        $this->advertiserEventRepository = $advertiserEventRepository;
    }
    
    public function actionDetail($id)
    {
        $this->id = $id;
        $item = $this->advertiserEventRepository->getActiveItem($id);
        
        if (!$item)
        {
            throw new \Nette\Application\BadRequestException;
        }
        
        $this->template->item = $item;
    }
}