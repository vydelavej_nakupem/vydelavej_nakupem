<?php

namespace App\FrontModule\Presenter;

use App\Model\AdvertisersRepository;

/**
 * @persistent(list)
 */
class StorePresenter extends BasePresenter
{
    protected $presenterNamesWithPublicAccess = ['Front:Store', 'Front:Sign', 'Error'];
    
    private $advertisersRepository;
    
    private $id;
    
    public function __construct(AdvertisersRepository $advertisersRepository)
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
    }
    
    public function actionDetail($id)
    {
        $this->id = $id;
        $store = $this->advertisersRepository->getActiveStore($id);
        
        if(!$store) // || $store->{AdvertisersRepository::DB_COL_ACCOUNT_STATUS} != \App\Enum\EAdvertAccountStatus::ACTIVE) - enum neex.
        {
            throw new \Nette\Application\BadRequestException;
        }
        $this->template->store = $store;
    }
    
    public function createComponentFavouriteStore()
    {
        return $this->context->createService('FavouriteStore');
    }
    
    public function createComponentFavouriteStoreList()
    {
        return $this->context->createService('FavouriteStoreList');
    }
    
    public function createComponentStoreDetail()
    {
        /* @var $service \App\Component\StoreDetail */
        $service = $this->context->createService('StoreDetail');
        $service->setStoreRequest($this->storeRequest);
        
        return $service;
    }
    
    public function createComponentStoreEventList()
    {
        /* @var $service \App\Component\EventList */
        $service = $this->context->createService('StoreEventList');
        $service->setId($this->id);
        
        return $service;
    }
}
