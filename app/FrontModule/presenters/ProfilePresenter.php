<?php

namespace App\FrontModule\Presenter;

use Nette\Application\UI\Form;
use App\Model;

/**
 * @persistent(list)
 */
class ProfilePresenter extends BasePresenter
{
    /** @var \App\Tools\Authenticator */
    private $authenticator;
    
    /** @var Model\StateRepository */
    protected $stateRepository;
    
    /** @var Model\UserRepository */
    protected $userRepository;
    
    /** @var Model\UserAccountRepository */
    protected $userAccountRepository;
    
    const LAST_REWARD_LIMIT = 5;
    
    function __construct(
            \App\Tools\Authenticator $authenticator, 
            Model\StateRepository $stateRepository, 
            Model\UserRepository $userRepository,
            Model\UserAccountRepository $userAccountRepository)
    {
        $this->authenticator = $authenticator;
        $this->stateRepository = $stateRepository;
        $this->userRepository = $userRepository;
        $this->userAccountRepository = $userAccountRepository;
    }

    protected function startup()
    {
        parent::startup();
        
        if (!$this->user->isInRole(\App\Enum\ERole::USER))
        {
            $this->redirect("Default:default");
        }
    }
    
    public function actionSettings()
    {
    }
    
    public function actionInvite()
    {
        $userId = $this->user->id;
        $slavesCount = $this->userRepository->getOwnerUsers($userId);
        
        $this->template->slavesCount = $slavesCount;
    }
    
    public function actionAccount()
    {
        if(!$this->user->isLoggedIn())
        {
            $this->flashMessage("Pro tuto akci se přihlaste.", "warning");
            $this->redirect("Default:");
        }
        
        $userId = $this->user->id;
        $state = $this->userAccountRepository->getCurrentState($userId);
        $lastPositive = $this->userAccountRepository->getLastPositiveReward($userId);
        $lastNegative = $this->userAccountRepository->getPayoutReward($userId);
        $forPaycheck = $this->userAccountRepository->getForpaycheckReward($userId);
        $slavesCount = $this->userRepository->getOwnerUsers($userId);
        
        $lastRewards = $this->userAccountRepository->getLastRewards($this->getUser()->getId())->limit(self::LAST_REWARD_LIMIT);
        
        
        $this->template->lastRewards = $lastRewards;
        $this->template->currentState = $state;
        $this->template->lastPositive = $lastPositive;
        $this->template->lastNegative = $lastNegative;
        $this->template->forPaycheck = $forPaycheck;
        $this->template->slavesCount = $slavesCount;
    }

    protected function createComponentUserForm()
    {
        $form = $this->context->createService("UserForm");
        $form->profile = TRUE;
        
        return $form;
    }  
    
    /**
     * @return Nette\Application\UI\Form
     */
    protected function createComponentPasswordForm()
    {
        $form = new \RTsoft\Form\BootstrapForm();
        
        $form->addPassword('oldPassword', 'Staré heslo:', 30)
                ->addRule(Form::FILLED, 'Je nutné zadat staré heslo.');
        $form->addPassword('newPassword', 'Nové heslo:', 30)
                ->addRule(Form::MIN_LENGTH, 'Nové heslo musí mít alespoň %d znaků.',  Model\UserRepository::MIN_PSWD_LEN)
                ->addRule(~Form::EQUAL, 'Nové heslo nesmí být stejné jako staré..', $form['oldPassword']);

        $form->addSubmit('set', 'Změnit heslo');
        $form->onSuccess[] = $this->passwordFormSubmitted;

        return $form;
    }
    
    protected function createComponentRegisterLinkForm()
    {
        $form = new \RTsoft\Form\BootstrapForm();
        $registerUrl = $this->getRegisterUrl();
        
        $form->addText('register_url', 'Link pro pozvání:')->setDefaultValue($registerUrl);
        $form->addButton('copy_url', 'Kopírovat');

        return $form;
    }

    public function passwordFormSubmitted(Form $form, \stdClass $values)
    {
        try
        {
            $this->authenticator->authenticate(array(
                $this->user->identity->email,
                $values->oldPassword
            ));

            /*// provereni jestli uz nove zadane heslo nebylo driv
            if ($this->userRepository->isPasswordInHistory($this->user->id, $values->newPassword))
            {
                $form->addError('Zadané heslo již bylo použito v minulosti.');
                return;
            }*/

            /*// zapis zmeny hesla
            $this->userRepository->userChangedPassword($this->user->id);
            $this->user->identity->has_to_change_password = 0;*/

            $this->authenticator->setPassword($this->user->id, $values->newPassword);

            // hotovo
            $this->user->logout();
            $this->flashMessage('Heslo bylo změněno.', 'success');
            $this->redirect('Sign:in');
        }
        catch (\Nette\Security\AuthenticationException $e)
        {
            $form->addError('Zadané heslo není správné.');
        }
    }
    
    public function handlePayOut()
    {
        $userId = $this->user->id;
        $payLimit = $this->getContextParameter('payLimit');
        if($this->userAccountRepository->createPayOutRequest($userId, $payLimit))
        {                
            $this->redrawControl('account-info');
        }
    }
    
    public function getRegisterUrl()
    {
        $registerUrl = null;
        
        if($this->user->isLoggedIn())
        {
            $registerUrl = substr($this->getHttpRequest()->getUrl()->baseUrl, 0, -1) . 
                    $this->link('Sign:register', array('register_hash' => $this->userRepository->getUserSid($this->user->id)));
        }
        return $registerUrl;
    }
}
