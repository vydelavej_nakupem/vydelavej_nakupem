<?php

namespace App\FrontModule\Presenter;

use App\Model\CategoryRepository;
use App\Model\UserProjectSession;

abstract class BasePresenter extends \App\Presenter\BasePresenter
{
    protected $storeRequest;
    
    protected $presenterNamesWithPublicAccess = [
        'Front:Default',
        'Front:Profile',
        'Front:Sign',
        'Front:Error',
        'Front:AboutUs',
        'Front:Contact',
        'Front:Toc',
        'Front:Search',
        'Front:Faq',
        'Front:Category',
        'Front:BrowserExtension',
        'Front:Event',
        'Front:UserProject'
    ];
    
    private $categoryRepository;
    
    public function injectCategoryRepository(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    
    public function beforeRender()
    {
        parent::beforeRender();
        
        $this->template->storeRequest = $this->storeRequest = $this->storeRequest();
        $this->template->mainCategories = $this->categoryRepository->getAllActiveMainCategories();
    }
    
    public function isDevelopVersion()
    {
        $address = $this->getHttpRequest()->getRemoteAddress();
        return $address != '127.0.0.1';
    }
    
    public function getUserId()
    {
        return $this->user ? $this->user->id : null;
    }
    
    /**
     * Vrati dany prarametr z konfigu
     * @param array $parameterNames - jednotlive urovne parametru
     * @return type
     */
    public function getContextParameter($parameterNames)
    {
        $parameters = $this->context->parameters;
        $parameter = null;
        
        if(!is_array($parameterNames))
        {
            $parameterNames = array($parameterNames);
        }
        
        foreach ($parameterNames as $parameterName)
        {
            if(!empty($parameters[$parameterName]))
            {
                $parameter = $parameters = $parameters[$parameterName];
            }
        }
        return $parameter;
    }
    
    public function getCurrentUrl()
    {
        $url = $this->getHttpRequest()->getUrl();
        return $url;
    }
    
    public function createComponentUserAccountState()
    {
        return $this->context->getService("UserAccountState");
    }
    
    public function createComponentCookieBar()
    {
        return $this->context->getService("CookieBar");
    }
}
