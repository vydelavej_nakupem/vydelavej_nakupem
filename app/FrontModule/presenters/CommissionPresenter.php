<?php

namespace App\FrontModule\Presenter;

use Nette\Application\UI\Form;
use App\Model;

/**
 * @persistent(list)
 */
class CommissionPresenter extends BasePresenter
{
    protected $presenterNamesWithPublicAccess = ['Front:Commission', 'Front:Sign', 'Error'];
    
    public function actionSummary()
    {
        $this->template->userId = $this->getUserId();
    }
    
    public function createComponentUserSummaryList()
    {
        return $this->context->getService('UserSummaryList');
    }
    
    public function createComponentUserSummary()
    {
        return $this->context->getService('UserSummary');
    }
}
