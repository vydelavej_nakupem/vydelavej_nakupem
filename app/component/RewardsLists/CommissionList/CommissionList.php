<?php

namespace App\Component;

use App\Model\UserAccountRepository;

/**
 * Vypisuje seznam připsaných provizí
 */
class CommissionList extends BaseComponent
{
    private $userAccountRepository;
    
    public function __construct(UserAccountRepository $userAccountRepository)
    {
        parent::__construct();
        
        $this->userAccountRepository = $userAccountRepository;
    }
    
    public function render($params = NULL)
    {
        $this->template->commissions = $this->userAccountRepository->getCommissionsForListFrontend();
        
        parent::render($params);
    }
}
