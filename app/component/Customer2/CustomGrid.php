<?php

namespace App\Component;

class CustomGrid extends \RTsoft\Component\GridForm\DataGrid
{
    public function render()
    {
        // vlastni filtr - pridani aaa
        $this->template->addFilter('aaa', function ($s)
        {
            return $s . " aaa";
        }
        );
        
        // doplneni dalsich veci do template, ktere lze pouzivat i v NecoList_grid.latte, ale lepsi je pouzit $this->gridTemplate->neco = "xyz";
        //$this->template->ccc = "Cccc";
        
        parent::render();
    }

}
