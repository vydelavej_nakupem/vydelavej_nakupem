<?php

namespace App\Component;

use App\Model;
use RTsoft\Component\GridForm\GridComponent;
use RTsoft\Model\GridFormRepository as R;

class StateList extends ListComponent
{            
    /** @var \Model\StateRepository */
    protected $stateRepository;
    
    /** @var Model\UserRepository */
    protected $userRepository;
    
    function __construct(Model\StateRepository $stateRepository, Model\UserRepository $userRepository)
    {
        $this->stateRepository = $stateRepository;
        $this->userRepository = $userRepository;

        parent::__construct($stateRepository);
    }
    
    protected function createComponentDataGrid()
    {
        //$this->stateRepository->columns["state_id"][R::COLUMN_ITEMS] = $this->stateRepository->fetchStatesForSelectbox(TRUE);
        
        return parent::createComponentDataGrid();
    }
    
    /*public function createFilterForm()
    {
        if ($this->presenter->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->stateRepository->columns["user_id"][R::COLUMN_ITEMS] = $this->userRepository->fetchItemsForSelectbox(array("role_id" => \App\Enum\ERole::LECTOR));
        }
        
        return parent::createFilterForm();
    }*/
    
    protected function modifyOrder(&$order)
    {
        if (!$order)
        {
            // default razeni
            $order = "state.name ASC";
        }
        else
        {
            if ($order[0] == "state_id")
            {
                $order = "state.name {$order[1]}";
            }
        }
        
        parent::modifyOrder($order);
    }
    
    /*protected function modifyOptions(array &$options)
    {
        // pokud neni admin, vidi pouze sve zaznamy
        if (!$this->presenter->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $options["user_id"] = $this->presenter->user->id;
        }
        
        parent::modifyOptions($options);
    }*/
    
}
