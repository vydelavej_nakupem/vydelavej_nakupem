<?php

namespace App\Component;

use App\Model;
use RTsoft\Model\GridFormRepository as R;
use Nette\Application\UI\Form;

class StateForm extends FormComponent
{    
    /** @var Model\StateRepository */
    protected $stateRepository;
       
    function __construct(Model\StateRepository $stateRepository)
    {
        $this->stateRepository = $stateRepository;
     
        $this->redirectAfterSavingTo = "State:default";
        $this->nameOfColumnForAnnouncement = "name";

        parent::__construct($stateRepository);
    }
        
    protected function createComponentForm()
    {
        //$this->stateRepository->columns["state_id"][R::COLUMN_ITEMS] = $this->stateRepository->fetchStatesForSelectbox(TRUE);
        
        // vygenerovani formulare
        $form = parent::createComponentForm();
        
        //$form["state_id"]->setPrompt("-- Žádná --");
        
        $this->addSubmitButton($form);
        
        return $form;
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        if ($id)
        {
            $this->redirectAfterSavingTo = array("closeFancybox", array("refreshGrid" => TRUE));
        }
        else
        {
            $this->redirectAfterSavingTo = "State:default";
        }
        
        // ulozeni do DB
        
        if ($values["vat"] === "")
        {
            $values["vat"] = NULL;
        }    
        return parent::saveForm($form, $values, $id);
    }
}
