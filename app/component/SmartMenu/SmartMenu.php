<?php

namespace App\Component;

use Nette;
use RTsoft\Component;

/**
 * Class SmartMenu
 * @package App\Component
 */
class SmartMenu extends Component\BaseComponent
{
    /**
     * @var array položky menu
     */
    private $menu;

    /**
     * @var Nette\Security\User
     */
    private $user;


    /**
     * SmartMenu constructor.
     * @param array $menu
     */
    public function __construct(array $menu, Nette\Security\User $user)
    {
        $this->menu = $menu;
        $this->user = $user;

        parent::__construct();
    }


    /**
     * Normalizuje formát menu pro vykreslení
     * @param array $menu
     * @return array
     */
    protected function normalizeMenu(array $menu) {

        foreach ($menu as $name => &$item)
        {
            if (!is_array($item))
            {
                $item = ['link' => $item];
            }

            if (!isset($item['item']))
            {
                $item['item'] = NULL;
            }

            // Normalizujeme i subpoložky
            if ($item['item'] && is_array($item['item']))
            {
                $item['item'] = $this->normalizeMenu($item['item']);
            }
        }

        return $menu;
    }


    /**
     * Je tento odkaz pro aktuálního uživatele dostupný?
     * @param array $menuItem
     * @param bool $basedOnSubItems
     * @return bool
     */
    public function isLinkAllowed(array $menuItem, $basedOnSubItems = TRUE) {

        // Položka bez odkazu se zobrazuje podle jejích potomků
        if (!$menuItem['link'] || $menuItem['link'] === '#')
        {
            if ($basedOnSubItems)
            {
                return $this->isAnyLinkInTreeAllowed($menuItem);
            }
            else
            {
                return TRUE;
            }
        }
        else
        {
            $parsedLink = explode(':', $menuItem['link']);

            return $this->user->isAllowed($parsedLink[0], $parsedLink[1]);
        }
    }


    /**
     * Je jakákoliv položka v aktuálním stromu menu aktivní pro aktuálního uživatele?
     * @param array $menuItem
     * @return bool
     */
    protected function isAnyLinkInTreeAllowed(array $menuItem) {

        if (!empty($menuItem['item']))
        {
            $isAnyAllowed = FALSE;

            foreach ($menuItem['item'] as $subItem)
            {
                if (!$subItem['link'] || $subItem['link'] === '#')
                {
                    if ($this->isAnyLinkInTreeAllowed($subItem))
                    {
                        $isAnyAllowed = TRUE;
                    }
                }
                else
                {
                    $parsedLink = explode(':', $subItem['link']);

                    if ($this->user->isAllowed($parsedLink[0], $parsedLink[1]))
                    {
                        $isAnyAllowed = TRUE;
                    }
                }
            }

            return $isAnyAllowed;
        }
        else
        {
            return FALSE;
        }
    }


    /**
     * Považujeme zadanou položku menu za aktivní?
     * @param array $menuItem
     * @return bool
     */
    public function isLinkActive(array $menuItem)
    {
        if(!empty($menuItem['link']))
        {
            $linkPart = explode(':', $menuItem['link']);

            if(empty($linkPart[1]))
            {
                $linkPart[1] = 'default';
            }

            return ($linkPart[0] === $this->presenter->name && $linkPart[1] === $this->presenter->action);
        }

        return FALSE;
    }


    /**
     * Vygenerování href atributu odkazu položky v menu
     * @param array $menuItem
     * @return string
     */
    public function getLinkHref(array $menuItem)
    {
        if (empty($menuItem['link']) || $menuItem['link'] === '#')
        {
            return '#';
        }
        else
        {
            return $this->presenter->link($menuItem['link']);
        }
    }


    /**
     * Vykreslí tuto komponentu
     * @param array|null $params
     */
    public function render($params = NULL) 
    {
        if (!empty($params['name']))
        {
            if (!empty($this->menu[$params['name']]))
            {
                $this->template->menu  = $this->normalizeMenu($this->menu[$params['name']]);
                $this->template->class = isset($params['class']) ? $params['class'] : '';

                parent::render();
            }
            else
            {
                throw new Nette\InvalidStateException('Attempt to render a non-existing menu [' . $params['name'] . '], see menu.neon!');
            }
        }
        else
        {
            throw new Nette\InvalidArgumentException('Parametr name is required!');
        }
    }

}