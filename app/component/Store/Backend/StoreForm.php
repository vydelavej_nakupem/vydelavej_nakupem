<?php

namespace App\Component;

use App\Model;

use App\Model\AdvertisersRepository;
use Nette\Application\UI\Form;

class StoreBackendForm extends FormComponent
{
    const LOGO_PATH = '/../../../../www/img/logos/';
    
    /** @var Model\AdvertiserCategoryRepository */
    protected $advertiserCategoryRepository;    
    
    /** @var AdvertisersRepository */
    protected $advertiserRepository;
    
    /** @var Model\categoryRepository */
    protected $categoryRepository;
    
    /** @var Model\StateRepository */
    protected $stateRepository;
    
    function __construct(
            Model\AdvertiserCategoryRepository $advertiserCategoryRepository,
            AdvertisersRepository $advertisersRepository,
            Model\CategoryRepository $categoryRepository)
    {
        $this->advertiserCategoryRepository = $advertiserCategoryRepository;
        $this->advertiserRepository = $advertisersRepository;
        $this->categoryRepository = $categoryRepository;
     
        $this->redirectAfterSavingTo = "Store:default";
        $this->nameOfColumnForAnnouncement = "advertiser_name";

        parent::__construct($advertisersRepository);
    }
    
    public function render($params = NULL)
    {
        parent::render($params);
    }
    
    protected function createComponentForm()
    {
        $this->advertiserRepository->columns[AdvertisersRepository::DB_COL_COMMISSION_LINK][AdvertisersRepository::COLUMN_TYPE] = AdvertisersRepository::TYPE_TEXT;
        
        $form = parent::createComponentForm();
        
        $form[AdvertisersRepository::DB_COL_CATEGORY_ID]->setItems(
                $this->categoryRepository->getCategoriesForSelectBox());
        
        $form[AdvertisersRepository::DB_COL_SITE_DOMAIN]->setItems(\App\Enum\EAdvertiserDomain::$domains);
        
        $form[AdvertisersRepository::DB_COL_PARENT_ID]->setItems(
                $this->advertiserRepository->getItemsForSelectBox());
        
        return $form;
    }
    
    protected function setFormDefaults($row)
    {
        if(!$row)
        {
            $this['form'][Model\AdvertisersRepository::DB_COL_ACTIVE]->setValue(FALSE);
        }
        parent::setFormDefaults($row);
        
        
        if($this->id)
        {
            $categories = $this->advertiserCategoryRepository->getAdvertiserCategories($this->id);
            $this["form"][Model\AdvertisersRepository::DB_COL_CATEGORY_ID]->setDefaultValue($categories);
        }
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        $oldRow = null;
        
        if($id)
        {
            $oldRow = $this->repository->findRow($id);
        }
        
        if($values->logo_delete || $values->logo_upload && $values->logo_upload->name)
        {
            if($oldRow && $oldRow->logo)
            {
                @unlink(__DIR__ . self::LOGO_PATH . $oldRow->logo);
            }
        }
        
        if($values->logo_upload && $values->logo_upload->isImage())
        {
            $filePath = $values->logo_upload->getName();
            $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
            $file = $values->logo_upload->getContents();
            
            $logoName = time() . ".$fileExtension";
            
            file_put_contents(__DIR__ . self::LOGO_PATH . $logoName, $file);
            $values[Model\AdvertisersRepository::DB_COL_LOGO] = $logoName;
            
            unset($values['logo_upload']);
        }
        
        $categories = $values->category_id;
        
        if (!$id)
        {
            $values->source = \App\Enum\EAdvertiserSource::VYDELAVEJNAKUPEM;
        }
        
        unset($values->category_id);
        
        $row = parent::saveForm($form, $values, $id);
        
        $this->advertiserCategoryRepository->saveAdvertiserCategories($row->id, $categories);
        
        return $row;
    }
}
