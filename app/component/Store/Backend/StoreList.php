<?php

namespace App\Component;

use App\Enum\EAdvertAccountStatus;
use App\Model\AdvertisersRepository;

class StoreBackendList extends ListComponent
{            
    /** @var AdvertisersRepository */
    protected $advertisersRepository;
    
    function __construct(AdvertisersRepository $advertisersRepository)
    {
        $this->advertisersRepository = $advertisersRepository;

        parent::__construct($advertisersRepository);
    }
    
    public function createComponentDataGrid()
    {
        $this->advertisersRepository->columns[AdvertisersRepository::DB_COL_COMMISSION_LINK][AdvertisersRepository::COLUMN_TYPE] = AdvertisersRepository::TYPE_BOOL;
        $this->advertisersRepository->columns[AdvertisersRepository::DB_COL_ACCOUNT_STATUS][AdvertisersRepository::COLUMN_ITEMS] = EAdvertAccountStatus::$types;
        
        return parent::createComponentDataGrid();
    }
    
    public function modifyOptions(array &$options)
    {
        if (isset($options[AdvertisersRepository::DB_COL_COMMISSION_LINK]))
        {
            if ($options[AdvertisersRepository::DB_COL_COMMISSION_LINK] == AdvertisersRepository::BOOL_POSITIVE)
            {
                $column = AdvertisersRepository::DB_COL_COMMISSION_LINK . ' != ?';
            }
            else if ($options[AdvertisersRepository::DB_COL_COMMISSION_LINK] == AdvertisersRepository::BOOL_NEGATIVE)
            {
                $column = AdvertisersRepository::DB_COL_COMMISSION_LINK . ' = ?';
            }
            
            $this->advertisersRepository->columns[$column] = [];
            $this->advertisersRepository->columns[$column][AdvertisersRepository::COLUMN_TYPE] = AdvertisersRepository::TYPE_TEXT;
            $this->advertisersRepository->columns[$column][AdvertisersRepository::COLUMN_FILTER_TYPE] = AdvertisersRepository::FILTER_TYPE_OWN;
            $options[$column] = '';
            
            unset($options[AdvertisersRepository::DB_COL_COMMISSION_LINK]);
        }
        parent::modifyOptions($options);
    }
    
    public function modifyOrder(&$order)
    {
        if (!$order)
        {
            $order[] = 'advertiser_name';
        }
        parent::modifyOrder($order);
    }
}
