<?php

namespace App\Component;

use App\Model\AdvertisersRepository;

class StorePreview extends BaseComponent
{

    /**
     * @param bool $params = store
     */
    public function render($params = NULL)
    {
        $store = $params;
        
        $this->template->store = $store;

        parent::render(null);
    }
}
