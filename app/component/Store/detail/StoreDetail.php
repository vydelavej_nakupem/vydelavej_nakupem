<?php

namespace App\Component;

use App\Model\AdvertisersRepository;
use App\Model\UserProjectSession;

class StoreDetail extends BaseComponent
{
    private $advertisersRepository;
    private $userProjectSession;
    private $storeRequest;
    
    public function __construct(
        AdvertisersRepository $advertisersRepository,
        UserProjectSession $userProjectSession
    )
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
        $this->userProjectSession = $userProjectSession;
    }
    
    public function setStoreRequest($storeRequest)
    {
        $this->storeRequest = $storeRequest;
    }
    
    /**
     * @param bool $params = store
     */
    public function render($params = NULL)
    {
        $store = $params;
        
        $this->template->store = $store;
        $this->template->storeRequest = $this->storeRequest;
        $this->template->project = $this->userProjectSession->getProject();
        $this->template->storeCommissionLink = $this->advertisersRepository->getLinkWithoutHttp($store->{\App\Model\AdvertisersRepository::DB_COL_COMMISSION_LINK});
        
        parent::render(null);
    }
}
