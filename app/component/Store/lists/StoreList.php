<?php

namespace App\Component;

use App\Model\AdvertisersRepository;

class StoreList extends BaseComponent
{
    const PREVIEW_LIMIT = 10;
    const ITEMS_PER_PAGE = 20;
    
    protected $advertisersRepository;
    private $storesOrder;
    private $storesLimit;
    
    private $stores = NULL;
    protected $title = 'Obchody';
    
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

    public function __construct(AdvertisersRepository $advertisersRepository)
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
        
        $this->storesOrder = AdvertisersRepository::DB_COL_ADVERTISER_NAME;
        $this->storesLimit = self::PREVIEW_LIMIT;
    }   
    
    public function render($params = NULL)
    {
        $stores = $this->stores;
        
        if (!$stores)
        {
            $stores = $this->advertisersRepository->getAllActiveAdvertisers();
        }
        
        $stores->order($this->storesOrder);
        
        $storesCount = $stores->count();
        $stores->limit($this->storesLimit);
        
        $this->template->title = $this->title;
        $this->template->stores = $stores;
        $this->template->currentLimit = $this->storesLimit;
        $this->template->enableNextPage = $storesCount > $this->storesLimit;
        
        
        parent::render($params);
    }
    
    public function handleShowNextPage($currentLimit)
    {
        $this->storesLimit = $currentLimit + self::ITEMS_PER_PAGE;
        $this->redrawControl('favourite-block');
    }
}
