<?php

namespace App\Component;

use App\Model\AdvertisersRepository;
use App\Model\CategoryRepository;

class StoreCategoryList extends StoreList
{
    private $categoryId;
    private $categoryRepository;
    
    public function __construct(
            AdvertisersRepository $advertisersRepository,
            CategoryRepository $categoryRepository
    )
    {
        parent::__construct($advertisersRepository);
        
        $this->categoryRepository = $categoryRepository;
    }   
    
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }
        
    public function render($params = NULL)
    {
        $stores = $this->advertisersRepository->getAllActiveAdvertisersInCategory($this->categoryId);
        $category = $this->categoryRepository->getActiveCategory($this->categoryId);
        
        $this->title = 'Obchody v kategorii '.$category->title;
        $this->setStores($stores);
        
        $this->template->setFile(__DIR__ . '/StoreList.latte');
        parent::render($params);
    }

}
