<?php

namespace App\Component;

use App\Model\AdvertisersRepository;

class StoreSearchList extends BaseComponent
{
    const PREVIEW_LIMIT = 10;
    const ITEMS_PER_PAGE = 20;
    
    private $advertisersRepository;
    private $storesOrder;
    private $storesLimit;
    
    public function __construct(AdvertisersRepository $advertisersRepository)
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
        
        $this->storesOrder = AdvertisersRepository::DB_COL_ADVERTISER_NAME;
        $this->storesLimit = self::PREVIEW_LIMIT;
    }   
    
    public function render($params = NULL)
    {
        $keyword = $this->getSearchKeyword();
        
        $stores = $this->advertisersRepository->searchAllActiveAdvertisers($keyword);
        $stores->order($this->storesOrder);
        
        $storesCount = $stores->count();
        $stores->limit($this->storesLimit);
        
        $this->template->stores = $stores;
        $this->template->keyword = $keyword;
        $this->template->currentLimit = $this->storesLimit;
        $this->template->enableNextPage = $storesCount > $this->storesLimit;
        
        
        parent::render(null);
    }
    
    public function handleShowNextPage($currentLimit)
    {
        $this->storesLimit = $currentLimit + self::ITEMS_PER_PAGE;
        $this->redrawControl('search-block');
    }
    
    protected function getSearchKeyword()
    {
        return $this->presenter->getParameter("q");
    }
}
