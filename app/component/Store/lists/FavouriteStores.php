<?php

namespace App\Component;

use App\Model\AdvertisersRepository;

class FavouriteStores extends BaseComponent
{
    const PREVIEW_LIMIT = 10;
    
    private $advertisersRepository;
    private $storesOrder;
    
    public function __construct(AdvertisersRepository $advertisersRepository)
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
        $this->storesOrder = AdvertisersRepository::DB_COL_ADVERTISER_NAME;
    }
    
    public function render($params = NULL)
    {
        $stores = $this->advertisersRepository->getFavouriteAdvertisers();
        
        $stores->order($this->storesOrder)->limit(self::PREVIEW_LIMIT);
        $this->template->stores = $stores;
        
        parent::render(null);
    }
}
