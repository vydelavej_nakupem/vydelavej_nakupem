<?php

namespace App\Component;

use App\Model\AdvertisersRepository;
use App\Model\UserRepository;
use App\Model\UserAccountRepository;

class HomepageStatistics extends BaseComponent
{
    private $advertisersRepository;
    private $userRepository;
    private $userAccountRepository;
    
    public function __construct(
        AdvertisersRepository $advertisersRepository,
        UserAccountRepository $userAccountRepository,
        UserRepository $userRepository
    )
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
        $this->userAccountRepository = $userAccountRepository;
        $this->userRepository = $userRepository;
    }
    
    public function render($params = NULL)
    {
        $this->template->shopsCount = $this->advertisersRepository->getAllActiveAdvertisers()->count('*');
        $this->template->customersCount = $this->userRepository->findAll()->count('*');
        $this->template->savedMoney = $this->userAccountRepository->getSavedMoney();
        
        parent::render($params);
    }
}
