<?php

namespace App\Component;

use App\Model;
use RTsoft\Component\GridForm\GridComponent;
use RTsoft\Model\GridFormRepository as R;

class RoleList extends GridComponent
{            
    /** @var \Model\RoleRepository */
    protected $roleRepository;
    
    function __construct(Model\RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;

        parent::__construct($roleRepository);
    }
}
