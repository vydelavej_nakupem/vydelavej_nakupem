<?php

namespace App\Component;

use App\Model;
use RTsoft\Model\GridFormRepository as R;
use Nette\Application\UI\Form;

class RoleForm extends FormComponent
{
    /** @var Model\RoleRepository */
    protected $roleRepository;
    
    /** @var Model\UserRepository */
    protected $userRepository;
    
    /** @var Model\UserRoleRepository */
    protected $userRoleRepository;
    
    function __construct(Model\UserRepository $userRepository, Model\RoleRepository $roleRepository, Model\UserRoleRepository $userRoleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->userRoleRepository = $userRoleRepository;
     
        $this->redirectAfterSavingTo = "Role:default";
        $this->nameOfColumnForAnnouncement = "title";

        parent::__construct($roleRepository);
    }
    
    protected function setFormDefaults($row)
    {
        if (!empty($row))
        {
            $row = $row->toArray();
            $row["users"] = $this->userRoleRepository->findUsersForRole($this->presenter->request->parameters["id"], FALSE, FALSE);
        }
        
        parent::setFormDefaults($row);
    }

    
    protected function createComponentForm()
    {
        $form = parent::createComponentForm();
        
        // users checkboxlist
        $users = $this->userRepository->fetchItemsForSelectbox();
        $form->addCheckboxList("users", "Uživatelé", $users);
        
        $this->addSubmitButton($form);
        
        return $form;
    }
    
    public function formSubmitted(Form $form, \stdClass $values)
    {
        $id = $this->presenter->request->parameters["id"];
        
        // kontrola duplicity loginu
        $role = $this->roleRepository->findByNameOrTitle(NULL /*$values["name"]*/, $values["title"]);

        if ($role && $id != $role->id)
        {
            $form->addError('Název má jiná role.');
            return;
        }
        
        parent::formSubmitted($form, $values);
    }

    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        // kdyz editace, po zavreni fancyboxu se jenom refreshne grid bez reloadu stranky
        if ($id)
        {
            $this->redirectAfterSavingTo = array("Role:closeFancybox", array("refreshGrid" => TRUE));
        }
        
        $users = $values["users"];
        unset($values["users"]);
            
        // ulozeni do DB
        $row = parent::saveForm($form, $values, $id);
        
        // ulozeni uzivatelu v roli
        $this->userRoleRepository->saveUsersForRole($users, $row->id);
        
        return $row;
    }
}
