<?php

namespace App\Component;

use App\Model;

class AdvertiserCategoryList extends ListComponent
{            
    /** @var \Model\categoryRepository */
    protected $categoryRepository;
    
    function __construct(Model\CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
        parent::__construct($categoryRepository);
    }
}
