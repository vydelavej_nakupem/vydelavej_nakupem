<?php

namespace App\Component;

use App\Model;
use App\Model\CategoryRepository;
use App\Model\AdvertiserCategoryRepository;
use Nette\Application\UI\Form;

class AdvertiserCategoryForm extends FormComponent
{
    /** @var Model\AdvertiserCategoryRepository */
    protected $categoryRepository;
    
    
    function __construct(categoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
     
        $this->redirectAfterSavingTo = "Category:default";
        $this->nameOfColumnForAnnouncement = "title";

        parent::__construct($categoryRepository);
    }
    
    protected function createComponentForm()
    {
        $form = parent::createComponentForm();
        
        $form[CategoryRepository::DB_COL_PARENT_ID]->setItems(
                $this->categoryRepository->getCategoriesForSelectBox($this->id));
        
        return $form;
    }
    
    protected function setFormDefaults($row)
    {
        parent::setFormDefaults($row);
        
        if(!$this->id)
        {
            $this["form"][CategoryRepository::DB_COL_ACTIVE]->setDefaultValue(true);
        }
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        $row = parent::saveForm($form, $values, $id);        
        
        return $row;
    }

}
