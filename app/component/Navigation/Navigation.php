<?php

namespace App\Component;

use App\Model\CategoryRepository;
use App\Model\AdvertiserCategoryRepository;

class Navigation extends BaseComponent
{

    private $categoryRepository;
    private $advertiserCategoryRepository;

    public function __construct(
            CategoryRepository $categoryRepository,
            AdvertiserCategoryRepository $advertiserCategoryRepository
    )
    {
        parent::__construct();

        $this->categoryRepository = $categoryRepository;
        $this->advertiserCategoryRepository = $advertiserCategoryRepository;
    }

    public function render($params = NULL)
    {
        $this->template->categories = $this->categoryRepository->getMenuArray();
        $this->template->topShops = $this->advertiserCategoryRepository->getTopAdvertisersForCategories();

        parent::render($params);
    }

}
