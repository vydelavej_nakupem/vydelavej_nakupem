<?php

namespace App\Component;

class ListComponent extends \RTsoft\Component\GridForm\GridComponent
{
    protected $itemsPerPage = 30;
    
    protected $selectboxFilterPromptText = "-- Vše --";
    
    //put your code here
    protected function modifyOrder(&$order)
    {
         if ($order)
        {
            if ($order[0] == "created")
            {
                if ($order[1] == "ASC")
                {
                    $order[1] = "DESC";
                }
                else 
                {
                    $order[1] = "ASC";
                }
            }    
        }
        
        parent::modifyOrder($order);
    }

}
