<?php

namespace App\Component;

use App\Model;
use RTsoft\Model\GridFormRepository as R;

class CustomerList extends ListComponent
{            
    /** @var \Model\CustomerRepository */
    protected $customerRepository;
    
    /** @var \Model\UserRepository */
    protected $userRepository;
    
    function __construct(Model\CustomerRepository $customerRepository, Model\UserRepository $userRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->userRepository = $userRepository;
        
        $this->itemsPerPage = 10;

        parent::__construct($customerRepository);
    }
    
    protected function createComponentDataGrid()
    {
        // možnost zasáhnout do pole $columns, ještě než se grid vygeneruje
        if (!$this->presenter->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->customerRepository->columns["user_id"][R::COLUMN_OPTIONS][] = R::OPTION_NOT_IN_GRID;
        }
        
        return parent::createComponentDataGrid();
    }
    
    public function createFilterForm()
    {
        if ($this->presenter->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->customerRepository->columns["user_id"][R::COLUMN_ITEMS] = $this->userRepository->fetchItemsForSelectbox(array("role_id" => \App\Enum\ERole::USER));
        }
        
        // vygenerovani filtru
        $form = parent::createFilterForm();
                
        return $form;
    }
    
    protected function modifyOrder(&$order)
    {
        if ($order)
        {
            // uprava razeni - bud takhle po staru, nebo pouzit COLUMN_ORDER_BY
            if ($order[0] == "name")
            {
                $order = "lastname {$order[1]}, firstname {$order[1]}";
            }
            
            if($order[0] == "address")
            {
                $order = "CONCAT(street, land_registry_number, house_number, city, postcode) {$order[1]}";
            }
        }
        else
        {
            // default razeni
            $order = "lastname ASC, firstname ASC";
        }
        
        parent::modifyOrder($order);
    }
    
    protected function modifyOptions(array &$options)
    {
        // pokud neni admin, vidi pouze sve zaznamy
        if (!$this->presenter->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $options["user_id"] = $this->presenter->user->id;
        }
        
        parent::modifyOptions($options);
        
        
    }

}
