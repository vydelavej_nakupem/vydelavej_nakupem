<?php

namespace App\Component;

use App\Model;
use RTsoft\Model\GridFormRepository as R;
use Nette\Application\UI\Form;

class CustomerForm extends FormComponent
{
    /** @var Model\CustomerRepository */
    protected $customerRepository;
    
    /** @var Model\StateRepository */
    protected $stateRepository;
    
     /** @var Model\UserRepository */
    protected $userRepository;
    
    function __construct(Model\CustomerRepository $customerRepository, Model\StateRepository $stateRepository, Model\UserRepository $userRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->stateRepository = $stateRepository;
        $this->userRepository = $userRepository;
     
        $this->redirectAfterSavingTo = "Customer:default";
        $this->nameOfColumnForAnnouncement = NULL;

        parent::__construct($customerRepository);
    }
    
    protected function checkPermissionToEdit(\Nette\Database\Table\ActiveRow $row, \Nette\Security\User $user)
    {
        // kdyz neni admin, je treba kontrolovat, zda smi editovat
        if (!$user->isInRole(\App\Enum\ERole::ADMIN))
        {
            if ($row->user_id != $user->id)
            {
                return FALSE;
            }
        }
        
        return TRUE;
    }

    
    protected function createComponentForm()
    {
        // naplneni selectboxu - staty se budou plnit samy, uzivatele po staru kvuli rolim
        $this->customerRepository->columns["state_id"][R::COLUMN_REPOSITORY] = $this->stateRepository;
        //$this->customerRepository->columns["state_id"][R::COLUMN_ITEMS] = $this->stateRepository->fetchStatesForSelectbox(array("role_id" => \App\Enum\ERole::USER));
        
        if ($this->presenter->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            $this->customerRepository->columns["user_id"][R::COLUMN_ITEMS] = $this->userRepository->fetchItemsForSelectbox(array("role_id" => \App\Enum\ERole::USER));
        }
        else
        {
            // kdyz neni admin, nemuze zadavat uzivatele
            $this->customerRepository->columns["user_id"][R::COLUMN_OPTIONS][] = R::OPTION_NOT_IN_FORM;
        }
        
        // vygenerovani formulare
        $form = parent::createComponentForm();
        
        // dalsi validace
        $form["land_registry_number"]
            ->addConditionOn($form["house_number"], ~Form::FILLED)
            ->addRule(Form::FILLED, "Musí být vyplněno číslo popisné a/nebo číslo orientační.");
        
        // dalsi policka
        $form->addSelect("phone_pre", "Předvolba", $this->stateRepository->fetchPhonesForSelectbox())
             ->setAttribute("class", "normal-selectbox");
        
        return $form;
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        // kdyz editace, po zavreni fancyboxu se jenom refreshne grid bez reloadu stranky
        if ($id)
        {
            $this->redirectAfterSavingTo = array("Customer:closeFancybox", array("refreshGrid" => TRUE));
        }
       
        /*if (!empty($this->presenter->request->parameters["returnTo"]))
        {
            if ($this->presenter->request->parameters["returnTo"] == Model\CustomerRepository::RETURNTO_CUSTOMER_DEFAULT)
            {
                $this->redirectAfterSavingTo = "Customer:default";
            }
        }*/
        
        if (!$this->presenter->user->isInRole(\App\Enum\ERole::ADMIN))
        {
            // kdyz neni admin, jako uzivatel se da prihlaseny uzivatel
            $values["user_id"] = $this->presenter->user->id;
        }
        
        // ulozeni
        try
        {
            $row = parent::saveForm($form, $values, $id);        
        }
        catch (\PDOException $e)
        {
            if($e->getCode() == 23000 && strpos($e->getMessage(), "1062") !== FALSE)
            {
                if (strpos($e->getMessage(), "'email'") !== FALSE)
                {
                    $form->addError("Zadaný email má jiný zákazník.");
                }
                
                return;
            }
            
            throw $e;
        }
        
        return $row;
    }

    protected function setFormDefaults($row)
    {
        if (empty($row))      
        {     
            $row["state_id"] = Model\StateRepository::DEFAULT_STATE;
            $row["phone_pre"] = Model\StateRepository::DEFAULT_PHONE_PRE;
        }
        else
        {
            
        }
        
        parent::setFormDefaults($row);
    }
}
