<?php

namespace App\Component\Backend;

use App\Model;

use App\Model\UserProjectRepository;
use App\Model\UserRepository;
use Nette\Application\UI\Form;

class UserProjectForm extends \App\Component\FormComponent
{
    const LOGO_PATH = '/../../../../www/img/projects/';
    
    /** @var UserProjectRepository */
    protected $userProjectRepository;    
        
    /** @var UserRepository */
    protected $userRepository;    
    
    function __construct(
            UserProjectRepository $userProjectRepository,
            UserRepository $userRepository
    )
    {
        $this->userProjectRepository = $userProjectRepository;
        $this->userRepository = $userRepository;
     
        $this->redirectAfterSavingTo = "UserProject:default";
        $this->nameOfColumnForAnnouncement = "title";

        parent::__construct($userProjectRepository);
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        $oldRow = null;
        
        if($id)
        {
            $oldRow = $this->repository->findRow($id);
        }
        else
        {
            $userData = [
                'created' => new \Nette\Database\SqlLiteral('NOW()'),
            ];

            $userRow = $this->userRepository->insert($userData);
            $this->userRepository->generateSid($userRow);

            $values['user_id'] = $userRow->id;
        }
        
        if($values->img_delete || $values->img_upload && $values->img_upload->name)
        {
            if($oldRow && $oldRow->img)
            {
                @unlink(__DIR__ . self::LOGO_PATH . $oldRow->img);
            }
        }
        
        if($values->img_upload && $values->img_upload->isImage())
        {
            $filePath = $values->img_upload->getName();
            $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
            $file = $values->img_upload->getContents();
            
            $logoName = time() . ".$fileExtension";
            
            if (!file_exists(__DIR__ . self::LOGO_PATH))
            {
                mkdir(__DIR__ . self::LOGO_PATH);
            }
            
            file_put_contents(__DIR__ . self::LOGO_PATH . $logoName, $file);
            $values[UserProjectRepository::DB_COL_IMG] = $logoName;
            
            unset($values['img_upload']);
        }
        
        $row = parent::saveForm($form, $values, $id);
        
        return $row;
    }
}
