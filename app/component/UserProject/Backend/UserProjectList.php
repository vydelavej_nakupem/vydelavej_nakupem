<?php

namespace App\Component\Backend;

use App\Model\UserProjectRepository;

class UserProjectList extends \App\Component\ListComponent
{            
    /** @var UserProjectRepository */
    protected $userProjectRepository;
    
    function __construct(UserProjectRepository $userProjectRepository)
    {
        $this->userProjectRepository = $userProjectRepository;

        parent::__construct($userProjectRepository);
    }
    
    public function createComponentDataGrid()
    {
        return parent::createComponentDataGrid();
    }
    
    public function modifyOrder(&$order)
    {
        if (!$order)
        {
            $order[] = 'title';
        }
        parent::modifyOrder($order);
    }
}
