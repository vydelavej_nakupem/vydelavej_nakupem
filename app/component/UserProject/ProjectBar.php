<?php

namespace App\Component;

use App\Model\UserProjectSession;

/**
 * @author tttpapi
 */
class ProjectBar extends BaseComponent
{
    private $userProjectSession;
    
    public function __construct(UserProjectSession $userProjectSession)
    {
        $this->userProjectSession = $userProjectSession;
        
        parent::__construct();
    }
    
    public function render($params = NULL)
    {
        $this->template->userProject = $this->userProjectSession->getProject();
        parent::render(null);
    }
    
    public function handleStopSupport()
    {
        $this->userProjectSession->deleteProject();
        
        $this->presenter->redirect('this');
    }
}
