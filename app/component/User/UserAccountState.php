<?php

namespace App\Component;

use App\Model\UserAccountRepository;

/**
 * @author Samuel
 */
class UserAccountState extends \RTsoft\Component\BaseComponent
{
    private $userAccountRepository;
    
    public function __construct(UserAccountRepository $userAccountRepository)
    {
        $this->userAccountRepository = $userAccountRepository;
    }
    
    public function render($params = NULL)
    {
        $userId = $this->getUserId();
        $state = $this->userAccountRepository->getCurrentState($userId);
        
        $this->template->state = $state;

        parent::render(null);
    }
    
    private function getUserId()
    {
        return $this->presenter->user->id;
    }
}
