<?php

namespace App\Component;

/**
 * Tlacidlo pro prihlaseni via FB 
 *
 * @author Samuel
 */
class FbSiginButton extends \RTsoft\Component\BaseComponent
{
    const PARAM_FB                          = 'fb';
    const PARAM_APP_ID                      = 'app_id';
    const PARAM_APP_SECRET                  = 'app_secret';
    const PARAM_APP_DATA_HANDLER            = 'persistent_data_handler';
    const PARAM_APP_DEFAULT_GRAPH_VERSION   = 'default_graph_version';
    
    const FB_SIGN_PATH = "signIn!";
    const FB_GET_USER_INFO = "/me?fields=id,email";
    
    const FB_PERMISSION_EMAIL   = 'email';
    const FB_PERMISSION_PROFILE = 'public_profile';
    
    private $userRepository;
    private $fbDataHandler;
    
    private $appId;
    private $secretKey;
    private $graphVesrion;
    
    private $fbInstance;
    
    public function __construct(
            $appId, $secretKey, $graphVersion,
            \App\Model\FbDataHandler $fbDataHandler,
            \App\Model\UserRepository $userRepository,
            \Nette\ComponentModel\IContainer $parent = NULL, 
            $name = NULL)
    {
        $this->appId = $appId;
        $this->secretKey = $secretKey;
        $this->graphVesrion = $graphVersion;
        
        $this->getFbIntance();
        
        parent::__construct($parent, $name);
        $this->userRepository = $userRepository;
        $this->fbDataHandler = $fbDataHandler;
    }
    
    public function render($params = NULL)
    {
        $label = $params;
        
        $this->template->signInUrl = $this->getLoginUrl();
        $this->template->label = $label;
                
        parent::render();
    }
    
    protected function getSignUrl()
    {
        $registerHash = $this->getRegisterHash();
        $urlPart = $this->link(self::FB_SIGN_PATH, array($registerHash));
        $currentUrl = $this->presenter->getCurrentUrl();
        
        $currentUrl->setQuery(null);
        $currentUrl->setPath($urlPart);
        
        return $currentUrl->getAbsoluteUrl();
    }
    
    protected function getLoginUrl()
    {
        $callback = $this->getSignUrl();
        $loginHeleper = $this->getLoginHelper();
        $permissions = [self::FB_PERMISSION_EMAIL, self::FB_PERMISSION_PROFILE];
        $signInUrl = $loginHeleper->getLoginUrl($callback, $permissions);
        
        return $signInUrl;
    }

    public function getLoginHelper()
    {
        $fb = $this->getFbIntance();
        
        return $fb->getRedirectLoginHelper();
    }
    
    public function getFbIntance()
    {
        if($this->fbInstance)
        {
            return $this->fbInstance;
        }
           
        if(empty($this->appId) || empty($this->secretKey) && empty($this->graphVesrion))
        {
            throw new \Nette\Neon\Exception('Missing facebook parameters.');
        }
        
        $this->fbInstance = new \Facebook\Facebook([
            self::PARAM_APP_ID                      => $this->appId,
            self::PARAM_APP_SECRET                  => $this->secretKey,
            self::PARAM_APP_DEFAULT_GRAPH_VERSION   => $this->graphVesrion,
            self::PARAM_APP_DATA_HANDLER            => $this->fbDataHandler
        ]);
        
        return $this->fbInstance;
    }
    
    /**
     * Registrace/nacteni uzivatele
     * @return type
     */
    public function signInUser($registerHash = null)
    {
        $fb = $this->getFbIntance();
        $helper = $this->getLoginHelper();
        $accessToken = null;

        try 
        {
            $accessToken = $helper->getAccessToken();
        } 
        catch(Facebook\Exceptions\FacebookResponseException $e) 
        {
            \Tracy\Debugger::log($e->getMessage(), \Tracy\Debugger::ERROR);
        } 
        catch(Facebook\Exceptions\FacebookSDKException $e) 
        {
            \Tracy\Debugger::log($e->getMessage(), \Tracy\Debugger::ERROR);
        }

        if (!isset($accessToken)) 
        {            
            if ($helper->getError()) 
            {
                \Tracy\Debugger::log($helper->getError(), \Tracy\Debugger::ERROR);
            } 
            \Tracy\Debugger::log("FB login error.", \Tracy\Debugger::ERROR);
            
            return null;
        }
        
        $oAuth2Client = $fb->getOAuth2Client();
        $tokenMetadata = null;
        
        if($oAuth2Client)
        {
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        }
        
        if($tokenMetadata)
        {
            $tokenMetadata->validateAppId($this->appId); 
            $tokenMetadata->validateExpiration();
        }
        if (! $accessToken->isLongLived()) 
        {
            try 
            {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } 
            catch (Facebook\Exceptions\FacebookSDKException $e) 
            {
                \Tracy\Debugger::log($e->getMessage(), \Tracy\Debugger::ERROR);
            }
        }
        $response = $fb->get(self::FB_GET_USER_INFO, $accessToken);
        $decodeBody = $response->getDecodedBody();
        
        $parentId = null;
        $parentUser = $this->userRepository->checkUserSid($registerHash);
        
        if($parentUser)
        {
            $parentId = $parentUser->id;
        }
        
        return $this->userRepository->registerFbUser($decodeBody, $parentId);
    }
    
    public function handleSignIn($registerHash = null)
    {
        if ($this->presenter->user->loggedIn)
        {
            $this->presenter->redirect('Default:');
            $this->afterLoginRedirect();
        }
        
        $user = $this->signInUser($registerHash);
        
        if(!$user)
        {
            $this->presenter->flashMessage('Přihlášení se nepodařilo.');
            $this->presenter->redirect('Sign:in');
        }
        else
        {
            $this->presenter->user->login($user->id, $user->fb_id);
            $this->afterLoginRedirect();
        }
    }
    
    private function afterLoginRedirect()
    {
        $key = $this->presenter->getParameter("returnKey");

        if ($key)
        {
            $this->presenter->restoreRequest($key);
        }   
        else
        {
            $this->presenter->redirect('Default:');
        }
    }


    /**
     * Overi platnost registarcni hashe/parent SID a vrati ho.
     * @return type
     */
    protected function getRegisterHash()
    {
        $registerHash = $this->presenter->getParameter('register_hash');
        $user = $this->userRepository->checkUserSid($registerHash);
        return $user ? $user->sid : null;
    }
}
