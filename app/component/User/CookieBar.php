<?php

namespace App\Component;

use Nette\Http;
use Nette\Utils\DateTime;

/**
 * @author Samuel
 */
class CookieBar extends BaseComponent
{
    const COOKIE_COOKIEBAR_NAME       = 'cookie-bar',
          COOKIE_COOKIEBAR_EXPIRATION = '+ 10 year';
 
    /** @var \Nette\Http\Request @inject */
    private $request;
    
    /** @var \Nette\Http\Response @inject */
    private $response;
    
    private $enabled;

    
    public function __construct(Http\Request $request, \Nette\Http\Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->enabled = $this->isCookieBarEnabled();
        
        parent::__construct();
    }
    
    public function render($params = NULL)
    {
        $this->template->isCookieEnabled = $this->enabled;
        parent::render(null);
    }
    
    public function isCookieBarEnabled()
    {
        $cookie = $this->request->getCookie(self::COOKIE_COOKIEBAR_NAME);
        return empty($cookie);
    }
    
    public function setCookieBarDisabled()
    {
        if($this->isCookieBarEnabled())
        {
            $expiration = new DateTime(self::COOKIE_COOKIEBAR_EXPIRATION);
            $this->enabled = false;
            $this->response->setCookie(self::COOKIE_COOKIEBAR_NAME, true, $expiration);
        }
    }
    
    public function handleConfrimCookieBar()
    {
        $this->setCookieBarDisabled();
        $this->redrawControl('cookieBar');
    }
}
