<?php

namespace App\Component;

use App\Model;
use RTsoft\Model\GridFormRepository as R;
use Nette\Application\UI\Form;

class UserForm extends FormComponent
{
    /** @var bool Priznak urcujici, zda je komponenta pouzita v registracnim formulari. */
    private $register = FALSE;
    
    /** @var bool Priznak urcujici, zda je komponenta pouzita ve formulari na upravu profilu. */
    private $profile = FALSE;
    
    /** @var array udalost dokonceni registrace */
    public $onRegistrationCompleted;
    
    /** @var Model\RoleRepository */
    protected $roleRepository;
    
    /** @var Model\UserRepository */
    protected $userRepository;
    
    /** @var Model\UserRoleRepository */
    protected $userRoleRepository;
    
    /** @var Model\RegisterLogRepository */
    protected $registerLogRepository;
    
    private $user;
    
    function __construct(
            Model\UserRepository $userRepository, 
            Model\RoleRepository $roleRepository, 
            Model\UserRoleRepository $userRoleRepository,
            Model\RegisterLogRepository $registerLogRepository,
            \Nette\Security\User $user
    )
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->userRoleRepository = $userRoleRepository;
        $this->registerLogRepository = $registerLogRepository;
        $this->user = $user;
     
        $this->redirectAfterSavingTo = "Default:default";
        $this->nameOfColumnForAnnouncement = "email";

        parent::__construct($userRepository);
    }
    
    public function render($params = NULL)
    {
        if ($this->profile && !$this["form"]->isSubmitted())
        {
            $row = $this->userRepository->findRow($this->presenter->user->id);
            $this->setFormDefaults($row);
        }
        
        parent::render();
    }

    
    protected function setFormDefaults($row)
    {
        if (!empty($row))
        {
            $row = $row->toArray();
            $row["roles"] = $this->userRoleRepository->findRolesForUser($this->presenter->user->id, FALSE, FALSE);
        }
        else
        {
            $row["roles"] = Model\RoleRepository::$defaultRolesForNewUser;
        }
        
        parent::setFormDefaults($row);
    }

    
    protected function createComponentForm()
    {
        $isEdit = isset($this->presenter->request->parameters["id"]);
        $isAdministration = in_array('admin', $this->user->roles);
        // zmeny pro registracni a profilovy formular
        if (($this->register || $this->profile) && !$isAdministration)
        {
            $this->userRepository->columns['roles'][R::COLUMN_OPTIONS][] = R::OPTION_NOT_IN_FORM;
            $this->userRepository->columns['active'][R::COLUMN_OPTIONS][] = R::OPTION_NOT_IN_FORM;
        }    
        
        // zmeny pro profilovy formular
        if ($this->profile)
        {            
            $this->userRepository->columns['password'][R::COLUMN_OPTIONS][] = R::OPTION_NOT_IN_FORM;
            $this->userRepository->columns['password2'][R::COLUMN_OPTIONS][] = R::OPTION_NOT_IN_FORM;
        }

        unset($this->userRepository->columns['firstname']);
        unset($this->userRepository->columns['lastname']);
        
        // vygenerovani formulare
        $form = parent::createComponentForm();
        
        $form["bank_wire"]
            ->setAttribute('placeholder', 'xxxxxx-xxxxxxxxxx/xxxx')
            ->addRule(Form::PATTERN, 'Zadejte platný bankovní účet', Model\UserRepository::BANK_WIRE_PATTERN);
        
        // uprava trid u labelu a inputu, aby se to tam veslo
        //$form->setControlClass("col-sm-8");
        //$form->setLabelClass("col-sm-5");
        
        // heslo povinne jen kdyz se pridava novy uzivatel
        if (!$isEdit && isset($form['password']))
        {
            $form['password']->setRequired('Zadejte prosím heslo');
        }
        
        // hesla musi byt stejna a potvrzeni je povinne, pokud je vyplneno heslo
        if (isset($form['password2']))
        {
            $form["password2"]
                ->addConditionOn($form["password"], Form::FILLED)
                ->addRule(Form::FILLED, 'Nové heslo je nutné zadat ještě jednou pro potvrzení.')
                ->addRule(Form::EQUAL, 'Zadaná hesla se musí shodovat.', $form['password']);
        }
        
        // role checkboxlist
        if ($isAdministration)
        {    
            $roles = $this->roleRepository->fetchItemsForSelectbox();
            $form->addCheckboxList("roles", "Role", $roles)
                 ->addRule(Form::MIN_LENGTH, 'Musí být vybrána alespoň jedna role.', 1);
        }
        
        if ($this->register)
        {
            $registerHash = $this->getRegisterHash();
            
            if($registerHash)
            {
                $form->addHidden('register_hash')->setDefaultValue($registerHash);
            }
            
            // souhlas s obchodnimi podminkami
            $form->addCheckbox("agree_with_rules", "")
                 ->addRule(Form::EQUAL, "Je nutné souhlasit s obchodními podmínkami.", TRUE);

            // ochrana formularu dostupnych bez prihlaseni
            /*if($this->presenter->isDevelopVersion())
            {
                $form->addReCaptcha('captcha', "Ochrana", 'Prokažte prosím svou nerobotičnost.');
            }*/
        }
        
        $this->addSubmitButton($form);
        
        return $form;
    }
    
    protected function addSubmitButton($form, $name = "save", $glyphicon = "ok", $text = "Uložit")
    {
        parent::addSubmitButton($form, $name, '', $this->register ? "Registrace" : $text);
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        // kdyz editace, po zavreni fancyboxu se jenom refreshne grid bez reloadu stranky
        if ($id)
        {
            $this->redirectAfterSavingTo = array("User:closeFancybox", array("refreshGrid" => TRUE));
        }
        
        if ($this->profile)
        {
            $id = $this->presenter->user->id;
        }
        
//        if (!$this->profile) tohle nedava smysl?
        {
            // role - pri registraci se davaji defaultni
            if (!isset($values["roles"]))
            {
                $roles = Model\RoleRepository::$defaultRolesForNewUser;
            }   
            else
            {    
                $roles = $values["roles"];
                unset($values["roles"]);
            }
    
            // osetreni hesel
            unset($values["password2"]);

            if (!empty($values["password"]))
            {
                $values["password"] = \Nette\Security\Passwords::hash($values["password"]);
            }
            else
            {
                unset($values["password"]);
            }
        }
        
        if (isset($values["agree_with_rules"]))
        {
            unset($values["agree_with_rules"]);
        }
        
        if ($this->register)
        {
            if(!empty($values['register_hash']))
            {
                $parentUser = $this->userRepository->checkUserSid($values['register_hash']);
                
                if($parentUser)
                {
                    $values['parent_id'] = $parentUser->id;
                }
                unset($values['register_hash']);
            }
            
            $values["active"] = FALSE;
        }
        
        // ulozeni do DB
        try
        {
            $row = parent::saveForm($form, $values, $id);
            
            if(!$this->userRepository->generateSid($row))
            {
                $this->registerLogRepository->saveLog($row->id, 'SID failed');
            }
            if($row)
            {
                $this->presenter->clearRegisterHash();
            }
        }
        catch (\PDOException $e)
        {
            if($e->getCode() == 23000 && strpos($e->getMessage(), "1062") !== FALSE)
            {
                if (strpos($e->getMessage(), "'email'") !== FALSE)
                {
                    $form->addError("Tento e-mail již používá jiný uživatel.");
                    
                    return;
                }
            }
            \Tracy\Debugger::log($e);
            throw $e;
        }
        
//        if (!$this->profile)
        {
            // ulozeni roli
            $this->userRoleRepository->saveRolesForUser($roles, $row->id);
        }
        
        if ($this->profile)
        {
            $this->redirectAfterSavingTo = "Profile:settings";
        }
        
        if ($this->register)
        {
            $this->redirectAfterSavingTo = "Sign:in";
            $this->nameOfColumnForAnnouncement = FALSE;
            $this->onRegistrationCompleted($this, $row);
        }
        
        return $row;
    }
    
    function setRegister($register)
    {
        $this->register = $register;
    }

    function setProfile($profile)
    {
        $this->profile = $profile;
    }
    
    /**
     * Overi platnost registarcni hashe/parent SID a vrati ho.
     * @return type
     */
    protected function getRegisterHash()
    {
        $registerHash = $this->presenter->getParameter('register_hash');
        $user = $this->userRepository->checkUserSid($registerHash);
        return $user ? $user->sid : null;
    }
}
