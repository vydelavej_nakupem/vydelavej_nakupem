<?php

namespace App\Component\Backend;

use App\Model\UserAccountRepository;
use App\Component\ListComponent;
use App\Enum\EUserAccountType;

class PayoutList extends ListComponent
{            
    /** @var UserAccountRepository */
    protected $userAccountRepository;
    
    function __construct(UserAccountRepository $userAccountRepository)
    {
        $this->userAccountRepository = $userAccountRepository;

        parent::__construct($userAccountRepository);
    }    
    
    protected function modifyOptions(array &$options)
    {
        $options[UserAccountRepository::DB_COL_TYPE] = EUserAccountType::PAYOUT;
        
        parent::modifyOptions($options);
    }
}
