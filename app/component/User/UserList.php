<?php

namespace App\Component;

use App\Model;
use RTsoft\Component\GridForm\GridComponent;
use RTsoft\Model\GridFormRepository as R;

class UserList extends GridComponent
{            
    /** @var \Model\UserRepository */
    protected $userRepository;
    
    function __construct(Model\UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;

        parent::__construct($userRepository);
    }
    
    protected function modifyOrder(&$order)
    {
        if ($order)
        {
            if ($order[0] == "name")
            {
                $order = "lastname $order[1], firstname $order[1]";
            }
        }
        else
        {
            $order = "lastname ASC, firstname ASC";
        }
        
        parent::modifyOrder($order);
    }
    
    protected function modifyOptions(array &$options)
    {
        parent::modifyOptions($options);
    }

}
