<?php

namespace App\Component;

/**
 * Tlacidlo pro prihlaseni via FB 
 *
 * @author Samuel
 */
class FbShareButton extends FbComponent
{
    const SHARE_MODE_REGISTER = 'reg';
    
    const FB_END_POINT      = "/me/feed";
    const FB_PARAM_LINK     = "link";
    const FB_PARAM_MESSAGE  = "message";
    const FB_CALLBACK_PATH  = "share!";
    
    const FB_PERMISSION_STATUS = 'publish_actions',
          FB_PERMISSION_MANAGE = 'manage_pages';

    public function render($params = NULL)
    {
        $mode = $params;
        
        if(!$mode)
        {
            $mode = self::SHARE_MODE_REGISTER;
        }
        $this->template->shareUrl = $this->getShareUrl($mode);

        parent::render();
    }
    
    protected function getShareUrl($mode)
    {
        $urlPart = $this->link(self::FB_CALLBACK_PATH, [$mode]);
        $currentUrl = $this->presenter->getCurrentUrl();
        
        $currentUrl->setQuery(null);
        $currentUrl->setPath($urlPart);
        
        $callback = $currentUrl->getAbsoluteUrl();
        $loginHeleper = $this->getLoginHelper();
        $permissions = [self::FB_PERMISSION_STATUS, self::FB_PERMISSION_MANAGE];
        $shareUrl = $loginHeleper->getLoginUrl($callback, $permissions);
        
        return $shareUrl;
    }

    private function shareData($linkData)
    {
        $instance = $this->getFbIntance();
        $accessToken = $this->getAccessToken();
        $graphNode = null;
        
        try
        {
            $response = $instance->post(self::FB_END_POINT, $linkData, $accessToken);
            $graphNode = $response->getGraphNode();
        } 
        catch (Facebook\Exceptions\FacebookResponseException $e)
        {
            \Tracy\Debugger::log($e->getMessage(), \Tracy\ILogger::ERROR);
        } 
        catch (Facebook\Exceptions\FacebookSDKException $e)
        {
            \Tracy\Debugger::log($e->getMessage(), \Tracy\ILogger::ERROR);
        }
        
        return $graphNode;
    }

    public function handleShare($mode)
    {
        if($mode == self::SHARE_MODE_REGISTER && $this->presenter instanceof \App\FrontModule\Presenter\ProfilePresenter)
        {
            $link = $this->presenter->getRegisterUrl();
            $this->shareData([self::FB_PARAM_LINK     => $link]);
        }
    }
}
