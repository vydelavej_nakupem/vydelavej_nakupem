<?php

namespace App\Component;

use Nette\Application\UI\Form;

class SearchForm extends BaseComponent
{
    private $advertisersRepository;
    
    
    public function __construct(
            \App\Model\AdvertisersRepository $advertisersRepository)
    {
        parent::__construct();
        
        $this->advertisersRepository = $advertisersRepository;
    }
    
    public function render($params = NULL)
    {
        $this->template->serviceUrl = $this->link("whisperSugestion!");
        $this->template->onSelect = $this->link("selectSugestion!");
        
        parent::render(null);
    }
    
    protected function createComponentForm()
    {
        $form = new Form();
        $keyword = null;
        
        if($this->presenter->getName() == 'Front:Search')
        {
            $keyword = $this->presenter->getParameter('q');
        }
        
        $form->addText('search', '')->setDefaultValue($keyword);
        $form->addSubmit('submit', '');
        
        $form->onSuccess[] = $this->saveForm;
        
        return $form;
    }

    public function saveForm(Form $form, \stdClass $values)
    {
        if($values->search)
        {
            $this->presenter->redirect('Search:', array($values->search));
        }
    }
    
    public function handleWhisperSugestion()
    {
        $keyword = $this->presenter->getParameter("query");
        
        $results = $this->advertisersRepository->getSearchWhisperResults($keyword);
        
        $data = array(
            
            "query"         => "Unit",
            "suggestions"   => $results
        );
        
        $this->presenter->sendJson($data);
    }
    
    public function handleSelectSugestion()
    {
        $storeId = $this->presenter->getParameter("storeId");
        $url = $this->presenter->link("Store:detail", array($storeId));
        
        $this->presenter->sendJson(array('url' => $url));
    }
}
