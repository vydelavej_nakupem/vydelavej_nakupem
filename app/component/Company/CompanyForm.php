<?php

namespace App\Component;

use App\Model;
use RTsoft\Model\GridFormRepository as R;
use Nette\Application\UI\Form;

class CompanyForm extends FormComponent
{
    /** @var Model\CompanyRepository */
    protected $companyRepository;
    
    /** @var Model\StateRepository */
    protected $stateRepository;
    
    function __construct(Model\CompanyRepository $companyRepository, Model\StateRepository $stateRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->stateRepository = $stateRepository;
     
        $this->redirectAfterSavingTo = "Company:default";
        $this->nameOfColumnForAnnouncement = "company_name";

        parent::__construct($companyRepository);
    }
    
    public function render($params = NULL)
    {
        // dalsi data do sablony
        //$this->template->something = ...
        
        parent::render();
    }
    
    protected function createComponentForm()
    {
        // naplneni selectboxu
        $this->companyRepository->columns["state_id"][R::COLUMN_ITEMS] = $this->stateRepository->fetchStatesForSelectbox();
        
        // vygenerovani formulare
        $form = parent::createComponentForm();
        
        // dalsi validace
        $form["land_registry_number"]
            ->addConditionOn($form["house_number"], ~Form::FILLED)
            ->addRule(Form::FILLED, "Musí být vyplněno číslo popisné a/nebo číslo orientační.");
        
        // dalsi policka
        $form->addSelect("phone_pre", "Předvolba", $this->stateRepository->fetchPhonesForSelectbox())
             ->setAttribute("class", "normal-selectbox");
        
        return $form;
    }
    
    protected function setFormDefaults($row)
    {
        if (empty($row)) // defaultni hodnoty pro novy zaznam
        {     
            $row["state_id"] = Model\StateRepository::DEFAULT_STATE;    
            $row["phone_pre"] = Model\StateRepository::DEFAULT_PHONE_PRE;
        } 
        
        parent::setFormDefaults($row);
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        // doplneni dalsich hodnot do ukladaneho zaznamu
        if (empty($id)) 
        {
            // hodnoty pro novy zaznam
            $values["user_created_id"] = $this->presenter->user->id;
        }
        else
        {
            // hodnoty pro editaci
            // $values["xxx"] = ...
        }
        
        // ulozeni
        try
        {
            $row = parent::saveForm($form, $values, $id);        
        }
        catch (\PDOException $e)
        {
            if($e->getCode() == 23000 && strpos($e->getMessage(), "1062") !== FALSE)
            {
                if (strpos($e->getMessage(), "'ic'") !== FALSE)
                {
                    $form->addError("Zadané IČO má jiná společnost.");
                }
                
                if (strpos($e->getMessage(), "'email'") !== FALSE)
                {
                    $form->addError("Zadaný email má jiný zákazník.");
                }
                
                return;
            }
            
            throw $e;
        }
        
        return $row;
    }

}
