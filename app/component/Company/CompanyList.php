<?php

namespace App\Component;

use App\Model;
use RTsoft\Model\GridFormRepository as R;

class CompanyList extends ListComponent
{            
    /** @var \Model\CompanyRepository */
    protected $companyRepository;
    
    function __construct(Model\CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;

        parent::__construct($companyRepository);
    }

    protected function modifyOrder(&$order)
    {
        if ($order)
        {
            /*if ($order[0] == "name")
            {
                $order = "CASE WHEN person_type = 'legal' THEN `company_name` ELSE CONCAT(`lastname`, `firstname`) END {$order[1]}";
            }*/
        }
        else
        {
            // default razeni
            $order = "name ASC";
        }
        
        parent::modifyOrder($order);
    }
    
    protected function modifyOptions(array &$options)
    {
        parent::modifyOptions($options);
        
        $options["not_deleted"] = TRUE;
    }

}
