<?php

namespace App\Component;

class StoreEventList extends EventList
{
    private $id;
    
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function render($params = NULL)
    {
        $this->template->setFile(__DIR__ . '/EventList.latte');
        parent::render($params);
    }


    protected function getData()
    {
        return $this->advertiserEventRepository->getActiveStoreItems($this->id);
    }
}
