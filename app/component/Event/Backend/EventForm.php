<?php

namespace App\Component\Backend;

use App\Model\AdvertiserEventRepository;
use App\Model\AdvertisersRepository;
use Nette\Application\UI\Form;
use App\Component\FormComponent;

class EventForm extends FormComponent
{
    /** @var AdvertiserEventRepository */
    protected $advertiserEventRepository;
    
    /** @var AdvertisersRepository */
    protected $advertisersRepository;
    
    
    function __construct(
            AdvertiserEventRepository $advertiserEventRepository,
            AdvertisersRepository $advertisersRepository)
    {
        $this->advertiserEventRepository = $advertiserEventRepository;
        $this->advertisersRepository = $advertisersRepository;
     
        $this->redirectAfterSavingTo = "Event:default";
        $this->nameOfColumnForAnnouncement = "title";

        parent::__construct($advertiserEventRepository);
    }
    
    protected function createComponentForm()
    {
        $form = parent::createComponentForm();
        
        $form[AdvertiserEventRepository::DB_COL_ADVERTISER_ID]->setItems(
                $this->advertisersRepository->getItemsForSelectBox());
        
        return $form;
    }
    
    protected function setFormDefaults($row)
    {
        parent::setFormDefaults($row);
        
        if(!$this->id)
        {
            $this["form"][AdvertiserEventRepository::DB_COL_ACTIVE]->setDefaultValue(true);
        }
    }
    
    protected function saveForm(Form $form, \stdClass $values, $id)
    {
        $row = parent::saveForm($form, $values, $id);        
        
        return $row;
    }

}
