<?php

namespace App\Component\Backend;

use App\Model;
use App\Component\ListComponent;

class EventList extends ListComponent
{            
    /** @var \App\Model\AdvertiserEventRepository */
    protected $advertiserEventRepository;
    
    function __construct(Model\AdvertiserEventRepository $advertiserEventRepository)
    {
        $this->advertiserEventRepository = $advertiserEventRepository;
        parent::__construct($advertiserEventRepository);
    }
}
