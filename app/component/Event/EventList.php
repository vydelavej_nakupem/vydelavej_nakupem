<?php

namespace App\Component;

use App\Model\AdvertiserEventRepository;

class EventList extends BaseComponent
{
    const PREVIEW_LIMIT = 10;
    const ITEMS_PER_PAGE = 20;
    
    protected $advertiserEventRepository;
    
    public function __construct(AdvertiserEventRepository $advertiserEventRepository)
    {
        parent::__construct();
        
        $this->advertiserEventRepository = $advertiserEventRepository;
    }   
    
    public function render($params = NULL)
    {
        $items = $this->getData();
        
        $this->template->items = $items;
        
        parent::render($params);
    }
        
    protected function getData()
    {
        return $this->advertiserEventRepository->getActiveItems();
    }
}
