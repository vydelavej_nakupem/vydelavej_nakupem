<?php

namespace App\Component;

use App\Model;

/**
 * Prehled provizi prihlaseneho uzivatele.
 *
 * @author Samuel
 */
class UserSummaryList extends \RTsoft\Component\GridForm\GridComponent
{
    /** @var Model\CommissionLogRepository */
    protected $commissionLogRepository;
    
    /** @var Model\UserRepository */
    protected $userRepository;
    
    private $userId;
    
    function __construct(
            Model\CommissionLogRepository $commissionLogRepository,
            Model\UserRepository $userRepository)
    {
        $this->commissionLogRepository = $commissionLogRepository;
        $this->userRepository = $userRepository;
        
        parent::__construct($commissionLogRepository);
    }
    
    /**
     * @param int $params zde jako User ID
     */
    public function render($params = NULL)
    {
        if($params && is_int($params))
        {
            $this->userId = $params; 
        }
        parent::render();
    }
    
    /**
     * Predani userId do repository -> podle toho sid -> podle toho provize
     * @param array $options
     */
    protected function modifyOptions(array &$options)
    {
        $options['user_id'] = $this->userId;
        parent::modifyOptions($options);
    }
    
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
}
