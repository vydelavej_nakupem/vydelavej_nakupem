<?php

namespace App\Component;

use App\Model;

/**
 * Prehled provizi prihlaseneho uzivatele.
 *
 * @author Samuel
 */
class UserSummary extends \RTsoft\Component\BaseComponent
{
    const TYPE_ME       = 'me';
    const TYPE_ALL      = 'all';
    const TYPE_SLAVE    = 'slave';
    
    /** @var Model\CommissionLogRepository */
    protected $commissionLogRepository;
    
    /** @var Model\UserRepository */
    protected $userRepository;
    
    public function __construct(
            Model\CommissionLogRepository $commissionLogRepository, 
            Model\UserRepository $userRepository)
    {
        $this->commissionLogRepository = $commissionLogRepository;
        $this->userRepository = $userRepository;
    }
    
    public function render($params = NULL, $type = null)
    {
        $this->initType($type);
        
        $userId = $params;
        $users = array();
        
        if($type == self::TYPE_ALL || $type == self::TYPE_SLAVE)
        {
            $users = $this->userRepository->getOwnerUsersId($userId);
        }
        if($type == self::TYPE_SLAVE)
        {
            $userId = null;
        }
        
        $this->template->summary = $this->commissionLogRepository->getUserSummary($userId, $users);
        $this->template->type = $type;
        
        parent::render();
    }
    
    protected function initType(&$type)
    {
        if($type)
        {
            if($type != self::TYPE_ALL && $type != self::TYPE_ME && $type != self::TYPE_SLAVE)
            {
                $type = null;
            }
        }
        
        if(!$type)
        {
            $type = self::TYPE_ALL;
        }
    }
}
