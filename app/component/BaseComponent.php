<?php

namespace App\Component;

class BaseComponent extends \Nette\Application\UI\Control
{

    protected $latteFile;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function render($params = NULL)
    {
        if (!$this->template->getFile())
        {
            $this->template->setFile(str_replace(".php", "", $this->reflection->fileName) . ".latte");
        }
        $this->template->render();
    }
    
    /**
     * Univerzální metoda pro vytváření komponent.
     * Pokud se vyskytne v latte {control xyz}, tato metoda je zavolána s parametrem $name = "xyz".
     * V metodě se otestuje, zda náhodou neexistuje metoda createComponentXyz,
     * - pokud ano, zavolá se parent z Nette, ve kterém se zavolá createComponentXyz,
     * - pokud ne, tak se zavolá vytvoření služby, jejíž název musí být v config.neon
     *   (pokud není, spadne to).
     *
     * @param string $name
     * @return mixed
     */
    protected function createComponent($name)
    {
        if (method_exists($this, "createComponent" . ucfirst($name)))
        {
            return parent::createComponent($name);
        }

        // Komponenta pro sluzbu Xyz se muze jmenovat xyz nebo Xyz
        try
        {
            // Nazev komponenty se shoduje presene
            $c = $this->presenter->context->createService($name);
        }
        catch (\Nette\DI\MissingServiceException $e)
        {
            // Zkusit "ucfirst" nazev
            try
            {
                // Pokus o prevod xyz na Xyz
                $c = $this->presenter->context->createService(ucfirst($name));
            }
            catch (\Nette\DI\MissingServiceException $e)
            {
                // "Vlastni" hlaska pro informaci o 2 pokusech
                throw new \Nette\DI\MissingServiceException("Services '$name' or '" . ucfirst($name) . "' not found.");
            }
        }
        
        // predani http requestu, pokud se v komponente vyskytuje setter pro nej
        if (method_exists($c, "setHttpRequest"))
        {
            $c->httpRequest = $this->getHttpRequest();
        }
        
        return $c;
    }
}
