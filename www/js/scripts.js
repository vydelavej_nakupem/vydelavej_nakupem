var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
// Firefox 1.0+
var isFirefox = typeof InstallTrigger !== 'undefined';
// Safari 3.0+ "[object HTMLElementConstructor]" 
var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
// Internet Explorer 6-11
var isIE = /*@cc_on!@*/false || !!document.documentMode;
// Edge 20+
var isEdge = !isIE && !!window.StyleMedia;
// Chrome 1+
var isChrome = !!window.chrome && !!window.chrome.webstore;
// Blink engine detection
var isBlink = (isChrome || isOpera) && !!window.CSS;


jQuery(document).ready(function () {

    new Clipboard('.copy-url');

    jQuery("#mobile-btn").click(function () {
        jQuery(".main-navigation").animate({
            height: 'toggle'
        });
    });

    $('.main-navigation li').mouseover(function () {
        $(this).children('.submenu').show();
    });

    $('.main-navigation li').mouseout(function () {
        $(this).children('.submenu').hide();

    });

    $('.submenu').mouseover(function () {
        $(this).show();

    });

    $('.submenu').mouseout(function () {
        $(this).hide();

    });

    $('#login-submenu-btn').mouseover(function () {
        $('.profile-menu').show();
    });

    $('#login-submenu-btn').mouseout(function () {
        $('.profile-menu').hide();
    });

    $('.profile-menu').mouseover(function () {
        $('.profile-menu').show();
    });

    $('.profile-menu').mouseout(function () {
        $('.profile-menu').hide();
    });

    $.initSearchWhisper('frm-searchForm-form-search');
    
    if (isChrome)
    {
        $("#chrome-toolbar").show();
        $("#no-toolbar").hide();
    }
    else
    {
        $("#chrome-toolbar").hide();
        $("#no-toolbar").show();
    }
});


$.initSearchWhisper = function (elementId)
{
    var _element = $('#' + elementId);

    if (_element.length !== 1)
    {
        return;
    }

    _element.autocomplete({
        serviceUrl: _element.data('service'),
        onSelect: function (suggestion) {

            $.nette.ajax({

                data: {'storeId': suggestion.data},
                url: _element.data('select'),
                success: function (data) {

                    window.location.replace(data.url);
                }
            });
        },
        beforeRender: function (container, suggestions) {
            container.find('.autocomplete-suggestion').each(function (i, suggestion) {
                $(this).prepend("<img width='80' src='"+basePath+"/www/img/logos/"+suggestions[i].logo+"' title='"+suggestions[i].value+"' alt='"+suggestions[i].value+"'> ");
            });
        }
    });
};