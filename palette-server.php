<?php

/**
 * Inicializace nette pro odychytávání requestů pro generování miniatur.
 * Zde je to nutné udělat pomocí samostatného souboru z toho důvodu že složka se soubory
 * je o úroveň výše než aplikace a soubory Nette.
 */
$container = require '/app/bootstrap.php';
$container->getService('application')->run();