/**
 * Definice požadovaných NPM / Gulp modulů
 */
var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var cleanCss = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concatCss = require('gulp-concat-css');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var wait = require('gulp-wait');
var plumber = require('gulp-plumber');
var merge = require('merge-stream');
var rewriteCSS = require('gulp-rewrite-css');
var sass = require('gulp-sass');

/**
 * Úložiště css souborů
 * @type {string}
 */
var publicPath = 'www/';

/**
 * Služba zajišťující sjednocení, kompilaci a mimifikaci css a less souborů
 */
gulp.task('style', function()
{
    /**
     * Definice potřebných css souborů
     * @type {string[]}
     */
    sourceCss = [

        'www/bower/bootstrap/dist/css/bootstrap.min.css',
        'www/bower/jquery-ui/themes/ui-lightness/jquery-ui.css',
        'www/bower/jqueryui-timepicker-addon/dist/jquery-ui-timepicker-addon.css',
        'www/bower/fancybox/source/jquery.fancybox.css',
        'www/bower/voda-date-input/dateInput.css',
        'www/bower/components-font-awesome/css/font-awesome.min.css',
        'vendor/nextras/datagrid/bootstrap-style/bootstrap3.nextras.datagrid.css',
        'www/bower/bootstrap-select/dist/css/bootstrap-select.min.css',
        'vendor/rtsoft/library/www/css/page.css',
        'vendor/rtsoft/library/www/css/common.css',
        'www/bower/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css'
    ];

    // Sjednocení url cest v css souborech
    cssStream = [];
    sourceCss.forEach(function(stream)
    {
        cssStream.push(

            gulp.src([stream]).pipe(rewriteCSS({destination: publicPath}))
        );
    });

    // Stream Css souborů
    var cssStream = merge(cssStream).pipe(concat('css.css'));

    // Stream Less souborů
    var lessStream = gulp.src(['www/styles.less'])
        .pipe(less({
            compress: false,
            strictMath: true,
            relativeUrls: true
        }))
        .pipe(concatCss('less.css'));

    // Stream Sass souborů
    var sassStream = gulp.src(['www/styles.scss'])
        .pipe(sass().on('error', sass.logError));

    // Sloučení CSS a LESS souborů
    return merge(cssStream, lessStream, sassStream)
        .pipe(concat('styles.css'))
        .pipe(cleanCss({compatibility: 'ie7'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest(publicPath));
});

/**
 * Služba zajišťující sjednocení, kompilaci a mimifikaci css a less souborů
 */
gulp.task('front_style', function()
{
    /**
     * Definice potřebných css souborů
     * @type {string[]}
     */
    sourceCss = [
        'www/bower/components-font-awesome/css/font-awesome.min.css',
        'www/bower/bootstrap/dist/css/bootstrap.min.css',
        'www/css/style.css'
    ];
    
    // Sjednocení url cest v css souborech
    cssStream = [];
    sourceCss.forEach(function(stream)
    {
        cssStream.push(

            gulp.src([stream]).pipe(rewriteCSS({destination: publicPath}))
        );
    });

    // Stream Css souborů
    var cssStream = merge(cssStream).pipe(concat('css.css'));

    // Stream Less souborů
    var lessStream = gulp.src(['www/front-styles.less'])
        .pipe(less({
            compress: false,
            strictMath: true,
            relativeUrls: true
        }))
        .pipe(concatCss('less.css'));

    // Stream Sass souborů
    var sassStream = gulp.src(['www/front-styles.scss'])
        .pipe(sass().on('error', sass.logError));

    // Sloučení CSS a LESS souborů
    return merge(cssStream, lessStream, sassStream)
        .pipe(concat('front-styles.css'))
        .pipe(cleanCss({compatibility: 'ie7'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest(publicPath));
});

/**
 * Kompilace JS souborů aplikace
 */
gulp.task('js', function()
{
    return gulp.src([

        'www/bower/jquery/dist/jquery.min.js',
        'www/bower/bootstrap/dist/js/bootstrap.min.js',
        'www/bower/jquery-ui/jquery-ui.min.js',
        'vendor/rtsoft/library/www/js/jquery-rtsoft.js',
        'www/bower/jquery-ui/ui/i18n/datepicker-cs.js',
        'www/bower/jqueryui-timepicker-addon/dist/jquery-ui-timepicker-addon.min.js',
        'www/bower/jqueryui-timepicker-addon/dist/i18n/jquery-ui-timepicker-cs.js',
        'www/bower/fancybox/source/jquery.fancybox.pack.js',
        'www/bower/nette-forms/src/assets/netteForms.js',
        'www/bower/voda-date-input/dateInput.js',
        'www/bower/nette.ajax.js/nette.ajax.js',
        'www/bower/nette.ajax.js/extensions/spinner.ajax.js',
        'www/bower/bootstrap-select/dist/js/bootstrap-select.min.js',
        'www/bower/bootstrap-select/dist/js/i18n/defaults-cs_CZ.min.js',
        'www/vendor/nextras.datagrid.js',
        'www/bower/ckeditor/ckeditor.js',
        'www/bower/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js',
        'vendor/uestla/recaptcha-control/client-side/recaptcha.js',
        'vendor/rtsoft/library/www/js/gridComponent.js',
        'vendor/rtsoft/library/www/js/formComponent.js',
        'vendor/rtsoft/library/www/js/scripts.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('scripts.js'))
        .pipe(uglify({

            preserveComments: 'license',
            compress: false
        }))
        .pipe(rename('scripts.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('www'))
});

/**
 * Kompilace JS souborů aplikace
 */
gulp.task('front_js', function()
{
    return gulp.src([
        'www/bower/jquery/dist/jquery.min.js',
        'www/bower/clipboard/dist/clipboard.min.js',
        'www/bower/bootstrap/dist/js/bootstrap.min.js',
        'www/bower/devbridge-autocomplete/dist/jquery.autocomplete.min.js',
        'www/bower/nette.ajax.js/nette.ajax.js',
        'www/js/scripts.js'
    ])
        .pipe(sourcemaps.init())
        .pipe(concat('front-scripts.js'))
        .pipe(uglify({

            preserveComments: 'license',
            compress: false
        }))
        .pipe(rename('front-scripts.js'))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('www'))
});

/**
 * Sledování změn v souborech a automatické spuštění potřebných akcí
 */
gulp.task('watch', function()
{
    gulp.watch([

        'www/styles.less',
        'www/less/*.less',
        'vendor/rtsoft/library/www/css/**/*.css',
        'vendor/rtsoft/library/www/css/**/*.less',
        'www/bower/**/*.css',
        'www/styles.scss'
    ],
        ['style']);

    gulp.watch([

        'www/js/**/*.js',
        'www/bower/**/*.js',
        'vendor/rtsoft/library/www/js/**/*.js'
    ],
        ['js']);
});

/**
 * Defaultní akce gulpu
 */
gulp.task('default', ['style', 'front_style', 'front_js', 'js']);